# ESSPASS PDA1

[![YARN quality task](https://github.com/EMPLabBrx/esspass-web-wallet/actions/workflows/yarn-quality.yml/badge.svg)](https://github.com/EMPLabBrx/esspass-web-wallet/actions/workflows/yarn-quality.yml)

[![YARN Audit](https://github.com/EMPLabBrx/esspass-web-wallet/actions/workflows/yarn-audit.yml/badge.svg)](https://github.com/EMPLabBrx/esspass-web-wallet/actions/workflows/yarn-audit.yml)

## Project description

ESSPASS PDA1 is exploring ways to facilitate the exercise of citizens’ social security rights across borders and the verification of their entitlements and validity of documents by the competent institutions and other actors.

A working demo has been developed to simulate the end-to-end flow of PDA1 Verifiable Credential, from it's issuance by the National Social Security Institution, its download by the posted worker in their Digital Wallet to finally its verification by an inspector using a Verifier App.

You will find here the links to all essential elements of the ESSPASS initiative.
This project is a web wallet for the ESSPASS project. It is a web application concept that allows users to manage their EBSI compliant verifiable credentials.

### Technology stack

NodeJs ( v18 and higher) , TypeScript , EBSI , Angular , MongoDB , OIDC 2.0 ( Keycloak ) , API Gateway ( APISix ) , Let's Encrypt , AWS , Kubernetes , Github / Actions

### Helicopter view of the architecture

  <img src="tools/utils/documentation/arch-overview.png" width="1000" height="600">

**Microservices pattern**

Project is developed using microservices architecture. Each service is a separate entity and can be deployed independently.

Architectural decision log can be found ( access throuhg the Confluence / EULogin and access grant needed ) [CITNet wiki page](https://citnet.tech.ec.europa.eu/CITnet/confluence/display/ESSPPDA1/Architectural+Decision+Log)

### Deployment

### Features

Below are the features of the ESSPASS PDA1 web wallet represented in the form of user action flows.

**1. PDA1 request form submission**

User can submit a request for a PDA1 credential. The form is validated and the request is sent to the backend service.

  <img src="tools/utils/documentation/request-form/request-form-submitt.png" width="800" height="400">

Test the feature:

    1. Open the issuer-frontoffice application and click on the "Request PDA1" button.
    2. Fill in the form and click on the "Submit" button.
    3. Check the response message.
    4. Open the issuer-backoffice application and check the request in the "PDA1 requests" section.

### Flows

**Issuance flow V2**

The issuance flow is the process of creating a verifiable credential and sending it to the user's wallet. The flow is initiated by the user submitting a request for a credential. The request is validated and sent to the backend service. The backend service creates a credential and sends it to the user's wallet.

**Issuance flow V3**

The issuance flow is the process of creating a verifiable credential and sending it to the 3party user's wallet.
Application is using the EBSI compliant verifiable credentials.
Real production applications for Android and iOS are provided by the iGrant.io and CorpoSIGN companies.

### Utils

1. Generate license checker report [Report generation](./tools/utils/license-checker/README.MD)
