/* eslint-disable */
export default {
  displayName: "auth-server",
  preset: "../../jest.preset.js",
  testEnvironment: "node",
  setupFiles: ["<rootDir>/src/authentication/test/.env.js"],
  transform: {
    "^.+\\.[tj]s$": ["ts-jest", { tsconfig: "<rootDir>/tsconfig.spec.json" }],
  },
  testMatch: [
    "**/+(*.)+(spec|test).+(ts|js)?(x)",
    "!**/+(*.)+(integration).+(spec|test).+(ts|js)?(x)",
  ],
  moduleFileExtensions: ["ts", "js", "html"],
  coverageDirectory: "../../coverage/apps/auth-server",
};
