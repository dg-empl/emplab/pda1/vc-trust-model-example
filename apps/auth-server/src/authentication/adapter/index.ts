import { logger } from "@esspass-web-wallet/logging-lib";
import AuthService from "../application/auth.service";
import { OAuth2TokenError } from "../errors";
import { JsonWebKeySet } from "@esspass-web-wallet/crypto-lib";

const log = logger("auth-server");

/**
 * start auth service
 */
const authService = AuthService.getInstance(log);

export const getOPMetadataAdapter = async (req, res) => res.send(authService.getOPMetadata());

export const postForAuthTokenAdapter = async (req, res) => {
  const { headers, body } = req;
  const contentType = headers["content-type"];

  if (!contentType.toLowerCase().includes("application/x-www-form-urlencoded")) {
    throw new OAuth2TokenError("invalid_request", {
      errorDescription: "Content-type must be application/x-www-form-urlencoded",
    });
  }

  res.send(await authService.token(body));
};

export const getPublicJwksAdapter = async (req, res): Promise<JsonWebKeySet> => {
  log.debug("jwksAdapter entry");
  const jwks = await authService.getJwks();
  res.setHeader("Content-type", "application/jwk-set+json");
  log.debug("jwksAdapter exit :" + JSON.stringify(jwks));
  return res.send(jwks);
};
