import { PEXv2 } from "@sphereon/pex";
import { importJWK, KeyLike } from "jose";
import { Resolver } from "did-resolver";
import { getResolver as getEbsiDidResolver } from "@cef-ebsi/ebsi-did-resolver";
import { getResolver as getKeyDidResolver } from "@cef-ebsi/key-did-resolver";
import type { Level } from "level";
import {
  getKeyPair,
  JsonWebKeySet,
  JWKWithKid,
  LevelDbKeyAuth,
  LevelDbObjectAuth,
  OPMetadata,
  TokenResponse,
} from "@esspass-web-wallet/crypto-lib";
import {
  authorize,
  directPost,
  getOPMetadata,
  getPrivateJwks,
  getPrivateKeyJwk,
  getPublicJwks,
  getRequestById,
  token,
} from "./utils";
import { GetAuthorizeDto } from "./dto";

export class AuthServer {
  private privateJwks?: JWKWithKid[];

  private privateKey?: Uint8Array | KeyLike;

  private privateKeyJwk?: JWKWithKid;

  private db: Level<LevelDbKeyAuth, LevelDbObjectAuth>;

  /**
   * EBSI DID Resolver (did:ebsi v1)
   */
  private readonly ebsiResolver: Resolver;

  /**
   * Key DID Resolver (did:key)
   */
  private readonly keyResolver: Resolver;

  /**
   * Request timeout
   */
  private readonly timeout?: number;

  /**
   * Presentation exchange
   */
  private readonly pex: PEXv2;

  private readonly ebsiAuthority: string;

  private readonly url: string;

  private readonly did: string;

  /**
   * Issuer public key JWK (to verify the signature of issuer_state)
   * Note: in practice, the Auth Server would load the JWKS from the Issuer in order to get the public key.
   */
  private issuerPrivateKey: string;

  private cacheManager: {
    interval: ReturnType<typeof setInterval>;
    keys: { key: LevelDbKeyAuth; expiration: number }[];
  };

  constructor(opts: {
    db: Level<LevelDbKeyAuth, LevelDbObjectAuth>;
    did: string;
    url: string;
    didRegistryApiUrl: string;
    ebsiAuthority: string;
    timeout?: number;
  }) {
    this.db = opts.db;
    this.did = opts.did;
    this.url = opts.url;
    this.ebsiResolver = new Resolver(getEbsiDidResolver({ registry: opts.didRegistryApiUrl }));
    this.keyResolver = new Resolver(getKeyDidResolver());
    this.timeout = opts.timeout;
    this.pex = new PEXv2();
    this.ebsiAuthority = opts.ebsiAuthority;
    this.cacheManager = {
      interval: setInterval(() => {
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        this.manageCache().catch(() => {});
      }, 120_000),
      keys: [],
    };
  }

  private addKeyToCacheManager(key: LevelDbKeyAuth, ttl: number = Number.MAX_SAFE_INTEGER) {
    this.cacheManager.keys.push({ key, expiration: Date.now() + ttl });
    // sort by expiration: The first ones to
    // expire are at the beginning of the list
    this.cacheManager.keys.sort((a, b) => a.expiration - b.expiration);
  }

  private async manageCache() {
    const now = Date.now();
    // search which ID has not expired yet
    const id = this.cacheManager.keys.findIndex(a => now < a.expiration);
    // remove the expired keys (from 0 until id-1)
    const expiredKeys = this.cacheManager.keys.splice(0, id);
    // remove data from database
    await this.db.batch(
      expiredKeys.map(k => ({
        type: "del",
        key: k.key,
      }))
    );
  }

  async close() {
    clearInterval(this.cacheManager.interval);
    // remove data from cache
    try {
      await this.db.batch(
        this.cacheManager.keys.map(k => ({
          type: "del",
          key: k.key,
        }))
      );
    } catch {
      // empty
    }
  }

  private async getPrivateKey() {
    if (!this.privateJwks) this.privateJwks = await getPrivateJwks(this.db, this.did);
    this.privateKeyJwk = getPrivateKeyJwk(this.privateJwks);
    return importJWK(this.privateKeyJwk);
  }

  getOPMetadata(): OPMetadata {
    return getOPMetadata(this.url);
  }

  /**
   * Process client's auth request.
   *
   * @returns The redirect location.
   */
  async authorize(query: GetAuthorizeDto): Promise<string> {
    if (!this.privateKey) {
      this.privateKey = await this.getPrivateKey();
    }
    const { kid } = this.privateKeyJwk as JWKWithKid;

    const issuerKeyPair = await getKeyPair(this.issuerPrivateKey);

    return authorize(
      this.db,
      (key: LevelDbKeyAuth, ttl: number = Number.MAX_SAFE_INTEGER) => {
        this.addKeyToCacheManager(key, ttl);
      },
      this.did,
      this.url,
      kid,
      this.privateKey,
      query,
      issuerKeyPair.publicKeyJwk
    );
  }

  /**
   * Get Authorization Request by ID.
   */
  async getRequestById(requestId: string): Promise<string> {
    return getRequestById(this.db, this.did, requestId);
  }

  /**
   * Process /direct_post request.
   *
   * @returns The location URI based on the Authentication Request `redirect_uri`, with `code` and `state` params.
   */
  async directPost(query: unknown): Promise<string> {
    return directPost(
      this.db,
      (key: LevelDbKeyAuth, ttl: number = Number.MAX_SAFE_INTEGER) => {
        this.addKeyToCacheManager(key, ttl);
      },
      this.did,
      this.url,
      this.ebsiAuthority,
      this.pex,
      this.ebsiResolver,
      this.keyResolver,
      this.timeout,
      query
    );
  }

  /**
   * Process /token request.
   * Access Token is delivered as a response payload from a successful Token Endpoint initiation.
   * `c_nonce` (Challenge Nonce) must be stored until a new one is given.
   *
   * @see https://www.rfc-editor.org/rfc/rfc6749#section-4.1.4
   *
   * @param body The POST /token request payload
   * @returns A token response.
   */
  async token(query: unknown): Promise<TokenResponse> {
    if (!this.privateKey) this.privateKey = await this.getPrivateKey();
    const { kid } = this.privateKeyJwk as JWKWithKid;
    return token(this.db, this.did, this.url, kid, this.privateKey, query, this.timeout);
  }
}

export default AuthServer;
