import { z } from "zod";
import { isJWT, isJSON } from "class-validator";

export const directPostVpTokenSchema = z.object({
  vp_token: z.string().refine(isJWT, { message: "Must be a valid JWT" }),
  presentation_submission: z
    .string()
    .refine(isJSON, { message: "Must be a valid stringified JSON" }),
  state: z.string(),
});

export type DirectPostVpToken = z.infer<typeof directPostVpTokenSchema>;
