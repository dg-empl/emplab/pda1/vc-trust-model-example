import { z } from "zod";
import { holderWalletClientMetadataSchema } from "./holder-wallet-client-metadata.validator";

/**
 * Client Metadata - Service Wallet
 *
 * @see https://www.rfc-editor.org/rfc/rfc7591.html#section-4.1.2
 * @see https://openid.net/specs/openid-connect-self-issued-v2-1_0.html#name-a-set-of-static-configuratio
 * @see https://api-conformance.ebsi.eu/docs/specs/providers-and-wallets-metadata#service-wallet-metadata
 */
export const serviceWalletClientMetadataSchema = holderWalletClientMetadataSchema.extend({
  jwks_uri: z.string().url(),
});

export default serviceWalletClientMetadataSchema;
