import { calculateJwkThumbprint } from "jose";
import AuthService from "./auth.service";

describe("AuthService", () => {
  it("should be calculated", async () => {
    const jwkKey = {
      kty: "EC",
      x: "6NKhtR9tlbKA5F8h6psq5N-NNB-ty_Qb5rnUlWBpe3Y",
      y: "CDNr__fFocwmMy0EkXFnOwFr1Q0HVzvlgyx1gf8InIY",
      crv: "P-256",
    };

    const alg = "ES256";

    // Format as JWK
    const jwk = {
      kty: "EC",
      crv: alg === "ES256" ? "P-256" : "secp256k1",
      alg,
      x: jwkKey.x,
      y: jwkKey.y,
    };

    const thumbprint = await calculateJwkThumbprint(jwk);

    expect(thumbprint).toEqual("iu1Mav6kmbMnkh11-cx7v0A7KERwdfqCpsXZRwPtfg4");
  });

  it("should expose JWKS end-point from hex string", async () => {
    const authService = new AuthService();

    const jwks = await authService.getJwks();

    expect(jwks).toEqual({
      keys: [
        {
          kty: "EC",
          crv: "P-256",
          alg: "ES256",
          x: "Fh8zs56eHXRiwCPmT-ELSAv1u5rqX7n5Ecb5Zb5mkEc",
          y: "T-OHgKm2JGBj2QmAInEr0tKdZQVTx9V3IMRGOfSRFuQ",
          kid: "5lCSKRHDjg2vyzZ3Y-R-Y7rSRLbSdkVJcsCWvDxpaVg",
        },
      ],
    });
  });
});
