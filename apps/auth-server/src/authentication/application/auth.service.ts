import { BadRequestError, NotFoundError } from "@cef-ebsi/problem-details-errors";
import { Level } from "level";
import {
  getKeyPair,
  JsonWebKeySet,
  KeyPair,
  LevelDbKeyAuth,
  LevelDbObjectAuth,
} from "@esspass-web-wallet/crypto-lib";
import AuthServer from "./auth-server/AuthServer";
import { GetAuthorizeDto } from "./auth-server/dto";
import { OPMetadata, TokenResponse } from "./auth-server/interfaces";
import { Logger } from "@esspass-web-wallet/logging-lib";
import { calculateJwkThumbprint } from "jose";

export class AuthService {
  private static authService: AuthService;

  private readonly authServer: AuthServer;

  private readonly db: Level<LevelDbKeyAuth, LevelDbObjectAuth>;

  private readonly privateKeyHex: string;

  private readonly DIDR_API_PATH = "/did-registry/v4/identifiers";

  /**
   * Issuer ES256 and ES256K key pairs (private + public key JWKs)
   */
  private issuerKeyPair: Record<"ES256" | "ES256K", KeyPair | undefined>;

  static getInstance(log: Logger): AuthService {
    if (!this.authService) {
      this.authService = new AuthService();
      this.authService.onModuleInit().then(() => {
        log.info("db/auth: memory db opened");
      });
    }
    return this.authService;
  }

  constructor() {
    this.privateKeyHex = process.env.AUTH_PRIVATE_KEY;
    const conformanceDomain = process.env.CONFORMANCE_DOMAIN;
    const domain = process.env.EBSI_ENV;
    const apiUrlPrefix = "/conformance/v3";
    const didRegistryApiUrl = domain + this.DIDR_API_PATH;
    const timeout = parseInt(process.env.REQUEST_TIMEOUT || "30000", 10);

    this.db = new Level<LevelDbKeyAuth, LevelDbObjectAuth>("db/auth", {
      keyEncoding: "json",
      valueEncoding: "json",
    });

    this.authServer = new AuthServer({
      db: this.db,
      did: "",
      url: `${conformanceDomain}${apiUrlPrefix}/auth`,
      didRegistryApiUrl,
      ebsiAuthority: domain.replace(/^https?:\/\//, ""),
      timeout,
    });

    this.issuerKeyPair = {
      ES256: undefined,
      ES256K: undefined,
    };
  }

  async onModuleInit() {
    await this.db.open();
    const { privateKeyJwk } = await getKeyPair(this.privateKeyHex);
    await this.db.put({ did: "", jwks: true }, [privateKeyJwk]);
  }

  getOPMetadata(): OPMetadata {
    return this.authServer.getOPMetadata();
  }

  /**
   * Load Issuer key pair from environment.
   *
   * @returns The private and public key JWKs (including "kid")
   */
  private async getIssuerKeyPair(alg: "ES256" | "ES256K"): Promise<KeyPair> {
    let keyPair = this.issuerKeyPair[alg];
    if (keyPair === undefined) {
      keyPair = await getKeyPair(this.privateKeyHex, alg);
      this.issuerKeyPair[alg] = keyPair;
    }
    return keyPair;
  }

  /**
   * Expose Issuer public keys.
   * Used in "Issue & Revoke" tests.
   *
   * @returns Issuer JWKS
   */
  async getJwks(): Promise<JsonWebKeySet> {
    const { publicKeyJwk } = await this.getIssuerKeyPair("ES256");

    // Return JWKS
    return {
      keys: [publicKeyJwk],
    };
  }

  /**
   * Process client's auth request.
   *
   * @returns The redirect location.
   */
  async authorize(query: GetAuthorizeDto): Promise<string> {
    return this.authServer.authorize(query);
  }

  /**
   * Get Authorization Request by ID.
   */
  async getRequestById(requestId: string): Promise<string> {
    try {
      return await this.authServer.getRequestById(requestId);
    } catch (error) {
      throw new NotFoundError(NotFoundError.defaultTitle, {
        detail: `No Authorization Request found with the ID ${requestId}`,
      });
    }
  }

  /**
   * Process /direct_post request.
   *
   * @returns The location URI based on the Authentication Request `redirect_uri`, with `code` and `state` params.
   */
  async directPost(query: unknown): Promise<string> {
    try {
      return await this.authServer.directPost(query);
    } catch (error) {
      if (error instanceof Error) {
        throw new BadRequestError(BadRequestError.defaultTitle, {
          detail: error.message,
        });
      }
      throw error;
    }
  }

  /**
   * Process /token request.
   * Access Token is delivered as a response payload from a successful Token Endpoint initiation.
   * `c_nonce` (Challenge Nonce) must be stored until a new one is given.
   *
   * @see https://www.rfc-editor.org/rfc/rfc6749#section-4.1.4
   *
   * @param body The POST /token request payload
   * @returns A token response.
   */
  async token(query: unknown): Promise<TokenResponse> {
    return this.authServer.token(query);
  }
}

export default AuthService;
