import { describe, afterAll, it, expect } from "@jest/globals";
import * as request from "supertest";
import { authServer } from "../../main";

describe("Auth Module", () => {
  const authUri = process.env.AUTH_URI;

  afterAll(async () => {
    // Avoid jest open handle error
    await new Promise(r => {
      setTimeout(r, 500);
    });
  });

  describe("GET /.well-known/openid-configuration", () => {
    it("should return the well-known OpenID configuration", async () => {
      // given when
      const response = await request(authServer).get(`/.well-known/openid-configuration`);

      // then
      expect(response.status).toBe(200);

      expect(response.body).toStrictEqual({
        redirect_uris: [`${authUri}/conformance/v3/auth/direct_post`],
        issuer: `${authUri}/conformance/v3/auth`,
        authorization_endpoint: `${authUri}/conformance/v3/auth/authorize`,
        token_endpoint: `${authUri}/conformance/v3/auth/token`,
        jwks_uri: `${authUri}/conformance/v3/auth/jwks`,
        scopes_supported: ["openid"],
        response_types_supported: ["vp_token", "id_token"],
        response_modes_supported: ["query"],
        grant_types_supported: ["authorization_code"],
        subject_types_supported: ["public"],
        id_token_signing_alg_values_supported: ["ES256"],
        request_object_signing_alg_values_supported: ["ES256"],
        request_parameter_supported: true,
        request_uri_parameter_supported: true,
        token_endpoint_auth_methods_supported: ["private_key_jwt"],
        request_authentication_methods_supported: {
          authorization_endpoint: ["request_object"],
        },
        vp_formats_supported: {
          jwt_vp: {
            alg_values_supported: ["ES256"],
          },
          jwt_vc: {
            alg_values_supported: ["ES256"],
          },
        },
        subject_syntax_types_supported: ["did:key", "did:ebsi"],
        subject_syntax_types_discriminations: ["did:key:jwk_jcs-pub", "did:ebsi:v1"],
        subject_trust_frameworks_supported: ["ebsi"],
        id_token_types_supported: ["subject_signed_id_token", "attester_signed_id_token"],
      });
    });
  });
});
