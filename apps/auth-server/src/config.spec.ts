/**
 * @jest-environment node
 */
// reads in config file
import * as yaml from "js-yaml";
import * as fs from "fs";
import * as path from "path";

// Get cfg or throw error
export const create_config = (file: string) => {
  try {
    return yaml.load(fs.readFileSync(path.resolve(__dirname, file), "utf8"));
  } catch (e) {
    console.log(e);
  }
};

describe("Config", () => {
  it("should be defined", () => {
    const resp = create_config("assets/config.yaml");

    console.log(JSON.stringify(resp));
  });
});
