import { Tracer, Logger, logger } from "@esspass-web-wallet/logging-lib";
const applicationName = "auth-server";

/**
 * OpenTelemetry
 */
const tracer = new Tracer(applicationName);

/**
 * OpenTelemetry
 */

import * as express from "express";
import "express-async-errors";
import * as cors from "cors";
import * as bodyParser from "body-parser";

import { NotFoundError } from "@esspass-web-wallet/domain-lib";
import { errorHandler } from "@esspass-web-wallet/backend-utils-lib";
import {
  postForAuthTokenAdapter,
  getOPMetadataAdapter,
  getPublicJwksAdapter,
} from "./authentication/adapter";

const log = logger("auth-server");

const app = express();

/* needed for TSL termination */
app.set("trust proxy", true);

app.use(cors());

app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);

app.use(bodyParser.json());

function setupRoutes() {
  /**
   * version - version deployed
   */
  app.get("/version", (req, res) => {
    return res.send({
      version: process.env.VERSION || "development",
      revision: process.env.GIT_REVISION || "1",
      branch: process.env.GIT_BRANCH || "master",
    });
  });

  app.get("/.well-known/openid-configuration", getOPMetadataAdapter);
  app.post("/conformance/v3/auth/token", postForAuthTokenAdapter);
  app.get("/conformance/v3/auth/jwks", getPublicJwksAdapter);

  /**
   * error handler
   */
  app.all("*", (req, res) => {
    throw new NotFoundError("Not found route");
  });
  app.use(errorHandler(log));
}

function startServer() {
  log.info("App name : " + applicationName);
  log.info("App version : " + process.env.VERSION || "development");
  log.info("Environment : " + process.env.ENVIRONMENT || "development");

  const port = 4300;
  const server = app.listen(port, () => log.info(`Listening at port: ${port}`));
  server.on("error", log.error);
}

//start the auth-server
setupRoutes();
startServer();

export { app as authServer };
