import { getCredentialStatus } from "./commons";

describe("getCredentialStatus from TIP proxy registered end-point", () => {
  jest.setTimeout(10_000);

  it("should return not revoked credential status list item", async () => {
    const proxyEndPoint =
      "https://api-pilot.ebsi.eu/trusted-issuers-registry/v4/issuers/did:ebsi:zeex3EqMqZQTpuqM7j1krDK/proxies/0xcfb2313e90007db4e517398cd762a18d85e686f78de6fd960a79600c58115f71/credentials/status/1";

    //given
    const credentialStatuses = [
      {
        id: "did:ebsi:z21NqAkAUjTgG9FazC6LzG35",
        type: "StatusList2021Entry",
        statusPurpose: "revocation",
        statusListIndex: "1",
        statusListCredential: proxyEndPoint,
      },
    ];

    //when
    const response = await getCredentialStatus(credentialStatuses);

    //then
    expect(response).toEqual([
      {
        credentialStatusObject: {
          id: "did:ebsi:z21NqAkAUjTgG9FazC6LzG35",
          type: "StatusList2021Entry",
          statusPurpose: "revocation",
          statusListIndex: "1",
          statusListCredential: proxyEndPoint,
        },
        revocationStatus: {
          returnCode: 0,
          resultClass: 0,
          returnMsg: "No Error: No errors occurred during process.",
          body: "Not revoked",
        },
      },
    ]);
  });

  // it("should return revoked credential status list item", async () => {
  //   //given
  //   const credentialStatuses = [
  //     {
  //       id: "did:ebsi:z21NqAkAUjTgG9FazC6LzG35",
  //       type: "StatusList2021Entry",
  //       statusPurpose: "revocation",
  //       statusListIndex: "42",
  //       statusListCredential:
  //         "https://api-pilot.ebsi.eu/trusted-issuers-registry/v4/issuers/did:ebsi:z21NqAkAUjTgG9FazC6LzG35/proxies/0xeaa0f32f96789895003d0516ef8b456e40fa0fac5a9992f230c195c729927945/credentials/status/1#list",
  //     },
  //   ];
  //
  //   //when
  //   const response = await getCredentialStatus(credentialStatuses);
  //
  //   //then
  //   expect(response).toEqual([
  //     {
  //       credentialStatusObject: {
  //         id: "did:ebsi:z21NqAkAUjTgG9FazC6LzG35",
  //         type: "StatusList2021Entry",
  //         statusPurpose: "revocation",
  //         statusListIndex: "42",
  //         statusListCredential:
  //           "https://api-pilot.ebsi.eu/trusted-issuers-registry/v4/issuers/did:ebsi:z21NqAkAUjTgG9FazC6LzG35/proxies/0xeaa0f32f96789895003d0516ef8b456e40fa0fac5a9992f230c195c729927945/credentials/status/1#list",
  //       },
  //       revocationStatus: {
  //         returnCode: 0,
  //         resultClass: 0,
  //         returnMsg: "No Error: No errors occurred during process.",
  //         body: "Revoked",
  //       },
  //     },
  //   ]);
  // });
});
