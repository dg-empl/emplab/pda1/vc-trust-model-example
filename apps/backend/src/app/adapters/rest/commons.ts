import { VerifiableCredentialDbModel } from "@esspass-web-wallet/crypto-lib";
import {
  CredentialStatus,
  NotFoundError,
  VerifiableCredential,
  verifiableCredentialSchema,
  VerifiableCredentialStatus,
} from "@esspass-web-wallet/domain-lib";
import { decodeJwt } from "jose";
import axios, { AxiosResponse } from "axios";
import { logger } from "@esspass-web-wallet/logging-lib";
import { nssiBEACOk } from "../../schemes/errors/errorCatalog";
import { ungzip } from "node-gzip";

export function extractBeforeString(kid: string, extractionString = "#") {
  const index = kid.indexOf(extractionString);
  if (index !== -1) {
    return kid.substring(0, index);
  } else throw new Error("Invalid issuerUrl jwks end-point");
}

export function extractAfterString(bearer: string, extractionString = "#") {
  const index = bearer.indexOf(extractionString);
  if (index !== -1) {
    return bearer.substring(index + extractionString.length, bearer.length - 1);
  } else throw new Error("Invalid Authorization Bearer token");
}

// const REVOCATION_STATUS_LISTS_API = "/credential-status-item";
const log = logger("backend");

export const decodeToVerifiableCredential = (
  dbResponse: VerifiableCredentialDbModel
): VerifiableCredential => {
  let parsedVcJwt: unknown;
  try {
    parsedVcJwt = decodeJwt(JSON.parse(dbResponse["vc"]))["vc"];
  } catch (e) {
    throw new NotFoundError("VerifiableCredentialContentAdapter decode vc jwt: " + e.message);
  }

  let vcResponse: VerifiableCredential = null;
  try {
    vcResponse = verifiableCredentialSchema.parse(parsedVcJwt);
  } catch (e) {
    throw new NotFoundError("VerifiableCredentialContentAdapter cannot parse vc jwt: " + e.message);
  }
  return vcResponse;
};

// export async function getCredentialStatus(credentialSubject: VerifiableCredentialStatus[]) {
//   const hostname = process.env.REVOCATION_URL + `${REVOCATION_STATUS_LISTS_API}`;
//
//   const result = { response: null };
//   let response: AxiosResponse<any, any>;
//   try {
//     response = await axios({
//       method: "GET",
//       url: hostname,
//       data: credentialSubject,
//     });
//   } catch (e) {
//     log.error("queryPda1Adapter:getCredentialStatus error: " + e.message);
//     throw new NotFoundError("Revocation connection error :" + e.message);
//   }
//
//   if (!response) {
//     throw new NotFoundError("No response from revocation server !");
//   }
//
//   const data = response.data;
//   result.response = JSON.stringify(data);
//   return result;
// }

export async function getCredentialStatus(
  credentialSubject: VerifiableCredentialStatus[]
): Promise<CredentialStatusItem[]> {
  const resultList = [];
  const statusListMap = new Map<string, string>();

  for (const credentialStatus of credentialSubject) {
    const credentialStatusItem: CredentialStatusItem = {
      credentialStatusObject: null,
      revocationStatus: null,
    };

    credentialStatusItem.credentialStatusObject = credentialStatus;
    credentialStatusItem.revocationStatus = await getCredentialStatusFromList(
      credentialStatus,
      statusListMap
    );

    resultList.push(credentialStatusItem);
  }

  return resultList;
}

/**This function decodes revocation status from given credentialStatus and statusListMap
 *
 * @param credentialStatusObject credentialStatus for which we want to check revocation
 * @param statusListMap map with status list link as a key, and jwt response as a value (which is obtained by following the link in the key) - if map is empty and initialized, it will be populated within the function. Use empty map to get refreshed data
 * @returns
 */
const getCredentialStatusFromList = async (
  credentialStatusObject: Partial<CredentialStatus>,
  statusListMap: Map<string, string>
) => {
  //as frontend expects data in this format, I am suing error objects copied from revocation (possible common lib candidate)
  const result = { ...nssiBEACOk };
  const revocationStatus = ["Not revoked", "Revoked"];
  const strings = credentialStatusObject.statusListCredential.split("/");
  const temp = strings.pop();
  const statusListID = Number(temp.split("#")[0]);

  //obtain the status list link from credential status itself
  const hostname = credentialStatusObject.statusListCredential;
  let responseRevocationStatusList: string | AxiosResponse<any, any>;

  // if map already contains status list link as a key, just use stored value, instead of calling service again.
  if (statusListMap.has(hostname)) {
    responseRevocationStatusList = statusListMap.get(hostname);
    //if not, make a call to a service by following status list link, obtain result with jwt inside, and store everything into the map.
  } else {
    //this is to be working with localhost only for debug
    // if(hostname.includes("https://localhost:")){
    //   const regex2 = /https:/;
    //   hostname = hostname.replace(regex2, "http:");
    // }
    try {
      responseRevocationStatusList = await axios({
        method: "GET",
        url: hostname,
      });
    } catch (e) {
      log.error("queryPda1Adapter:getCredentialStatusFromList error: " + e.message);
      throw new NotFoundError("Revocation connection error :" + e.message);
    }
    statusListMap.set(
      credentialStatusObject.statusListCredential,
      (responseRevocationStatusList as AxiosResponse<any, any>).data
    );
  }

  // decode obtained jwt, do mathematics, calculate revocation status, and send it back in necessery format.
  let statusList, vc, encodedList, credSubjFromVC;

  try {
    const jwtPayload = decodeJwt(statusListMap.get(hostname));
    if (jwtPayload) {
      vc = jwtPayload.vc;
      credSubjFromVC = vc.credentialSubject;
      encodedList = credSubjFromVC.encodedList;
      statusList = JSON.stringify(encodedList);
    } else {
      console.error("Invalid or expired token");
    }
  } catch (error) {
    console.error("Error decoding token:", error);
  }

  const base64decoded = Buffer.from(statusList, "base64");
  const credentialStatus = await calculateCredentialStatus(base64decoded, credentialStatusObject);

  result.body = revocationStatus[credentialStatus];
  log.trace("queryPda1Adapter:getCredentialStatusFromList:finished");
  return result;
};

async function calculateCredentialStatus(
  base64decoded: Buffer,
  credentialStatusObject: Partial<CredentialStatus>
) {
  const inflated = await ungzip(base64decoded);
  const byteIndex = Math.trunc(Number(credentialStatusObject.statusListIndex) / 8);
  const retVal = inflated[byteIndex];
  const bitIndex = Math.pow(2, 7 - (Number(credentialStatusObject.statusListIndex) % 8)) % 255;
  return (retVal & bitIndex) === 0 ? 0 : 1;
}

export interface CredentialStatusItem {
  credentialStatusObject: any | null;
  revocationStatus: any | null;
}

export type CredentialStatusRequestItem = {
  statusListCredential: string;
  statusListIndex: string;
};
