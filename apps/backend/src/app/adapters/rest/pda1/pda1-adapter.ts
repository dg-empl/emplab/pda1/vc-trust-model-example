import { logger } from "@esspass-web-wallet/logging-lib";
import { verifiableCredentialModel } from "@esspass-web-wallet/crypto-lib";
import { Request, Response } from "express";

const log = logger("backend");

export const deleteCredentialAdapter = async (req: Request, res: Response) => {
  verifiableCredentialModel.deleteOne(
    { did: `${req.params.did}`, id: `${req.params.id}` },
    function (err) {
      log.error(err);
    }
  );

  res.status(200).send({ status: "ok" });
};
