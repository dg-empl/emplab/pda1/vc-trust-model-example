import {
  credentialStatusResponseSchema,
  verifiableCredentialSchema,
} from "@esspass-web-wallet/domain-lib";
import { decodeJwt } from "jose";

describe("QueryPda1Adapter", () => {
  it("should extract credentialStatus from jwt", async () => {
    //given
    const jwt =
      "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImRpZDplYnNpOnoyMm04ZDVpWjZqUWdUN2V5SnBjMVdrbSNrZXlzLTEifQ.eyJqdGkiOiJ1cm46dXVpZDozMTViZjM2Ny1jYTM0LTRiODQtYjE2ZC0xNDE0MmI0NTViZmUiLCJzdWIiOiJkaWQ6ZWJzaTp6ZVc2WGlRTFk0QWRZRTR2VUtBcmdoN2VBdG5GaVBEZ1ZIODN3anJBbWJOd2UiLCJpc3MiOiJkaWQ6ZWJzaTp6MjJtOGQ1aVo2alFnVDdleUpwYzFXa20iLCJuYmYiOjE2ODAwMDgxMTcsImlhdCI6MTY4MDAwODExNywiZXhwIjoxNzExNTQ0MTE3LCJ2YyI6eyJAY29udGV4dCI6WyJodHRwczovL3d3dy53My5vcmcvMjAxOC9jcmVkZW50aWFscy92MSJdLCJpZCI6InVybjp1dWlkOjMxNWJmMzY3LWNhMzQtNGI4NC1iMTZkLTE0MTQyYjQ1NWJmZSIsInR5cGUiOlsiVmVyaWZpYWJsZUNyZWRlbnRpYWwiLCJWZXJpZmlhYmxlQXR0ZXN0YXRpb24iXSwiaXNzdWVyIjoiZGlkOmVic2k6ejIybThkNWlaNmpRZ1Q3ZXlKcGMxV2ttIiwiaXNzdWFuY2VEYXRlIjoiMjAyMy0wMy0yOFQxMjo1NToxNy43OTlaIiwidmFsaWRGcm9tIjoiMjAyMy0wMy0yOFQxMjo1NToxNy43OTlaIiwiaXNzdWVkIjoiMjAyMy0wMy0yOFQxMjo1NToxNy43OTlaIiwiY3JlZGVudGlhbFN0YXR1cyI6eyJpZCI6ImRpZDplYnNpOnoyMm04ZDVpWjZqUWdUN2V5SnBjMVdrbSIsInR5cGUiOiJTdGF0dXNMaXN0MjAyMUVudHJ5Iiwic3RhdHVzUHVycG9zZSI6InJldm9jYXRpb24iLCJzdGF0dXNMaXN0SW5kZXgiOiIzIiwic3RhdHVzTGlzdENyZWRlbnRpYWwiOiJodHRwOi8vbG9jYWxob3N0OjIwMDgwL2NyZWRlbnRpYWxzL3N0YXR1cy8xI2xpc3QifSwiY3JlZGVudGlhbFN1YmplY3QiOnsic2VjdGlvbjEiOnsicGVyc29uYWxJZGVudGlmaWNhdGlvbk51bWJlciI6IlBMMTIzMzEyMyIsInNleCI6IjAxIiwic3VybmFtZSI6Ilphd2FkemtpIiwiZm9yZW5hbWVzIjoiTWFyY2luIiwiZGF0ZUJpcnRoIjoiMTk5MC0wMS0wMSIsInN1cm5hbWVBdEJpcnRoIjoidGVzdCIsInBsYWNlQmlydGgiOnsidG93biI6ImtpZWxjZSIsInJlZ2lvbiI6ImtpZWxjZSIsImNvdW50cnlDb2RlIjoiUEwifSwibmF0aW9uYWxpdGllcyI6WyJQTCJdLCJzdGF0ZU9mUmVzaWRlbmNlQWRkcmVzcyI6eyJzdHJlZXRObyI6InRlc3QsIDEyMyAiLCJwb3N0Q29kZSI6IjEyMyIsInRvd24iOiJ0ZXN0IiwiY291bnRyeUNvZGUiOiJQTCJ9LCJzdGF0ZU9mU3RheUFkZHJlc3MiOnsic3RyZWV0Tm8iOiJ0ZXN0LCAxMjMgIiwicG9zdENvZGUiOiIxMjMiLCJ0b3duIjoidGVzdCIsImNvdW50cnlDb2RlIjoiUEwifX0sInNlY3Rpb24yIjp7Im1lbWJlclN0YXRlV2hpY2hMZWdpc2xhdGlvbkFwcGxpZXMiOiJBVCIsInN0YXJ0aW5nRGF0ZSI6IjIwMjMtMDQtMDEiLCJlbmRpbmdEYXRlIjoiMjAyMy0xMi0zMSIsImNlcnRpZmljYXRlRm9yRHVyYXRpb25BY3Rpdml0eSI6dHJ1ZSwiZGV0ZXJtaW5hdGlvblByb3Zpc2lvbmFsIjpmYWxzZSwidHJhbnNpdGlvblJ1bGVzQXBwbHlBc0VDODgzMjAwNCI6ZmFsc2V9LCJzZWN0aW9uMyI6eyJwb3N0ZWRFbXBsb3llZFBlcnNvbiI6ZmFsc2UsImVtcGxveWVkVHdvT3JNb3JlU3RhdGVzIjpmYWxzZSwicG9zdGVkU2VsZkVtcGxveWVkUGVyc29uIjp0cnVlLCJzZWxmRW1wbG95ZWRUd29Pck1vcmVTdGF0ZXMiOnRydWUsImNpdmlsU2VydmFudCI6dHJ1ZSwiY29udHJhY3RTdGFmZiI6ZmFsc2UsIm1hcmluZXIiOmZhbHNlLCJlbXBsb3llZEFuZFNlbGZFbXBsb3llZCI6ZmFsc2UsImNpdmlsQW5kRW1wbG95ZWRTZWxmRW1wbG95ZWQiOnRydWUsImZsaWdodENyZXdNZW1iZXIiOmZhbHNlLCJleGNlcHRpb24iOmZhbHNlLCJleGNlcHRpb25EZXNjcmlwdGlvbiI6IiIsIndvcmtpbmdJblN0YXRlVW5kZXIyMSI6ZmFsc2V9LCJzZWN0aW9uNCI6eyJlbXBsb3llZSI6ZmFsc2UsInNlbGZFbXBsb3llZEFjdGl2aXR5Ijp0cnVlLCJlbXBsb3llclNlbGZFbXBsb3llZEFjdGl2aXR5Q29kZXMiOlsidGVzdCJdLCJuYW1lQnVzaW5lc3NOYW1lIjoidGVzdCIsInJlZ2lzdGVyZWRBZGRyZXNzIjp7InN0cmVldE5vIjoidGVzdCwgMSAiLCJwb3N0Q29kZSI6IjEyMyIsInRvd24iOiJ0ZXN0IiwiY291bnRyeUNvZGUiOiJBVCJ9fSwic2VjdGlvbjUiOnsibm9GaXhlZEFkZHJlc3MiOnRydWV9LCJzZWN0aW9uNiI6eyJuYW1lIjoiT2ZmaWNlIE5hdGlvbmFsIGRlIFPDqWN1cml0w6kgU29jaWFsZSAvIFJpamtzZGllbnN0IHZvb3IgU29jaWFsZSBaZWtlcmhlaWQiLCJhZGRyZXNzIjp7InN0cmVldE5vIjoiTWFpbiBTdHJlZXQgMSIsInBvc3RDb2RlIjoiMTAwMCIsInRvd24iOiJCcnVzc2VscyIsImNvdW50cnlDb2RlIjoiQkUifSwiaW5zdGl0dXRpb25JRCI6Ik5TU0ktQkUtMDEiLCJvZmZpY2VQaG9uZU5vIjoiMDgwMCAxMjM0NSIsIm9mZmljZUZheE5vIjoiMDgwMCA5ODc2NSIsImVtYWlsIjoiaW5mb0Buc3NpLWJlLmV1IiwiZGF0ZSI6IjIwMjMtMDMtMjgiLCJzaWduYXR1cmUiOiJPZmZpY2lhbCBzaWduYXR1cmUifSwiaWQiOiJkaWQ6ZWJzaTp6ZVc2WGlRTFk0QWRZRTR2VUtBcmdoN2VBdG5GaVBEZ1ZIODN3anJBbWJOd2UifSwiY3JlZGVudGlhbFNjaGVtYSI6eyJpZCI6Imh0dHBzOi8vYXBpLXRlc3QuZWJzaS5ldS90cnVzdGVkLXNjaGVtYXMtcmVnaXN0cnkvdjIvc2NoZW1hcy8weDQ3YzYxNjY3MjgxY2MzODgzY2NhMDAxYmNhYzcwMzgyMzdjODVkMGU4ZjYxZjEzOTY1MjM3NWMzMzBhOTkwNjMiLCJ0eXBlIjoiRnVsbEpzb25TY2hlbWFWYWxpZGF0b3IyMDIxIn0sImV4cGlyYXRpb25EYXRlIjoiMjAyNC0wMy0yN1QxMjo1NToxNy4wMDBaIn19.VxChRkg9tqyFor_MDrXxMri2BmcjHtdRBvKL8DdCKOEU8ArgIEZwvs1Hx5xag9-B_lDQI2XR5Bscf6I2CjvH2A";
    const response = decodeJwt(jwt).vc;

    //when
    const credentialResponse = verifiableCredentialSchema.parse(response);

    //then
    expect(credentialResponse.credentialStatus).toBeTruthy();

    expect(credentialResponse.credentialStatus.id).toEqual("did:ebsi:z22m8d5iZ6jQgT7eyJpc1Wkm");
    expect(credentialResponse.credentialStatus.type).toEqual("StatusList2021Entry");
    expect(credentialResponse.credentialStatus.statusPurpose).toEqual("revocation");
    expect(credentialResponse.credentialStatus.statusListIndex).toEqual("3");
    expect(credentialResponse.credentialStatus.statusListCredential).toEqual(
      "http://localhost:20080/credentials/status/1#list"
    );
  });

  it("should extract credentialStatus from jwt - with workplaceName given", async () => {
    //given
    const jwt =
      "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImRpZDplYnNpOnoyMU5xQWtBVWpUZ0c5RmF6QzZMekczNSNpdTFNYXY2a21iTW5raDExLWN4N3YwQTdLRVJ3ZGZxQ3BzWFpSd1B0Zmc0In0.eyJqdGkiOiJkaWQ6ZWJzaTp6YzZZa29TS0xBZTVvQ3NIbmE3WDgyMXB4eXR0TU50R1JBTkFMOUQxSzVBeUEiLCJzdWIiOiJkaWQ6ZWJzaTp6YzZZa29TS0xBZTVvQ3NIbmE3WDgyMXB4eXR0TU50R1JBTkFMOUQxSzVBeUEiLCJpc3MiOiJkaWQ6ZWJzaTp6MjFOcUFrQVVqVGdHOUZhekM2THpHMzUiLCJuYmYiOjE3MDY2NDc4NjEsImlhdCI6MTcwNjY0Nzg2MSwiZXhwIjoxNzM4MTgzODYxLCJ2YyI6eyJAY29udGV4dCI6WyJodHRwczovL3d3dy53My5vcmcvMjAxOC9jcmVkZW50aWFscy92MSJdLCJpZCI6ImRpZDplYnNpOnpjNllrb1NLTEFlNW9Dc0huYTdYODIxcHh5dHRNTnRHUkFOQUw5RDFLNUF5QSIsInR5cGUiOlsiVmVyaWZpYWJsZUNyZWRlbnRpYWwiLCJWZXJpZmlhYmxlQXR0ZXN0YXRpb24iLCJWZXJpZmlhYmxlUG9ydGFibGVEb2N1bWVudEExIl0sImlzc3VlciI6ImRpZDplYnNpOnoyMU5xQWtBVWpUZ0c5RmF6QzZMekczNSIsImlzc3VhbmNlRGF0ZSI6IjIwMjQtMDEtMzBUMjA6NTE6MDEuNTkyWiIsInZhbGlkRnJvbSI6IjIwMjQtMDEtMzBUMjA6NTE6MDEuNTkyWiIsImlzc3VlZCI6IjIwMjQtMDEtMzBUMjA6NTE6MDEuNTkyWiIsImNyZWRlbnRpYWxTdGF0dXMiOnsiaWQiOiJkaWQ6ZWJzaTp6MjFOcUFrQVVqVGdHOUZhekM2THpHMzUiLCJ0eXBlIjoiU3RhdHVzTGlzdDIwMjFFbnRyeSIsInN0YXR1c1B1cnBvc2UiOiJyZXZvY2F0aW9uIiwic3RhdHVzTGlzdEluZGV4IjoiODgiLCJzdGF0dXNMaXN0Q3JlZGVudGlhbCI6Imh0dHA6Ly9sb2NhbGhvc3Q6MjAwODAvY3JlZGVudGlhbHMvc3RhdHVzLzEjbGlzdCJ9LCJjcmVkZW50aWFsU3ViamVjdCI6eyJpZCI6ImRpZDplYnNpOnpjNllrb1NLTEFlNW9Dc0huYTdYODIxcHh5dHRNTnRHUkFOQUw5RDFLNUF5QSIsInNlY3Rpb24xIjp7InBlcnNvbmFsSWRlbnRpZmljYXRpb25OdW1iZXIiOiJQTDk1OTI3NTMwIiwic2V4IjoiMDEiLCJzdXJuYW1lIjoiYSIsImZvcmVuYW1lcyI6ImEiLCJkYXRlQmlydGgiOiIxOTg5LTExLTEwIiwic3VybmFtZUF0QmlydGgiOiJudWxsIiwicGxhY2VCaXJ0aCI6eyJ0b3duIjoiV2Fyc3phd2EiLCJyZWdpb24iOiJXYXJzemF3YSIsImNvdW50cnlDb2RlIjoiUEwifSwibmF0aW9uYWxpdGllcyI6WyJQTCJdLCJzdGF0ZU9mUmVzaWRlbmNlQWRkcmVzcyI6eyJzdHJlZXRObyI6Ikp1bGlhbmEgVHV3aW1hIDciLCJwb3N0Q29kZSI6IjkwLTAxMCIsInRvd24iOiJXYXJzemF3YSIsImNvdW50cnlDb2RlIjoiUEwifSwic3RhdGVPZlN0YXlBZGRyZXNzIjp7InN0cmVldE5vIjoiSnVsaWFuYSBUdXdpbWEgNyIsInBvc3RDb2RlIjoiOTAtMDEwIiwidG93biI6IldhcnN6YXdhIiwiY291bnRyeUNvZGUiOiJQTCJ9fSwic2VjdGlvbjIiOnsibWVtYmVyU3RhdGVXaGljaExlZ2lzbGF0aW9uQXBwbGllcyI6IklUIiwic3RhcnRpbmdEYXRlIjoiMjAyNi0wMS0yMSIsImVuZGluZ0RhdGUiOiIyMDI3LTA1LTA5IiwiY2VydGlmaWNhdGVGb3JEdXJhdGlvbkFjdGl2aXR5Ijp0cnVlLCJkZXRlcm1pbmF0aW9uUHJvdmlzaW9uYWwiOnRydWUsInRyYW5zaXRpb25SdWxlc0FwcGx5QXNFQzg4MzIwMDQiOnRydWV9LCJzZWN0aW9uMyI6eyJwb3N0ZWRFbXBsb3llZFBlcnNvbiI6dHJ1ZSwiZW1wbG95ZWRUd29Pck1vcmVTdGF0ZXMiOnRydWUsInBvc3RlZFNlbGZFbXBsb3llZFBlcnNvbiI6dHJ1ZSwic2VsZkVtcGxveWVkVHdvT3JNb3JlU3RhdGVzIjp0cnVlLCJjaXZpbFNlcnZhbnQiOnRydWUsImNvbnRyYWN0U3RhZmYiOnRydWUsIm1hcmluZXIiOnRydWUsImVtcGxveWVkQW5kU2VsZkVtcGxveWVkIjp0cnVlLCJjaXZpbEFuZEVtcGxveWVkU2VsZkVtcGxveWVkIjp0cnVlLCJmbGlnaHRDcmV3TWVtYmVyIjp0cnVlLCJleGNlcHRpb24iOnRydWUsImV4Y2VwdGlvbkRlc2NyaXB0aW9uIjoibm8gZXhjZXB0aW9ucyIsIndvcmtpbmdJblN0YXRlVW5kZXIyMSI6dHJ1ZX0sInNlY3Rpb240Ijp7ImVtcGxveWVlIjp0cnVlLCJzZWxmRW1wbG95ZWRBY3Rpdml0eSI6dHJ1ZSwiZW1wbG95ZXJTZWxmRW1wbG95ZWRBY3Rpdml0eUNvZGVzIjpbIiJdLCJuYW1lQnVzaW5lc3NOYW1lIjoiOTE2NDE1MDAxIiwicmVnaXN0ZXJlZEFkZHJlc3MiOnsic3RyZWV0Tm8iOiJKdWxpYW5hIFR1d2ltYSA3IiwicG9zdENvZGUiOiI5MC0wMTAiLCJ0b3duIjoiV2Fyc3phd2EiLCJjb3VudHJ5Q29kZSI6IlBMIn19LCJzZWN0aW9uNSI6eyJ3b3JrUGxhY2VOYW1lcyI6W3siY29tcGFueU5hbWVWZXNzZWxOYW1lIjoiQXNzaWN1cmF6aW9uaSBHZW5lcmFsaSBTLnAuQSJ9XSwid29ya1BsYWNlQWRkcmVzc2VzIjpbeyJhZGRyZXNzIjp7InN0cmVldE5vIjoiUGlhenphIER1Y2EgZGVnbGkgQWJydXp6aSAyLCA5MzIiLCJ0b3duIjoiVHJpZXN0ZSIsInBvc3RDb2RlIjoiNTQ4NCIsImNvdW50cnlDb2RlIjoiSVQifX1dLCJub0ZpeGVkQWRkcmVzcyI6ZmFsc2V9LCJzZWN0aW9uNiI6eyJuYW1lIjoiT2ZmaWNlIE5hdGlvbmFsIGRlIFPDqWN1cml0w6kgU29jaWFsZSAvIFJpamtzZGllbnN0IHZvb3IgU29jaWFsZSBaZWtlcmhlaWQiLCJhZGRyZXNzIjp7InN0cmVldE5vIjoiTWFpbiBTdHJlZXQgMSIsInBvc3RDb2RlIjoiMTAwMCIsInRvd24iOiJCcnVzc2VscyIsImNvdW50cnlDb2RlIjoiQkUifSwiaW5zdGl0dXRpb25JRCI6Ik5TU0ktQkUtMDEiLCJvZmZpY2VQaG9uZU5vIjoiMDgwMCAxMjM0NSIsIm9mZmljZUZheE5vIjoiMDgwMCA5ODc2NSIsImVtYWlsIjoiaW5mb0Buc3NpLWJlLmV1IiwiZGF0ZSI6IjIwMjQtMDEtMzAiLCJzaWduYXR1cmUiOiJPZmZpY2lhbCBzaWduYXR1cmUifX0sImNyZWRlbnRpYWxTY2hlbWEiOnsiaWQiOiJodHRwczovL2FwaS1waWxvdC5lYnNpLmV1L3RydXN0ZWQtc2NoZW1hcy1yZWdpc3RyeS92Mi9zY2hlbWFzLzB4NDdjNjE2NjcyODFjYzM4ODNjY2EwMDFiY2FjNzAzODIzN2M4NWQwZThmNjFmMTM5NjUyMzc1YzMzMGE5OTA2MyIsInR5cGUiOiJGdWxsSnNvblNjaGVtYVZhbGlkYXRvcjIwMjEifSwiZXhwaXJhdGlvbkRhdGUiOiIyMDI1LTAxLTI5VDIwOjUxOjAxLjAwMFoifX0.MrVKF5_0xnRRrp55ES8wZ3uTxRst6Q9v5REZnLf6e4I0bSgnPEAGSFGk_H8K6LhEoD3ZQIKjVzaRC5h07PWHvA";
    const response = decodeJwt(jwt).vc;

    //when
    const credentialResponse = verifiableCredentialSchema.parse(response);

    //then
    expect(credentialResponse.credentialStatus).toBeTruthy();

    expect(credentialResponse.credentialStatus.id).toEqual("did:ebsi:z21NqAkAUjTgG9FazC6LzG35");
    expect(credentialResponse.credentialStatus.type).toEqual("StatusList2021Entry");
    expect(credentialResponse.credentialStatus.statusPurpose).toEqual("revocation");
    expect(credentialResponse.credentialStatus.statusListIndex).toEqual("88");
    expect(credentialResponse.credentialStatus.statusListCredential).toEqual(
      "http://localhost:20080/credentials/status/1#list"
    );
  });

  it("credential status response", () => {
    //given
    const credentialStatus = {
      returnCode: 0,
      resultClass: 0,
      returnMsg: "No Error: No errors occurred during process.",
      body: [
        {
          credentialStatusObject: {
            id: "urn:uuid:780091e2-56f1-4598-b6fc-5cdb516686fb",
            type: "StatusList2021Entry",
            statusPurpose: "revocation",
            statusListIndex: "0",
            statusListCredential: "https://localhost:20080/credentials/status/1#list",
          },
          revocationStatus: {
            returnCode: 0,
            resultClass: 0,
            returnMsg: "No Error: No errors occurred during process.",
            body: "Not revoked",
          },
        },
      ],
    };

    //when
    const credentialStatusResponse = credentialStatusResponseSchema.parse(credentialStatus);

    //then
    expect(credentialStatusResponse.returnCode).toEqual(0);
    expect(credentialStatusResponse.resultClass).toEqual(0);
    expect(credentialStatusResponse.returnMsg).toEqual(
      "No Error: No errors occurred during process."
    );
  });

  it("match credential status response body to revocation status", () => {
    //given
    const response = {
      returnCode: 0,
      resultClass: 0,
      returnMsg: "No Error: No errors occurred during process.",
      body: [
        {
          revocationStatus: {
            returnCode: 0,
            resultClass: 0,
            returnMsg: "No Error: No errors occurred during process.",
            body: "Not Revoked",
          },
          credentialStatusObject: {
            id: "urn:uuid:140b86e5-aa26-4c70-b633-0fe04e7bae49",
            type: "StatusList2021Entry",
            statusPurpose: "revocation",
            statusListIndex: "15",
            statusListCredential: "https://localhost:20080/credentials/status/1#list",
          },
        },
        {
          revocationStatus: {
            returnCode: 0,
            resultClass: 0,
            returnMsg: "No Error: No errors occurred during process.",
            body: "Revoked",
          },
          credentialStatusObject: {
            id: "urn:uuid:140b86e5-aa26-4c70-b633-0fe04e7bae40",
            type: "StatusList2021Entry",
            statusPurpose: "revocation",
            statusListIndex: "15",
            statusListCredential: "https://localhost:20080/credentials/status/1#list",
          },
        },
      ],
    };

    const pda1s = [
      {
        id: "urn:uuid:140b86e5-aa26-4c70-b633-0fe04e7bae49",
        date: "2023-03-30T11:34:13.383Z",
        endingDate: "2023-12-31",
        memberStateWhichLegislationApplies: "BG",
        time: "2023-03-30T11:34:13.383Z",
        govInstitutionName:
          "Office National de Sécurité Sociale / Rijksdienst voor Sociale Zekerheid",
        status: "true",
        credentialStatus: {
          id: "urn:uuid:140b86e5-aa26-4c70-b633-0fe04e7bae49",
          type: "StatusList2021Entry",
          statusPurpose: "revocation",
          statusListIndex: "15",
          statusListCredential: "https://localhost:20080/credentials/status/1#list",
        },
      },
      {
        id: "urn:uuid:140b86e5-aa26-4c70-b633-0fe04e7bae40",
        date: "2023-03-30T11:34:13.383Z",
        endingDate: "2023-12-31",
        memberStateWhichLegislationApplies: "BG",
        time: "2023-03-30T11:34:13.383Z",
        govInstitutionName:
          "Office National de Sécurité Sociale / Rijksdienst voor Sociale Zekerheid",
        status: "true",
        credentialStatus: {
          id: "urn:uuid:140b86e5-aa26-4c70-b633-0fe04e7bae40",
          type: "StatusList2021Entry",
          statusPurpose: "revocation",
          statusListIndex: "15",
          statusListCredential: "https://localhost:20080/credentials/status/1#list",
        },
      },
    ];

    //when
    const credentialStatusRevocation = {};

    response.body.forEach(credRevo => {
      credentialStatusRevocation[credRevo.credentialStatusObject.id] =
        credRevo.revocationStatus.body;
    });

    //then
    expect(credentialStatusRevocation["urn:uuid:140b86e5-aa26-4c70-b633-0fe04e7bae49"]).toEqual(
      "Not Revoked"
    );
    expect(credentialStatusRevocation["urn:uuid:140b86e5-aa26-4c70-b633-0fe04e7bae40"]).toEqual(
      "Revoked"
    );
  });
});
