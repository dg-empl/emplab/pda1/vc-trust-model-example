import {
  Pda1Dto,
  VerifiableCredential,
  CredentialStatus,
  verifiableCredentialSchema,
} from "@esspass-web-wallet/domain-lib";
import { decodeJwt } from "jose";
import {
  VerifiableCredentialDbModel,
  verifiableCredentialModel,
} from "@esspass-web-wallet/crypto-lib";
import { CredentialStatusItem, getCredentialStatus } from "../commons";
import { Request, Response } from "express";

/**
 * queryPda1Adapter - holder-wallet to query the db for all the pda1s for a given did
 * @param req
 * @param res
 */
export const queryPda1Adapter = async (req: Request, res: Response) => {
  const credentialStatuses: CredentialStatus[] = [];

  const dbResponse: VerifiableCredentialDbModel[] = await verifiableCredentialModel.find({
    did: `${req.params.did}`,
  });

  const pda1s: Pda1Dto[] = dbResponse
    .map(credential => {
      const id = credential["id"];

      const verifiableCredential: VerifiableCredential = verifiableCredentialSchema.parse(
        decodeJwt(JSON.parse(credential["vc"])).vc
      );
      return { verifiableCredential, id };
    })
    .map((v: { verifiableCredential: VerifiableCredential; id: string }) => {
      const { verifiableCredential: vc, id } = v;
      return {
        id: id,
        date: vc.issuanceDate,
        endingDate: vc.credentialSubject.section2.endingDate,
        memberStateWhichLegislationApplies:
          vc.credentialSubject.section2.memberStateWhichLegislationApplies,
        time: vc.issued as string,
        govInstitutionName: vc.credentialSubject.section6.name,
        status: "true",
        credentialStatus: vc.credentialStatus as CredentialStatus,
      };
    })
    .map(pda1 => {
      const tempPda1 = {
        ...pda1,
        credentialStatus: { ...pda1.credentialStatus, id: pda1.id },
      };
      credentialStatuses.push(tempPda1.credentialStatus);
      return tempPda1;
    });

  if (pda1s.length > 0) {
    const credentialResponses: CredentialStatusItem[] = await getCredentialStatus(
      credentialStatuses
    );
    const credentialStatusRevocation = {};

    credentialResponses.forEach(credRevo => {
      credentialStatusRevocation[credRevo.credentialStatusObject.id] =
        credRevo.revocationStatus.body;
    });

    pda1s.forEach(pda1 => {
      pda1.status = credentialStatusRevocation[pda1.id];
    });
  }

  res.send(pda1s);
};
