import { Request, Response } from "express";
import { check, validationResult } from "express-validator";
import {
  VerifiableCredentialDbModel,
  verifiableCredentialModel,
} from "@esspass-web-wallet/crypto-lib";
import {
  NotFoundError,
  Pda1Type,
  VerifiableCredential,
  VerifiableCredentialStatusExtended,
} from "@esspass-web-wallet/domain-lib";
import {
  CredentialStatusItem,
  decodeToVerifiableCredential,
  getCredentialStatus,
} from "../commons";
import { logger } from "@esspass-web-wallet/logging-lib";
import { changeBooleanToStringAnswers } from "./query-pda1-content-utils";

const log = logger("backend");

async function validateVerifiableCredentialContentAdapter(req: Request) {
  await check("docId").isString().run(req);

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const msg: string[] = [""];
    errors.array().forEach(error => {
      msg.push(" " + `${error.param} : ${error.msg}`);
    });
    throw new Error("Invalid verifiableCredentialContentAdapter request params " + msg);
  }
}

/**
 * get the content of vc from the db
 * @param req
 * @param res
 */
export const queryPda1ContentAdapter = async (req: Request, res: Response) => {
  /**
   * validate the request
   */
  await validateVerifiableCredentialContentAdapter(req);

  const pda1DbId = req.body["docId"];
  /**
   * get the vc from the db in db domain format
   */
  const dbResponse: VerifiableCredentialDbModel = await verifiableCredentialModel.findOne({
    id: pda1DbId,
  });

  /**
   * decode the vc from the db domain format
   */
  let vcResponse: VerifiableCredential = decodeToVerifiableCredential(dbResponse);

  /**
   * get the credential status from the vc
   */
  let credentialResponse: CredentialStatusItem[];
  try {
    credentialResponse = await getCredentialStatus([
      {
        ...vcResponse.credentialStatus,
        id: `${req.body.docId}`,
      },
    ]);
  } catch (e) {
    throw new NotFoundError(e.message);
  }

  const credentialStatusRevocation = {};

  /**
   * get the credential status from the response
   */
  credentialResponse.forEach(credRevo => {
    credentialStatusRevocation[credRevo.credentialStatusObject.id] = credRevo.revocationStatus.body;
  });

  /**
   * change the boolean to string for the answers
   */
  let pda1Type: Pda1Type;
  try {
    pda1Type = changeBooleanToStringAnswers(vcResponse.credentialSubject);
  } catch (e) {
    throw new NotFoundError(e.message);
  }

  vcResponse = {
    ...vcResponse,
    credentialSubject: pda1Type,
  };
  /**
   * return the vc matched with pda1DbId with the credential status
   */
  const response: VerifiableCredentialStatusExtended = {
    ...vcResponse,
    status: credentialStatusRevocation[pda1DbId],
    time: vcResponse.issued as string,
    govInstitutionName: vcResponse.credentialSubject.section6.name,
  };

  log.info("queryPda1ContentAdapter response : " + JSON.stringify(response));

  res.send(response);
};
