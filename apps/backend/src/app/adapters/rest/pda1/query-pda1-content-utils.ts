import { Pda1Type } from "@esspass-web-wallet/domain-lib";

const convertBooleanToString = (value: boolean): string => {
  return value ? "yes" : "no";
};

export const changeBooleanToStringAnswers = (data: Pda1Type): Pda1Type => {
  const { certificateForDurationActivity } = data.section2;

  const {
    postedEmployedPerson,
    employedTwoOrMoreStates,
    postedSelfEmployedPerson,
    selfEmployedTwoOrMoreStates,
    civilServant,
    contractStaff,
    mariner,
    employedAndSelfEmployed,
    civilAndEmployedSelfEmployed,
    flightCrewMember,
    exception,
    exceptionDescription,
    workingInStateUnder21,
  } = data.section3;

  const { employee } = data.section4;

  const stringSection3 = {
    postedEmployedPerson: convertBooleanToString(postedEmployedPerson as boolean),
    employedTwoOrMoreStates: convertBooleanToString(employedTwoOrMoreStates as boolean),
    postedSelfEmployedPerson: convertBooleanToString(postedSelfEmployedPerson as boolean),
    selfEmployedTwoOrMoreStates: convertBooleanToString(selfEmployedTwoOrMoreStates as boolean),
    civilServant: convertBooleanToString(civilServant as boolean),
    contractStaff: convertBooleanToString(contractStaff as boolean),
    mariner: convertBooleanToString(mariner as boolean),
    employedAndSelfEmployed: convertBooleanToString(employedAndSelfEmployed as boolean),
    civilAndEmployedSelfEmployed: convertBooleanToString(civilAndEmployedSelfEmployed as boolean),
    flightCrewMember: convertBooleanToString(flightCrewMember as boolean),
    exception: convertBooleanToString(exception as boolean),
    exceptionDescription: exceptionDescription,
    workingInStateUnder21: convertBooleanToString(workingInStateUnder21 as boolean),
  };

  const stringSection4 = {
    employee: convertBooleanToString(employee as boolean),
    selfEmployedActivity: convertBooleanToString(data.section4.selfEmployedActivity as boolean),
  };

  const stringSection5 = {
    noFixedAddress: convertBooleanToString(data.section5.noFixedAddress as boolean),
  };

  return {
    ...data,
    section2: {
      ...data.section2,
      certificateForDurationActivity: convertBooleanToString(
        certificateForDurationActivity as boolean
      ),
    },
    section3: stringSection3,
    section4: { ...data.section4, ...stringSection4 },
    section5: { ...data.section5, ...stringSection5 },
  };
};
