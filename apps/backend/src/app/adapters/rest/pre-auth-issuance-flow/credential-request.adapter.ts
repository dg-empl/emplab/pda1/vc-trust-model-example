import fetch from "node-fetch";
import { decodeJwt } from "jose";

import { CredentialError, verifiableCredentialModel } from "@esspass-web-wallet/crypto-lib";
import { logger } from "@esspass-web-wallet/logging-lib";
import {
  CustomError,
  NotFoundError,
  verifiableCredentialSchema,
} from "@esspass-web-wallet/domain-lib";

import { Request, Response } from "express";
import { ConversionError } from "@esspass-web-wallet/backend-utils-lib";
import { CredentialService } from "../../../application/credentials/credential.service";

const log = logger("backend");
const credentialService = CredentialService.getInstance(log);

/**
 * Request a credential from the issuer
 * @param issuer - The issuer's URL
 * @param proofJwt - The proof JWT
 * @type {Promise<Response>}
 * @return {Promise<VerifiableCredential>}
 * @throws {NotFoundError}
 */
async function getVCredential(issuer: string, proofJwt: string) {
  const response = await fetch(`${issuer}/v2/credential`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      format: "jwt_vc_json",
      types: ["VerifiableCredential"],
      proof: {
        proof_type: "jwt",
        jwt: `${proofJwt}`,
      },
    }),
  });

  const res = await response.json();

  if (res.errors) {
    throw new CredentialError("invalid_token", {
      errorDescription: `pda-management : ${res.errors[0].message}`,
    });
  }

  return res;
}

/**
 * credentialRequestAdapter
 * @param req
 * @param res
 */
export const credentialRequestAdapter = async (req: Request, res: Response) => {
  const request = req.body;

  const { userEmail } = request;
  const { access_token, c_nonce: nonce } = request.token;
  const { iss: issuer, doc_id: pdaId } = decodeJwt(access_token);

  /**
   * Create a proof JWT
   */
  const proofJwt = await credentialService.createProofJwt(
    issuer,
    nonce as string,
    pdaId as string,
    userEmail
  );

  /**
   * Request a credential from the issuer using the proof JWT
   */
  let credentialResponseObject: { vc: string; id: any; did: any };
  try {
    credentialResponseObject = await getVCredential(issuer, proofJwt);
  } catch (e) {
    throw new NotFoundError("getVCredential error: " + e.message);
  }

  /**
   * Decode the credential JWT
   */
  const verifiableCredential = verifiableCredentialSchema.safeParse(
    decodeJwt(credentialResponseObject.vc).vc
  );

  if (!verifiableCredential.success) {
    const errorDesc = verifiableCredential["error"].issues
      .map(issue => `'${issue.path.join(".")}': ${issue.message}`)
      .join("\n");

    log.error(`Error in response from issuer : ${errorDesc}`);

    throw new ConversionError("invalid_conversion_action", {
      errorDescription: `Error in response from issuer : ${errorDesc}`,
    });
  }

  /**
   * Save the credential to the holder-wallet database
   */
  await verifiableCredentialModel.create({
    id: credentialResponseObject.id,
    did: credentialResponseObject.did,
    vc: JSON.stringify(credentialResponseObject.vc),
  });

  /**
   * Return the credential to the holder-wallet
   */
  return res.send({ vc: verifiableCredential });
};
