import { extractBeforeString } from "../commons";

describe("credential-request.adapter", () => {
  it("decode DID from KID", () => {
    const kid =
      "did:ebsi:zquUMs49eQATDSCmMuETHPVsUBnpMsx6tvJfYEj8m9gJw#1qIcOb7eMPxb08bG6R9BKdK01LAHltGXVI_ayKLY0ew";

    expect(extractBeforeString(kid)).toBeTruthy();
  });

  it("should extract issuerUrl out of /jwks end-point", () => {
    //given
    const issuerUrl = "https://issuer.ebsi.xyz/jwks";

    //given
    const issuerUrlWithoutJwks = extractBeforeString(issuerUrl, "/jwks");

    //then
    expect(issuerUrlWithoutJwks).toBe("https://issuer.ebsi.xyz");
  });
});
