import { decodeJwt } from "jose";
import { Request } from "express";
import fetch, { Response } from "node-fetch";
import { logger } from "@esspass-web-wallet/logging-lib";
import { extractAfterString, extractBeforeString } from "../commons";
import { check, header, validationResult } from "express-validator";
import { CredentialError } from "@esspass-web-wallet/crypto-lib";
import { CredentialService } from "../../../application/credentials/credential.service";

const log = logger("backend");
const credentialService = CredentialService.getInstance(log);

async function validateParams(req: Request) {
  const bearerValidator = (str: string) => str.startsWith("Bearer ") && str.length > 7;

  await check("grantType").isString().run(req);
  await check("authCode").isString().run(req);
  await check("pin").isString().run(req);
  await header("authorization").custom(bearerValidator).run(req);

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    errors.array().forEach(error => {
      console.log(error);
    });

    throw new Error("Invalid parameters");
  }
}

export const webTokenRequestAdapter = async (req, res) => {
  let response: Response;
  const request = req.body;
  const headers = req.headers["authorization"];

  /**
   * Validate request params
   */
  await validateParams(req);

  let recipientEmail = "";
  try {
    recipientEmail = decodeJwt(extractAfterString(headers, "Bearer ")).email as string;
  } catch (e) {
    throw new CredentialError("invalid_token", {
      errorDescription: "Authorization header must contain a Bearer token with recipient email",
    });
  }

  const { grantType, authCode, pin } = request;

  /**
   * Validate pre-auth code cryptographic integrity
   * @param preAuthCode
   * @param recipientEmail of the aud ( audience ) in the handshake of v2 credential
   * @returns {Promise<string>} of issuer url /jwks if valid
   */
  const issuerUrlJwks = await credentialService.validatePreAuthCode(
    request["authCode"],
    recipientEmail
  );
  const issuerUrl = extractBeforeString(issuerUrlJwks, "/jwks");

  try {
    response = await fetch(
      `${issuerUrl}/v2/token?` +
        `grant_type=${grantType}&` +
        `pre-authorized_code=${authCode}` +
        `&user_pin=${pin}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      }
    );
  } catch (e) {
    log.error(e);
    return res.send(e);
  }

  return res.send(await response.json());
};
