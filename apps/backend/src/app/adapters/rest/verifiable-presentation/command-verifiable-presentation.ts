import fetch from "node-fetch";
import { logger } from "@esspass-web-wallet/logging-lib";
import { NotFoundError } from "@esspass-web-wallet/domain-lib";
import { Request, Response } from "express";
import { CredentialService } from "../../../application/credentials/credential.service";

const log = logger("backend");
const credentialService = CredentialService.getInstance(log);

function chooseRedirectUri() {
  if (!process.env.VERIFICATION_API_URL) {
    throw new NotFoundError(
      "VERIFICATION_API_URL env is unknown - cannot proceed with vpr redirection !"
    );
  }

  const redirectUri = process.env.VERIFICATION_API_URL + "/verifier/authentication-responses";
  log.trace(`redirectUri: ${redirectUri}`);

  return redirectUri;
}

export const commandVerifiablePresentationAdapter = async (req: Request, res: Response) => {
  let responseObj: any, response: string | object;

  const { email, verification, sharedCredentialId } = req.body;

  try {
    const idToken: string = await credentialService.createVprIdToken(email);
    const vpToken: string = await credentialService.createVprVpToken(email, sharedCredentialId);
    const redirectUri = chooseRedirectUri();

    const details = {
      id_token: idToken,
      vp_token: vpToken,
    };

    const formBody = [];
    for (const property in details) {
      const encodedKey = encodeURIComponent(property);
      const encodedValue = encodeURIComponent(details[property]);
      formBody.push(encodedKey + "=" + encodedValue);
    }
    const body = formBody.join("&");

    if (idToken && vpToken) {
      responseObj = await fetch(
        // "http://192.168.0.254:3001/verifier/authentication-responses",
        `${redirectUri}`,
        {
          method: "POST",
          body: body,
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            verification: `${verification}`,
          },
        }
      );

      response = await responseObj.json();
    }

    log.traceObj("response : ", response);
  } catch (e) {
    log.error(e);
    return res.send(e);
  }

  return res.send(response);
};
