import fetch from "node-fetch";
import { logger } from "@esspass-web-wallet/logging-lib";
import { Request, Response } from "express";

export const queryVerifiablePresentationDefinitionAdapter = async (req: Request, res: Response) => {
  let response: any;

  const log = logger("backend");
  const request = req.body;

  try {
    // response = await fetch(`http://192.168.0.254:3001/verifier/presentation-definition-uri?credType=pda1`, {
    response = await fetch(`${request.presentationDefinitionUri}`, {
      method: "GET",
    });
  } catch (e) {
    log.error(e);
    return res.send(e);
  }
  return res.send(await response.json());
};
