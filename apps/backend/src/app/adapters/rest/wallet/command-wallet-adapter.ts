import { CredentialError } from "@esspass-web-wallet/crypto-lib";
import { logger } from "@esspass-web-wallet/logging-lib";
import { decodeJwt } from "jose";
import { extractAfterString } from "../commons";
import { holderWalletModel } from "../../../domain/holder-wallet-schema";
import { CryptoWalletService } from "../../../application/crypto-wallet/crypto-wallet.service";

const log = logger("backend");

const walletService = CryptoWalletService.getInstance(log);

const commandWalletAdapter = async (req, res) => {
  const log = logger("backend");
  const headers = req.headers["authorization"];

  let ownerEmail: string;
  try {
    ownerEmail = decodeJwt(extractAfterString(headers, "Bearer ")).email as string;
  } catch (e) {
    throw new CredentialError("invalid_token", {
      errorDescription: `Invalid Authorization Token: owner email not found `,
    });
  }

  try {
    const record = await walletService.generateWallet(ownerEmail);
    const newWallet = holderWalletModel.create(record);
    log.traceObj("wallet created : ", newWallet);

    res.send({ code: 200, description: "Wallet created" });
  } catch (e) {
    log.error(e);
    res.send({ code: 202, description: "Error on wallet generation : " + e });
  }
};

const deleteWalletAdapter = async (req, res) => {
  const log = logger("backend");
  try {
    await walletService.deleteWallet(req.params.id);
    res.send({ code: 200, description: "Deleted" });
  } catch (e) {
    log.error(e);
    res.send({ code: 201, description: "Error on deletion : " + e });
  }
};

export { commandWalletAdapter, deleteWalletAdapter };
