import { logger } from "@esspass-web-wallet/logging-lib";
import { WalletDbType } from "@esspass-web-wallet/domain-lib";
import { CryptoWalletService } from "../../../application/crypto-wallet/crypto-wallet.service";

const log = logger("backend");
const walletService = CryptoWalletService.getInstance(log);

const queryWalletAdapter = async (req, res) => {
  try {
    const did = await retrieveDid(req.params.email);
    res.send(did ? { did: did } : null);
  } catch (e) {
    log.error(e);
  }

  async function retrieveDid(email: string) {
    const wallet: WalletDbType = await walletService.findWalletByEmail(email);

    if (wallet) {
      return JSON.parse(wallet.wallet).did;
    } else {
      return null;
    }
  }
};

export { queryWalletAdapter };
