/**
 * @jest-environment node
 */
import { logger } from "@esspass-web-wallet/logging-lib";
import { CredentialService } from "./credential.service";

describe("wallet generation and jwt verification", () => {
  let credentialService: CredentialService;

  beforeAll(async () => {
    // wallet = await createEssPassWallet();
    // expect(validate(wallet.did)).toEqual(2);
    // credentialService = CredentialService.getInstance(logger("credential-service-spec"));
  });

  it("should be true", () => {
    expect(true).toBe(true);
  });

  // it("create jwt with token", async () => {
  //   //given
  //   const nonceRnd = randomUUID();
  //   const docIdRnd = randomUUID();
  //   const alg = "ES256";
  //
  //   //when
  //   const jwt = await credentialService.createProofJwt(
  //     "http://localhost:3000",
  //     nonceRnd,
  //     docIdRnd,
  //     "sub"
  //   );
  //
  //   const privateKey = await importJWK(wallet.keys.ES256.privateKeyJwk, alg);
  //
  //   // const jwt = await new SignJWT({
  //   //   iss: "",
  //   //   aud: "",
  //   //   nonce: nonceRnd,
  //   //   doc_id: docIdRnd,
  //   // })
  //   //   .setProtectedHeader({
  //   //     alg: alg,
  //   //     kid: wallet.keys.ES256.publicKeyJwk as string,
  //   //   })
  //   //   .setIssuer("urn:example:issuer")
  //   //   .setAudience("urn:example:audience")
  //   //   .setIssuedAt(new Date().getTime())
  //   //   .sign(privateKey);
  //   //
  //   // const jwks = createLocalJWKSet({
  //   //   keys: [{ ...wallet.keys.ES256.publicKeyJwk, alg: alg }],
  //   // });
  //   //
  //   // //when
  //   // const { payload } = await jwtVerify(jwt, jwks, {
  //   //   issuer: "urn:example:issuer",
  //   //   audience: "urn:example:audience",
  //   // });
  //
  //   const payload = decodeJwt(jwt);
  //
  //   //then
  //   expect(payload.iss).toEqual("urn:example:issuer");
  //   expect(payload.aud).toEqual("urn:example:audience");
  //   expect(payload["nonce"]).toEqual(nonceRnd);
  //   expect(payload["doc_id"]).toEqual(docIdRnd);
  // });
});
