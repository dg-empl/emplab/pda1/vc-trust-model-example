import {
  calculateJwkThumbprint,
  decodeJwt,
  decodeProtectedHeader,
  importJWK,
  JWK,
  jwtVerify,
  SignJWT,
} from "jose";
import { createVerifiablePresentationJwt, EbsiIssuer } from "@cef-ebsi/verifiable-presentation";
import { randomUUID } from "node:crypto";

import { Logger } from "@esspass-web-wallet/logging-lib";
import {
  verifiableCredentialModel,
  PreAuthCodeGrantsPayload,
  preAuthCodeGrantsPayloadSchema,
  CredentialError,
  JWKWithKid,
} from "@esspass-web-wallet/crypto-lib";

import axios from "axios";
import { NotFoundError, WalletDbType } from "@esspass-web-wallet/domain-lib";
import { CryptoWalletService } from "../crypto-wallet/crypto-wallet.service";

export class CredentialService {
  private static log: Logger;
  private static credentialService: CredentialService;
  private walletService: CryptoWalletService;

  private constructor() {
    this.walletService = CryptoWalletService.getInstance(CredentialService.log);
    CredentialService.log
      ? CredentialService.log.debug("Creating new instance of CredentialService")
      : null;
  }

  public static getInstance(log: Logger): CredentialService {
    this.log = log;
    if (!this.credentialService) {
      this.credentialService = new CredentialService();
    }
    return this.credentialService;
  }

  public async createProofJwt(
    issuerUrl: string,
    nonce: string,
    pdaId: string,
    userEmail: string
  ): Promise<string> {
    const alg = "ES256";

    //used to populate the kid field in the jwt header
    const did = await this.holderWalletDid(userEmail);

    const { privateKeyJwk } = await this.retrieveKeys(userEmail);
    const privateKey = await importJWK(privateKeyJwk, alg);

    const jwtProof = new SignJWT({
      iss: "",
      aud: "",
      nonce: nonce,
      pdaId: pdaId,
    })
      .setProtectedHeader({
        alg: alg,
        kid: did,
      })
      .setIssuer("holder-web-wallet")
      .setAudience(issuerUrl)
      .setIssuedAt(new Date().getTime())
      .sign(privateKey);

    return jwtProof;
  }

  public async createVprIdToken(email: string) {
    const alg = "ES256";
    const { privateKeyJwk } = await this.retrieveKeys(email);
    const privateKey = await importJWK(privateKeyJwk, alg);
    const vpIssuerKid = await this.holderWalletKid(email);

    return await new SignJWT({
      _vp_token: {
        presentation_submission: {
          id: randomUUID(),
          definition_id: "verification_vp_request",
          descriptor_map: [
            {
              id: "verification_vp",
              format: "jwt_vp",
              path: "$",
            },
          ],
        },
      },
    })
      .setProtectedHeader({ alg: alg, typ: "JWT", kid: vpIssuerKid })
      .sign(privateKey);
  }

  public async createVprVpToken(subject: string, sharedCredentialId: string) {
    const vpIssuerKid = await this.holderWalletKid(subject);
    const vpIssuerDid = await this.holderWalletDid(subject);

    if (!vpIssuerDid) {
      throw new Error("No vpIssuerDid");
    }

    const alg = "ES256";
    const { publicKeyJwk, privateKeyJwk } = await this.retrieveKeys(subject);

    CredentialService.log.debug("sharedCredentialId : " + sharedCredentialId);

    const verifiableCredential = await this.getVerifiableCredential(
      vpIssuerDid,
      sharedCredentialId
    );

    let vpPayload;
    if (verifiableCredential) {
      const vcJwt = JSON.parse(verifiableCredential["vc"]);

      vpPayload = {
        id: randomUUID(),
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        type: ["VerifiablePresentation"],
        holder: vpIssuerDid,
        verifiableCredential: [vcJwt],
      };
    }

    const API_DID = "did:ebsi:zicftVRNkkwfRourZd3nE5f";
    const EBSI_AUTHORITY = "api.test-ebsi.eu";

    const vpIssuer: EbsiIssuer = {
      did: vpIssuerDid,
      kid: vpIssuerKid,
      publicKeyJwk,
      privateKeyJwk,
      alg: alg,
    };

    return await createVerifiablePresentationJwt(vpPayload, vpIssuer, API_DID, {
      skipValidation: true,
      ebsiAuthority: EBSI_AUTHORITY,
      exp: Math.floor(new Date().setFullYear(new Date().getFullYear() + 10) / 1000),
    });
  }

  private async findWalletByEmail(userEmail: string): Promise<WalletDbType> {
    return await this.walletService.findWalletByEmail(userEmail);
  }

  private async holderWalletKid(email: string) {
    const wallet: WalletDbType = await this.findWalletByEmail(email);
    let ebsiWallet: string;

    if (wallet) {
      ebsiWallet = wallet.wallet || "";
    } else {
      throw new Error("Cannot retrieve wallet object");
    }

    return JSON.parse(ebsiWallet).keys.ES256.id;
  }

  private async retrieveKeys(userEmail: string) {
    const wallet: WalletDbType = await this.findWalletByEmail(userEmail);
    let essPassWallet: any;

    if (wallet) {
      essPassWallet = wallet.wallet;
    } else {
      throw new Error("Cannot retrieve wallet object");
    }

    return JSON.parse(essPassWallet).keys.ES256;
  }

  private async holderWalletDid(userEmail: string): Promise<string> {
    const wallet = await this.findWalletByEmail(userEmail);
    return wallet.did;
  }

  private async getVerifiableCredential(did: string, sharedCredentialId: string) {
    // const verifiableCredential = await verifiableCredentialModel.findOne({
    //   id: `${sharedCredentialId}`,
    // });
    const res: any[] = await verifiableCredentialModel.find();
    const verifiableCredential = res.filter(vc => vc.id === sharedCredentialId)[0];

    if (!verifiableCredential) {
      throw new NotFoundError("Verifiable Credential not found");
    }

    return verifiableCredential;
  }

  public async validatePreAuthCode(preAuthCode: string, recipientEmail: string): Promise<string> {
    const jwt = decodeProtectedHeader(preAuthCode);
    const preAuthCodeGrantsPayload: PreAuthCodeGrantsPayload = preAuthCodeGrantsPayloadSchema.parse(
      decodeJwt(preAuthCode)
    );

    const authPublicKey = await this.getPublicKey(preAuthCodeGrantsPayload.iss, jwt.kid);

    try {
      await jwtVerify(preAuthCode, authPublicKey);
    } catch (e) {
      console.log(e);
      throw new CredentialError("invalid_token", {
        errorDescription: `Invalid PreAuthCode Token: ${
          e instanceof Error ? e.message : "invalid signature"
        }`,
      });
    }

    /**
     * logged / recipient user is not the audience ( intended to receive ) user
     */
    if (recipientEmail !== preAuthCodeGrantsPayload.aud) {
      throw new CredentialError("invalid_or_missing_proof", {
        errorDescription: "Logged / recipient user is not the audience user",
      });
    }

    return preAuthCodeGrantsPayload.iss;
  }

  private async getPublicKey(jwksEndpoint: any, kid: any) {
    const jwksResponse = await axios.get(jwksEndpoint);
    const jwks = jwksResponse.data;
    const key = jwks.keys.find((key: { kid: any }) => key.kid === kid);

    const jwkWithKid: JWKWithKid = {
      ...key,
      kid: await calculateJwkThumbprint(key as JWK),
    };

    return await importJWK(jwkWithKid);
  }
}
