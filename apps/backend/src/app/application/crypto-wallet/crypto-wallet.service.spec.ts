/**
 * @jest-environment node
 */
import { CryptoWalletService } from "./crypto-wallet.service";
import { WalletDbType } from "@esspass-web-wallet/domain-lib";
import { logger } from "@esspass-web-wallet/logging-lib";

const log = logger("unit-tests-crypto-wallet-service");

describe("CryptoWalletService", () => {
  let service: CryptoWalletService;

  beforeAll(() => {
    service = CryptoWalletService.getInstance(log);
  });

  afterAll(async () => {
    // Avoid jest open handle error
    await new Promise(r => {
      setTimeout(r, 500);
    });
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should generate the wallet", async () => {
    //given
    const email = "test@test.xyz";

    //when
    const wallet: WalletDbType = await service.generateWallet(email);

    //then
    expect(wallet).toBeDefined();
  });
});
