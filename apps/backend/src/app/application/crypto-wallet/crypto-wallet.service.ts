import { WalletDbType } from "@esspass-web-wallet/domain-lib";
import { holderWalletModel } from "../../domain/holder-wallet-schema";

import { Logger } from "@esspass-web-wallet/logging-lib";
import { createEssPassWallet, EssPassWallet } from "./wallet.class";

export class CryptoWalletService {
  private static log: Logger;
  private static cryptoWalletService: CryptoWalletService;

  private constructor() {
    CryptoWalletService.log
      ? CryptoWalletService.log.debug("Creating new instance of CryptoWalletService")
      : null;
  }

  public static getInstance(log: Logger): CryptoWalletService {
    this.log = log;
    if (!this.cryptoWalletService) {
      this.cryptoWalletService = new CryptoWalletService();
    }
    return this.cryptoWalletService;
  }

  public async findWalletByEmail(userEmail: string): Promise<any> {
    return holderWalletModel.findOne({ email: userEmail });
  }

  public deleteWallet(subject: string) {
    try {
      return holderWalletModel.deleteOne({ subject: subject });
    } catch (e) {
      CryptoWalletService.log.error(e);
    }
    throw new Error("deleteWallet() failed for : " + subject);
  }

  /**
   * Generate a new wallet
   * @param email - owner email taken from the JWT of IDP
   */
  public async generateWallet(email: string): Promise<WalletDbType> {
    const wallet: EssPassWallet = await createEssPassWallet();
    const record: WalletDbType | any = {};

    record.email = email;
    record.creationDate = wallet.creationDate;
    record.did = wallet.did;
    record.wallet = JSON.stringify(wallet);

    return record;
  }
}
