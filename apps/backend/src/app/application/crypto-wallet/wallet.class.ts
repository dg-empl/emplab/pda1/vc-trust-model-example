import { calculateJwkThumbprint, exportJWK, generateKeyPair } from "jose";
import EbsiWallet from "@cef-ebsi/wallet-lib";
import { Wallet } from "ethers";
import { base64url } from "multiformats/bases/base64";

export class EssPassWallet {
  keys: {
    ES256: {
      id: string;
      privateKeyJwk: any;
      publicKeyJwk: any;
      privateKeyEncryptionJwk: any;
      publicKeyEncryptionJwk: any;
    };
  };
  ethWallet: {
    _isSigner: boolean;
    address: string;
    provider: object;
  };
  did: string;
  creationDate: number;
}

export async function createEssPassWallet(): Promise<EssPassWallet> {
  const alg = "ES256";
  const { privateKey, publicKey } = await generateKeyPair(alg);
  const publicKeyJwk = await exportJWK(publicKey);
  const privateKeyJwk = await exportJWK(privateKey);

  const thumbprint = await calculateJwkThumbprint(publicKeyJwk, "sha256");
  const subjectIdentifier = base64url.baseDecode(thumbprint);
  const naturalPersonDid = EbsiWallet.createDid("NATURAL_PERSON", subjectIdentifier);

  const wallet: EssPassWallet = new EssPassWallet();
  wallet.ethWallet = Wallet.createRandom();
  wallet.did = naturalPersonDid;
  wallet.keys = {
    ES256: {
      id: naturalPersonDid + "#" + thumbprint,
      privateKeyJwk: privateKeyJwk,
      privateKeyEncryptionJwk: privateKeyJwk,
      publicKeyJwk: publicKeyJwk,
      publicKeyEncryptionJwk: publicKeyJwk,
    },
  };
  wallet.creationDate = new Date().getTime();
  return wallet;
}
