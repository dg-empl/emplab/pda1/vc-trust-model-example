import { Pda1Dto } from "@esspass-web-wallet/domain-lib";

const response: Pda1Dto[] = [
  {
    id: "1",
    date: "September 29, 2022",
    endingDate: "September 29, 2022",
    time: "14:27",
    memberStateWhichLegislationApplies: "AT",
    govInstitutionName: "Office National de Sécurité Sociale / Rijksdienst voor Sociale Zekerheid",
    status: "VALID",
  },
  {
    id: "2",
    date: "October 07, 2020",
    endingDate: "October 07, 2020",
    time: "05:32",
    memberStateWhichLegislationApplies: "PL",
    govInstitutionName: "Zakład Ubezpieczeń Społecznych",
    status: "VALID",
  },
];

export { response };
