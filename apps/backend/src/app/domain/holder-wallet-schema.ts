import * as mongoose from "mongoose";

const holderWallet = new mongoose.Schema({
  creationDate: { type: String, default: "" },
  did: { type: String, default: "" },
  wallet: { type: String, default: {} },
  email: { type: String, default: "" },
});

const db = mongoose.connection.useDb("esspass-holder-wallet");

export const holderWalletModel = mongoose.model("holderWallet", holderWallet);
