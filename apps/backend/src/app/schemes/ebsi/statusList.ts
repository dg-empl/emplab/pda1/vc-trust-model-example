import * as mongoose from "mongoose";

const statusListSchema = new mongoose.Schema({
  dateOfCreation: Number,
  modificationDate: Number,
  statusListID: Number,
  statusList: String,
  listSize: Number,
  lastUsedIndex: Number,
});

const db = mongoose.connection.useDb("esspass-web-wallet");

export const statusList = db.model("statusList", statusListSchema);
