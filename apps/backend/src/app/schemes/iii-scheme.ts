
// const iss = {
//   issuer: "https://issuer.example.com",
//   credentials: [
//     {
//       format: "jwt_vc_json",
//       types: ["VerifiableCredential", "UniversityDegreeCredential"],
//     },
//   ],
//   "pre-authorized_code": "adhjhdjajkdkhjhdj",
//   user_pin_required: true,
// };

export const issuerIssuanceInitiationScheme = {
  id: "/issSchema",
  type: "object",
  properties: {
    issuer: { type: "string" },
    credentials: {
      type: "array",
      items: {
        type: "object",
        properties: {
          format: { type: "string" },
          types: {
            type: "array",
            items: { type: "string" },
          },
        },
      },
    },
    "pre-authorized_code": { type: "string" },
    user_pin_required: { type: "boolean" },
  }
};


