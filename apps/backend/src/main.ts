import { Tracer, logger } from "@esspass-web-wallet/logging-lib";
const applicationName = "backend-wallet";

/**
 * OpenTelemetry
 */
const tracer = new Tracer(applicationName);
import { api } from "@opentelemetry/sdk-node";
/**
 * OpenTelemetry
 */

import * as express from "express";
import "express-async-errors";
import * as cors from "cors";
import * as bodyParser from "body-parser";

import "dotenv/config";

import {
  queryWalletAdapter,
  queryPda1Adapter,
  queryPda1ContentAdapter,
  commandWalletAdapter,
  deleteWalletAdapter,
} from "./app/adapters/rest";

import { Database, errorHandler } from "@esspass-web-wallet/backend-utils-lib";

import {
  credentialRequestAdapter,
  webTokenRequestAdapter,
  deleteCredentialAdapter,
  queryVerifiablePresentationDefinitionAdapter,
  commandVerifiablePresentationAdapter,
} from "./app/adapters/rest";
import { NotFoundError } from "@esspass-web-wallet/domain-lib";
import { keycloakSetup } from "./app/utils/keycloak";
import { param, body } from "express-validator";

const log = logger(applicationName);

const app = express();

/* needed for TSL termination */
app.set("trust proxy", true);

app.use(cors());

app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);

app.use(bodyParser.json());

//keycloak
const keycloak = keycloakSetup(app);
app.use(keycloak.middleware());

/**
 * end-points
 */
function setupRoutes() {
  /**
   * version - version deployed
   */
  app.get("/version", (req, res) => {
    return res.send({
      version: process.env.VERSION || "development",
      revision: process.env.GIT_REVISION || "1",
      branch: process.env.GIT_BRANCH || "master",
    });
  });

  /**
   * verifiable-share-credential
   */
  app.get("/pda1/:did", keycloak.protect(), queryPda1Adapter);
  app.post("/pda1-content", keycloak.protect(), queryPda1ContentAdapter);
  app.delete("/pda1-content/:did/:id", keycloak.protect(), deleteCredentialAdapter);

  /**
   * wallet
   */
  app.post("/wallet", keycloak.protect(), [body("email")], commandWalletAdapter);
  app.get("/wallet/:email", keycloak.protect(), [param("email")], queryWalletAdapter);
  app.delete("/wallet/:id", keycloak.protect(), [param("id")], deleteWalletAdapter);

  /**
   * verifiable-presentation
   */
  app.post(
    "/verifiable-presentation-uri",
    keycloak.protect(),
    queryVerifiablePresentationDefinitionAdapter
  );
  app.post("/verifiable-presentation", keycloak.protect(), commandVerifiablePresentationAdapter);

  /**
   * iii - flow
   */
  app.post("/v2/token", keycloak.protect(), webTokenRequestAdapter);
  app.post("/v2/credential", keycloak.protect(), credentialRequestAdapter);

  /**
   * error handler
   */
  app.all("*", (req, res) => {
    throw new NotFoundError("Not found route");
  });
  app.use(errorHandler(log));
}

function startServer() {
  log.info("Application : " + applicationName);
  log.info("App version : " + process.env.VERSION || "development");
  log.info("Environment : " + process.env.ENVIRONMENT || "development");

  /**
   * connect to database
   */
  Database.getInstance(
    process.env.DB_HOST,
    process.env.DB_USERNAME,
    process.env.DB_PASSWORD,
    process.env.DB_PORT,
    "esspass-holder-wallet",
    log
  );

  const port = 3333;
  const server = app.listen(port, () => log.info(`Listening at port: ${port}`));
  server.on("error", log.error);
}

setupRoutes();
startServer();
