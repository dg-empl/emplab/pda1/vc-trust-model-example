import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'esspass-web-wallet',
  webDir: '../../dist/apps/esspass-web-wallet',
  bundledWebRuntime: false,
};

export default config;
