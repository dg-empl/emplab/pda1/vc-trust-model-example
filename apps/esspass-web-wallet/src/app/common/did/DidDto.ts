import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject("DidDto")
export class DidDto {
  @JsonProperty("did", String)
  did = ''
}
