import { NavigationHandler, Strategy } from "./navigation-strategy";
import { VerifiableCredentialQrCode, VerifiablePresentationQrCode } from "@esspass-web-wallet/domain-lib";

class NavigationStrategy implements Strategy {
  private _validated = false;

  get validated(): boolean {
    return this._validated;
  }

  set validated(value: boolean) {
    this._validated = value;
  }

  validate(arg: VerifiableCredentialQrCode | VerifiablePresentationQrCode): void {
    this.validated = true;
  }
}

describe("NavigationHandler", () => {
  it("should create an instance", () => {
    expect(new NavigationHandler()).toBeTruthy();
  });

  it("should return false on non-existing validation strategy", async () => {
    //given
    const handler = new NavigationHandler();

    //when
    const validationResponse = await handler.navigate("ImplementationOfValidationStrategy", "testString");

    //then
    expect(validationResponse).toBeFalsy();
  });

  it("should return true on existing validation strategy", async () => {
    //given
    const handler = new NavigationHandler();
    const strategy = new NavigationStrategy();

    handler.use("firstImpl", strategy);

    //when
    await handler.navigate("firstImpl", "testString");

    //then
    expect(strategy.validated).toBeTruthy();
  });

});
