import {
  VerifiableCredentialQrCode,
  VerifiablePresentationQrCode,
} from "@esspass-web-wallet/domain-lib";

export interface Strategy {
  validate(arg: VerifiableCredentialQrCode | VerifiablePresentationQrCode) : void;
}

export class NavigationHandler {
  strategies: Record<string, Strategy> = {};
  use(name: string, strategy: Strategy) {
    this.strategies[name] = strategy;
  }
  async navigate(name: string, arg: any) {
    if (!this.strategies[name]) {
      console.error("Authentication policy has not been set!");
      return false;
    }
    return this.strategies[name].validate(arg);
  }
}