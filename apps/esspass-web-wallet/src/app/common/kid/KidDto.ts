import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject("KidDto")
export class KidDto {
  @JsonProperty("kid", String)
  kid = ''
}
