import { Inject, Injectable } from "@angular/core";
import { HttpWrapperService } from "@esspass-web-wallet/web-utils-lib";
import { DidDto } from "../index";
import { APP_CONFIG, AppConfigInterface } from "@esspass-web-wallet/security-web-lib";
import { Observable } from "rxjs";
import { Pda1Dto, pda1DtoSchema, WalletDbType } from "@esspass-web-wallet/domain-lib";

@Injectable({
  providedIn: "root",
})
export class MyCredentialsService {
  constructor(
    private httpService: HttpWrapperService,
    @Inject(APP_CONFIG) private config: AppConfigInterface
  ) {}

  public getPda1Observables(did: string | undefined): Observable<Pda1Dto[]> {
    return this.httpService.findArrayNoGeneric(`${this.config.serverUrl}/pda1/${did}`);
  }

  public getHolderWalletDid(subject: string): Observable<DidDto> {
    return this.httpService.find(subject, `${this.config.serverUrl}/wallet`, DidDto);
  }

  public createHolderWallet(email: string): Observable<WalletDbType> {
    return this.httpService.createWithRequestBody(`${this.config.serverUrl}/wallet`, {
      email: email,
    });
  }
}
