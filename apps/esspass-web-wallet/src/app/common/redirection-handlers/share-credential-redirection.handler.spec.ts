/**
 * @jest-environment jsdom
 */
import { inject, TestBed } from "@angular/core/testing";
import { SharePinCodePageRedirectionHandler } from "./share-pin-qr-redirection.handler";
import { JsonProperty } from "json2typescript";
import { TokenService } from "@esspass-web-wallet/issuance-flow-lib";
import { Router } from "@angular/router";
import {
  toTypeScriptObjectOfType,
  VerifiableCredentialQrCode,
  VerifiablePresentationQrCode,
} from "@esspass-web-wallet/domain-lib";
import { ShareCredentialPageRedirectionHandler } from "./share-credential-redirection.handler";

class MockRouter {
  navigate(path: string) {}

  createUrlTree(path: string) {}
}

describe("ShareCredentialPageRedirectionHandler", () => {
  let router: MockRouter | Router;

  beforeEach(() => {
    router = new MockRouter();
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShareCredentialPageRedirectionHandler],
    });
  });

  describe("ShareCredentialPageRedirectionHandler", () => {
    it("should be created", inject(
      [ShareCredentialPageRedirectionHandler],
      (service: ShareCredentialPageRedirectionHandler) => {
        expect(service).toBeTruthy();
      }
    ));
  });

  it("should redirect to share pin code page", () => {
    const tokenService = new TokenService();
    const service = new ShareCredentialPageRedirectionHandler(
      tokenService,
      router as Router
    );

    //given
    const spy = jest.spyOn(router, "navigate");
    const vpRequestCommand = jest.spyOn(tokenService.vpRequestCommand, "next");

    const qrCode = "openid://?scope=openid&response_type=id_token&client_id=https://conformance.test.intebsi.xyz/verifier/authentication-responses&redirect_uri=https://conformance.test.intebsi.xyz/verifier/authentication-responses&claims={\"id_token\":{\"email\":null},\"vp_token\":{\"presentation_definition\":{\"id\":\"verification_vp_request\",\"input_descriptors\":[{\"id\":\"verification_vp\",\"name\":\"Verification VP\",\"purpose\":\"Only accept a VP containing a Verification VA\",\"constraints\":{\"fields\":[{\"path\":[\"$.vc.credentialSchema\"],\"filter\":{\"allOf\":[{\"type\":\"array\",\"contains\":{\"type\":\"object\",\"properties\":{\"id\":{\"type\":\"string\",\"pattern\":\"https://api.preprod.ebsi.eu/trusted-schemas-registry/v2/schemas/z3kRpVjUFj4Bq8qHRENUHiZrVF5VgMBUe7biEafp1wf2J\"}},\"required\":[\"id\"]}}]}}]}}],\"format\":{\"jwt_vp\":{\"alg\":[\"ES256K\"]}}}}}&nonce=f078873b-02a0-421c-bc56-459123feb8da&verification=4cdc4e59-9909-4676-a4e7-1e4e88772803\"";
    const vpRequest = parseVpRequest(qrCode);

    //when
    service.validate(vpRequest);

    //then
    expect(spy).toHaveBeenCalledWith(["/share-credentials"]);
    expect(vpRequestCommand).toHaveBeenCalledWith(vpRequest);
  });
});

const parseVpRequest = (qrCode: string): VerifiablePresentationQrCode => {
  const urlSearchParams = new URLSearchParams(qrCode);

  const clientId = urlSearchParams.get("client_id");
  const redirectUri = urlSearchParams.get("redirect_uri");
  // eslint-disable-next-line  @typescript-eslint/no-non-null-assertion
  const claims = JSON.parse(urlSearchParams.get("claims")!);
  const nonce = urlSearchParams.get("nonce");
  const verification = urlSearchParams.get("verification");

  return new VerifiablePresentationQrCode(
    // eslint-disable-next-line  @typescript-eslint/no-non-null-assertion
    clientId!,
    // eslint-disable-next-line  @typescript-eslint/no-non-null-assertion
    redirectUri!,
    // eslint-disable-next-line  @typescript-eslint/no-non-null-assertion
    claims!,
    // eslint-disable-next-line  @typescript-eslint/no-non-null-assertion
    nonce!,
    // eslint-disable-next-line  @typescript-eslint/no-non-null-assertion
    verification!
  );
}
