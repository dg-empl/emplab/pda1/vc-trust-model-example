import { Injectable } from "@angular/core";
import { Strategy } from "../handler-strategy/navigation-strategy";
import { Router } from "@angular/router";
import {
  VerifiableCredentialQrCode,
  VerifiablePresentationQrCode,
} from "@esspass-web-wallet/domain-lib";
import { TokenService } from "@esspass-web-wallet/issuance-flow-lib";

@Injectable({ providedIn: "root" })
export class ShareCredentialPageRedirectionHandler implements Strategy {
  constructor(private tokenService: TokenService, private router: Router) {}

  private async redirect(arg: VerifiablePresentationQrCode) {
    try {
      /**
       * redirect to share-credentials page with information store in tokenService
       */
      this.tokenService.vpRequestCommand.next(arg);
      await this.router.navigate(["/share-credentials"]);
    } catch (e) {
      throw Error(String(e));
    }
  }

  async validate(arg: VerifiableCredentialQrCode | VerifiablePresentationQrCode) {
    if (arg instanceof VerifiablePresentationQrCode) {
      return this.redirect(arg);
    }
  }
}
