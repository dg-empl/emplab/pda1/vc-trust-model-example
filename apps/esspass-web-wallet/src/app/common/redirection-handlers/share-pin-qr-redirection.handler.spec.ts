/**
 * @jest-environment jsdom
 */
import { inject, TestBed } from "@angular/core/testing";
import { SharePinCodePageRedirectionHandler } from "./share-pin-qr-redirection.handler";
import { TokenService } from "@esspass-web-wallet/issuance-flow-lib";
import { Router } from "@angular/router";
import { verifiableCredentialQrCodeSchema } from "@esspass-web-wallet/domain-lib";

class MockRouter {
  navigate(path: string) {}

  createUrlTree(path: string) {}
}

describe("SharePinQrRedirectionHandler", () => {
  let router: MockRouter | Router;

  beforeEach(() => {
    router = new MockRouter();
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SharePinCodePageRedirectionHandler],
    });
  });

  describe("SharePinCodePageRedirectionHandler", () => {
    it("should be created", inject(
      [SharePinCodePageRedirectionHandler],
      (service: SharePinCodePageRedirectionHandler) => {
        expect(service).toBeTruthy();
      }
    ));
  });

  it("should redirect to share pin code page", () => {
    const tokenService = new TokenService();
    const service = new SharePinCodePageRedirectionHandler(tokenService, router as Router);

    //given
    const spy = jest.spyOn(router, "navigate");
    const authCodeSpy = jest.spyOn(tokenService.authCode, "next");

    const qrCode = verifiableCredentialQrCodeSchema.parse({
      "urn:ietf:params:oauth:grant-type:pre-authorized_code": {
        "pre-authorized_code":
          "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImtpZCI6IkROeVh6Tmx0V1FMSjhnNVM3UTBTUWE1enNrWk92RlhUdkVvMENxUFZKVXcifQ.eyJhdXRob3JpemF0aW9uX2RldGFpbHMiOlt7InR5cGUiOiJvcGVuaWRfY3JlZGVudGlhbCIsImZvcm1hdCI6Imp3dF92YyIsImxvY2F0aW9ucyI6WyJodHRwOi8vbG9jYWxob3N0OjMxMDAiXSwidHlwZXMiOlsiVmVyaWZpYWJsZUNyZWRlbnRpYWwiLCJWZXJpZmlhYmxlQXR0ZXN0YXRpb24iLCJWZXJpZmlhYmxlUG9ydGFibGVEb2N1bWVudEExIl19XSwiaWF0IjoxNjk3NzI0NTgxLCJleHAiOjE2OTc3NTMzODEsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzEwMC9qd2tzIiwiYXVkIjoibWFyY2luLnphd2FkemtpQG91dGxvb2suY29tIiwic3ViIjoiNjUzMTEzMGRkNDYyZjhhOGFlNTIwNTQwIn0.Btn_4kWKDjOJB0teCvrW76rVupPGIvar9jn9pdVvf8XdiLF1zMaCWbkIziGUoUX9VZgEsW3ToE7ggvXAiDG0_g",
        user_pin_required: true,
      },
    });

    //when
    service.validate(qrCode);

    //then
    expect(spy).toHaveBeenCalledWith(["/share-pin"]);
    expect(authCodeSpy).toHaveBeenCalledWith(
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImtpZCI6IkROeVh6Tmx0V1FMSjhnNVM3UTBTUWE1enNrWk92RlhUdkVvMENxUFZKVXcifQ.eyJhdXRob3JpemF0aW9uX2RldGFpbHMiOlt7InR5cGUiOiJvcGVuaWRfY3JlZGVudGlhbCIsImZvcm1hdCI6Imp3dF92YyIsImxvY2F0aW9ucyI6WyJodHRwOi8vbG9jYWxob3N0OjMxMDAiXSwidHlwZXMiOlsiVmVyaWZpYWJsZUNyZWRlbnRpYWwiLCJWZXJpZmlhYmxlQXR0ZXN0YXRpb24iLCJWZXJpZmlhYmxlUG9ydGFibGVEb2N1bWVudEExIl19XSwiaWF0IjoxNjk3NzI0NTgxLCJleHAiOjE2OTc3NTMzODEsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzEwMC9qd2tzIiwiYXVkIjoibWFyY2luLnphd2FkemtpQG91dGxvb2suY29tIiwic3ViIjoiNjUzMTEzMGRkNDYyZjhhOGFlNTIwNTQwIn0.Btn_4kWKDjOJB0teCvrW76rVupPGIvar9jn9pdVvf8XdiLF1zMaCWbkIziGUoUX9VZgEsW3ToE7ggvXAiDG0_g"
    );
  });
});
