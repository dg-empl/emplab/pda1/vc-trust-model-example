import { Injectable } from "@angular/core";
import { Strategy } from "../handler-strategy/navigation-strategy";
import { TokenService } from "@esspass-web-wallet/issuance-flow-lib";
import { Router } from "@angular/router";
import {
  VerifiableCredentialQrCode,
  verifiableCredentialQrCodeSchema,
  VerifiablePresentationQrCode,
} from "@esspass-web-wallet/domain-lib";

@Injectable({ providedIn: "root" })
export class SharePinCodePageRedirectionHandler implements Strategy {
  constructor(private tokenService: TokenService, private router: Router) {}

  private async redirect(arg: VerifiableCredentialQrCode) {
    try {
      const preAuthorizedCodeJwt =
        arg["urn:ietf:params:oauth:grant-type:pre-authorized_code"]["pre-authorized_code"];

      this.tokenService.authCode.next(preAuthorizedCodeJwt);

      //redirect to share pin
      await this.router.navigate(["/share-pin"]);
    } catch (e) {
      throw Error(String(e));
    }
  }

  async validate(arg: VerifiableCredentialQrCode | VerifiablePresentationQrCode) {
    if (verifiableCredentialQrCodeSchema.safeParse(arg).success) {
      await this.redirect(arg as VerifiableCredentialQrCode);
    }
  }
}
