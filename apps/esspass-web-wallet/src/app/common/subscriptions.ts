import { Subscription } from 'rxjs';

const unsubscribe = (subscription: Subscription) => {
    if (subscription) {
        subscription.unsubscribe();
    }
};
export { unsubscribe };
