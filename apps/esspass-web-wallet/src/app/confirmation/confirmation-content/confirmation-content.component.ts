import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ShareCredentialService } from "../../share-credentials/share-credential.service";
import { Observable } from "rxjs";

@Component({
  selector: "esspass-web-wallet-confirmation-content",
  templateUrl: "./confirmation-content.component.html",
  styleUrls: ["./confirmation-content.component.scss"],
})
export class ConfirmationContentComponent implements OnInit {
  constructor(
    private router: Router,
    private shareCredentialService: ShareCredentialService
  ) {}

  countryCode$ = this.shareCredentialService.getCountryCode();

  ngOnInit() {}

  async cancel() {
    await this.router.navigate(["/my-credentials"]);
  }

  resolveCountry(): string {
    return this.shareCredentialService.resolveCountry();
  }
}
