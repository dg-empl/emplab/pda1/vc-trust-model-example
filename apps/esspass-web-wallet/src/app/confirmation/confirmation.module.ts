import { NgModule } from "@angular/core";
import { CommonModule, NgOptimizedImage } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { ConfirmationPageRoutingModule } from "./confirmation-routing.module";

import { ConfirmationPage } from "./confirmation.page";
import { HeaderInfoModule } from "../header-info/header-info.module";
import { FooterModule } from "../footer/footer.module";
import { ConfirmationContentComponent } from "./confirmation-content/confirmation-content.component";
import { ProfileContentModule } from "../profile-content/profile-content.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConfirmationPageRoutingModule,
    HeaderInfoModule,
    FooterModule,
    ProfileContentModule,
    NgOptimizedImage,
  ],
  declarations: [ConfirmationPage, ConfirmationContentComponent],
})
export class ConfirmationPageModule {}
