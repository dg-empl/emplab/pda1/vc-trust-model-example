/**
 * @jest-environment jsdom
 */
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { ConfirmationPage } from "./confirmation.page";
import { MockComponent } from "ng2-mock-component";

describe("ConfirmationPage", () => {
  let component: ConfirmationPage;
  let fixture: ComponentFixture<ConfirmationPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        ConfirmationPage,
        MockComponent({ selector: "esspass-web-wallet-header-info" }),
        MockComponent({ selector: "esspass-web-wallet-footer" }),
        MockComponent({
          selector: "esspass-web-wallet-profile-content",
          inputs: ["backButton"],
        }),
        MockComponent({ selector: "esspass-web-wallet-confirmation-content" }),
      ],
      imports: [IonicModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(ConfirmationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
