import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./guard/guard";

const routes: Routes = [
  {
    path: "home",
    loadChildren: () =>
      import("./home/home.module").then((m) => m.HomePageModule),
  },
  {
    path: "logout",
    loadChildren: () =>
      import("./home/home.module").then((m) => m.HomePageModule),
  },
  {
    path: "my-credentials",
    canActivate: [AuthGuard],
    loadChildren: () =>
      import("./my-credentials/my-credentials.module").then(
        (m) => m.MyCredentialsPageModule
      ),
  },
  {
    path: "qr-code-reader",
    canActivate: [AuthGuard],
    loadChildren: () =>
      import("./qr-code-reader/qr-code-reader.module").then(
        (m) => m.QrCodeReaderPageModule
      ),
  },
  {
    path: "share-did",
    canActivate: [AuthGuard],
    loadChildren: () =>
      import("./share-did/share-did.module").then((m) => m.ShareDidPageModule),
  },
  {
    path: "share-pin",
    canActivate: [AuthGuard],
    loadChildren: () =>
      import("./share-pin/share-pin.module").then((m) => m.SharePinPageModule),
  },
  {
    path: "share-did-success",
    canActivate: [AuthGuard],
    loadChildren: () =>
      import("./share-did-success/share-did-success.module").then(
        (m) => m.ShareDidSuccessPageModule
      ),
  },
  {
    path: "pda1-content/:did/:docId",
    canActivate: [AuthGuard],
    loadChildren: () =>
      import("./pda1-content/pda1-content.module").then(
        (m) => m.Pda1ContentPageModule
      ),
  },
  {
    path: "confirmation",
    canActivate: [AuthGuard],
    loadChildren: () =>
      import("./confirmation/confirmation.module").then(
        (m) => m.ConfirmationPageModule
      ),
  },
  {
    path: "share-credentials",
    canActivate: [AuthGuard],
    loadChildren: () =>
      import("./share-credentials/share-credentials.module").then(
        (m) => m.ShareCredentialsPageModule
      ),
  },
  {
    path: "",
    pathMatch: "full",
    redirectTo: "home",
  },
  {
    path: "**",
    loadChildren: () =>
      import("./home/home.module").then((m) => m.HomePageModule),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class EsspassWebWalletRoutingModule {}
