import { APP_INITIALIZER, NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";

import { IonicModule, IonicRouteStrategy } from "@ionic/angular";

import { EsspassWebWalletComponent } from "./esspass-web-wallet.component";
import { EsspassWebWalletRoutingModule } from "./esspass-web-wallet-routing.module";
import { HttpClientModule } from "@angular/common/http";
import { KeycloakAngularModule, KeycloakService } from "keycloak-angular";
import {
  APP_CONFIG,
  APP_DI_CONFIG,
  initializeKeycloak,
} from "@esspass-web-wallet/security-web-lib";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

@NgModule({
  declarations: [EsspassWebWalletComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(),
    EsspassWebWalletRoutingModule,
    HttpClientModule,
    KeycloakAngularModule,
  ],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {
      provide: APP_CONFIG,
      useValue: APP_DI_CONFIG,
    },
    IonicRouteStrategy,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
    },
  ],
  bootstrap: [EsspassWebWalletComponent],
})
export class EsspassWebWalletModule {}
