/**
 * @jest-environment jsdom
 */
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { FooterComponent } from "./footer.component";
import { MockComponent } from "ng2-mock-component";
import { APP_CONFIG } from "@esspass-web-wallet/security-web-lib";

describe("FooterComponent", () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: APP_CONFIG, useValue: APP_CONFIG }],
      declarations: [
        FooterComponent,
        MockComponent({ selector: "ic-icon", inputs: ["icon"] }),
        MockComponent({ selector: "esspass-web-wallet-ec-logo" }),
      ],
      imports: [IonicModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
