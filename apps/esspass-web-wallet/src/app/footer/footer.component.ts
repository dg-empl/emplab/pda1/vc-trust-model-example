import { Component, Inject, OnInit } from "@angular/core";
import linkBoxOutline from "@iconify/icons-mdi/link-box-variant-outline";

import { APP_CONFIG, AppConfigInterface } from "@esspass-web-wallet/security-web-lib";

@Component({
  selector: "esspass-web-wallet-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.scss"],
})
export class FooterComponent implements OnInit {
  constructor(@Inject(APP_CONFIG) private config: AppConfigInterface) {}

  linkIcon = linkBoxOutline;

  version = this.config.version;

  ngOnInit() {}
}
