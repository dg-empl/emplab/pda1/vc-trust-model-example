import { NgModule } from "@angular/core";
import { CommonModule, NgOptimizedImage } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { FooterRoutingModule } from "./footer-routing.module";

import { FooterComponent } from "./footer.component";

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, FooterRoutingModule, NgOptimizedImage],
  declarations: [FooterComponent],
  exports: [FooterComponent],
})
export class FooterModule {}
