import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HeaderInfoComponent } from './header-info.component';

const routes: Routes = [
  {
    path: '',
    component: HeaderInfoComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HeaderInfoPageRoutingModule {}
