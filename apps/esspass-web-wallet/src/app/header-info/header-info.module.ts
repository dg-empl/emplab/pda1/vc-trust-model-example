import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HeaderInfoPageRoutingModule } from './header-info-routing.module';

import { HeaderInfoComponent } from './header-info.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HeaderInfoPageRoutingModule,
  ],
  declarations: [HeaderInfoComponent],
  exports: [HeaderInfoComponent]
})
export class HeaderInfoModule {}
