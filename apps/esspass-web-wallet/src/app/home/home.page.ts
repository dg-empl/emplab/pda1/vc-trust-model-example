import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'esspass-web-wallet-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  public isLoggedIn = false;

  constructor(private keycloak: KeycloakService, private router: Router, private navCtrl: NavController) {}

  public async ngOnInit() {
    this.isLoggedIn = await this.keycloak.isLoggedIn();

    if (this.isLoggedIn) {
      this.navCtrl.setDirection('root');
      await this.router.navigate(['/my-credentials']);
    }
  }

  async login() {
    await this.keycloak.login({
      redirectUri: window.location.origin,
    });
  }
}
