import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CredentialComponent } from './credential.component';

const routes: Routes = [
  {
    path: '',
    component: CredentialComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CredentialPageRoutingModule {}
