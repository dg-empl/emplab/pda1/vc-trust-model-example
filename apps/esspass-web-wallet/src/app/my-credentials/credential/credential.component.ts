import { Component, Input, OnInit } from "@angular/core";

@Component({
  selector: "esspass-web-wallet-credential",
  templateUrl: "./credential.component.html",
  styleUrls: ["./credential.component.scss"],
})
export class CredentialComponent implements OnInit {
  @Input({ required: true }) date: string;
  @Input({ required: true }) time: string;
  @Input({ required: true }) governmentInstitution: string;

  _status: string;
  isRevoked: boolean;

  @Input() set status(status: string) {
    if (status === "Not revoked") {
      this._status = "Valid";
    } else {
      this._status = status;
    }
  }

  ngOnInit(): void {
    this.isRevoked = this._status === "Revoked";
  }
}
