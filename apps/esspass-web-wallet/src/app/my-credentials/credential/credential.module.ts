import { NgModule } from "@angular/core";
import { CommonModule, NgOptimizedImage } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { CredentialPageRoutingModule } from "./credential-routing.module";
import { CredentialComponent } from "./credential.component";

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, CredentialPageRoutingModule, NgOptimizedImage],
  declarations: [CredentialComponent],
  exports: [CredentialComponent],
})
export class CredentialModule {}
