/**
 * @jest-environment jsdom
 */
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule, NavController } from "@ionic/angular";

import { MyCredentialsPage } from "./my-credentials-page.component";
import { MockComponent } from "ng2-mock-component";
import { RouterEvent } from "@angular/router";
import { KeycloakService } from "keycloak-angular";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { APP_CONFIG } from "@esspass-web-wallet/security-web-lib";
import { RouterTestingModule } from "@angular/router/testing";
import { ReplaySubject } from "rxjs";

const eventSubject = new ReplaySubject<RouterEvent>(1);

const routerMock = {
  navigate: jest.fn(function () {
    return {
      then: function (callback: (arg0: { foo: string }) => never) {
        return callback({ foo: "bar" });
      },
    };
  }),
  events: eventSubject.asObservable(),
  url: "test/url",
};

describe("MyCredentialsPage", () => {
  let component: MyCredentialsPage;
  let fixture: ComponentFixture<MyCredentialsPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: NavController, useValue: routerMock },
        {
          provide: KeycloakService,
          useValue: {
            isLoggedIn: () => true,
            loadUserProfile: () => {
              return { email: "sd" };
            },
            getKeycloakInstance: () => {},
          },
        },
        { provide: APP_CONFIG, useValue: APP_CONFIG },
      ],
      declarations: [
        MyCredentialsPage,
        MockComponent({ selector: "esspass-web-wallet-header-info" }),
        MockComponent({ selector: "esspass-web-wallet-footer" }),
        MockComponent({
          selector: "esspass-web-wallet-profile-content",
          inputs: ["backButton"],
        }),
        MockComponent({ selector: "esspass-web-wallet-confirmation-content" }),
      ],
      imports: [IonicModule.forRoot(), HttpClientTestingModule, RouterTestingModule],
    }).compileComponents();

    fixture = TestBed.createComponent(MyCredentialsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
