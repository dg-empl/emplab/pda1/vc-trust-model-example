import { Component, inject, OnDestroy, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { KeycloakService } from "keycloak-angular";
import { Pda1Dto } from "@esspass-web-wallet/domain-lib";
import { catchError, finalize, firstValueFrom, Observable, Subscription, tap } from "rxjs";
import { DidDto, unsubscribe, MyCredentialsService } from "../common";
import { isEmpty } from "lodash";
import { ProfileService, TokenService } from "@esspass-web-wallet/issuance-flow-lib";
import { AlertController, LoadingController } from "@ionic/angular";
import { SpinnerService } from "@esspass-web-wallet/common-web-lib";

@Component({
  selector: "esspass-web-wallet-my-credentials",
  templateUrl: "./my-credentials-page.component.html",
  styleUrls: ["./my-credentials-page.component.scss"],
})
export class MyCredentialsPage implements OnInit, OnDestroy {
  did: string | undefined;
  pda1Observables: Pda1Dto[] = [];
  private isLoggedIn: boolean | undefined;
  private pda1Observable$: Subscription;
  isLoading$: Observable<boolean>;

  private router: Router = inject(Router);
  private keycloak: KeycloakService = inject(KeycloakService);
  private tokenService: TokenService = inject(TokenService);
  private alertController: AlertController = inject(AlertController);
  private myCredentialsService: MyCredentialsService = inject(MyCredentialsService);
  private profileService: ProfileService = inject(ProfileService);
  private spinnerService = inject(SpinnerService);
  private loadingCtrl: LoadingController = inject(LoadingController);

  async ngOnInit() {
    try {
      this.isLoggedIn = await this.keycloak.isLoggedIn();
      if (!this.isLoggedIn) {
        return;
      }

      this.isLoading$ = this.spinnerService.data$;
      await this.createWallet();
      this.renderPda1Observables();
    } catch (e) {
      console.error(e);
    }
  }

  ngOnDestroy(): void {
    unsubscribe(this.pda1Observable$);
  }

  async navigateToCredential(vcId: string) {
    let loading;
    const email = this.profileService.getUserProfile().email;
    if (email) {
      const did: DidDto = await firstValueFrom(this.getHolderWalletDidByEmail(email));

      try {
        loading = await this.loadingCtrl.create({
          message: "Getting PDA1 credential details",
        });
        await loading.present();

        await this.router.navigate([`/pda1-content/`, `${did.did}`, `${vcId}`]);
      } catch (e: any) {
        console.log(e);
        await this.serverError(`${e.error.errors[0].message}`);
      } finally {
        loading && (await loading.dismiss());
      }
    }
  }

  handleRefresh($event: any) {
    setTimeout(() => {
      this.renderPda1Observables();
      $event.target.complete();
    }, 750);
  }

  private async createWallet() {
    let did: DidDto;

    const { email } = await this.keycloak.loadUserProfile();

    if (email) {
      did = await firstValueFrom(this.getHolderWalletDidByEmail(email));

      if (isEmpty(did)) {
        await firstValueFrom(this.myCredentialsService.createHolderWallet(email));
        did = await firstValueFrom(this.getHolderWalletDidByEmail(email));
      }

      this.tokenService.did.next(did);
      this.did = did["did"];
    }
  }

  private getHolderWalletDidByEmail(email: string): Observable<DidDto> {
    return this.myCredentialsService.getHolderWalletDid(email).pipe(
      catchError(error => {
        this.serverError(error.message).then();
        return [];
      })
    );
  }

  private renderPda1Observables() {
    this.spinnerService.load();
    this.pda1Observable$ = this.myCredentialsService
      .getPda1Observables(this.did)
      .pipe(
        catchError(error => {
          this.serverError(JSON.stringify(error)).then();
          return [];
        }),
        finalize(() => this.spinnerService.stop())
      )
      .subscribe((pda1: Pda1Dto[]) => {
        this.pda1Observables = pda1.sort((a, b) => {
          const dateA = new Date(a.date).getTime();
          const dateB = new Date(b.date).getTime();
          return dateB - dateA;
        });
      });
  }

  private async serverError(error: string) {
    const alert = await this.alertController.create({
      header: "Server Error",
      message: error,
      cssClass: "custom-alert",
      backdropDismiss: false,
      buttons: [
        {
          text: "OK",
          role: "confirm",
          cssClass: "alert-button-confirm",
        },
      ],
    });

    await alert.present();
  }
}
