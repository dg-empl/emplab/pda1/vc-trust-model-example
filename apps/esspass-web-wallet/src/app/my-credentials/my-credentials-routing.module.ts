import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { MyCredentialsPage } from "./my-credentials-page.component";

const routes: Routes = [
  {
    path: "",
    component: MyCredentialsPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyCredentialsPageRoutingModule {}
