import { NgModule } from "@angular/core";
import { CommonModule, NgOptimizedImage } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { MyCredentialsPageRoutingModule } from "./my-credentials-routing.module";

import { MyCredentialsPage } from "./my-credentials-page.component";
import { HeaderInfoModule } from "../header-info/header-info.module";
import { FooterModule } from "../footer/footer.module";
import { ProfileContentModule } from "../profile-content/profile-content.module";
import { CredentialModule } from "./credential/credential.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyCredentialsPageRoutingModule,
    HeaderInfoModule,
    FooterModule,
    CredentialModule,
    ProfileContentModule,
    NgOptimizedImage,
  ],
  declarations: [MyCredentialsPage],
})
export class MyCredentialsPageModule {}
