import { CredentialSubject } from "@esspass-web-wallet/domain-lib";

export const credentialSubject = {
  section1: {
    personalIdentificationNumber: "1233123",
    sex: "01",
    surname: "Zawadzki",
    forenames: "Marcin",
    dateBirth: "1977-01-01",
    surnameAtBirth: "terst",
    placeBirth: {
      town: "test",
      region: "test",
      countryCode: "PL",
    },
    nationalities: ["PL"],
    stateOfResidenceAddress: {
      streetNo: "test, 123 ",
      postCode: "123",
      town: "test",
      countryCode: "PL",
    },
    stateOfStayAddress: {
      streetNo: "test, 123 ",
      postCode: "123",
      town: "test",
      countryCode: "PL",
    },
  },
  section2: {
    memberStateWhichLegislationApplies: "ES",
    startingDate: "2023-01-01",
    endingDate: "2023-12-31",
    certificateForDurationActivity: true,
    determinationProvisional: false,
    transitionRulesApplyAsEC8832004: false,
  },
  section3: {
    postedEmployedPerson: true,
    employedTwoOrMoreStates: false,
    postedSelfEmployedPerson: true,
    selfEmployedTwoOrMoreStates: true,
    civilServant: true,
    contractStaff: false,
    mariner: false,
    employedAndSelfEmployed: false,
    civilAndEmployedSelfEmployed: true,
    flightCrewMember: false,
    exception: false,
    exceptionDescription: "",
    workingInStateUnder21: false,
  },
  section4: {
    employee: false,
    selfEmployedActivity: true,
    employerSelfEmployedActivityCodes: ["2131324234"],
    nameBusinessName: "test",
    registeredAddress: {
      streetNo: "test, 123 123",
      postCode: "123",
      town: "wwetwet",
      countryCode: "DK",
    },
  },
  section5: {
    workPlaceNames: [
      {
        seqno: 1,
        companyNameVesselName: "TELEFONICA",
      },
    ],
    workPlaceAddresses: [
      {
        seqno: 1,
        address: {
          streetNo: "whatever street, 123",
          town: "MADRID",
          postCode: "1233",
          countryCode: "ES",
        },
      },
    ],
    noFixedAddress: false,
  },
  section6: {
    name: "Office National de Sécurité Sociale / Rijksdienst voor Sociale Zekerheid",
    address: {
      streetNo: "Main Street 1",
      postCode: "1000",
      town: "Brussels",
      countryCode: "BE",
    },
    institutionID: "NSSI-BE-01",
    officePhoneNo: "0800 12345",
    officeFaxNo: "0800 98765",
    email: "esspass.noreply@gmail.com",
    date: "2022-12-16",
    signature: "Official signature",
  },
  id: "did:ebsi:zgW3EJT5sLoypiTZvSD7y7nc9wc1NcaGJznRrQ7sTE6ks",
} as unknown as CredentialSubject;
