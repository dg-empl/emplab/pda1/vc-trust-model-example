import { Component, ElementRef, Input, OnInit, ViewChild } from "@angular/core";
import { ToggleSection, ToggleSectionService } from "../toggle-section.service";
import { tap } from "rxjs";

@Component({
  selector: "esspass-web-wallet-pda1-accordion-header",
  templateUrl: "./pda1-accordion-header.component.html",
  styleUrls: ["./pda1-accordion-header.component.scss"],
})
export class Pda1AccordionHeaderComponent implements OnInit {
  @ViewChild("expand") expanded: ElementRef;
  @ViewChild("collapsed") collapsed: ElementRef;
  @ViewChild("header") header: ElementRef;

  private readonly dottedStyle = "1px dashed #0d0d0d";

  _toggle: boolean;

  @Input() number: string;
  @Input() text: string;
  @Input() section: ToggleSection;

  constructor(private toggleSectionService: ToggleSectionService) {}

  ngOnInit(): void {
    this.toggleSectionService.toggle$
      .pipe(
        tap((sections) => {
          if (this._toggle && !sections[this.section]) {
            this._toggle = false;
            this.clearCss();
          }
        })
      )
      .subscribe();
  }

  setExpand() {
    this._toggle = !this._toggle;
    this._toggle ? this.setCss() : this.clearCss();
  }

  private setCss() {
    this.expanded.nativeElement.style.opacity = 0;
    this.collapsed.nativeElement.style.opacity = 100;

    //header
    this.header.nativeElement.style.borderLeft = this.dottedStyle;
    this.header.nativeElement.style.borderTop = this.dottedStyle;
    this.header.nativeElement.style.borderRight = this.dottedStyle;
    this.header.nativeElement.style.background = "#F5F5F5";
  }

  private clearCss() {
    this.expanded.nativeElement.style.opacity = 100;
    this.collapsed.nativeElement.style.opacity = 0;

    //header
    this.header.nativeElement.style.borderLeft = null;
    this.header.nativeElement.style.borderTop = null;
    this.header.nativeElement.style.borderRight = null;
    this.header.nativeElement.style.background = "#FFFFFF";
  }
}
