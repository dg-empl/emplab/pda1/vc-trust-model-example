/**
 * @jest-environment jsdom
 */
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { Pda1AccordionComponent } from "./pda1-accordion.component";
import { MockComponent } from "ng2-mock-component";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { APP_CONFIG } from "@esspass-web-wallet/security-web-lib";
import { credentialSubject } from "./credential.spec.mock";

describe("Pda1AccordionPage", () => {
  let component: Pda1AccordionComponent;
  let fixture: ComponentFixture<Pda1AccordionComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: APP_CONFIG, useValue: APP_CONFIG }],
      declarations: [
        Pda1AccordionComponent,
        MockComponent({
          selector: "esspass-web-wallet-pda1-status",
          inputs: ["status", "issueDate", "issuer", "pdaId", "did"],
        }),
        MockComponent({
          selector: "esspass-web-wallet-pda1-accordion-header",
          inputs: ["text", "section", "number"],
        }),
        MockComponent({ selector: "esspass-web-wallet-header-info" }),
        MockComponent({ selector: "esspass-web-wallet-footer" }),
        MockComponent({
          selector: "esspass-web-wallet-pda1-personal-details",
          inputs: ["credSubject"],
        }),
        MockComponent({
          selector: "esspass-web-wallet-pda1-status-confirmation",
          inputs: ["credSubject"],
        }),
        MockComponent({
          selector: "esspass-web-wallet-pda1-details-of-employer",
          inputs: ["credSubject"],
        }),
        MockComponent({
          selector: "esspass-web-wallet-pda1-details-of-activity-pursued",
          inputs: ["credSubject"],
        }),
        MockComponent({
          selector: "esspass-web-wallet-pda1-institution-completing-form",
          inputs: ["credSubject"],
        }),
        MockComponent({
          selector: "esspass-web-wallet-pda1-member-state-legislation",
          inputs: ["credSubject"],
        }),
        MockComponent({ selector: "esspass-web-wallet-confirmation-content" }),
      ],
      imports: [IonicModule.forRoot(), HttpClientTestingModule],
    }).compileComponents();

    fixture = TestBed.createComponent(Pda1AccordionComponent);
    component = fixture.componentInstance;
    component.credentialSubject = credentialSubject;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
