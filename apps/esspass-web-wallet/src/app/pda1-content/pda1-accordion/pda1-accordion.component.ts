import { Component, ElementRef, Input, ViewChild } from "@angular/core";
import { CredentialSubject } from "@esspass-web-wallet/domain-lib";
import {
  ToggleSection,
  ToggleSectionService,
  toggleSectionsMapDefaults,
} from "./toggle-section.service";

@Component({
  selector: "esspass-web-wallet-pda1-accordion",
  templateUrl: "./pda1-accordion.component.html",
  styleUrls: ["./pda1-accordion.component.scss"],
})
export class Pda1AccordionComponent {
  @Input() credentialSubject: CredentialSubject;
  section1: ToggleSection = ToggleSection.section1;
  section2: ToggleSection = ToggleSection.section2;
  section3: ToggleSection = ToggleSection.section3;
  section4: ToggleSection = ToggleSection.section4;
  section5: ToggleSection = ToggleSection.section5;
  section6: ToggleSection = ToggleSection.section6;

  constructor(private toggleSelectionService: ToggleSectionService) {}

  @ViewChild("listenerOut", { static: true }) listenerOut: ElementRef;

  public accordionGroupChange(ev: any) {
    const selectedValue = ev.detail.value;

    if ( selectedValue !== undefined) {
      const toggleSectionElement: ToggleSection =
        ToggleSection[selectedValue as ToggleSection];

      this.toggleSelectionService.toggle.next({
        ...toggleSectionsMapDefaults,
        [toggleSectionElement]: true,
      });
    }
  }
}
