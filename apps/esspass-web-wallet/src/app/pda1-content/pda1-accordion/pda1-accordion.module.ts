import { NgModule } from "@angular/core";
import { CommonModule, NgOptimizedImage } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { Pda1AccordionComponent } from "./pda1-accordion.component";
import { Pda1PersonalDetailsComponent } from "./pda1-personal-details/pda1-personal-details.component";
import { Pda1MemberStateLegislationComponent } from "./pda1-member-state-legislation/pda1-member-state-legislation.component";
import { Pda1StatusConfirmationComponent } from "./pda1-status-confirmation/pda1-status-confirmation.component";
import { Pda1DetailsOfEmployerComponent } from "./pda1-details-of-employer/pda1-details-of-employer.component";
import { Pda1DetailsOfActivityPursuedComponent } from "./pda1-details-of-activity-pursued/pda1-details-of-activity-pursued.component";
import { Pda1InstitutionCompletingFormComponent } from "./pda1-institution-completing-form/pda1-institution-completing-form.component";
import { Pda1AccordionHeaderComponent } from "./pda1-accordion-header/pda1-accordion-header.component";
import { Pda1StatusComponent } from "./pda1-status/pda1-status.component";
import { Pda1ManagementComponent } from "./pda1-status/credential-management/pda1-management.component";
import { Pda1DeleteComponent } from "./pda1-status/credential-management/pda1-delete/pda1-delete.component";

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, NgOptimizedImage],
  declarations: [
    Pda1AccordionComponent,
    Pda1PersonalDetailsComponent,
    Pda1MemberStateLegislationComponent,
    Pda1StatusConfirmationComponent,
    Pda1DetailsOfEmployerComponent,
    Pda1DetailsOfActivityPursuedComponent,
    Pda1InstitutionCompletingFormComponent,
    Pda1AccordionHeaderComponent,
    Pda1StatusComponent,
    Pda1ManagementComponent,
    Pda1DeleteComponent,
  ],
  exports: [Pda1AccordionComponent],
})
export class Pda1AccordionModule {}
