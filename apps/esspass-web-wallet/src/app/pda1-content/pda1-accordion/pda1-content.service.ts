import { inject, Inject, Injectable } from "@angular/core";
import { HttpWrapperService } from "@esspass-web-wallet/web-utils-lib";
import { map, Observable } from "rxjs";
import {
  VerifiableCredentialStatus,
  VerifiableCredentialStatusPdaId,
} from "@esspass-web-wallet/domain-lib";
import { APP_CONFIG, AppConfigInterface } from "@esspass-web-wallet/security-web-lib";

@Injectable({
  providedIn: "root",
})
export class Pda1ContentService {
  private readonly PDA_ENDPOINT = "/pda1-content";

  private httpService = inject(HttpWrapperService);
  private config: AppConfigInterface = inject(APP_CONFIG);

  public getVerifiableCredential(
    did: string,
    docId: string
  ): Observable<VerifiableCredentialStatusPdaId> {
    return this.httpService
      .findRawFromPostMethd(`${this.config.serverUrl}${this.PDA_ENDPOINT}`, {
        did: did,
        docId: docId,
      })
      .pipe(
        map((res: VerifiableCredentialStatus) => {
          return { ...res, pdaId: docId };
        })
      );
  }

  public deleteCredential(did: string, docId: string): Observable<any> {
    return this.httpService.delete(`${this.config.serverUrl}${this.PDA_ENDPOINT}/${did}/${docId}`);
  }
}
