/**
 * @jest-environment jsdom
 */
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { Pda1DetailsOfActivityPursuedComponent } from "./pda1-details-of-activity-pursued.component";
import { credentialSubject } from "../credential.spec.mock";

describe("Pda1DetailsOfActivityPursuedPage", () => {
  let component: Pda1DetailsOfActivityPursuedComponent;
  let fixture: ComponentFixture<Pda1DetailsOfActivityPursuedComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [Pda1DetailsOfActivityPursuedComponent],
      imports: [IonicModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(Pda1DetailsOfActivityPursuedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should set input ", () => {
    //given when
    component.credSubject = credentialSubject;

    //then
    expect(component.workPlaceName).toEqual("TELEFONICA");
  });
});
