import { Component, Input } from "@angular/core";
import {
  CredentialSubject,
  Section5,
  WorkPlaceAddresses,
  WorkPlaceName,
} from "@esspass-web-wallet/domain-lib";

@Component({
  selector: "esspass-web-wallet-pda1-details-of-activity-pursued",
  templateUrl: "./pda1-details-of-activity-pursued.component.html",
  styleUrls: ["./pda1-details-of-activity-pursued.component.scss"],
})
export class Pda1DetailsOfActivityPursuedComponent {
  noFixedAddress: boolean;
  workPlaceName: string;

  streetNo: string;
  postCode: string;
  town: string;
  countryCode: string;

  @Input() set credSubject(value: CredentialSubject) {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const section5 = value.section5 as unknown as Section5;
    const { noFixedAddress, workPlaceAddresses, workPlaceNames } = section5;
    this.noFixedAddress = noFixedAddress;

    if (workPlaceNames && workPlaceAddresses) {
      this.workPlaceName = (workPlaceNames[0] as WorkPlaceName).companyNameVesselName ?? "-";
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      this.streetNo = (workPlaceAddresses[0] as WorkPlaceAddresses).address.streetNo ?? "-";
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      this.postCode = (workPlaceAddresses[0] as WorkPlaceAddresses).address.postCode ?? "-";
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      this.town = (workPlaceAddresses[0] as WorkPlaceAddresses).address.town ?? "-";
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      this.countryCode = (workPlaceAddresses[0] as WorkPlaceAddresses).address.countryCode ?? "-";
    }
  }
}
