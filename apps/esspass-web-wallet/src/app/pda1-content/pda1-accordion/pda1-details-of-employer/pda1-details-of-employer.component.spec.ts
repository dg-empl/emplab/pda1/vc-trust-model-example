/**
 * @jest-environment jsdom
 */
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { Pda1DetailsOfEmployerComponent } from "./pda1-details-of-employer.component";
import { credentialSubject } from "../credential.spec.mock";

describe("Pda1DetailsOfEmployerPage", () => {
  let component: Pda1DetailsOfEmployerComponent;
  let fixture: ComponentFixture<Pda1DetailsOfEmployerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [Pda1DetailsOfEmployerComponent],
      imports: [IonicModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(Pda1DetailsOfEmployerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should set input ", () => {
    //given when
    component.credSubject = credentialSubject;

    //then
    expect(component.employee).toBeFalsy();
    expect(component.employerSelfEmployedActivityCodes).toEqual("2131324234");
  });

});
