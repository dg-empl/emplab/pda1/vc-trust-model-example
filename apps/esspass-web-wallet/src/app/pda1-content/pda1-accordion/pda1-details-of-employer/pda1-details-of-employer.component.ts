import { Component, Input, OnInit } from "@angular/core";
import {
  CredentialSubject,
  RegisteredAddress,
  Section4,
} from "@esspass-web-wallet/domain-lib";

@Component({
  selector: "esspass-web-wallet-pda1-details-of-employer",
  templateUrl: "./pda1-details-of-employer.component.html",
  styleUrls: ["./pda1-details-of-employer.component.scss"],
})
export class Pda1DetailsOfEmployerComponent implements OnInit {
  employee: boolean;
  selfEmployedActivity: boolean;
  nameBusinessName: string;
  streetNo: string;
  town: string;
  countryCode: string;
  postCode: string;
  employerSelfEmployedActivityCodes: string;

  @Input() set credSubject(value: CredentialSubject) {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const section4 = value.section4! as Section4;
    const {
      employee,
      selfEmployedActivity,
      nameBusinessName,
      employerSelfEmployedActivityCodes,
    } = section4;
    this.employee = employee;
    this.selfEmployedActivity = selfEmployedActivity;
    this.nameBusinessName = nameBusinessName;
    this.employerSelfEmployedActivityCodes =
      employerSelfEmployedActivityCodes[0];
    this.registeredAddress(section4);
  }

  private registeredAddress(section4: Section4) {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const address = section4.registeredAddress! as RegisteredAddress;

    const { streetNo, town, countryCode, postCode } = address;

    this.streetNo = streetNo;
    this.town = town;
    this.countryCode = countryCode;
    this.postCode = postCode;
  }

  constructor() {}

  ngOnInit() {}
}
