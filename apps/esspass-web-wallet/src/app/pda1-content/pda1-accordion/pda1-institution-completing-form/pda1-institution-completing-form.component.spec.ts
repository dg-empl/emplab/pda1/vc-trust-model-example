/**
 * @jest-environment jsdom
 */
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { Pda1InstitutionCompletingFormComponent } from "./pda1-institution-completing-form.component";
import { credentialSubject } from "../credential.spec.mock";
import { CredentialSubject, Section6 } from "@esspass-web-wallet/domain-lib";

describe("Pda1InstitutionCompletingFormPage", () => {
  let component: Pda1InstitutionCompletingFormComponent;
  let fixture: ComponentFixture<Pda1InstitutionCompletingFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [Pda1InstitutionCompletingFormComponent],
      imports: [IonicModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(Pda1InstitutionCompletingFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should set input ", () => {
    //given when
    component.credSubject = credentialSubject;

    //then
    expect(component.streetNo).toEqual("Main Street 1");
  });

  it("should not set address when section6 is undef", () => {
    //given
    const credSubject: CredentialSubject = {
      ...credentialSubject,
    };

    (credSubject.section6 as unknown as Section6).address = undefined;

    //when
    component.credSubject = credSubject;

    //then
    expect(component.streetNo).toBeUndefined();
  });
});
