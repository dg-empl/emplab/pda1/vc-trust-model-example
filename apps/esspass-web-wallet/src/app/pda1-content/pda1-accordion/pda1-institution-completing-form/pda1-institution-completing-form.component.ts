import { Component, Input, OnInit } from "@angular/core";
import {
  CredentialSubject,
  RegisteredAddress,
  Section6,
} from "@esspass-web-wallet/domain-lib";

@Component({
  selector: "esspass-web-wallet-pda1-institution-completing-form",
  templateUrl: "./pda1-institution-completing-form.component.html",
  styleUrls: ["./pda1-institution-completing-form.component.scss"],
})
export class Pda1InstitutionCompletingFormComponent implements OnInit {
  streetNo: string;
  town: string;
  countryCode: string;
  postCode: string;
  name: string;
  institutionID: string;
  officeFaxNo: string;
  officePhoneNo: string;
  email: string;
  date: string;
  signature: string;

  ngOnInit() {}

  @Input() set credSubject(value: CredentialSubject) {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const section6 = value.section6! as Section6;

    const {
      name,
      institutionID,
      officeFaxNo,
      officePhoneNo,
      email,
      date,
      signature,
    } = section6;

    this.name = name;
    this.institutionID = institutionID;
    this.officeFaxNo = officeFaxNo;
    this.officePhoneNo = officePhoneNo;
    this.email = email;
    this.date = date;
    this.signature = signature;

    this.registeredAddress(section6);
  }

  private registeredAddress(section6: Section6) {
    const add = this.getAdd(section6);

    if (add) {
      const { streetNo, town, countryCode, postCode } = add;

      this.streetNo = streetNo;
      this.town = town;
      this.countryCode = countryCode;
      this.postCode = postCode;
    } else return;
  }

  private getAdd(section6: Section6) {
    return section6.address !== undefined
      ? (section6.address as unknown as RegisteredAddress)
      : null;
  }
}
