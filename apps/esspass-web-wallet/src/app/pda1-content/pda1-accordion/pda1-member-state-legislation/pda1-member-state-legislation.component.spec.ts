/**
 * @jest-environment jsdom
 */
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Pda1MemberStateLegislationComponent } from './pda1-member-state-legislation.component';
import { credentialSubject } from "../credential.spec.mock";

describe('Pda1MemberStateLegislationPage', () => {
  let component: Pda1MemberStateLegislationComponent;
  let fixture: ComponentFixture<Pda1MemberStateLegislationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [Pda1MemberStateLegislationComponent],
      imports: [IonicModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(Pda1MemberStateLegislationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("should set input ", () => {
    //given when
    component.credSubject = credentialSubject;

    //then
    expect(component.memberStateWhichLegislationApplies).toEqual("ES");
  });
});
