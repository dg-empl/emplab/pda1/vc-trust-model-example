import { ChangeDetectionStrategy, Component, Input, OnInit } from "@angular/core";
import { CredentialSubject, Section2 } from "@esspass-web-wallet/domain-lib";

@Component({
  selector: "esspass-web-wallet-pda1-member-state-legislation",
  templateUrl: "./pda1-member-state-legislation.component.html",
  styleUrls: ["./pda1-member-state-legislation.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class Pda1MemberStateLegislationComponent implements OnInit {
  @Input() set credSubject(value: CredentialSubject) {
    const section2 = value.section2! as Section2;
    const {
      memberStateWhichLegislationApplies,
      startingDate,
      endingDate,
      certificateForDurationActivity,
    } = section2;
    this.memberStateWhichLegislationApplies = memberStateWhichLegislationApplies;
    this.startingDate = startingDate;
    this.endingDate = endingDate;
    this.certificateForDurationActivity = certificateForDurationActivity;
  }

  memberStateWhichLegislationApplies!: string;
  startingDate!: string;
  endingDate!: string;
  certificateForDurationActivity!: boolean;

  constructor() {}

  ngOnInit() {}
}
