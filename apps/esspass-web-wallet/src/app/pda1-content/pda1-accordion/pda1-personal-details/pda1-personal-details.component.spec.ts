/**
 * @jest-environment jsdom
 */
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { Pda1PersonalDetailsComponent } from "./pda1-personal-details.component";
import {
  Section1,
  StateOfResidenceAddress,
} from "@esspass-web-wallet/domain-lib";
import { credentialSubject } from "../credential.spec.mock";

describe("Pda1PersonalDetailsPage", () => {
  let component: Pda1PersonalDetailsComponent;
  let fixture: ComponentFixture<Pda1PersonalDetailsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [Pda1PersonalDetailsComponent],
      imports: [IonicModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(Pda1PersonalDetailsComponent);
    component = fixture.componentInstance;
    component.credSubject = credentialSubject;
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
