import { Component, Input } from "@angular/core";
import {
  CredentialSubject,
  PlaceBirth,
  Section1,
  StateOfResidenceAddress,
} from "@esspass-web-wallet/domain-lib";

@Component({
  selector: "esspass-web-wallet-pda1-personal-details",
  templateUrl: "./pda1-personal-details.component.html",
  styleUrls: ["./pda1-personal-details.component.scss"],
})
export class Pda1PersonalDetailsComponent {
  pid!: string;
  surname!: string;
  forenames!: string;
  dateBirth!: string;
  nationalities!: string;
  surnameAtBirth!: string;

  //place birth
  birthTown!: string;
  birthCountryCode!: string;
  birthRegion!: string;

  //residence address
  streetNo!: string;
  town!: string;
  countryCode!: string;
  postCode!: string;

  //stay address
  stayStreetNo!: string;
  stayTown!: string;
  stayCountryCode!: string;
  stayPostCode!: string;

  @Input() set credSubject(value: CredentialSubject) {
    const section1 = value.section1 as unknown as Section1;
    const {
      personalIdentificationNumber,
      surname,
      surnameAtBirth,
      forenames,
      dateBirth,
      nationalities,
    } = section1;
    this.pid = personalIdentificationNumber;
    this.surname = surname;
    this.surnameAtBirth = surnameAtBirth;
    this.forenames = forenames;
    this.dateBirth = dateBirth;
    this.nationalities = nationalities.join(",");

    this.residenceAddress(section1);
    this.stayAddress(section1);
    this.placeBirth(section1);
  }

  private residenceAddress(section1: Section1) {
    const address =
      section1.stateOfResidenceAddress as unknown as StateOfResidenceAddress;

    const { streetNo, town, countryCode, postCode } = address;

    this.streetNo = streetNo;
    this.town = town;
    this.countryCode = countryCode;
    this.postCode = postCode;
  }

  private stayAddress(section1: Section1) {
    const stayAddress =
      section1.stateOfStayAddress as unknown as StateOfResidenceAddress;

    const { streetNo, town, countryCode, postCode } = stayAddress;

    this.stayStreetNo = streetNo;
    this.stayTown = town;
    this.stayCountryCode = countryCode;
    this.stayPostCode = postCode;
  }

  private placeBirth(section1: Section1) {
    const placeBirth = section1.placeBirth as unknown as PlaceBirth;

    const { town, countryCode, region } = placeBirth;

    this.birthTown = town;
    this.birthCountryCode = countryCode;
    this.birthRegion = region;
  }
}
