/**
 * @jest-environment jsdom
 */
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { Pda1StatusConfirmationComponent } from "./pda1-status-confirmation.component";
import { credentialSubject } from "../credential.spec.mock";

describe("Pda1StatusConfirmationPage", () => {
  let component: Pda1StatusConfirmationComponent;
  let fixture: ComponentFixture<Pda1StatusConfirmationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [Pda1StatusConfirmationComponent],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Pda1StatusConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should set input ", () => {
    //given when
    component.credSubject = credentialSubject;

    //then
    expect(component.postedEmployedPerson).toBeTruthy();
  });

});
