import { Component, Input } from "@angular/core";
import { CredentialSubject, Section3 } from "@esspass-web-wallet/domain-lib";

@Component({
  selector: "esspass-web-wallet-pda1-status-confirmation",
  templateUrl: "./pda1-status-confirmation.component.html",
  styleUrls: ["./pda1-status-confirmation.component.scss"],
})
export class Pda1StatusConfirmationComponent {
  @Input() set credSubject(value: CredentialSubject) {
    const section3 = value.section3 as unknown as Section3;

    const {
      postedEmployedPerson,
      employedTwoOrMoreStates,
      postedSelfEmployedPerson,
      selfEmployedTwoOrMoreStates,
      civilServant,
      contractStaff,
      mariner,
      employedAndSelfEmployed,
      civilAndEmployedSelfEmployed,
      flightCrewMember,
      exception,
      exceptionDescription,
      workingInStateUnder21,
    } = section3;

    this.postedEmployedPerson = postedEmployedPerson;
    this.employedTwoOrMoreStates = employedTwoOrMoreStates;
    this.postedSelfEmployedPerson = postedSelfEmployedPerson;
    this.selfEmployedTwoOrMoreStates = selfEmployedTwoOrMoreStates;
    this.civilServant = civilServant;
    this.contractStaff = contractStaff;
    this.mariner = mariner;
    this.employedAndSelfEmployed = employedAndSelfEmployed;
    this.civilAndEmployedSelfEmployed = civilAndEmployedSelfEmployed;
    this.flightCrewMember = flightCrewMember;
    this.exception = exception;
    this.exceptionDescription = exceptionDescription;
    this.workingInStateUnder21 = workingInStateUnder21;
  }

  postedEmployedPerson!: boolean;
  employedTwoOrMoreStates!: boolean;
  postedSelfEmployedPerson!: boolean;
  selfEmployedTwoOrMoreStates!: boolean;
  civilServant!: boolean;
  contractStaff!: boolean;
  mariner!: boolean;
  employedAndSelfEmployed!: boolean;
  civilAndEmployedSelfEmployed!: boolean;
  flightCrewMember!: boolean;
  exception!: boolean;
  exceptionDescription!: string;
  workingInStateUnder21!: boolean;
}
