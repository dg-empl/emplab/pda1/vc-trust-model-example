import { ComponentFixture, TestBed } from "@angular/core/testing";
import { Pda1DeleteComponent } from "./pda1-delete.component";
import { PopoverController } from "@ionic/angular";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { Pda1ContentService } from "../../../pda1-content.service";

describe("Pda1DeleteComponent", () => {
  let component: Pda1DeleteComponent;
  let fixture: ComponentFixture<Pda1DeleteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [Pda1DeleteComponent],
      providers: [
        { provide: PopoverController, useValue: {} },
        { provide: Pda1ContentService, useValue: {} },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(Pda1DeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
