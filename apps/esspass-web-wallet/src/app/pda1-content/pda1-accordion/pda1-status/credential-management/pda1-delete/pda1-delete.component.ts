import { Component, inject, Input } from "@angular/core";
import { PopoverController } from "@ionic/angular";
import { Pda1ContentService } from "../../../pda1-content.service";
import { catchError, firstValueFrom, tap } from "rxjs";
import { Router } from "@angular/router";

@Component({
  selector: "esspass-web-wallet-pda1-delete",
  template: ` <div class="container">
    <div>
      <img ngSrc="../../../../../../assets/icon/thrash.svg" alt="Trash" width="16" height="18" />
    </div>
    <div>Are you sure you want to delete this credential?</div>
    <div>
      <img
        ngSrc="../../../../../../assets/vc-icon.svg"
        alt="Document secured"
        width="20"
        height="20" />
      <span>PDA1</span>
    </div>
    <div>
      <span style="color: #6a6a6a">Issue date </span>
    </div>
    <span class="issue-date">{{ issueDate | date : "d MMMM y, HH:mm" }}</span>
    <div>
      <span style="color: #6a6a6a">Issuer</span>
    </div>
    <span class="issuer">{{ issuer }}</span>
    <span class="actions">
      <button class="cancel" (click)="onCancel()">Cancel</button>
      <button class="delete" type="button" (click)="onDelete()">Delete</button>
    </span>
  </div>`,
  styleUrls: ["./pda1-delete.component.scss"],
})
export class Pda1DeleteComponent {
  /**
   * @description id of the request form passed from parent component
   */
  @Input({ required: true }) did: string;
  @Input({ required: true }) pdaId: string;
  @Input({ required: true }) issueDate: string;
  @Input({ required: true }) issuer: string;

  private popover = inject(PopoverController);
  private pda1ContentService = inject(Pda1ContentService);
  private router = inject(Router);

  async onCancel() {
    await this.popover.dismiss();
  }

  async onDelete() {
    const result = await firstValueFrom(
      this.pda1ContentService.deleteCredential(this.did, this.pdaId).pipe(
        catchError(error => {
          return [];
        })
      )
    );

    if (result.status === "ok") {
      await this.popover.dismiss();

      await this.router.navigate(["/my-credentials"]);
      /**
       * Refresh the page to update the list of credentials
       */
      window.location.reload();
    }
  }
}
