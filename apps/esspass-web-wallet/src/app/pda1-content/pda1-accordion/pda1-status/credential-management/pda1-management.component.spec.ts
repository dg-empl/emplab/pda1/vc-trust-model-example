import { ComponentFixture, TestBed } from "@angular/core/testing";
import { Pda1ManagementComponent } from "./pda1-management.component";
import { PopoverController } from "@ionic/angular";

describe("Pda1ManagementComponent", () => {
  let component: Pda1ManagementComponent;
  let fixture: ComponentFixture<Pda1ManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Pda1ManagementComponent],
      providers: [{ provide: PopoverController, useValue: {} }],
    }).compileComponents();

    fixture = TestBed.createComponent(Pda1ManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
