import { Component, inject, Input } from "@angular/core";
import { PopoverController } from "@ionic/angular";
import { Pda1DeleteComponent } from "./pda1-delete/pda1-delete.component";

@Component({
  selector: "esspass-web-wallet-pda1-management",
  template: ` <p (click)="onClick()">
    <img
      class="shape"
      ngSrc="../../../../../assets/icon/actions-round.svg"
      width="20"
      height="20"
      alt="actions" />
    <img
      class="dots"
      ngSrc="../../../../../assets/icon/actions-dots.svg"
      width="13"
      height="3"
      alt="actions" />
  </p>`,
  styleUrls: ["./pda1-management.component.scss"],
})
export class Pda1ManagementComponent {
  private popover = inject(PopoverController);

  @Input({ required: true }) did: string;
  @Input({ required: true }) pdaId: string;
  @Input({ required: true }) issuer: string;
  @Input({ required: true }) issueDate: string;

  async onClick() {
    const popoverElement = await this.popover.create({
      component: Pda1DeleteComponent,
      backdropDismiss: false,
      componentProps: {
        did: this.did,
        pdaId: this.pdaId,
        issuer: this.issuer,
        issueDate: this.issueDate,
      },
    });

    await popoverElement.present();

    const { role } = await popoverElement.onDidDismiss();
    // this.roleMsg = `Popover dismissed with role: ${role}`;
  }
}
