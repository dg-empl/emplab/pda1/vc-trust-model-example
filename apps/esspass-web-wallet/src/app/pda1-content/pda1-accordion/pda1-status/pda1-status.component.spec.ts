/**
 * @jest-environment jsdom
 */
import { ComponentFixture, TestBed } from "@angular/core/testing";

import { Pda1StatusComponent } from "./pda1-status.component";
import { MockComponent } from "ng2-mock-component";

describe("Pda1StatusComponent", () => {
  let component: Pda1StatusComponent;
  let fixture: ComponentFixture<Pda1StatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        Pda1StatusComponent,
        MockComponent({
          selector: "esspass-web-wallet-pda1-management",
          inputs: ["pdaId", "issuer", "issueDate", "did"],
        }),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(Pda1StatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
