import { Component, Input, OnInit } from "@angular/core";

@Component({
  selector: "esspass-web-wallet-pda1-status",
  templateUrl: "./pda1-status.component.html",
  styleUrls: ["./pda1-status.component.scss"],
})
export class Pda1StatusComponent implements OnInit {
  _status: string;
  isRevoked: boolean;

  @Input() set status(sta: string) {
    if (sta === "Not revoked") {
      this._status = "Valid";
    } else {
      this._status = sta;
    }
  }

  @Input({ required: true }) did: string;
  @Input({ required: true }) issueDate: string;
  @Input({ required: true }) issuer: string;
  @Input({ required: true }) pdaId: string;

  ngOnInit(): void {
    this.isRevoked = this._status === "Revoked";
  }
}
