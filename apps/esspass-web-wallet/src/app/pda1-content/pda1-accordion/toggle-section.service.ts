import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

export enum ToggleSection {
  section1="section1",
  section2="section2",
  section3="section3",
  section4="section4",
  section5="section5",
  section6="section6"
}

export type ToggleSectionsMap = {
  [keys in ToggleSection]: boolean;
};

export const toggleSectionsMapDefaults: ToggleSectionsMap = {
  "section1": false,
  "section2": false,
  "section3": false,
  "section4": false,
  "section5": false,
  "section6": false,
}

@Injectable({ providedIn: "root" })
export class ToggleSectionService {

  toggle: BehaviorSubject<ToggleSectionsMap> = new BehaviorSubject<ToggleSectionsMap>(toggleSectionsMapDefaults);
  toggle$ = this.toggle.asObservable();
}
