import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve } from "@angular/router";
import { firstValueFrom, Observable } from "rxjs";
import { Pda1ContentService } from "./pda1-accordion/pda1-content.service";
import {
  VerifiableCredentialStatus,
  VerifiableCredentialStatusPdaId,
} from "@esspass-web-wallet/domain-lib";

@Injectable({
  providedIn: "root",
})
export class Pda1ContentPageResolver implements Resolve<VerifiableCredentialStatus> {
  constructor(private pda1ContentService: Pda1ContentService) {}

  resolve(
    route: ActivatedRouteSnapshot
  ):
    | Observable<VerifiableCredentialStatusPdaId>
    | Promise<VerifiableCredentialStatusPdaId>
    | VerifiableCredentialStatusPdaId {
    const did = route.paramMap.get("did")!;
    const docId = route.paramMap.get("docId")!;

    return firstValueFrom(this.pda1ContentService.getVerifiableCredential(did, docId));
  }
}
