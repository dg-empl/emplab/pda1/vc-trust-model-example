import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { Pda1ContentPage } from "./pda1-content.page";
import { Pda1ContentPageResolver } from "./pda1-content-page-resolver.service";

const routes: Routes = [
  {
    path: "",
    component: Pda1ContentPage,
    resolve: {
      data: Pda1ContentPageResolver,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Pda1ContentPageRoutingModule {}
