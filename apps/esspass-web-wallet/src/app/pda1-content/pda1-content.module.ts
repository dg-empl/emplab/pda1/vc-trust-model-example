import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { Pda1ContentPageRoutingModule } from "./pda1-content-routing.module";

import { Pda1ContentPage } from "./pda1-content.page";
import { HeaderInfoModule } from "../header-info/header-info.module";
import { FooterModule } from "../footer/footer.module";
import { Pda1AccordionModule } from "./pda1-accordion/pda1-accordion.module";
import { ProfileContentModule } from "../profile-content/profile-content.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Pda1ContentPageRoutingModule,
    HeaderInfoModule,
    FooterModule,
    Pda1AccordionModule,
    ProfileContentModule,
  ],
  declarations: [Pda1ContentPage],
})
export class Pda1ContentPageModule {}
