/**
 * @jest-environment jsdom
 */
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { Pda1ContentPage } from "./pda1-content.page";
import { MockComponent } from "ng2-mock-component";
import { RouterTestingModule } from "@angular/router/testing";
import { ActivatedRoute, Data } from "@angular/router";

describe("Pda1ContentPage", () => {
  let component: Pda1ContentPage;
  let fixture: ComponentFixture<Pda1ContentPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: {
              subscribe: (fn: (value: Data) => void) =>
                fn({
                  data: {},
                }),
            },
          },
        },
      ],
      declarations: [
        Pda1ContentPage,
        MockComponent({ selector: "esspass-web-wallet-header-info" }),
        MockComponent({ selector: "esspass-web-wallet-footer" }),
        MockComponent({
          selector: "esspass-web-wallet-pda1-accordion",
          inputs: ["credentialSubject"],
        }),
        MockComponent({
          selector: "esspass-web-wallet-profile-content",
          inputs: ["backButton"],
        }),
        MockComponent({ selector: "esspass-web-wallet-confirmation-content" }),
      ],
      imports: [IonicModule.forRoot(), RouterTestingModule],
    }).compileComponents();

    fixture = TestBed.createComponent(Pda1ContentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
