import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { CredentialSubject } from "@esspass-web-wallet/domain-lib";

@Component({
  selector: "esspass-web-wallet-pda1-content",
  templateUrl: "./pda1-content.page.html",
  styleUrls: ["./pda1-content.page.scss"],
})
export class Pda1ContentPage implements OnInit {
  credentialSubject: CredentialSubject;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.data.subscribe(({ data }) => {
      this.credentialSubject = {
        ...data.credentialSubject,
        status: data.status,
        time: data.time,
        govInstitutionName: data.govInstitutionName,
        pdaId: data.pdaId,
      } satisfies CredentialSubject;
    });
  }
}
