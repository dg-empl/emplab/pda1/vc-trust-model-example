/**
 * @jest-environment jsdom
 */
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { RouterTestingModule } from "@angular/router/testing";
import { KeycloakService } from "keycloak-angular";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { LogoutComponent } from "./logout.component";
import { APP_CONFIG } from "@esspass-web-wallet/security-web-lib";

describe("LogoutComponent", () => {
  let component: LogoutComponent;
  let fixture: ComponentFixture<LogoutComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: KeycloakService,
          useValue: {
            loadUserProfile: () => {},
          },
        },
        { provide: APP_CONFIG, useValue: APP_CONFIG },
      ],
      declarations: [LogoutComponent],
      imports: [IonicModule.forRoot(), RouterTestingModule, HttpClientTestingModule],
    }).compileComponents();

    fixture = TestBed.createComponent(LogoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
