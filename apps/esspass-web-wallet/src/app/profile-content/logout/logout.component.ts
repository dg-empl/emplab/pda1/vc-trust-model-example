import { Component, Inject, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { KeycloakService } from "keycloak-angular";
import { ProfileService } from "@esspass-web-wallet/issuance-flow-lib";
import { KeycloakProfile } from "keycloak-js";
import { APP_CONFIG, AppConfigInterface } from "@esspass-web-wallet/security-web-lib";

@Component({
  selector: "esspass-web-wallet-logout",
  templateUrl: "./logout.component.html",
  styleUrls: ["./logout.component.scss"],
})
export class LogoutComponent implements OnInit {
  userProfile: KeycloakProfile | null = null;
  uCaseInitials: string;

  constructor(
    private router: Router,
    private keycloak: KeycloakService,
    private profile: ProfileService,
    @Inject(APP_CONFIG) private config: AppConfigInterface
  ) {}

  async ngOnInit() {
    this.profile.userProfile.next(await this.keycloak.loadUserProfile());
    //check for logged user wallet
    this.userProfile = this.profile.getUserProfile();

    this.uCaseInitials = this.upperCaseInitials();
  }

  async logoutPopup() {
    try {
      await this.keycloak.logout(this.config.redirectUrl);
      await this.router.navigate(["/home"]);
    } catch (e) {
      console.error(e);
    }
  }

  upperCaseInitials() {
    return `${this.userProfile?.firstName?.slice(0, 1)?.toUpperCase()}${this.userProfile?.lastName
      ?.slice(0, 1)
      ?.toUpperCase()}`;
  }
}
