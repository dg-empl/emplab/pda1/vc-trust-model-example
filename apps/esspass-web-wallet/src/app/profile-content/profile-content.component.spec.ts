/**
 * @jest-environment jsdom
 */
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { ProfileContentComponent } from "./profile-content.component";
import { RouterTestingModule } from "@angular/router/testing";
import { KeycloakService } from "keycloak-angular";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { APP_CONFIG } from "@esspass-web-wallet/security-web-lib";
import { ProfileService } from "@esspass-web-wallet/issuance-flow-lib";

describe("ProfileContentPage", () => {
  let component: ProfileContentComponent;
  let fixture: ComponentFixture<ProfileContentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        {
          provide: KeycloakService,
          useValue: {
            loadUserProfile: jest.fn(),
          },
        },
        { provide: APP_CONFIG, useValue: APP_CONFIG },
        {
          provide: ProfileService,
          useValue: {
            userProfile: {
              next: jest.fn(),
            },
            getUserProfile: jest.fn(),
          },
        },
      ],
      declarations: [ProfileContentComponent],
      imports: [IonicModule.forRoot(), RouterTestingModule, HttpClientTestingModule],
    }).compileComponents();

    fixture = TestBed.createComponent(ProfileContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
