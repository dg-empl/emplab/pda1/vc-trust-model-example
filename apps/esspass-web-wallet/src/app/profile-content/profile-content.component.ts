import { Component, Input, OnInit } from "@angular/core";
import { KeycloakService } from "keycloak-angular";
import { PopoverController } from "@ionic/angular";
import { Router } from "@angular/router";
import { KeycloakProfile } from "keycloak-js";
import { ProfileService } from "@esspass-web-wallet/issuance-flow-lib";
import { LogoutComponent } from "./logout/logout.component";

@Component({
  selector: "esspass-web-wallet-profile-content",
  templateUrl: "./profile-content.component.html",
  styleUrls: ["./profile-content.component.scss"],
})
export class ProfileContentComponent implements OnInit {
  @Input() backButton: boolean | undefined;

  userProfile: KeycloakProfile | null = null;
  uCaseInitials: string;
  roleMsg: string;

  constructor(
    private router: Router,
    private keycloak: KeycloakService,
    private popover: PopoverController,
    private profile: ProfileService
  ) {}

  async ngOnInit() {
    this.profile.userProfile.next(await this.keycloak.loadUserProfile());
    this.userProfile = this.profile.getUserProfile();
    this.uCaseInitials = this.upperCaseInitials();
  }

  async back() {
    await this.router.navigate(["/my-credentials"]);
  }

  async logoutPopup() {
    const popoverElement = await this.popover.create({
      component: LogoutComponent,
    });

    await popoverElement.present();

    const { role } = await popoverElement.onDidDismiss();
    this.roleMsg = `Popover dismissed with role: ${role}`;
  }

  upperCaseInitials() {
    return `${this.userProfile?.firstName?.slice(0, 1)?.toUpperCase()}${this.userProfile?.lastName
      ?.slice(0, 1)
      ?.toUpperCase()}`;
  }
}
