import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { ProfileContentComponent } from "./profile-content.component";
import { HttpClientModule } from "@angular/common/http";
import { LogoutComponent } from "./logout/logout.component";
import { RouterLink } from "@angular/router";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    RouterLink,
  ],
  declarations: [ProfileContentComponent, LogoutComponent],
  exports: [ProfileContentComponent],
})
export class ProfileContentModule {}
