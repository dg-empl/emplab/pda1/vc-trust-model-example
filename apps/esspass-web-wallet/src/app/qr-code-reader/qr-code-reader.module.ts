import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QrCodeReaderPageRoutingModule } from './qr-code-reader-routing.module';

import { QrCodeReaderPage } from './qr-code-reader.page';
import { HeaderInfoModule } from '../header-info/header-info.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QrCodeReaderPageRoutingModule,
    HeaderInfoModule
  ],
  declarations: [QrCodeReaderPage],
})
export class QrCodeReaderPageModule {}
