/**
 * @jest-environment jsdom
 */
import {
  VerifiableCredentialQrCode,
  toTypeScriptObjectOfType,
  verifiableCredentialQrCodeSchema,
} from "@esspass-web-wallet/domain-lib";
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { KeycloakService } from "keycloak-angular";
import { APP_CONFIG } from "@esspass-web-wallet/security-web-lib";
import { IonicModule } from "@ionic/angular";
import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { QrCodeReaderPage } from "./qr-code-reader.page";
import { MockComponent } from "ng2-mock-component";

describe("QrCodeReaderPage", () => {
  let component: QrCodeReaderPage;
  let fixture: ComponentFixture<QrCodeReaderPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: KeycloakService, useValue: { loadUserProfile: () => {} } },
        { provide: APP_CONFIG, useValue: APP_CONFIG },
      ],
      declarations: [
        QrCodeReaderPage,
        MockComponent({
          selector: "esspass-profile-content",
          inputs: ["profile"],
        }),
        MockComponent({
          selector: "esspass-web-wallet-header-info",
          inputs: ["profile"],
        }),
      ],
      imports: [IonicModule.forRoot(), RouterTestingModule, HttpClientTestingModule],
    }).compileComponents();

    fixture = TestBed.createComponent(QrCodeReaderPage);
    component = fixture.componentInstance;

    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should parse a valid QR code", () => {
    const qrCodeResponse = verifiableCredentialQrCodeSchema.parse({
      "urn:ietf:params:oauth:grant-type:pre-authorized_code": {
        "pre-authorized_code":
          "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImtpZCI6IkROeVh6Tmx0V1FMSjhnNVM3UTBTUWE1enNrWk92RlhUdkVvMENxUFZKVXcifQ.eyJhdXRob3JpemF0aW9uX2RldGFpbHMiOlt7InR5cGVzIjpbIlZlcmlmaWFibGVQb3J0YWJsZURvY3VtZW50QTEiXX1dLCJpYXQiOjE2OTg5NDMyMTIsImV4cCI6MTY5ODk3MjAxMiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDozMTAwL2p3a3MiLCJhdWQiOiJtYXJjaW4uemF3YWR6a2lAb3V0bG9vay5jb20iLCJzdWIiOiI2NTQzZDBlOTI4ZTE1YjRhMWIzODZiZTEifQ.W0Yb5fNt0joXcEeVhIKZjdu1LazDiuMD8iUmujcbJPGWRMDSdvgLpk-xi3Mn1SpdEXX9H7iQnnS-E0IuWvx50g",
        user_pin_required: true,
      },
    });

    expect(qrCodeResponse).toBeTruthy();
  });
});
