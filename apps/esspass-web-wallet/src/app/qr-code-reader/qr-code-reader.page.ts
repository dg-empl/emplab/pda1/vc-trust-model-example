import { Component } from "@angular/core";
import { Html5Qrcode, Html5QrcodeSupportedFormats } from "html5-qrcode";
import { AlertController, ViewWillEnter, ViewWillLeave } from "@ionic/angular";
import {
  VerifiableCredentialQrCode,
  verifiableCredentialQrCodeSchema,
  VerifiablePresentationQrCode,
} from "@esspass-web-wallet/domain-lib";
import { NavigationHandler } from "../common/handler-strategy/navigation-strategy";
import { VerifiablePresentationService } from "./vp.service";
import {
  ShareCredentialPageRedirectionHandler,
  SharePinCodePageRedirectionHandler,
} from "../common";

@Component({
  selector: "esspass-web-wallet-qrcodereader",
  templateUrl: "./qr-code-reader.page.html",
  styleUrls: ["./qr-code-reader.page.scss"],
})
export class QrCodeReaderPage implements ViewWillEnter, ViewWillLeave {
  error: string | undefined;
  private html5QrcodeScanner: Html5Qrcode | any;
  private navigationHandler: NavigationHandler;

  constructor(
    private alertController: AlertController,
    private vpService: VerifiablePresentationService,
    private sharePinCodePageRedirectionHandler: SharePinCodePageRedirectionHandler,
    private shareCredentialPageRedirectionHandler: ShareCredentialPageRedirectionHandler
  ) {
    this.navigationHandler = new NavigationHandler();

    this.navigationHandler.use(
      "verifiableCredentialQrCode",
      this.sharePinCodePageRedirectionHandler
    );
    this.navigationHandler.use(
      "verifiablePresentationQrCode",
      this.shareCredentialPageRedirectionHandler
    );
  }

  /**
   * issues with going back to this page , OnInit not working, Ionic events works fine
   */
  ionViewWillEnter() {
    this.html5QrcodeScanner = new Html5Qrcode("reader");
    Html5Qrcode.getCameras()
      .then(() => this.startScanner())
      .catch(err => {
        this.error = err;
      });
  }

  private async startScanner() {
    const formatsToSupport = [
      Html5QrcodeSupportedFormats.QR_CODE,
      Html5QrcodeSupportedFormats.UPC_A,
      Html5QrcodeSupportedFormats.UPC_E,
      Html5QrcodeSupportedFormats.UPC_EAN_EXTENSION,
    ];

    this.html5QrcodeScanner
      .start(
        { facingMode: "environment" },
        {
          fps: 5,
          qrbox: 350,
          formatsToSupport,
        },
        this.onScanSuccess()
      )
      .catch((err: Error) => {
        console.error(`Unable to start scanning, error: ${err}`);
        alert(`Unable to start scanning, error: ${err}`);
      });
  }

  private onScanSuccess() {
    return async (qrCodeMessage: string) => {
      const qrCode = this.parseQrResponse(qrCodeMessage);

      await this.navigationHandler.navigate("verifiableCredentialQrCode", qrCode);
      await this.navigationHandler.navigate("verifiablePresentationQrCode", qrCode);

      if (
        !(qrCode as VerifiableCredentialQrCode) &&
        !(qrCode instanceof VerifiablePresentationQrCode)
      ) {
        await this.invalidQrCode();
      }
    };
  }

  /**
   * Decide if the QR code is a VerifiableCredentialQrCode or a VerifiablePresentationQrCode
   * @param qrCodeMessage
   * @private
   */
  private parseQrResponse(
    qrCodeMessage: string
  ): VerifiableCredentialQrCode | VerifiablePresentationQrCode | undefined {
    let safeVerifiableCredentialQrCode: any;

    const isIssuanceQrCode = (str: string) => {
      const isJsonParsable = (data: string): boolean => {
        try {
          JSON.parse(data);
          return true;
        } catch (error) {
          return false;
        }
      };

      if (isJsonParsable(str)) {
        safeVerifiableCredentialQrCode = verifiableCredentialQrCodeSchema.safeParse(
          JSON.parse(str)
        );
        return safeVerifiableCredentialQrCode.success;
      }
      return false;
    };

    return isIssuanceQrCode(qrCodeMessage)
      ? safeVerifiableCredentialQrCode.data
      : this.vpService.parseVpRequest(qrCodeMessage);
  }

  private async invalidQrCode() {
    const alert = await this.alertController.create({
      header: "Invalid QR Code",
      message: "Invalid code generated",
      cssClass: "custom-alert",
      buttons: [
        {
          text: "OK",
          role: "confirm",
          cssClass: "alert-button-confirm",
        },
      ],
    });

    await alert.present();
  }

  private async stopScanning() {
    try {
      await this.html5QrcodeScanner.stop();
    } catch (e) {
      console.error("Unable to stop scanning :", e);
    }
  }

  async ionViewWillLeave() {
    await this.stopScanning();
  }
}
