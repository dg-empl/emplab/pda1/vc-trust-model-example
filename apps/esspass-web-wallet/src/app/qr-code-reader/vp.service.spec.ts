/**
 * @jest-environment jsdom
 */
import { TestBed } from "@angular/core/testing";
import { VerifiablePresentationService } from "./vp.service";
import { TokenService } from "@esspass-web-wallet/issuance-flow-lib";
import { HttpClient } from "@angular/common/http";
import { APP_CONFIG } from "@esspass-web-wallet/security-web-lib";
import { KeycloakService } from "keycloak-angular";
import { VerifiablePresentationQrCode } from "@esspass-web-wallet/domain-lib";

describe("VpService", () => {
  let service: VerifiablePresentationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        VerifiablePresentationService,
        {
          provide: TokenService,
          useValue: {},
        },
        { provide: APP_CONFIG, useValue: APP_CONFIG },
        {
          provide: HttpClient,
          useValue: {},
        },
        {
          provide: KeycloakService,
          useValue: {
            isLoggedIn: () => true,
            loadUserProfile: () => {},
            getKeycloakInstance: () => {},
          },
        },
      ],
    });
    service = TestBed.inject(VerifiablePresentationService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should parse a valid VerifiablePresentationQrCode", () => {
    //given
    const qrCode =
      "openid://?scope=openid&response_type=id_token" +
      "    client_id=1" +
      "    claims=['email']" +
      "    redirect_uri=http://localhost" +
      "    nonce=1aef" +
      "    verification=true";

    //when
    const vpRequest = service.parseVpRequest(qrCode);

    //then
    expect(vpRequest).toBeInstanceOf(VerifiablePresentationQrCode);
  });

  it("should return not parsed VerifiablePresentationQrCode", () => {
    //given
    const qrCode = "test";

    //when
    const vpRequest = service.parseVpRequest(qrCode);

    //then
    expect(vpRequest).toBeUndefined();
  });
});
