import { Inject, Injectable } from "@angular/core";
import {
  toTypeScriptObjectOfType,
  VerifiablePresentationDtoResponse,
  VerifiablePresentationQrCode,
} from "@esspass-web-wallet/domain-lib";
import { KeycloakService } from "keycloak-angular";
import { APP_CONFIG, AppConfigInterface } from "@esspass-web-wallet/security-web-lib";
import { TokenService } from "@esspass-web-wallet/issuance-flow-lib";
import { MyCredentialsService } from "../common";

@Injectable({
  providedIn: "root",
})
export class VerifiablePresentationService {
  constructor(
    private token: TokenService,
    private credentials: MyCredentialsService,
    private keycloak: KeycloakService,
    @Inject(APP_CONFIG) private config: AppConfigInterface
  ) {}

  private readonly vpBody = [
    "openid://?scope=openid&response_type=id_token",
    "client_id",
    "claims",
    "redirect_uri",
    "nonce",
    "verification",
  ];

  private isVpRequest(qrCode: string): boolean {
    return this.vpBody.every(keyword => qrCode.includes(keyword));
  }

  public parseVpRequest(qrCode: string): VerifiablePresentationQrCode | undefined {
    if (!this.isVpRequest(qrCode)) return;

    const urlSearchParams = new URLSearchParams(qrCode);

    const clientId = urlSearchParams.get("client_id");
    const redirectUri = urlSearchParams.get("redirect_uri");
    // eslint-disable-next-line  @typescript-eslint/no-non-null-assertion
    const claims = JSON.parse(urlSearchParams.get("claims")!);
    const nonce = urlSearchParams.get("nonce");
    const verification = urlSearchParams.get("verification");

    return new VerifiablePresentationQrCode(
      // eslint-disable-next-line  @typescript-eslint/no-non-null-assertion
      clientId!,
      // eslint-disable-next-line  @typescript-eslint/no-non-null-assertion
      redirectUri!,
      // eslint-disable-next-line  @typescript-eslint/no-non-null-assertion
      claims!,
      // eslint-disable-next-line  @typescript-eslint/no-non-null-assertion
      nonce!,
      // eslint-disable-next-line  @typescript-eslint/no-non-null-assertion
      verification!
    );
  }

  public async getPresentationDefinition(presDefinitionUri: string): Promise<Response> {
    const authToken = this.keycloak.getKeycloakInstance().token;

    return await fetch(`${this.config.serverUrl}/verifiable-presentation-uri`, {
      method: "POST",
      body: JSON.stringify({
        presentationDefinitionUri: `${presDefinitionUri}`,
      }),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${authToken}`,
      },
    });
  }

  public async sendVerifiablePresentationRequest(
    email: string,
    redirectUri: string,
    credentialSchemaId: string,
    nonce: string,
    verification: string,
    sharedCredentialId: string
  ): Promise<VerifiablePresentationDtoResponse> {
    const authToken = this.keycloak.getKeycloakInstance().token;

    const response = await fetch(`${this.config.serverUrl}/verifiable-presentation`, {
      method: "POST",
      body: JSON.stringify({
        credentialSchemaId: `${credentialSchemaId}`,
        nonce: `${nonce}`,
        redirectUri: `${redirectUri}`,
        email: `${email}`,
        verification: `${verification}`,
        sharedCredentialId: `${sharedCredentialId}`,
      }),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${authToken}`,
      },
    });

    return toTypeScriptObjectOfType(await response.json(), VerifiablePresentationDtoResponse);
  }
}
