/**
 * @jest-environment jsdom
 */
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { ShareCredentialContentComponent } from "./share-credential-content.component";
import { KeycloakService } from "keycloak-angular";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { DidDto, MyCredentialsService } from "../../common";
import { Observable, of } from "rxjs";
import { Pda1Dto, pda1DtoSchema } from "@esspass-web-wallet/domain-lib";
import * as moment from "moment";

describe("ShareCredentialContentPage", () => {
  jest.setTimeout(10_000);
  let component: ShareCredentialContentComponent;
  let fixture: ComponentFixture<ShareCredentialContentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ShareCredentialContentComponent],
      imports: [IonicModule.forRoot(), HttpClientTestingModule],
      providers: [
        {
          provide: MyCredentialsService,
          useValue: {
            getHolderWalletDid(did: string): Observable<DidDto> {
              return of({ did: "did" });
            },
            getPda1Observables(): Observable<Pda1Dto[]> {
              return of([]);
            },
          },
        },
        {
          provide: KeycloakService,
          useValue: {
            isLoggedIn: () => true,
            loadUserProfile: () => {},
            getKeycloakInstance() {
              return { subject: test };
            },
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ShareCredentialContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should mark expired credentials as EXPIRED when in past", () => {
    //given
    const input = [
      {
        id: "urn:uuid:3b6bcc67-be61-4620-88cc-6625c832a6a0",
        date: "2023-02-13T11:36:08.307Z",
        endingDate: "2023-01-31",
        time: "2023-02-13T11:36:08.307Z",
        govInstitutionName:
          "Office National de Sécurité Sociale / Rijksdienst voor Sociale Zekerheid",
        status: "Revoked",
        memberStateWhichLegislationApplies: "BE",
        credentialStatus: {
          id: "urn:uuid:07d320c0-9764-450e-afa3-3cae7b7feba0",
          type: "StatusList2021Entry",
          statusPurpose: "revocation",
          statusListIndex: "96",
          statusListCredential:
            "https://api-pilot.ebsi.eu/trusted-issuers-registry/v4/issuers/did:ebsi:zsqBYQuKunNdr3XfGfPUvU7/proxies/0xe34e137dbefa842d71c0c840ddb4900af3611e297d8403e80f99a13e187e8d63/credentials/status/1#list",
        },
      },
      {
        id: "urn:uuid:cd747745-48c9-45fc-8210-0ecd7ba02702",
        date: "2023-02-13T11:40:36.177Z",
        endingDate: "2023-12-31",
        time: "2023-02-13T11:40:36.177Z",
        govInstitutionName:
          "Office National de Sécurité Sociale / Rijksdienst voor Sociale Zekerheid",
        status: "Revoked",
        memberStateWhichLegislationApplies: "BE",
        credentialStatus: {
          id: "urn:uuid:07d320c0-9764-450e-afa3-3cae7b7feba0",
          type: "StatusList2021Entry",
          statusPurpose: "revocation",
          statusListIndex: "96",
          statusListCredential:
            "https://api-pilot.ebsi.eu/trusted-issuers-registry/v4/issuers/did:ebsi:zsqBYQuKunNdr3XfGfPUvU7/proxies/0xe34e137dbefa842d71c0c840ddb4900af3611e297d8403e80f99a13e187e8d63/credentials/status/1#list",
        },
      },
    ];

    const pda1s = input.map(data => pda1DtoSchema.parse(data));

    //when
    const status1 = moment(pda1s[0].endingDate).isBefore(moment(new Date()));

    pda1s[1].endingDate = String(moment(new Date()).add(1, "month"));
    const status2 = moment(pda1s[1].endingDate).isAfter(moment(new Date()));

    //then
    expect(status1).toBeTruthy();
    expect(status2).toBeTruthy();
  });
});
