import { Component, EventEmitter, inject, OnDestroy, OnInit, Output } from "@angular/core";
import { Pda1Dto } from "@esspass-web-wallet/domain-lib";
import { catchError, firstValueFrom, Observable, of, Subscription } from "rxjs";
import { ProfileService } from "@esspass-web-wallet/issuance-flow-lib";
import { DidDto, MyCredentialsService, unsubscribe } from "../../common";
import { ShareCredentialService } from "../share-credential.service";
import * as moment from "moment";
import { AlertController } from "@ionic/angular";
import { SpinnerService } from "@esspass-web-wallet/common-web-lib";

@Component({
  selector: "esspass-web-wallet-share-credential-content",
  templateUrl: "./share-credential-content.component.html",
  styleUrls: ["./share-credential-content.component.scss"],
})
export class ShareCredentialContentComponent implements OnInit, OnDestroy {
  pda1Observables: Pda1Dto[] = [];
  private pda1Observable$: Subscription;
  selected = false;

  @Output() sharedPda1IdEmitter = new EventEmitter<string>();
  @Output() cancelSharingPda1Emitter = new EventEmitter<boolean>();
  @Output() sharePda1Emitter = new EventEmitter<boolean>();
  countryCode: string;

  private profileService = inject(ProfileService);
  private shareCredentialService = inject(ShareCredentialService);
  private myCredentialsService = inject(MyCredentialsService);
  private alertController = inject(AlertController);
  private spinnerService = inject(SpinnerService);

  isLoading$: Observable<boolean> = this.spinnerService.data$;

  async ngOnInit() {
    this.spinnerService.load();

    try {
      const email = this.profileService.getUserProfile().email;
      if (email) {
        const did: DidDto = await firstValueFrom(this.getHolderWalletDidByEmail(email));
        this.renderPda1Observables(did);
      }

      this.shareCredentialService.selectedIndex.subscribe(selected => {
        this.selected = selected !== -1;
      });
    } catch (e) {
      console.error(e);
    } finally {
      setTimeout(() => {
        this.spinnerService.stop();
      }, 7500);
    }
  }

  ngOnDestroy(): void {
    unsubscribe(this.pda1Observable$);
  }

  selectCredential(index: number) {
    this.shareCredentialService.selectedIndex.next(index);
  }

  cancelSharingPda1Id() {
    this.cancelSharingPda1Emitter.next(true);
  }

  onShare($event: string) {
    this.sharedPda1IdEmitter.next($event);
  }

  share() {
    this.sharePda1Emitter.next(true);
  }

  status(pda1: Pda1Dto) {
    const { endingDate, status } = pda1;
    if (status === "Not revoked") {
      return moment(endingDate).isAfter(moment(new Date())) ? "VALID" : "EXPIRED";
    } else if (status === "Revoked") {
      return "REVOKED";
    }
    throw new Error("Invalid status");
  }

  onCountryCode($event: string) {
    this.countryCode = $event;
    this.shareCredentialService.countryCode.next($event);
  }

  resolveCountry(): string {
    return this.shareCredentialService.resolveCountry();
  }

  private getHolderWalletDidByEmail(email: string): Observable<DidDto> {
    return this.myCredentialsService.getHolderWalletDid(email).pipe(
      catchError(error => {
        this.serverError(error.message).then();
        return [];
      })
    );
  }

  private getPda1ObservablesByDid({ did }: DidDto): Observable<Pda1Dto[]> {
    return did ? this.myCredentialsService.getPda1Observables(did) : of([]);
  }

  private sortByDateDescending(a: Pda1Dto, b: Pda1Dto) {
    const dateA = new Date(a.date).getTime();
    const dateB = new Date(b.date).getTime();
    return dateB - dateA;
  }

  renderPda1Observables(did: DidDto) {
    this.pda1Observable$ = this.getPda1ObservablesByDid(did).subscribe(pda1Observables => {
      this.pda1Observables = pda1Observables.sort(this.sortByDateDescending);
    });
  }

  private async serverError(error: string) {
    const alert = await this.alertController.create({
      header: "Server Error",
      message: error,
      cssClass: "custom-alert",
      backdropDismiss: false,
      buttons: [
        {
          text: "OK",
          role: "confirm",
          cssClass: "alert-button-confirm",
        },
      ],
    });

    await alert.present();
  }
}
