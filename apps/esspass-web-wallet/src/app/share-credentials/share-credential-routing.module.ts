import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { ShareCredentialsPage } from "./share-credentials-page";

const routes: Routes = [
  {
    path: "",
    component: ShareCredentialsPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShareCredentialPageRoutingModule {}
