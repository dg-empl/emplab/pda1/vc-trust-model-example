/**
 * @jest-environment jsdom
 */
import { TestBed } from "@angular/core/testing";
import { ShareCredentialService } from "./share-credential.service";

describe('ShareCredentialService', () => {
  let service: ShareCredentialService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ShareCredentialService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});