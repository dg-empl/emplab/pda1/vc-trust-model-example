import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { contryCode } from "@esspass-web-wallet/domain-lib";

@Injectable({
  providedIn: "root",
})
export class ShareCredentialService {
  selectedIndex: BehaviorSubject<number> = new BehaviorSubject<number>(-1);
  selectedIndex$ = this.selectedIndex.asObservable();

  countryCode: BehaviorSubject<string> = new BehaviorSubject<string>("");
  countryCode$ = this.countryCode.asObservable();

  resolveCountry(): string {
    const ret = contryCode.find(code => code.code === this.countryCode.value);
    return ret ? ret.name : "";
  }

  getCountryCode() {
    return this.countryCode$;
  }

}
