import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from "@angular/core";
import { ShareCredentialService } from "../share-credential.service";

@Component({
  selector: "esspass-web-wallet-share-credential",
  templateUrl: "./share-credential.component.html",
  styleUrls: ["./share-credential.component.scss"],
})
export class ShareCredentialComponent implements OnInit, OnDestroy {
  @Input() pdaId: string;
  @Input() index: number;
  @Input() date: string;
  @Input() time: string;
  @Input() status: string;
  @Input() countryCode: string;
  @Input() governmentInstitution: string;

  @Output() sharedPda1IdEmitter = new EventEmitter<string>();
  @Output() sharedPda1CountryCodeEmitter = new EventEmitter<string>();

  private readonly NOT_SELECTED = -1;
  highlightColor = "white";
  isSelected = false;
  isRevoked: boolean;

  constructor(private shareCredentialService: ShareCredentialService) {}

  ngOnInit() {
    this.shareCredentialService.selectedIndex$
      .pipe()
      .subscribe(selectedIndex => {
        this.isSelected = selectedIndex === this.index;
        this.highlightColor =
          selectedIndex === this.index ? "#294597" : "white";
        if (this.isSelected) {
          this.sharedPda1IdEmitter.next(this.pdaId);
          this.sharedPda1CountryCodeEmitter.next(this.countryCode);
        }
      });
    this.isRevoked = this.status === "REVOKED";
  }

  ngOnDestroy(): void {
    this.shareCredentialService.selectedIndex.next(this.NOT_SELECTED);
  }
}
