/**
 * @jest-environment jsdom
 */
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { ShareCredentialsPage } from "./share-credentials-page";
import { MockComponent } from "ng2-mock-component";
import { KeycloakService } from "keycloak-angular";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { DidDto, MyCredentialsService } from "../common";
import { Observable, of } from "rxjs";
import { Pda1Dto } from "@esspass-web-wallet/domain-lib";
import { VerifiablePresentationService } from "../qr-code-reader/vp.service";

describe("ShareCredentialPage", () => {
  let component: ShareCredentialsPage;
  let fixture: ComponentFixture<ShareCredentialsPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        ShareCredentialsPage,
        MockComponent({ selector: "esspass-web-wallet-header-info" }),
        MockComponent({
          selector: "esspass-web-wallet-profile-content",
          inputs: ["backButton"],
        }),
        MockComponent({
          selector: "esspass-web-wallet-share-credential-content",
        }),
        MockComponent({ selector: "esspass-web-wallet-footer" }),
      ],
      providers: [
        { provide: VerifiablePresentationService, useValue: {} },
        {
          provide: MyCredentialsService,
          useValue: {
            getHolderWalletDid(did: string): Observable<DidDto> {
              return of({ did: "did" });
            },
            getPda1Observable(): Observable<Pda1Dto[]> {
              return of([]);
            },
          },
        },
        {
          provide: KeycloakService,
          useValue: {
            isLoggedIn: () => true,
            loadUserProfile: () => {},
            getKeycloakInstance() {
              return { subject: test };
            },
          },
        },
      ],
      imports: [IonicModule.forRoot(), HttpClientTestingModule],
    }).compileComponents();

    fixture = TestBed.createComponent(ShareCredentialsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
