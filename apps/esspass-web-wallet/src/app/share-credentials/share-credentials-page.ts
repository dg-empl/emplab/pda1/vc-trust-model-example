import { Component, OnInit } from "@angular/core";
import {
  extractCredentialId,
  NotFoundError,
  PresentationDefinition,
  schemaPresentationDefinition,
  VerifiablePresentationDtoResponse,
  VerifiablePresentationQrCode,
} from "@esspass-web-wallet/domain-lib";
import { AlertController, LoadingController } from "@ionic/angular";
import { VerifiablePresentationService } from "../qr-code-reader/vp.service";
import { KeycloakService } from "keycloak-angular";
import { Router } from "@angular/router";
import { TokenService } from "@esspass-web-wallet/issuance-flow-lib";

@Component({
  selector: "esspass-web-wallet-share-credentials",
  templateUrl: "./share-credentials-page.html",
  styleUrls: ["./share-credentials-page.scss"],
})
export class ShareCredentialsPage implements OnInit {
  private sharedCredentialId = "";

  constructor(
    private router: Router,
    private keycloak: KeycloakService,
    private tokenService: TokenService,
    private loadingCtrl: LoadingController,
    private alertController: AlertController,
    private vpService: VerifiablePresentationService
  ) {}

  ngOnInit() {}

  onShareCredentialId($event: string) {
    this.sharedCredentialId = $event;
  }

  async onShareConfirmation($event: boolean) {
    if (!this.sharedCredentialId) {
      throw Error("None credential selected !");
    }

    const vpRequestCommand: VerifiablePresentationQrCode = this.tokenService.vpRequestCommand.value;
    if (!vpRequestCommand) {
      throw Error("None VerifiablePresentationQrCode selected !");
    }

    /**
     *  Get presentation_definition
     */
    let definition: PresentationDefinition | null;

    try {
      definition = await this.getPresentationDefinition(
        `${vpRequestCommand.getPresentationDefinitionUri()}?credType=pda1`
      );
    } catch (e: any) {
      await this.invalidVerifiablePresentationResponseCode(e.message);
      throw new NotFoundError(e.message);
    }

    /**
     * Get verifiable presentation response
     */
    const presentationDtoResponse = await this.sendVerifiableResponse(
      vpRequestCommand,
      definition,
      this.sharedCredentialId
    );

    if (!presentationDtoResponse) {
      throw new NotFoundError("presentationDtoResponse has not been generated!");
    }

    const { vpFormat, presentation, credential } = presentationDtoResponse?.validations ?? {
      vpFormat: {
        status: "",
      },
      presentation: {
        status: "",
      },
      credential: {
        status: "",
      },
    };

    if (
      presentationDtoResponse.result &&
      vpFormat["status"] &&
      presentation["status"] &&
      credential["status"]
    ) {
      await this.router.navigate(["/confirmation"]);
    } else {
      console.error("presentationResponse : ", presentationDtoResponse);
      await this.invalidVerifiablePresentationResponseCode();
    }
  }

  private async getPresentationDefinition(presentDefinitionUri: string) {
    const presentationDefinition = await this.vpService.getPresentationDefinition(
      presentDefinitionUri
    );

    const response = await presentationDefinition.json();

    /**
     * Check if the response is an error from backend
     */
    const message = response.hasOwnProperty("message") && response["message"];
    if (message) {
      throw new NotFoundError("Error from backend getting presentationDefinition:" + message);
    }

    let presentationDefinitionResponse: PresentationDefinition | null = null;
    try {
      presentationDefinitionResponse = schemaPresentationDefinition.parse(response);
    } catch (e: any) {
      throw new NotFoundError(e.message);
    }

    return presentationDefinitionResponse;
  }

  private async sendVerifiableResponse(
    vpRequestCommand: VerifiablePresentationQrCode,
    definition: PresentationDefinition,
    sharedCredentialId: string
  ) {
    const { email } = await this.keycloak.loadUserProfile();

    const loading = await this.loadingCtrl.create({
      message: "Loading verifiable presentation response ...",
    });

    await loading.present();

    let presentationDtoResponse: VerifiablePresentationDtoResponse | null = null;

    try {
      presentationDtoResponse = await this.vpService.sendVerifiablePresentationRequest(
        email ?? "",
        vpRequestCommand.redirectUri,
        extractCredentialId(definition),
        vpRequestCommand.nonce,
        vpRequestCommand.verification,
        sharedCredentialId
      );
    } catch (e) {
      await loading.dismiss();
      await this.invalidVerifiablePresentationResponseCode();
    } finally {
      await loading.dismiss();
    }

    return presentationDtoResponse;
  }

  private async invalidVerifiablePresentationResponseCode(msg = "") {
    const alert = await this.alertController.create({
      header: "Invalid Verifiable Presentation",
      message: msg ?? "Invalid verifiable presentation response generated",
      cssClass: "custom-alert",
      buttons: [
        {
          text: "OK",
          role: "confirm",
          cssClass: "alert-button-confirm",
          handler: async () => {
            await this.router.navigate(["/my-credentials"]);
          },
        },
      ],
    });

    await alert.present();
  }

  async onCancelSharingCredentialId($event: boolean) {
    await this.router.navigate(["/my-credentials"]);
  }
}
