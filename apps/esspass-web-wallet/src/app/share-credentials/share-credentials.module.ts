import { NgModule } from "@angular/core";
import { CommonModule, NgOptimizedImage } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { ShareCredentialPageRoutingModule } from "./share-credential-routing.module";

import { ShareCredentialsPage } from "./share-credentials-page";
import { HeaderInfoModule } from "../header-info/header-info.module";
import { FooterModule } from "../footer/footer.module";
import { ProfileContentModule } from "../profile-content/profile-content.module";

import { ShareCredentialContentComponent } from "./share-credential-content/share-credential-content.component";
import { ShareCredentialComponent } from "./share-credential/share-credential.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShareCredentialPageRoutingModule,
    HeaderInfoModule,
    FooterModule,
    ProfileContentModule,
    NgOptimizedImage,
  ],
  declarations: [ShareCredentialsPage, ShareCredentialContentComponent, ShareCredentialComponent],
})
export class ShareCredentialsPageModule {}
