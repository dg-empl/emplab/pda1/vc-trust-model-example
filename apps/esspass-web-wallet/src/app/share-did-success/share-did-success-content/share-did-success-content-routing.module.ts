import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShareDidSuccessContentComponent } from './share-did-success-content.component';

const routes: Routes = [
  {
    path: '',
    component: ShareDidSuccessContentComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShareDidSuccessContentPageRoutingModule {}
