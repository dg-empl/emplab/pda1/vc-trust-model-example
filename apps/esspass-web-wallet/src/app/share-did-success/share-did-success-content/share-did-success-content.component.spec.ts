/**
 * @jest-environment jsdom
 */
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ShareDidSuccessContentComponent } from './share-did-success-content.component';
import { RouterTestingModule } from "@angular/router/testing";

describe('ShareDidSuccessContentPage', () => {
  let component: ShareDidSuccessContentComponent;
  let fixture: ComponentFixture<ShareDidSuccessContentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ShareDidSuccessContentComponent],
      imports: [IonicModule.forRoot(), RouterTestingModule],
    }).compileComponents();

    fixture = TestBed.createComponent(ShareDidSuccessContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
