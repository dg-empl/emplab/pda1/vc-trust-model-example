import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "esspass-web-wallet-share-did-success-content",
  templateUrl: "./share-did-success-content.component.html",
  styleUrls: ["./share-did-success-content.component.scss"],
})
export class ShareDidSuccessContentComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {}

  async myCredentials() {
    await this.router.navigate(["/my-credentials"]);
  }
}
