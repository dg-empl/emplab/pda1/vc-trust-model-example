import { NgModule } from "@angular/core";
import { CommonModule, NgOptimizedImage } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { ShareDidSuccessContentPageRoutingModule } from "./share-did-success-content-routing.module";

import { ShareDidSuccessContentComponent } from "./share-did-success-content.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShareDidSuccessContentPageRoutingModule,
    NgOptimizedImage,
  ],
  declarations: [ShareDidSuccessContentComponent],
  exports: [ShareDidSuccessContentComponent],
})
export class ShareDidSuccessContentModule {}
