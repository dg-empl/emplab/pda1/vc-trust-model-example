import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShareDidSuccessPage } from './share-did-success.page';

const routes: Routes = [
  {
    path: '',
    component: ShareDidSuccessPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShareDidSuccessPageRoutingModule {}
