import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { ShareDidSuccessPageRoutingModule } from "./share-did-success-routing.module";

import { ShareDidSuccessPage } from "./share-did-success.page";
import { HeaderInfoModule } from "../header-info/header-info.module";
import { FooterModule } from "../footer/footer.module";
import { ShareDidSuccessContentModule } from "./share-did-success-content/share-did-success-content.module";
import { ProfileContentModule } from "../profile-content/profile-content.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShareDidSuccessPageRoutingModule,
    HeaderInfoModule,
    FooterModule,
    ShareDidSuccessContentModule,
    ProfileContentModule,
  ],
  declarations: [ShareDidSuccessPage],
})
export class ShareDidSuccessPageModule {}
