/**
 * @jest-environment jsdom
 */
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { ShareDidSuccessPage } from "./share-did-success.page";
import { MockComponent } from "ng2-mock-component";
import { RouterTestingModule } from "@angular/router/testing";

describe("ShareDidSuccessPage", () => {
  let component: ShareDidSuccessPage;
  let fixture: ComponentFixture<ShareDidSuccessPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        ShareDidSuccessPage,
        MockComponent({ selector: "esspass-web-wallet-header-info" }),
        MockComponent({ selector: "esspass-web-wallet-footer" }),
        MockComponent({ selector: "esspass-web-wallet-pda1-personal-details" }),
        MockComponent({
          selector: "esspass-web-wallet-share-did-success-content",
        }),
        MockComponent({
          selector: "esspass-web-wallet-profile-content",
          inputs: ["backButton"],
        }),
        MockComponent({
          selector: "esspass-web-wallet-pda1-member-state-legislation",
        }),
        MockComponent({ selector: "esspass-web-wallet-confirmation-content" }),
      ],
      imports: [IonicModule.forRoot(), RouterTestingModule],
    }).compileComponents();

    fixture = TestBed.createComponent(ShareDidSuccessPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
