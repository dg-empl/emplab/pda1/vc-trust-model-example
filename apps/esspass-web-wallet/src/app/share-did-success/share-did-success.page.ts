import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'esspass-web-wallet-share-did-success',
  templateUrl: './share-did-success.page.html',
  styleUrls: ['./share-did-success.page.scss'],
})
export class ShareDidSuccessPage implements OnInit {
  constructor(private router: Router) {
  }

  ngOnInit(): void {
  }

  async back() {
    await this.router.navigate(["/qr-code-reader"]);
  }
}
