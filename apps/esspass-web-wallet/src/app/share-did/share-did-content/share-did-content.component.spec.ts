/**
 * @jest-environment jsdom
 */
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { ShareDidContentComponent } from "./share-did-content.component";
import { RouterTestingModule } from "@angular/router/testing";
import { KeycloakService } from "keycloak-angular";
import { APP_CONFIG } from "@esspass-web-wallet/security-web-lib";

describe("ShareDidContentPage", () => {
  let component: ShareDidContentComponent;
  let fixture: ComponentFixture<ShareDidContentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: KeycloakService, useValue: {} },
        { provide: APP_CONFIG, useValue: APP_CONFIG },
      ],
      declarations: [ShareDidContentComponent],
      imports: [IonicModule.forRoot(), RouterTestingModule],
    }).compileComponents();

    fixture = TestBed.createComponent(ShareDidContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
