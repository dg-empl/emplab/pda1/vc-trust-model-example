import { Component, Inject } from "@angular/core";
import { TokenService } from "@esspass-web-wallet/issuance-flow-lib";
import { KeycloakService } from "keycloak-angular";
import { AlertController, LoadingController, NavController } from "@ionic/angular";
import { Router } from "@angular/router";
import { APP_CONFIG, AppConfigInterface } from "@esspass-web-wallet/security-web-lib";
import { VerifiableCredential, verifiableCredentialSchema } from "@esspass-web-wallet/domain-lib";

@Component({
  selector: "esspass-web-wallet-share-did-content",
  templateUrl: "./share-did-content.component.html",
  styleUrls: ["./share-did-content.component.scss"],
})
export class ShareDidContentComponent {
  private CREDENTIAL_ENDPOINT = "/v2/credential";

  constructor(
    private router: Router,
    private tokenService: TokenService,
    private keycloakService: KeycloakService,
    private loadingCtrl: LoadingController,
    private alertController: AlertController,
    private navCtrl: NavController,
    @Inject(APP_CONFIG) private config: AppConfigInterface
  ) {}

  async sendCredentialRequest() {
    let loading;
    this.navCtrl.setDirection("root");

    try {
      loading = await this.loadingCtrl.create({
        message: "Getting verifiable credential data",
      });
      await loading.present();

      const responsePromise = await this.sendBackendCredentialRequest();
      const response = await responsePromise.json();

      if (response["vc"].data) {
        const verifiableCredential = verifiableCredentialSchema.parse(response["vc"].data);
        this.tokenService.credentialResponse.next(verifiableCredential);
      } else {
        throw new Error("Invalid credential response");
      }

      if (loading) {
        await loading.dismiss();
      }

      await this.router.navigate(["/share-did-success"]);
    } catch (e) {
      console.error(e);
      await this.invalidAuthentication();
      await this.router.navigate(["/my-credentials"]);
      await loading?.dismiss();
    }
  }

  async sendBackendCredentialRequest(): Promise<Response> {
    const { token, profile } = this.keycloakService.getKeycloakInstance();

    const response = await fetch(`${this.config.serverUrl}${this.CREDENTIAL_ENDPOINT}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        token: this.tokenService.token.value,
        userEmail: profile!.email,
      }),
    });

    return response;
  }

  private async invalidAuthentication() {
    const alert = await this.alertController.create({
      header: "Invalid authentication",
      message:
        "Authentication failure prevents the verifiable share credential process from continuing",
      cssClass: "custom-alert",
      buttons: [
        {
          text: "OK",
          role: "confirm",
          cssClass: "alert-button-confirm",
        },
      ],
    });

    await alert.present();
  }

  async reject() {
    await this.router.navigate(["/my-credentials"]);
  }
}
