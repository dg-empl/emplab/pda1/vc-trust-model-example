import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShareDidPage } from './share-did.page';

const routes: Routes = [
  {
    path: '',
    component: ShareDidPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShareDidPageRoutingModule {}
