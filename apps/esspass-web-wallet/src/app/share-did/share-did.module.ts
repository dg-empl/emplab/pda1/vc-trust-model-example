import { NgModule } from "@angular/core";
import { CommonModule, NgOptimizedImage } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { ShareDidPageRoutingModule } from "./share-did-routing.module";

import { ShareDidPage } from "./share-did.page";
import { HeaderInfoModule } from "../header-info/header-info.module";
import { FooterModule } from "../footer/footer.module";
import { ShareDidContentComponent } from "./share-did-content/share-did-content.component";
import { ProfileContentModule } from "../profile-content/profile-content.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShareDidPageRoutingModule,
    HeaderInfoModule,
    FooterModule,
    ProfileContentModule,
    NgOptimizedImage,
  ],
  declarations: [ShareDidPage, ShareDidContentComponent],
})
export class ShareDidPageModule {}
