/**
 * @jest-environment jsdom
 */
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { ShareDidPage } from "./share-did.page";
import { MockComponent } from "ng2-mock-component";
import { RouterTestingModule } from "@angular/router/testing";

describe("ShareDidPage", () => {
  let component: ShareDidPage;
  let fixture: ComponentFixture<ShareDidPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        ShareDidPage,
        MockComponent({ selector: "esspass-web-wallet-header-info" }),
        MockComponent({ selector: "esspass-web-wallet-share-did-content" }),
        MockComponent({
          selector: "esspass-web-wallet-profile-content",
          inputs: ["backButton"],
        }),
        MockComponent({ selector: "esspass-web-wallet-footer" }),
      ],
      imports: [IonicModule.forRoot(), RouterTestingModule],
    }).compileComponents();

    fixture = TestBed.createComponent(ShareDidPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
