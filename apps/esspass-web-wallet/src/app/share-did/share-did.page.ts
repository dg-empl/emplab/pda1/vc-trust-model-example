import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "esspass-web-wallet-share-did",
  templateUrl: "./share-did.page.html",
  styleUrls: ["./share-did.page.scss"],
})
export class ShareDidPage implements OnInit {
  constructor(private router: Router) {}

  ngOnInit(): void {}

  async back() {
    await this.router.navigate(["/qr-code-reader"]);
  }
}
