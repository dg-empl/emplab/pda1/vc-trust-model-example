import { AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild } from "@angular/core";
import { PinService } from "../pin.service";
import { filter } from "rxjs";

enum DIGIT {
  first = "first",
  second = "second",
  third = "third",
  fourth = "fourth",
  // fifth = "fifth",
  // sixth = "sixth",
}

@Component({
  selector: "esspass-web-wallet-pin-code",
  templateUrl: "./pin-code.component.html",
  styleUrls: ["./pin-code.component.scss"],
})
export class PinCodeComponent implements OnInit {
  @ViewChild("first", { static: true }) first: ElementRef<HTMLInputElement>;
  @ViewChild("second", { static: false }) second: ElementRef<HTMLInputElement>;
  @ViewChild("third", { static: false }) third: ElementRef<HTMLInputElement>;
  @ViewChild("fourth", { static: false }) fourth: ElementRef<HTMLInputElement>;
  // @ViewChild("fifth", { static: false }) fifth: ElementRef<HTMLInputElement>;
  // @ViewChild("sixth", { static: false }) sixth: ElementRef<HTMLInputElement>;

  constructor(private pinService: PinService, private renderer: Renderer2) {}

  ngOnInit(): void {
    this.pinService.clearPin$.pipe(filter(clear => clear)).subscribe(clear => {
      this.first.nativeElement.value = "";
      this.second.nativeElement.value = "";
      this.third.nativeElement.value = "";
      this.fourth.nativeElement.value = "";
      // this.fifth.nativeElement.value = "";
      // this.sixth.nativeElement.value = "";
    });
  }

  private whichDigitFocused($event: KeyboardEvent): DIGIT {
    const target = $event.target;
    if (this.first.nativeElement === target) {
      return DIGIT.first;
    } else if (this.second.nativeElement === target) {
      return DIGIT.second;
    } else if (this.third.nativeElement === target) {
      return DIGIT.third;
    } else if (this.fourth.nativeElement === target) {
      return DIGIT.fourth;
    }
    // else if (this.fifth.nativeElement === target) {
    //   return DIGIT.fifth;
    // } else if (this.sixth.nativeElement === target) {
    //   return DIGIT.sixth;
    // }
    return DIGIT.first;
  }

  onKeyUp($event: KeyboardEvent) {
    const maxLength = 1;

    if (!$event?.target) return;
    const target = $event.target;

    this.selectionCleared($event);

    const myLength = (<HTMLInputElement>target).value.length;

    //add current digit input to pin string
    this.pinService.appendPin((<HTMLInputElement>target).value);

    if (myLength >= maxLength) {
      let next: Element | null = <HTMLInputElement>target;
      while ((next = next.nextElementSibling)) {
        if (next.tagName.toLowerCase() == "input") {
          (<HTMLInputElement>next).focus();
          break;
        }
        if (next == null) {
          // this.sixth.nativeElement.blur();
          this.fourth.nativeElement.blur();
        }
      }
    }

    if (myLength === 0) {
      const next = <HTMLInputElement>target;
      while (next == next.previousElementSibling) {
        if (next == null) break;
        if (next.tagName.toLowerCase() == "input") {
          next.focus();
          break;
        }
      }
    }
  }

  private selectionCleared($event: KeyboardEvent): boolean {
    if ($event.key === "Backspace") {
      const focused = this.whichDigitFocused($event);
      const prevElement = this[focused].nativeElement.previousElementSibling;

      this.pinService.removeLastDigit();

      if (!prevElement) return true;

      this[focused].nativeElement.value = "";

      if (prevElement) {
        (prevElement as HTMLInputElement).focus();
      }
      return true;
    }
    return false;
  }
}
