import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable({ providedIn: "root" })
export class PinService {
  private pinCode: string[] = [];

  private _clearPin: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public readonly clearPin$ = this._clearPin.asObservable();

  set clearPin(value: boolean) {
    this._clearPin.next(value);
    this.pinCode = [];
  }

  public appendPin(value: string) {
    //not to allowed to push empty string as a digit
    value && this.pinCode.push(value);
  }

  public get pinCodeValue(): string {
    return this.pinCode.join("");
  }

  public removeLastDigit() {
    this.pinCode.pop();
  }
}
