/**
 * @jest-environment jsdom
 */
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule, NavController } from "@ionic/angular";

import { SharePinContentComponent } from "./share-pin-content.component";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { KeycloakService } from "keycloak-angular";
import { FormsModule } from "@angular/forms";
import { APP_CONFIG } from "@esspass-web-wallet/security-web-lib";

describe("SharePinContentComponent", () => {
  let component: SharePinContentComponent;
  let fixture: ComponentFixture<SharePinContentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: KeycloakService, useValue: {} },
        { provide: APP_CONFIG, useValue: APP_CONFIG },
      ],
      declarations: [SharePinContentComponent],
      imports: [IonicModule.forRoot(), HttpClientTestingModule, RouterTestingModule, FormsModule],
    }).compileComponents();

    fixture = TestBed.createComponent(SharePinContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
