import { Component, Inject } from "@angular/core";
import { TokenService } from "@esspass-web-wallet/issuance-flow-lib";
import { Router } from "@angular/router";
import { AlertController, NavController } from "@ionic/angular";
import { KeycloakService } from "keycloak-angular";
import { accessTokenSchema, AccessTokenType } from "@esspass-web-wallet/domain-lib";
import { APP_CONFIG, AppConfigInterface } from "@esspass-web-wallet/security-web-lib";

@Component({
  selector: "esspass-web-wallet-share-pin-content",
  templateUrl: "./share-pin-content.component.html",
  styleUrls: ["./share-pin-content.component.scss"],
})
export class SharePinContentComponent {
  pinCode: string | null;
  private readonly TOKEN_ENDPOINT = "/v2/token";

  constructor(
    private tokenService: TokenService,
    private router: Router,
    private navCtrl: NavController,
    private alertController: AlertController,
    private keycloak: KeycloakService,
    @Inject(APP_CONFIG) private config: AppConfigInterface
  ) {}

  async sendRequestForToken() {
    try {
      const response = await this.sendBackendTokenRequest();
      const tokenResponse: AccessTokenType = accessTokenSchema.parse(await response.json());

      this.tokenService.token.next(tokenResponse);

      this.navCtrl.setDirection("root");
      await this.router.navigate(["/share-did"]);
    } catch (e) {
      console.error(e);
      await this.invalidAuthentication();
    }
  }


  clearPinCode() {
    this.pinCode = null;    
    this.navCtrl.setDirection('root');
    this.router.navigate(['/my-credentials']);
  }

  private async sendBackendTokenRequest(): Promise<Response> {
    const authToken = this.keycloak.getKeycloakInstance().token;

    return await fetch(`${this.config.serverUrl}${this.TOKEN_ENDPOINT}`, {
      method: "POST",
      body: JSON.stringify({
        grantType: "urn:ietf:params:oauth:grant-type:pre-authorized_code",
        authCode: `${this.tokenService.authCode.value}`,
        pin: `${this.pinCode}`,
      }),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${authToken}`,
      },
    });
  }

  private async invalidAuthentication() {
    const alert = await this.alertController.create({
      header: "Invalid authentication",
      message:
        "Invalid authentication credentials passed in order to progress getting verifiable share-credential",
      cssClass: "custom-alert",
      buttons: [
        {
          text: "OK",
          role: "confirm",
          cssClass: "alert-button-confirm",
        },
      ],
    });

    await alert.present();
  }
}
