import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { SharePinPage } from "./share-pin.page";

const routes: Routes = [
  {
    path: "",
    component: SharePinPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SharePinPageRoutingModule {}
