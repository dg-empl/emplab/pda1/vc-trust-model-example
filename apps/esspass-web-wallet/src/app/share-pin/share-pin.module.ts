import { NgModule } from "@angular/core";
import { CommonModule, NgOptimizedImage } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { SharePinPageRoutingModule } from "./share-pin-routing.module";

import { SharePinPage } from "./share-pin.page";
import { HeaderInfoModule } from "../header-info/header-info.module";
import { FooterModule } from "../footer/footer.module";
import { SharePinContentComponent } from "./share-pin-content/share-pin-content.component";
import { ProfileContentModule } from "../profile-content/profile-content.module";
import { PinCodeComponent } from "./share-pin-content/pin-code/pin-code.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharePinPageRoutingModule,
    HeaderInfoModule,
    FooterModule,
    ProfileContentModule,
    NgOptimizedImage,
  ],
  declarations: [SharePinPage, SharePinContentComponent, PinCodeComponent],
})
export class SharePinPageModule {}
