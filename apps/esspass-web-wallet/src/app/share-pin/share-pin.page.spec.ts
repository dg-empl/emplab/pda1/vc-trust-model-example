/**
 * @jest-environment jsdom
 */
import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { SharePinPage } from "./share-pin.page";
import { MockComponent } from "ng2-mock-component";

describe("SharePinPage", () => {
  let component: SharePinPage;
  let fixture: ComponentFixture<SharePinPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        SharePinPage,
        MockComponent({ selector: "esspass-web-wallet-share-pin-content" }),
        MockComponent({ selector: "esspass-web-wallet-footer" }),
        MockComponent({ selector: "esspass-web-wallet-header-info" }),
        MockComponent({ selector: "esspass-web-wallet-profile-content", inputs: ["backButton"] })
      ],
      imports: [IonicModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(SharePinPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
