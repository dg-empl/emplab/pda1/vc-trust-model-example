import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { EsspassWebWalletModule } from './app/esspass-web-wallet.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

console.log("ENV_NAME : ", process.env['ENVIRONMENT_NAME']);
console.log("APP_VERSION : ", process.env['APP_VERSION']);

platformBrowserDynamic()
  .bootstrapModule(EsspassWebWalletModule)
  .catch((err) => console.error(err));
