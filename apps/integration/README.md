## Integration issuance tests for v3 specification

**There are two possibilities to run integration tests for v3 issuance:**

1.  Run the tests locally ( docker or localhost )
2.  Run the tests in a memory using `supatest` lib

## Run the tests locally

### Prerequisites

1. Get the local ip address of your machine (e.g. `ifconfig getifaddr en0` for mac)
2. Put the ip address into .env files in the root of the project: `issuer-backoffice/src/.env` , `auth-server/src/.env` and `integration/.env`

For auth-server:

    CONFORMANCE_DOMAIN=http://172.17.131.230:4300

For issuance-backoffice:

    ISSUER_URI=http://172.17.131.230:4299
    AUTH_URI=http://172.17.131.230:4300

For integration:

    ISSUER_SERVICE_URL=http://172.17.131.230:4299

3. Make sure you have the document record in esspass-nssi-be db, collection pda1documents, and point .env in integration to it.

For integration:

    PDA_ID=65031da07a89a6102da3560f

### Run the tests locally

1. Run the `yarn integration-test-env` task using `cross-env` library

   ```
     cross-env \
        ISSUER_SERVICE_URL=http://$(ipconfig getifaddr en0):4299 \
        ISSUER_URI=http://$(ipconfig getifaddr en0):4299 \
        AUTH_URI=http://$(ipconfig getifaddr en0):4300 \
        CONFORMANCE_DOMAIN=http://$(ipconfig getifaddr en0):4300 \
     yarn integration-test-env
   ```

   For linux environment run instead

   ```
    cross-env \
        ISSUER_SERVICE_URL=http://$(hostname -i):4299 \
        ISSUER_URI=http://$(hostname -i):4299 \
        AUTH_URI=http://$(hostname -i):4300 \
        CONFORMANCE_DOMAIN=http://$(hostname -i):4300 \
    yarn integration-test-env
   ```

```

## Run the tests in a memory using `supatest` lib

### Prerequisites

1. Setup memory version of mongo-server

```

    //open mongo in memory
    const mongoServer = await MongoMemoryServer.create({
      instance: {
        port: 27017,
        ip: "127.0.0.1",
        dbName: "esspass-nssi-be",
      },
      auth: {
        customRootName: "root",
        customRootPwd: "password",
      },
    });

```

2. Prepare test data for mongo internal database.

```

    // create test PDA1
      await createTestData("127.0.0.1", 27017, "esspass-nssi-be", {
        collection: "pda1documents",
        data: pda1TestData,
        id: pdaId,
        check: { requestFormId: "64e23c5c0648af12d05e3104" },
      });

````

```angular2html
   export const createTestData = async (
    mongoIpAddress: string,
    mongoDbPort: number,
    dbName: string,
    testData: Record<string, any>
    ) => {
      // Connect to the container using the MongoDB driver
      let client;
      try {
        client = await MongoClient.connect(`mongodb://root:password@${mongoIpAddress}:${mongoDbPort}`);
        const db = client.db(dbName);

        const col = db.collection(testData.collection);
        const id = testData.id;
        const document = { ...testData.data, _id: new ObjectId(id) };
        const result = await col.insertOne(document);
        const cursor = await col.find(testData.check).toArray();

        if (!result || !cursor || cursor.length === 0 || cursor.length > 1) {
            throw new Error("Test data not inserted");
        }
      } catch (e) {
        console.error(e);
        throw new Error(e);
      } finally {
          await client.close();
      }
   };
```
2. Make sure you have the document record in esspass-nssi-be db, collection pda1documents, and point .env in integration to it.

For integration:

    PDA_ID=65031da07a89a6102da3560f


3. Run the `yarn integration-test-mem` task using `cross-env` library

   ```
     cross-env \
        ISSUER_SERVICE_URL=http://$(ipconfig getifaddr en0):4299 \
        ISSUER_URI=http://$(ipconfig getifaddr en0):4299 \
        AUTH_URI=http://$(ipconfig getifaddr en0):4300 \
        CONFORMANCE_DOMAIN=http://$(ipconfig getifaddr en0):4300 \
     yarn integration-test-mem
   ```
````
