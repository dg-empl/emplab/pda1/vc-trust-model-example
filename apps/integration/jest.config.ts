/* eslint-disable */
export default {
  displayName: "integration",
  preset: "../../jest.preset.js",
  globals: {},
  transform: {
    "^.+\\.[tj]s$": ["ts-jest", { tsconfig: "<rootDir>/tsconfig.spec.json" }],
  },
  testMatch: [
    "**/+(*.)+(spec|test).+(ts|js)?(x)",
    "!**/+(*.)+(integration).+(spec|test).+(ts|js)?(x)",
  ],
  testEnvironment: "node",
  moduleFileExtensions: ["ts", "js", "html"],
  coverageDirectory: "../../coverage/apps/integration",
};
