/* eslint-disable */
export default {
  displayName: "integrations",
  preset: "../../jest.preset.js",
  globals: {
    globals: {
      "process.env.version": "#{args:version}",
    },
  },
  testMatch: ["<rootDir>/src/**/*.integration.aut.spec.ts"],
  testEnvironment: "node",
  transform: {
    "^.+\\.[tj]s$": "ts-jest",
  },
  moduleFileExtensions: ["ts", "js", "html"],
};
