import "dotenv/config";
import { generateKeyPair, exportJWK, SignJWT } from "jose";
import { util as keyDidHelpers } from "@cef-ebsi/key-did-resolver";
import { URLSearchParams } from "node:url";
import * as qs from "qs";
import {
  CredentialIssuerMetadata,
  CredentialOffer,
  CredentialOfferPayload,
  CredentialResponse,
  getUserPinV3,
  OPMetadata,
  TokenResponse,
} from "@esspass-web-wallet/crypto-lib";
import { PostTokenPreAuthorizedCodeDto } from "@esspass-web-wallet/domain-lib";
import { verifyCredentialJwt, VerifyCredentialOptions } from "@cef-ebsi/verifiable-credential";

import { logger } from "@esspass-web-wallet/logging-lib";

const log = logger("integration");

import app from "../../../../issuer-backoffice/src/main";

import { pda1TestData } from "./utils";
import { MongoMemoryServer } from "mongodb-memory-server";
import * as dbUtils from "@esspass-web-wallet/backend-utils-lib";

import { createTestData, purgeTestData } from "@esspass-web-wallet/backend-utils-lib";
import * as request from "supertest";
import { authServer } from "../../../../auth-server/src/main";

describe("IssuancePreAuthDeferredPDA1Flow", () => {
  jest.setTimeout(10_000);

  const DIDR_API_PATH = "/did-registry/v4/identifiers";
  const TIR_API_PATH = "/trusted-issuers-registry/v4/issuers";
  const TPR_API_PATH = "/trusted-policies-registry/v2/users";

  let clientDid;
  let clientKid;
  let globalClientPrivateKey;

  let issuerServiceUrl;
  let issuerKid;
  let apiUrlPrefix;
  let parsedCredentialOffer;
  let requestParams;
  let credentialOfferPayload;
  let credentialIssuerConfig: CredentialIssuerMetadata;
  let authConfig;

  // /token access token response data
  let globalAccessToken;
  let globalcNonce;

  // /credential acceptance token response data
  let globalAcceptanceToken;

  let mongoServer: MongoMemoryServer;

  // test PDA1 given from env file
  const pdaId = process.env.PDA_ID;

  function getPathnameWithoutPrefix(uri: string) {
    return new URL(uri).pathname.replace(issuerServiceUrl, "");
  }

  beforeAll(async () => {
    issuerServiceUrl = process.env.ISSUER_SERVICE_URL;
    log.debug("issuerServiceUrl : " + process.env.ISSUER_SERVICE_URL);
    log.debug("issuerUri : " + process.env.ISSUER_URI);
    log.debug("authUri : " + process.env.AUTH_URI);
    log.debug("conformanceDomain : " + process.env.CONFORMANCE_DOMAIN);
    issuerKid = process.env.ISSUER_KID;
    apiUrlPrefix = process.env.API_URL_PREFIX;

    //open mongo in memory
    mongoServer = await dbUtils.createMongoMemoryServer();
    log.debug("Mongo Memory Server Started()");

    // create test PDA1
    await createTestData(mongoServer.getUri(), "esspass-nssi-be", {
      collection: "pda1documents",
      data: pda1TestData,
      id: pdaId,
      check: { requestFormId: "6540978eb361ee4b2e9bc1d7" },
    });
    log.debug("Test data added to mongo db : " + pdaId);
  });

  afterAll(async () => {
    await purgeTestData(mongoServer.getUri(), "esspass-nssi-be", {
      collection: "pda1documents",
    });
    log.debug("test data purged");

    await dbUtils.closeDatabase(mongoServer);
    log.debug("mongoServer.stop()");
  });

  it("1. initiate-credential-offer", async () => {
    expect(true).toBe(true);
  });

  it("1. initiate-credential-offer", async () => {
    const clientKeyPair = await generateKeyPair("ES256");
    const clientPublicKeyJwk = await exportJWK(clientKeyPair.publicKey);
    clientDid = keyDidHelpers.createDid(clientPublicKeyJwk);
    const methodSpecificIdentifier = clientDid.replace("did:key:", "");
    clientKid = `${clientDid}#${methodSpecificIdentifier}`;
    const { privateKey: clientPrivateKey } = clientKeyPair;
    globalClientPrivateKey = clientPrivateKey;

    // Initiate credential offer from Issuer
    requestParams = {
      credential_type: "VerifiablePortableDocumentA1",
      credential_offer_endpoint: "openid-credential-offer://",
      client_id: clientDid,
      pdaId: pdaId,
    } as const;

    /**
     * 1. initiate-credential-offer
     *
     */

    // http://localhost:4299/api/v1/issuer/initiate-credential-offer?
    //  credential_type=VerifiablePortableDocumentA1&
    //  credential_offer_endpoint=openid-credential-offer%3A%2F%2F&
    //  client_id=did%3Akey%3Az2dmzD81cgPx8Vki7JbuuMmF....
    //  pdaId=775488548DH343

    const response = await request(app).get(
      `${apiUrlPrefix}/issuer/initiate-credential-offer?${new URLSearchParams(
        requestParams
      ).toString()}`
    );

    expect(response.status).toBe(200);
    expect(response.text).toStrictEqual(
      expect.stringContaining(requestParams.credential_offer_endpoint)
    );

    // openid-credential-offer://?
    //  credential_offer_uri=https%3A%2F%2Fapi-conformance.ebsi.eu%2Fconformance
    //  %2Fv3%2Fissuer%2Foffers%2F225f2d90-bf18-4acb-8284-1567918a485e

    const { search } = new URL(response.text);

    parsedCredentialOffer = qs.parse(search.slice(1)) satisfies CredentialOffer;

    expect(parsedCredentialOffer).toStrictEqual({
      credential_offer_uri: expect.any(String),
    });
  });

  it("2. get credential_offer_uri", async () => {
    /**
     * 2. get credential_offer_uri
     */
    const response = await request(app).get(
      getPathnameWithoutPrefix(parsedCredentialOffer.credential_offer_uri)
    );

    expect(response.status).toBe(200);
    credentialOfferPayload = JSON.parse(response.text) satisfies CredentialOfferPayload;

    // {
    //   credential_issuer: 'http://localhost:4299',
    //     credentials: [ { format: 'jwt_vc', types: [Array], trust_framework: [Object] } ],
    //   grants: {
    //   'urn:ietf:params:oauth:grant-type:pre-authorized_code': {
    //     'pre-authorized_code': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImtpZCI6Ik...'
    //       user_pin_required: true
    //   }
    // }

    log.debug("credentialOfferPayload : " + JSON.stringify(credentialOfferPayload));

    expect(credentialOfferPayload).toStrictEqual({
      credential_issuer: expect.any(String), // credential_issuer: issuerServiceUrl,
      credentials: [
        {
          format: "jwt_vc",
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          types: ["VerifiableCredential", "VerifiableAttestation", requestParams.credential_type],
        },
      ],
      grants: {
        "urn:ietf:params:oauth:grant-type:pre-authorized_code": {
          "pre-authorized_code": expect.any(String),
          user_pin_required: true,
        },
      },
    });
  });

  it("3. get openid-credential-issuer end point", async () => {
    /**
     * 3. get .well-known/openid-credential-issuer end-point
     */

    const response = await request(app).get(`/.well-known/openid-credential-issuer`);

    expect(response.status).toBe(200);
    credentialIssuerConfig = JSON.parse(response.text) satisfies CredentialIssuerMetadata;
  });

  it("4. get openid-configuration from auth-server", async () => {
    if (!credentialIssuerConfig.authorization_server)
      throw new Error("authorization_server not defined");

    const response = await request(authServer).get(`/.well-known/openid-configuration`);

    expect(response.status).toBe(200);
    authConfig = JSON.parse(response.text) satisfies OPMetadata;
    expect(authConfig).toBeDefined();
  });

  it("5. get access token from auth-server from /auth/token", async () => {
    //given
    const preAuthorizedCode = credentialOfferPayload.grants[
      "urn:ietf:params:oauth:grant-type:pre-authorized_code"
    ]?.["pre-authorized_code"] as string;

    const tokenRequestQueryParams = {
      grant_type: "urn:ietf:params:oauth:grant-type:pre-authorized_code",
      "pre-authorized_code": preAuthorizedCode,
      user_pin: getUserPinV3(clientDid),
    } satisfies PostTokenPreAuthorizedCodeDto;

    log.debug(
      "authConfig.token_endpoint : " + getPathnameWithoutPrefix(`${authConfig.token_endpoint}`)
    );

    //when
    //  /conformance/v3/auth/token
    const response = await request(authServer)
      .post(getPathnameWithoutPrefix(`${authConfig.token_endpoint}`))
      .set("Content-Type", "application/x-www-form-urlencoded")
      .send(new URLSearchParams(tokenRequestQueryParams).toString());

    log.debug("response : " + response.text);

    //then
    expect(response.status).toBe(200);

    const { access_token: accessToken, c_nonce: cNonce } = JSON.parse(
      response.text
    ) satisfies TokenResponse;

    globalAccessToken = accessToken;
    globalcNonce = cNonce;

    expect(globalAccessToken).toBeDefined();
    expect(globalcNonce).toBeDefined();
  });

  it("6. get acceptance token from /credential", async () => {
    //given
    const proofJwt = await new SignJWT({
      nonce: globalcNonce,
    })
      .setProtectedHeader({
        typ: "openid4vci-proof+jwt",
        alg: "ES256",
        kid: clientKid,
      })
      .setIssuer(clientDid)
      .setAudience(issuerServiceUrl)
      .setIssuedAt()
      .setExpirationTime("5m")
      .sign(globalClientPrivateKey);

    const credentialRequestParams = {
      types: ["VerifiableCredential", "VerifiableAttestation", requestParams.credential_type],
      format: "jwt_vc",
      proof: {
        proof_type: "jwt",
        jwt: proofJwt,
      },
    };

    //when
    const response = await request(app)
      .post(getPathnameWithoutPrefix(`${credentialIssuerConfig.credential_endpoint}`))
      .set("Authorization", `Bearer ${globalAccessToken}`)
      .send(credentialRequestParams);

    //then
    expect(response.status).toBe(200);
    expect(JSON.parse(response.text)).toStrictEqual({
      acceptance_token: expect.any(String),
    });

    const { acceptance_token: acceptanceToken } = JSON.parse(response.text) as {
      acceptance_token: string;
    };

    globalAcceptanceToken = acceptanceToken;
  });

  it("7. get verifiable credential from /credential_deferred", async () => {
    // given
    // when
    let response;
    try {
      await request(app)
        .post(getPathnameWithoutPrefix(credentialIssuerConfig.deferred_credential_endpoint))
        .send({}) // empty body
        .set("Authorization", `Bearer ${globalAcceptanceToken}`);
    } catch (e) {
      // then
      // console.log(e.response);
      expect(e.response.status).toBe(418);
      // expect(e.response.status).toBe(400);
    }

    // Wait 3.5 seconds (> 3 seconds)
    // when
    await new Promise(r => {
      setTimeout(r, 3500);
    });

    response = await request(app)
      .post(getPathnameWithoutPrefix(credentialIssuerConfig.deferred_credential_endpoint))
      .send({}) // empty body
      .set("Authorization", `Bearer ${globalAcceptanceToken}`);

    // then
    expect(response.status).toBe(200);
    expect(JSON.parse(response.text)).toStrictEqual({
      format: "jwt_vc",
      credential: expect.any(String),
    });

    // verify VC
    const ebsiAuthority = process.env.DOMAIN.replace(/^https?:\/\//, ""); // remove http protocol scheme
    const didRegistry = `${process.env.DOMAIN}${DIDR_API_PATH}`;
    const trustedIssuersRegistry = `${process.env.DOMAIN}${TIR_API_PATH}`;
    const trustedPoliciesRegistry = `${process.env.DOMAIN}${TPR_API_PATH}`;

    const options: VerifyCredentialOptions = {
      ebsiAuthority,
      ebsiEnvConfig: {
        didRegistry,
        trustedIssuersRegistry,
        trustedPoliciesRegistry,
      },
    };

    const { credential } = JSON.parse(response.text) satisfies CredentialResponse;
    const vcPayload = await verifyCredentialJwt(credential, options);

    expect(vcPayload).toStrictEqual({
      "@context": ["https://www.w3.org/2018/credentials/v1"],
      id: expect.stringContaining("vc:ebsi:conformance#"),
      type: ["VerifiableCredential", "VerifiableAttestation", requestParams.credential_type],
      issuer: issuerKid.split("#")[0],
      issuanceDate: expect.any(String),
      issued: expect.any(String),
      validFrom: expect.any(String),
      expirationDate: expect.any(String),
      credentialSubject: {
        id: expect.stringMatching(/^did:key:[a-zA-Z0-9]+/),
        section1: {
          personalIdentificationNumber: expect.any(String),
          sex: "01",
          surname: expect.any(String), //"Dalton",
          surnameAtBirth: expect.any(String), //"Dalton",
          forenames: expect.any(String), //"Joe Jack William Averell",
          dateBirth: expect.any(String), // "1985-08-15",
          nationalities: expect.any(Array), // ["BE"],
          placeBirth: expect.any(Object), //  Object {
          // +         "countryCode": "AT",
          // +         "region": "Vienne
          // +         "town": "Vienne
          // +       },
          stateOfResidenceAddress: {
            streetNo: expect.any(String), //"sss, nnn ",
            postCode: expect.any(String), //"ppp",
            town: expect.any(String), //"ccc",
            countryCode: expect.any(String), //"BE",
          },
          stateOfStayAddress: {
            streetNo: expect.any(String), //"sss, nnn ",
            postCode: expect.any(String), //"ppp",
            town: expect.any(String), //"ccc",
            countryCode: expect.any(String), //"BE",
          },
        },
        section2: {
          memberStateWhichLegislationApplies: expect.any(String), //"DE",
          startingDate: expect.any(String), //"2022-10-09",
          endingDate: expect.any(String), // "2022-10-29",
          certificateForDurationActivity: true,
          determinationProvisional: expect.any(Boolean), //true,
          transitionRulesApplyAsEC8832004: expect.any(Boolean), //false,
        },
        section3: {
          postedEmployedPerson: expect.any(Boolean), //false,
          employedTwoOrMoreStates: expect.any(Boolean), //true,
          postedSelfEmployedPerson: expect.any(Boolean), //true,
          selfEmployedTwoOrMoreStates: expect.any(Boolean), //true,
          civilServant: expect.any(Boolean), //true,
          contractStaff: expect.any(Boolean), //false,
          mariner: expect.any(Boolean), //false,
          employedAndSelfEmployed: expect.any(Boolean), //false,
          civilAndEmployedSelfEmployed: expect.any(Boolean), //true,
          flightCrewMember: expect.any(Boolean), //false,
          exception: expect.any(Boolean), //false,
          exceptionDescription: expect.any(String), //false,,
          workingInStateUnder21: expect.any(Boolean), //false,
        },
        section4: {
          employee: expect.any(Boolean), //false,
          selfEmployedActivity: true,
          nameBusinessName: expect.any(String), //"1",
          registeredAddress: {
            streetNo: expect.any(String), //"1, 1 1",
            postCode: expect.any(String), //"1",
            town: expect.any(String), //"1",
            countryCode: expect.any(String), //"DE",
          },
          employerSelfEmployedActivityCodes: expect.any(Array), //["1"],
        },
        section5: {
          noFixedAddress: expect.any(Boolean), //true,
          // workPlaceAddresses: [
          //   {
          //     address: {
          //       streetNo: expect.any(String), //"1, 1 1",
          //       postCode: expect.any(String), //"1",
          //       town: expect.any(String), //"1",
          //       countryCode: expect.any(String), //"DE",
          //     },
          //     seqno: expect.any(Number), //1,
          //   },
          // ],
          // workPlaceNames: [
          //   {
          //     companyNameVesselName: expect.any(String), //"1",
          //     seqno: expect.any(Number), //1,
          //   },
          // ],
        },
        section6: {
          name: expect.any(String), //"National Institute for the Social Security of the Self-employed (NISSE)",
          address: {
            streetNo: expect.any(String), //"Quai de Willebroeck 35",
            postCode: "1000",
            town: expect.any(String), //"Bruxelles",
            countryCode: "BE",
          },
          institutionID: expect.any(String), //"NSSIE/INASTI/RSVZ",
          officeFaxNo: expect.any(String), //"",
          officePhoneNo: expect.any(String), //"0800 12 018",
          email: expect.any(String), //"info@rsvz-inasti.fgov.be",
          date: expect.any(String), //"2022-10-28",
          signature: "Official signature",
        },
      },
      credentialSchema: {
        id: "https://api-pilot.ebsi.eu/trusted-schemas-registry/v2/schemas/z5qB8tydkn3Xk3VXb15SJ9dAWW6wky1YEoVdGzudWzhcW",
        type: "FullJsonSchemaValidator2021",
      },
      // termsOfUse: {
      // id: configService.get<string>("issuerAccreditationUrl"),
      // id: 1,
      // type: "IssuanceCertificate",
      // },
    });
  });
});
