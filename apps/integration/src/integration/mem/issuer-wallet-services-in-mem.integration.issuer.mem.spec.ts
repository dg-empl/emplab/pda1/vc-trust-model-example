import { logger } from "@esspass-web-wallet/logging-lib";
import { MongoMemoryServer } from "mongodb-memory-server";
import supertest from "supertest";

const log = logger("integration");

import app from "../../../../issuer-wallet/src/main";

import {
  issuerIdentitiesTestData,
  createTestData,
  purgeTestData,
  credentialStatusMockData,
} from "./utils";
import axios from "axios";
import { AxiosRequestConfig } from "axios";

describe("IssuancePreAuthDeferredPDA1Flow", () => {
  jest.setTimeout(10_000);

  let globalAcceptanceToken;

  let mongoServer;

  let idp;
  let idp_realm;
  let idp_client_id;
  let idp_client_secret;
  let idp_username;
  let idp_password;

  let issuer_did_stringified;

  // test wallet given from env file
  const issuerWalletIdentityId = process.env.ISSUER_WALLET_ID;

  beforeAll(async () => {
    idp = process.env.IDP;
    idp_realm = process.env.IDP_REALM;

    idp_client_id = process.env.IDP_CLIENT_ID;
    idp_client_secret = process.env.IDP_CLIENT_SECRET;

    idp_username = process.env.IDP_USERNAME;
    idp_password = process.env.IDP_PASSWORD;

    log.debug("idp : " + process.env.IDP);
    log.debug("idp_realm : " + process.env.IDP_REALM);
    log.debug("idp_client_id : " + process.env.IDP_CLIENT_ID);

    //open mongo in memory
    mongoServer = await MongoMemoryServer.create({
      instance: {
        port: 27017,
        ip: "127.0.0.1",
        dbName: "esspass-nssi-wallet",
      },
      auth: {
        customRootName: "root",
        customRootPwd: "password",
      },
    });

    log.debug("mongoServer.started()");

    // create test isser identities
    await createTestData("127.0.0.1", 27017, "esspass-nssi-wallet", {
      collection: "issueridentities",
      data: issuerIdentitiesTestData,
      id: issuerWalletIdentityId,
      check: { did: "did:ebsi:zhkNKqkC86BZuzrdGEU9acH" },
    });
    log.debug("test data issuer-identity added");
  });

  afterAll(async () => {
    await purgeTestData("127.0.0.1", 27017, "esspass-nssi-wallet", {
      collection: "issueridentities",
    });
    log.debug("test data purged");

    await mongoServer.stop();
    log.debug("mongoServer.stop()");
  });

  it("1. get-issuer-wallet", async () => {
    const response = await supertest(app).get(`/get-issuer-wallet`);
    log.debug("response status:" + response.status);
    let styomgBody = JSON.stringify(response.body);
    log.debug("response body:" + styomgBody);

    expect(response.status).toBe(200);
  });

  it("2. get access token from https://idp.dev.esspass-poc.eu", async () => {
    // get access token
    const idpData = {
      idp: idp,
      realm: idp_realm,
      client_id: idp_client_id,
      client_secret: idp_client_secret,
      username: idp_username,
      password: idp_password,
    };

    const response = await getIdpToken(idpData);

    const access_token = response.data.access_token;
    log.debug("response access_token:" + JSON.stringify(response.data));
    globalAcceptanceToken = access_token;
    expect(response.status).toBe(200);
  });

  it("3. ebsi-identifier-by-id?", async () => {
    const queryParams = {
      id: "did:ebsi:zfYr725YkjDLxuFejRz8hsh",
    };

    const response = await supertest(app)
      .get(`/ebsi-identifier-by-id`)
      .auth(globalAcceptanceToken, { type: "bearer" })
      .set("accept", "application/json")
      .set("Content-Type", "application/json")
      .query(queryParams);

    log.debug("response status:" + response.status);
    let styomgBody = JSON.stringify(response);

    log.debug("response body:" + styomgBody);
    log.debug("response location:" + response.header.location);

    response.header.location = response.header.location;

    expect(response.status).toBe(200);
  });

  it("4. issuer-did", async () => {
    const response = await supertest(app)
      .get(`/issuer-did`)
      .auth(globalAcceptanceToken, { type: "bearer" })
      .set("accept", "application/json")
      .set("Content-Type", "application/json");

    log.debug("response status:---------------" + response.status);
    issuer_did_stringified = response.body.body;

    log.debug("response body:----------------" + issuer_did_stringified);

    response.header.location = response.header.location;

    expect(response.status).toBe(200);
  });

  it("5. ebsi-identifiers", async () => {
    const response = await supertest(app)
      .get(`/ebsi-identifiers`)
      .auth(globalAcceptanceToken, { type: "bearer" })
      .set("accept", "application/json")
      .set("Content-Type", "application/json");

    log.debug("response status:" + response.status);
    let styomgBody = JSON.stringify(response);
    log.debug("response body:" + styomgBody);
    expect(response.status).toBe(200);
  }, 30000);

  it("6. create-holder-wallet", async () => {
    const response = await supertest(app)
      .post(`/create-holder-wallet`)
      .auth(globalAcceptanceToken, { type: "bearer" })
      .set("accept", "application/json")
      .set("Content-Type", "application/json");

    log.debug("response status:" + response.status);
    let styomgBody = JSON.stringify(response);
    log.debug("response body:" + styomgBody);
    expect(response.status).toBe(200);
  });

  it("7. create-vc-from-cs", async () => {
    let validFromDate = new Date();
    let result, credentialStatus;
    credentialStatus = credentialStatusMockData;
    log.trace("MOCK redential status created");

    const response = await supertest(app)
      .post(`/create-vc-from-cs`)
      .auth(globalAcceptanceToken, { type: "bearer" })
      .set("accept", "application/json")
      .set("Content-Type", "application/json")
      .send(credentialStatus);
    log.debug("response status:" + response.status);
    let styomgBody = JSON.stringify(response);
    log.debug("response body:" + styomgBody);
    expect(response.status).toBe(200);
  });
});

export const getIdpToken = async (env: Record<string, any>) => {
  const config: AxiosRequestConfig = {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
  };

  const data = new URLSearchParams({
    scope: "openid",
    grant_type: "password",
    ...env,
  });

  return await axios.post(
    env.idp + `/realms/${env.realm}/protocol/openid-connect/token`,
    data.toString(),
    config
  );
};
