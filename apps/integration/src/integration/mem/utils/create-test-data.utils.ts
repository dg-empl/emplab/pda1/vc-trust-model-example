import { MongoClient, ObjectId } from "mongodb";
import { logger, Logger } from "@esspass-web-wallet/logging-lib";

const log = logger("integration");

export const createTestData = async (
  mongoIpAddress: string,
  mongoDbPort: number,
  dbName: string,
  testData: Record<string, any>
) => {
  // Connect to the container using the MongoDB driver
  let client;
  try {
    client = await MongoClient.connect(`mongodb://root:password@${mongoIpAddress}:${mongoDbPort}`);
    const db = client.db(dbName);

    const col = db.collection(testData.collection);
    const id = testData.id;
    const document = { ...testData.data, _id: new ObjectId(id) };
    const result = await col.insertOne(document);
    const cursor = await col.find(testData.check).toArray();

    if (!result || !cursor || cursor.length === 0 || cursor.length > 1) {
      throw new Error("Test data not inserted");
    }
  } catch (e) {
    console.error(e);
    throw new Error(e);
  } finally {
    await client.close();
  }
};

export const purgeTestData = async (
  mongoIpAddress: string,
  mongoDbPort: number,
  dbName: string,
  testData: Record<string, any>
) => {
  // Connect to the container using the MongoDB driver
  let client;
  try {
    client = await MongoClient.connect(`mongodb://root:password@${mongoIpAddress}:${mongoDbPort}`);
    const db = client.db(dbName);
    const col = db.collection(testData.collection);
    const result = await col.drop();

    if (!result) {
      throw new Error("Collection not dropped");
    }
  } catch (e) {
    console.error(e);
    throw new Error(e);
  } finally {
    await client.close();
  }
};

export const printTokenParts = (token, log: Logger) => {
  let headerB64u: any;
  let payloadB64u: any;

  if (typeof token === "string") {
    const parts = token.split(".");
    if (parts.length === 3 || parts.length === 5) {
      headerB64u = parts[0];
      payloadB64u = parts[1];
    }
  }

  log.debug("decodedHeader : " + Buffer.from(headerB64u, "base64").toString());
  log.debug("decodedPayload : " + Buffer.from(payloadB64u, "base64").toString());
};
