export const credentialStatusMockData ={
      "@context": [
        "https://www.w3.org/2018/credentials/v1"
      ],
      id: "urn:uuid:941e95ce-37bb-4d38-961b-5f259944da7c",
      type: [
        "VerifiableCredential",
        "VerifiableAttestation"
      ],
      issuanceDate: "2023-10-04T11:53:51Z",
      validFrom: "2023-10-04T11:53:51Z",
      issued: "2023-10-04T11:53:51Z",
      credentialStatus: {
        id: "did:ebsi:zsqBYQuKunNdr3XfGfPUvU7",
        type: "StatusList2021Entry",
        statusPurpose: "revocation",
        statusListIndex: "38",
        statusListCredential: "https://localhost:20080/credentials/status/1#list"
      },
      credentialSubject: {
        section1: {
          personalIdentificationNumber: "34534",
          sex: "01",
          surname: "Smith",
          forenames: "John",
          dateBirth: "1111-11-11",
          surnameAtBirth: "fdg",
          placeBirth: {
            town: "asdfdfs",
            region: "asdfdfs",
            countryCode: "EE"
          },
          nationalities: [
            "EE"
          ],
          stateOfResidenceAddress: {
            streetNo: "dfghdfgh, 33 234",
            postCode: "345",
            town: "rettr",
            countryCode: "ES"
          },
          stateOfStayAddress: {
            streetNo: "dfghdfgh, 33 234",
            postCode: "345",
            town: "rettr",
            countryCode: "ES"
          }
        },
        section2: {
          memberStateWhichLegislationApplies: "BG",
          startingDate: "2023-11-01",
          endingDate: "2023-12-31",
          certificateForDurationActivity: true,
          determinationProvisional: false,
          transitionRulesApplyAsEC8832004: false
        },
        section3: {
          postedEmployedPerson: false,
          employedTwoOrMoreStates: false,
          postedSelfEmployedPerson: true,
          selfEmployedTwoOrMoreStates: true,
          civilServant: true,
          contractStaff: false,
          mariner: false,
          employedAndSelfEmployed: false,
          civilAndEmployedSelfEmployed: true,
          flightCrewMember: false,
          exception: false,
          exceptionDescription: "",
          workingInStateUnder21: false
        },
        section4: {
          employee: false,
          selfEmployedActivity: true,
          employerSelfEmployedActivityCodes: [
            "45345"
          ],
          nameBusinessName: "gdfghfg",
          registeredAddress: {
            streetNo: "gfhfgh, 435 45",
            postCode: "444",
            town: "dfg",
            countryCode: "HR"
          }
        },
        section5: {
          "noFixedAddress": true
        },
        section6: {
          name: "Office National de Sécurité Sociale / Rijksdienst voor Sociale Zekerheid",
          address: {
            streetNo: "Main Street 1",
            postCode: "1000",
            town: "Brussels",
            countryCode: "BE"
          },
          institutionID: "NSSI-BE-01",
          officePhoneNo: "0800 12345",
          officeFaxNo: "0800 98765",
          email: "esspass.noreply@gmail.com",
          date: "2023-10-04",
          signature: "Official signature"
        },
        id: "did:ebsi:zfjdmpnHpGvhBgPAjczngRGbYZo7e3Tg3D3GEFLgDkYFM"
      },
      credentialSchema: {
        id: "https://api-pilot.ebsi.eu/trusted-schemas-registry/v2/schemas/0x47c61667281cc3883cca001bcac7038237c85d0e8f61f139652375c330a99063",
        type: "FullJsonSchemaValidator2021"
      }
    }