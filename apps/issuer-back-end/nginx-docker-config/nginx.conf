events {
  worker_connections  1024;  ## Default: 1024
}

http {
    server {
        listen       80;
        server_name  localhost;
        root /usr/share/nginx/html;

        include /etc/nginx/mime.types;

        # X-Frame-Options is to prevent from clickJacking attack
        add_header X-Frame-Options SAMEORIGIN;

        # disable content-type sniffing on some browsers.
        add_header X-Content-Type-Options nosniff;

        # This header enables the Cross-site scripting (XSS) filter
        add_header X-XSS-Protection "1; mode=block";

        add_header Referrer-Policy "no-referrer-when-downgrade";

        # enable gzip compression
        # Turns on/off the gzip compression.
        gzip on;

        # Compression level (1-9).
        # 5 is a perfect compromise between size and cpu usage, offering about
        # 75% reduction for most ascii files (almost identical to level 9).
        gzip_comp_level    5;

        # The minimum size file to compress the files.
        gzip_min_length  1100;

        # Set the buffer size of gzip, 4 32k is good enough for almost everybody.
        gzip_buffers  4 32k;

        # Compress data even for clients that are connecting to us via proxies,
        # identified by the "Via" header (required for CloudFront).
        gzip_proxied       any;

        # This directive let you specify which file types should be compressed, in this case plain text, js files, xml and #css.
        gzip_types    text/plain application/javascript text/xml text/css;

        # Enables response header of “Vary: Accept-Encoding
        gzip_vary on;
        # end gzip configuration

        location / {
            try_files $uri $uri/ /index.html;
            expires -1;
            etag off;
            add_header Cache-Control "no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0";
        }
    }
}

