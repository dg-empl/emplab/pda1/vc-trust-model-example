import { TestBed } from "@angular/core/testing";
import { AppComponent } from "./app.component";
import { RouterTestingModule } from "@angular/router/testing";
import { MockComponent } from "ng2-mock-component";

describe("AppComponent", () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [
        AppComponent,
        MockComponent({ selector: "issuer-header-menu" }),
        MockComponent({ selector: "issuer-footer", inputs: ["officeName"] }),
      ],
    }).compileComponents();
  });

  it(`should app root component to be defined`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeDefined();
  });
});
