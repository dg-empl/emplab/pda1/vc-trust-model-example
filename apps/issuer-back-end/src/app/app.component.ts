import { Component, inject, NgZone } from "@angular/core";
import {
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  Router,
} from "@angular/router";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";
import { delay } from "rxjs";
import { PopoverDetectorService } from "./manage-pda1-requests/manage-pda1-requests-home/request-form-table/application/popover-detector.service";

@Component({
  selector: "issuer-back-end-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  private loadingModule = new BehaviorSubject<boolean>(false);
  loadingModule$ = this.loadingModule.asObservable();

  popoverDetector = inject(PopoverDetectorService);
  private router = inject(Router);

  test = "test";

  ngOnInit(): void {
    this.router.events.pipe(delay(0)).subscribe(event => {
      if (event instanceof NavigationStart) {
        this.loadingModule.next(true);
      }
      if (event instanceof NavigationEnd) {
        this.loadingModule.next(false);
      }
      if (event instanceof NavigationCancel) {
        this.loadingModule.next(false);
      }
      if (event instanceof NavigationError) {
        this.loadingModule.next(false);
      }
    });
  }
}
