import { APP_INITIALIZER, NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
import { IssuerBackEndRoutingModule } from "./app.routes";
import { IonicModule } from "@ionic/angular";
import { HeaderMenuComponent } from "./header-menu/header-menu.component";
import { CommonWebLibModule } from "@esspass-web-wallet/common-web-lib";
import {
  APP_CONFIG,
  APP_DI_CONFIG,
  initializeKeycloak,
} from "@esspass-web-wallet/security-web-lib";
import { KeycloakAngularModule, KeycloakService } from "keycloak-angular";
import { HomeComponent } from "./home-page/home/home.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";
import { QRCodeModule } from "angularx-qrcode";

@NgModule({
  declarations: [AppComponent, HeaderMenuComponent, HomeComponent],
  imports: [
    BrowserAnimationsModule,
    IssuerBackEndRoutingModule,
    IonicModule.forRoot(),
    HttpClientModule,
    KeycloakAngularModule,
    QRCodeModule,
    CommonWebLibModule,
  ],
  providers: [
    {
      provide: APP_CONFIG,
      useValue: APP_DI_CONFIG,
    },
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
