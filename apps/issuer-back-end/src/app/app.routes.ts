import {
  PreloadAllModules,
  provideRouter,
  Route,
  RouterModule,
  withComponentInputBinding,
} from "@angular/router";
import { NgModule } from "@angular/core";
import { AuthGuard } from "./guard/guard";

const appRoutes: Route[] = [
  {
    path: "home",
    loadChildren: () => import("./home-page/home-page.module").then(m => m.HomePageModule),
  },
  {
    path: "manage-pda1-request",
    canActivate: [AuthGuard],
    loadChildren: () =>
      import("./manage-pda1-requests/manage-pda1-requests.module").then(
        m => m.ManagePda1RequestsModule
      ),
  },
  {
    path: "present-pda1-content",
    canActivate: [AuthGuard],
    loadChildren: () =>
      import("./present-pda-content/present-pda-content.module").then(
        m => m.PresentPdaContentModule
      ),
  },
  {
    path: "",
    redirectTo: "home",
    pathMatch: "full",
  },
  {
    path: "**",
    pathMatch: "full",
    loadChildren: () => import("./home-page/home-page.module").then(m => m.HomePageModule),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {
      onSameUrlNavigation: "reload",
      preloadingStrategy: PreloadAllModules,
      scrollPositionRestoration: "enabled", // Add options right here
    }),
  ],
  providers: [provideRouter(appRoutes, withComponentInputBinding())],
  exports: [RouterModule],
})
export class IssuerBackEndRoutingModule {}
