import { NgModule } from "@angular/core";
import { IonicModule } from "@ionic/angular";
import { IssuerHomePageRoutingModule } from "./home-page.routes";

@NgModule({
  declarations: [],
  imports: [IonicModule.forRoot(), IssuerHomePageRoutingModule],
  providers: [],
})
export class HomePageModule {}
