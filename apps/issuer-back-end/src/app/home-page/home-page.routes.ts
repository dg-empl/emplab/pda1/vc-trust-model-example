import { Route, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { HomeComponent } from "./home/home.component";

export const appRoutes: Route[] = [
  {
    path: "",
    component: HomeComponent,
    // children: [
    //   { path: "", redirectTo: "home", pathMatch: "full" },
    //   {
    //     path: "home",
    //     component: HomeComponent
    //   },
    // ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule],
})
export class IssuerHomePageRoutingModule {}
