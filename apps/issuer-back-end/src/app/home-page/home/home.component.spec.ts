import { ComponentFixture, TestBed } from "@angular/core/testing";
import { HomeComponent } from "./home.component";
import { KeycloakService } from "keycloak-angular";
import { LoginService } from "@esspass-web-wallet/common-web-lib";
import { MockComponent } from "ng2-mock-component";

describe("HomeComponent", () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        HomeComponent,
        MockComponent({ selector: "issuer-login-button", inputs: ["loggedLabel", "isLoggedIn"] }),
        MockComponent({ selector: "issuer-footer", inputs: ["officeName"] }),
      ],
      providers: [
        {
          provide: LoginService,
          useValue: { login: jest.fn(), isAuthenticated: jest.fn().mockReturnValue(true) },
        },
        {
          provide: KeycloakService,
          useValue: {
            isLoggedIn: () => true,
            loadUserProfile: () => true,
            getKeycloakInstance: () => true,
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
