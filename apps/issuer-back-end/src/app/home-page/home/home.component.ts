import { Component, inject, OnInit } from "@angular/core";
import { LoginService } from "@esspass-web-wallet/common-web-lib";
import { Router } from "@angular/router";

@Component({
  selector: "issuer-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit {
  isLoggedIn = false;
  private loginService = inject(LoginService);
  private router = inject(Router);

  async ngOnInit() {
    this.isLoggedIn = await this.loginService.isAuthenticated();
  }

  async onLoginEvent($event: boolean) {
    if (!$event) {
      await this.loginService.logout();
      return;
    }
    await this.loginService.login();
  }

  async managePda1() {
    await this.router.navigate(["/manage-pda1-request"]);
  }
}
