import { ComponentFixture, TestBed } from "@angular/core/testing";
import { ManagePda1RequestsHomeComponent } from "./manage-pda1-requests-home.component";
import { MockComponent } from "ng2-mock-component";

describe("ManagePda1RequestsHomeComponent", () => {
  let component: ManagePda1RequestsHomeComponent;
  let fixture: ComponentFixture<ManagePda1RequestsHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        ManagePda1RequestsHomeComponent,
        MockComponent({ selector: "issuer-search-pda1" }),
        MockComponent({ selector: "issuer-request-form-table" }),
        MockComponent({ selector: "issuer-footer", inputs: ["officeName"] }),
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ManagePda1RequestsHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
