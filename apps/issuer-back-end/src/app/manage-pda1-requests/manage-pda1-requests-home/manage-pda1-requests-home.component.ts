import { Component } from "@angular/core";

@Component({
  selector: "issuer-manage-pda1-requests-home",
  templateUrl: "./manage-pda1-requests-home.component.html",
  styleUrls: ["./manage-pda1-requests-home.component.scss"],
})
export class ManagePda1RequestsHomeComponent {}
