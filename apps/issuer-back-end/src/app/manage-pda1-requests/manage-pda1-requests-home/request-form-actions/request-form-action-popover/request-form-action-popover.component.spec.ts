import { ComponentFixture, TestBed } from "@angular/core/testing";
import { RequestFormActionPopoverComponent } from "./request-form-action-popover.component";
import { PopoverController } from "@ionic/angular";

describe("RequestFormActionPopoverComponent", () => {
  let component: RequestFormActionPopoverComponent;
  let fixture: ComponentFixture<RequestFormActionPopoverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RequestFormActionPopoverComponent],
      providers: [
        {
          provide: PopoverController,
          useValue: {},
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(RequestFormActionPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
