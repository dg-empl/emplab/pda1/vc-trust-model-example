import { Component, inject, Input, OnInit } from "@angular/core";
import {
  RequestFormActionEnum,
  possibleStateToActionsMapper,
  requestFormActionEnumAdapter,
  requestFormActionEnumSchema,
  RequestFormStatusEnum,
} from "@esspass-web-wallet/domain-lib";
import { PopoverController } from "@ionic/angular";
import { RequestFormActionSubmissionPopoverComponent } from "../request-form-action-submission-popover/request-form-action-submission-popover.component";
import { SubmitActionService } from "../../request-form-table/application/submit-action.service";
import { RevokeFunctionalitySubmissionPopoverComponent } from "../revoke-functionality-submission-popover/revoke-functionality-submission-popover.component";
import { ThirdPartyFunctionalityPopoverComponent } from "../third-party-functionality-popover/third-party-functionality-popover.component";
import { Router } from "@angular/router";
import { RequestFormIssuePopoverComponent } from "../request-form-issue-popover/request-form-issue-popover.component";
import { PopoverDetectorService } from "../../request-form-table/application/popover-detector.service";

@Component({
  selector: "issuer-request-form-action-popover",
  templateUrl: "./request-form-action-popover.component.html",
  styleUrls: ["./request-form-action-popover.component.scss"],
})
export class RequestFormActionPopoverComponent implements OnInit {
  possibleActions: { action: RequestFormActionEnum; value: string }[] = [];
  @Input({ required: true }) requestStatus: RequestFormStatusEnum;
  @Input({ required: true }) id: string;
  @Input({ required: true }) email: string;

  private router = inject(Router);
  private popoverController = inject(PopoverController);
  private submitActionService = inject(SubmitActionService);
  /**
   * @description to detect if popover is open
   */
  private popoverDetector = inject(PopoverDetectorService);

  ngOnInit(): void {
    const actionDescription = possibleStateToActionsMapper(this.requestStatus);
    this.possibleActions = actionDescription.map(action => {
      return {
        action: action,
        value: requestFormActionEnumSchema[action].value,
      };
    });
  }

  async onAction(action: RequestFormActionEnum) {
    const actionCallback = this.actionRegister[action] as (
      id: string
    ) => Promise<HTMLIonPopoverElement>;
    const popoverElement = await actionCallback(action);

    if (popoverElement) {
      await popoverElement.present();
      this.popoverDetector.popoverOpened.next("opened");

      popoverElement.onDidDismiss().then(() => {
        this.popoverDetector.popoverOpened.next("closed");
        this.reloadTableResults(action);
      });
    }

    this.reloadTableResults(action);
  }

  private reloadTableResults(action: RequestFormActionEnum) {
    const requestFormActionEnum = requestFormActionEnumAdapter[action];
    this.submitActionService.submit.next({ action: requestFormActionEnum });
  }

  private readonly actionRegister: Partial<
    Record<
      keyof typeof RequestFormActionEnum,
      (action: RequestFormActionEnum) => Promise<HTMLIonPopoverElement> | Promise<void>
    >
  > = {
    Delete: action => this.createDefaultPopover(action),
    Issue: action =>
      this.popoverController.create({
        component: RequestFormIssuePopoverComponent,
        backdropDismiss: false,
        dismissOnSelect: false,
        cssClass: "action-submission-popover-request-form-issue",
        side: "top",
        componentProps: {
          action: action,
          description: requestFormActionEnumSchema[action].description,
          id: this.id,
        },
      }),
    Revoke: action => this.createDefaultPopover(action),
    Reject: action => this.createDefaultPopover(action),
    RevokeFunctionality: action =>
      this.popoverController.create({
        component: RevokeFunctionalitySubmissionPopoverComponent,
        backdropDismiss: false,
        dismissOnSelect: false,
        cssClass: "action-revoke-functionality-popover",
        componentProps: {
          action: action,
          description: requestFormActionEnumSchema[action].description,
          id: this.id,
          email: this.email,
        },
      }),
    "3RDParty": action =>
      this.popoverController.create({
        component: ThirdPartyFunctionalityPopoverComponent,
        backdropDismiss: true,
        dismissOnSelect: false,
        cssClass: "action-third-party-functionality-popover",
        componentProps: {
          action: action,
          id: this.id,
          description: requestFormActionEnumSchema[action].description,
          email: this.email,
        },
      }),
    Present: action => this.presentPdaContent(),
  };

  private createDefaultPopover(action: RequestFormActionEnum): Promise<HTMLIonPopoverElement> {
    return this.popoverController.create({
      component: RequestFormActionSubmissionPopoverComponent,
      backdropDismiss: false,
      dismissOnSelect: false,
      cssClass: "action-submission-popover",
      componentProps: {
        action: action,
        description: requestFormActionEnumSchema[action].description,
        id: this.id,
      },
    });
  }

  private async presentPdaContent(): Promise<void> {
    await this.router.navigate(["/present-pda1-content", this.id]);
  }
}
