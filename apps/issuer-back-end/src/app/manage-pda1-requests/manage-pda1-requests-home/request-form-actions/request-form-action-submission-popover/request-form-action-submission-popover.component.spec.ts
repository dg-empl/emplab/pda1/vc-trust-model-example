import { ComponentFixture, TestBed } from "@angular/core/testing";
import { RequestFormActionSubmissionPopoverComponent } from "./request-form-action-submission-popover.component";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { PopoverController } from "@ionic/angular";
import { APP_CONFIG, APP_DI_CONFIG } from "@esspass-web-wallet/security-web-lib";

describe("RequestFormActionSubmissionPopoverComponent", () => {
  let component: RequestFormActionSubmissionPopoverComponent;
  let fixture: ComponentFixture<RequestFormActionSubmissionPopoverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RequestFormActionSubmissionPopoverComponent],
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: PopoverController,
          useValue: {},
        },
        {
          provide: APP_CONFIG,
          useValue: APP_DI_CONFIG,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(RequestFormActionSubmissionPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
