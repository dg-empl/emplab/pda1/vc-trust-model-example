import { Component, inject, Input } from "@angular/core";
import { RequestFormActionEnum, requestFormActionEnumSchema } from "@esspass-web-wallet/domain-lib";
import { RequestFormActionsService } from "../request-form-actions.service";
import { catchError, finalize } from "rxjs/operators";
import { Observable, tap } from "rxjs";
import { PopoverController, ToastController } from "@ionic/angular";
import { SpinnerService } from "@esspass-web-wallet/common-web-lib";
import { PopoverDetectorService } from "../../request-form-table/application/popover-detector.service";
import { SearchFilterService } from "../../request-form-table/application/search-filter.service";

@Component({
  selector: "issuer-request-form-action-submission-popover",
  templateUrl: "./request-form-action-submission-popover.component.html",
  styleUrls: ["./request-form-action-submission-popover.component.scss"],
})
export class RequestFormActionSubmissionPopoverComponent {
  _description: string;
  _actionValue: string;

  private spinnerService = inject(SpinnerService);
  private popoverCtrl = inject(PopoverController);
  private toastController = inject(ToastController);
  private requestFormActionsService = inject(RequestFormActionsService);
  /**
   * to detect if the popover is open or not
   */
  private popoverDetector = inject(PopoverDetectorService);

  private readonly actionRegister: Partial<
    Record<keyof typeof RequestFormActionEnum, (id: any) => Observable<any>>
  > = {
    Issue: id => this.requestFormActionsService.issueRequestForm(id),
    Delete: id => this.requestFormActionsService.deleteRequestForm(id),
    Reject: id => this.requestFormActionsService.rejectRequestForm(id),
    Present: id => this.requestFormActionsService.presentPdaForm(id),
    Revoke: id => this.requestFormActionsService.revokePdaVerifiableCredential(id),
    "3RDParty": id => this.requestFormActionsService.thirdPartyPdaForm(id),
  };

  _action: keyof typeof RequestFormActionEnum;

  /**
   * @description id of the request form passed from parent component
   */
  @Input({ required: true }) id: string;

  /**
   * @description action of the request form passed from parent component
   * @param action keyof typeof RequestFormActionEnum
   */
  @Input({ required: true }) set action(action: keyof typeof RequestFormActionEnum) {
    if (!requestFormActionEnumSchema[action]) {
      throw new Error(`Invalid action: ${action}`);
    }

    this._actionValue = requestFormActionEnumSchema[action].value;
    this._action = action;
    this._description = requestFormActionEnumSchema[action].description;
  }

  /**
   * @description confirm the action , check if the action is valid and call the appropriate service registered through the actionRegister
   */
  async confirm() {
    const actionCallback = this.actionRegister[this._action] as (id: string) => Observable<any>;
    this.spinnerService.load();
    this.popoverDetector.popoverOpened.next("opened");

    actionCallback(this.id)
      .pipe(
        tap(response => {
          const responseMessage = `Action ${this._action} submitted properly`;
          `${this._action}` !== "Delete"
            ? this.responseMessage(responseMessage)
            : this.responseMessage(`Action Hide submitted properly`);
        }),
        catchError(error => {
          this.responseMessage(error.message);
          return [];
        }),
        finalize(async () => {
          this.spinnerService.stop();
          await this.popoverCtrl.dismiss();
          this.popoverDetector.popoverOpened.next("closed");
        })
      )
      .subscribe();
  }

  async cancel() {
    await this.popoverCtrl.dismiss();
    this.popoverDetector.popoverOpened.next("closed");
  }

  private async responseMessage(message: string) {
    const toast = await this.toastController.create({
      message: message,
      position: "top",
      duration: 4000,
      cssClass: "custom-toast",
    });

    await toast.present();
  }
}
