import { ComponentFixture, TestBed } from "@angular/core/testing";
import { RequestFormActionsComponent } from "./request-form-actions.component";
import { PopoverController } from "@ionic/angular";

describe("RequestFormActionsComponent", () => {
  let component: RequestFormActionsComponent;
  let fixture: ComponentFixture<RequestFormActionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RequestFormActionsComponent],
      providers: [
        {
          provide: PopoverController,
          useValue: {},
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(RequestFormActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
