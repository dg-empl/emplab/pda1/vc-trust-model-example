import { Component, inject, Input, OnInit } from "@angular/core";
import { PopoverController } from "@ionic/angular";
import { RequestFormActionPopoverComponent } from "./request-form-action-popover/request-form-action-popover.component";
import { RequestFormStatusEnum } from "@esspass-web-wallet/domain-lib";
import { PopoverDetectorService } from "../request-form-table/application/popover-detector.service";

@Component({
  selector: "issuer-request-form-actions",
  templateUrl: "./request-form-actions.component.html",
  styleUrls: ["./request-form-actions.component.scss"],
})
export class RequestFormActionsComponent {
  private popoverController = inject(PopoverController);
  private popoverDetector = inject(PopoverDetectorService);

  @Input({ required: true }) requestStatus: RequestFormStatusEnum;
  @Input({ required: true }) id: string;
  @Input({ required: true }) email: string;

  async onClick(event: Event) {
    event.stopPropagation();

    const popoverElement = await this.popoverController.create({
      component: RequestFormActionPopoverComponent,
      backdropDismiss: true,
      dismissOnSelect: true,
      event,
      cssClass: "popover-over-parent",
      componentProps: {
        requestStatus: this.requestStatus,
        id: this.id,
        email: this.email,
      },
    });

    this.popoverDetector.popoverOpened.next("opened");
    await popoverElement.present();

    popoverElement.onDidDismiss().then(() => {
      this.popoverDetector.popoverOpened.next("closed");
    });
  }
}
