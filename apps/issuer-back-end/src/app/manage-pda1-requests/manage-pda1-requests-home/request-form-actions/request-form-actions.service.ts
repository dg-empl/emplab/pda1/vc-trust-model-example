import { inject, Injectable } from "@angular/core";
import { RequestFormCollectionsRestService } from "../request-form-table/rest/request-form-collections-rest.service";
import { firstValueFrom, Observable, of } from "rxjs";
import { RequestFormStatusEnum } from "@esspass-web-wallet/domain-lib";

@Injectable({ providedIn: "root" })
export class RequestFormActionsService {
  private requestFormCollRestService: RequestFormCollectionsRestService = inject(
    RequestFormCollectionsRestService
  );

  public issueRequestForm(requestFormId: string): Observable<number> {
    return this.requestFormCollRestService.issueRequestForm(requestFormId);
  }

  public deleteRequestForm(requestFormId: string): Observable<number> {
    return this.requestFormCollRestService.deleteRequestForm(requestFormId);
  }

  public rejectRequestForm(requestFormId: string): Observable<number> {
    return this.requestFormCollRestService.rejectRequestForm(requestFormId);
  }

  public presentPdaForm(requestFormId: string) {
    return this.requestFormCollRestService.presentPdaForm(requestFormId);
  }

  public generateQrCode(requestFormId: string, email: string) {
    return this.requestFormCollRestService.generateQrCode(requestFormId, email);
  }

  /**
   * Generate QR code for PDA V3
   * @param requestFormId
   * @param email
   */
  public async generateQrCodeV3(requestFormId: string, email: string): Promise<string> {
    const requestParams = {
      credential_type: "VerifiablePortableDocumentA1",
      credential_offer_endpoint: "openid-credential-offer://",
      pdaId: requestFormId,
      email: email,
    };

    const issuerServiceUrl = process.env["ISSUER_V3_URL"];
    const apiUrlPrefix = "/api/v1";

    /**
     * 1. initiate-credential-offer
     *  http://localhost:4299/api/v1/issuer/initiate-credential-offer?
     *  credential_type=VerifiablePortableDocumentA1&
     *  credential_offer_endpoint=openid-credential-offer%3A%2F%2F&
     *  user_pin=1234 ("user_pin" is optional)
     *  pdaId=1234
     *  email=abc%40example.com
     */

    const searchParams = new URLSearchParams();

    for (const [key, value] of Object.entries(requestParams)) {
      searchParams.append(key, value ? value : "");
    }

    const url = `${issuerServiceUrl}${apiUrlPrefix}/issuer/initiate-credential-offer?${searchParams.toString()}`;

    const response = await firstValueFrom(this.requestFormCollRestService.generateQrCodeV3(url));

    if (response) {
      return response;
    } else {
      throw new Error("Initiate credential offer backend error");
    }
  }

  public setPdaState(requestFormId: string, desiredState: RequestFormStatusEnum) {
    return this.requestFormCollRestService.setPdaState(requestFormId, desiredState);
  }

  public revokePdaVerifiableCredential(pdaId: any) {
    return this.requestFormCollRestService.revokePdaVerifiableCredential(pdaId);
  }

  thirdPartyPdaForm(id: string): Observable<string> {
    return of("ok");
  }
}
