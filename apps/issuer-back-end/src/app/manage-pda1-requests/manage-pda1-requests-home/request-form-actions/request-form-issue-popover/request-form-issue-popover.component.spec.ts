import { ComponentFixture, TestBed } from "@angular/core/testing";
import { RequestFormIssuePopoverComponent } from "./request-form-issue-popover.component";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { PopoverController } from "@ionic/angular";
import { APP_CONFIG, APP_DI_CONFIG } from "@esspass-web-wallet/security-web-lib";

describe("RequestFormIssuePopoverComponent", () => {
  let component: RequestFormIssuePopoverComponent;
  let fixture: ComponentFixture<RequestFormIssuePopoverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: PopoverController,
          useValue: {},
        },
        {
          provide: APP_CONFIG,
          useValue: APP_DI_CONFIG,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(RequestFormIssuePopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
