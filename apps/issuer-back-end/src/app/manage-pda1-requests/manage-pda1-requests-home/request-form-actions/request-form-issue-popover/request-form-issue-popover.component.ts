import { Component, inject, Input } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RequestFormCollectionsRestService } from "../../request-form-table/rest/request-form-collections-rest.service";
import { RequestFormActionsService } from "../request-form-actions.service";
import { tap } from "rxjs";
import { catchError, finalize } from "rxjs/operators";
import { SpinnerService } from "@esspass-web-wallet/common-web-lib";
import { IonicModule, PopoverController, ToastController } from "@ionic/angular";
import { PopoverDetectorService } from "../../request-form-table/application/popover-detector.service";

@Component({
  selector: "issuer-request-form-issue-popover",
  standalone: true,
  imports: [CommonModule, IonicModule],
  templateUrl: "./request-form-issue-popover.component.html",
  styleUrls: ["./request-form-issue-popover.component.scss"],
})
export class RequestFormIssuePopoverComponent {
  private requestFormCollRestService: RequestFormCollectionsRestService = inject(
    RequestFormCollectionsRestService
  );
  private requestFormActionsService = inject(RequestFormActionsService);
  /**
   * message / ui handling services
   * @private
   */
  private spinnerService = inject(SpinnerService);
  private popoverCtrl = inject(PopoverController);
  private toastController = inject(ToastController);
  /**
   * @description to detect if popover is open
   */
  private popoverDetector = inject(PopoverDetectorService);

  data: any;

  private readonly _action = "Issue Pda1";

  /**
   * @description id of the request form passed from parent component
   */
  @Input({ required: true }) id: string;

  ngOnInit(): void {
    const requestFormId = this.id;
    this.requestFormCollRestService
      .findRequestFormFromPda1(requestFormId)
      .subscribe(requestForm => {
        this.data = JSON.parse(requestForm.requestFormData);
      });
  }

  onAccept() {
    this.spinnerService.load();

    this.requestFormActionsService
      .issueRequestForm(this.id)
      .pipe(
        tap(response => {
          const responseMessage = `Action ${this._action} submitted properly`;
          this.responseMessage(responseMessage);
        }),
        catchError(error => {
          this.responseMessage(error.message);
          return [];
        }),
        finalize(async () => {
          this.spinnerService.stop();
          await this.popoverCtrl.dismiss();
          this.popoverDetector.popoverOpened.next("closed");
        })
      )
      .subscribe();
  }

  async onCancel() {
    await this.popoverCtrl.dismiss();
  }

  private async responseMessage(message: string) {
    const toast = await this.toastController.create({
      message: message,
      position: "top",
      duration: 4000,
      cssClass: "custom-toast",
    });

    await toast.present();
  }
}
