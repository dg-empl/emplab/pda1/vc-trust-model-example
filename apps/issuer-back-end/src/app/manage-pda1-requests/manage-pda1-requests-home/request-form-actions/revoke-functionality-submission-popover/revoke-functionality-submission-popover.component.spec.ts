import { ComponentFixture, TestBed } from "@angular/core/testing";
import { RevokeFunctionalitySubmissionPopoverComponent } from "./revoke-functionality-submission-popover.component";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { PopoverController } from "@ionic/angular";
import { MockComponent } from "ng2-mock-component";
import { APP_CONFIG, APP_DI_CONFIG } from "@esspass-web-wallet/security-web-lib";

describe("RevokeFunctionalitySubmissionPopoverComponent", () => {
  let component: RevokeFunctionalitySubmissionPopoverComponent;
  let fixture: ComponentFixture<RevokeFunctionalitySubmissionPopoverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        RevokeFunctionalitySubmissionPopoverComponent,
        MockComponent({ selector: "input", inputs: ["ngModel"] }),
      ],
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: PopoverController,
          useValue: {},
        },
        {
          provide: APP_CONFIG,
          useValue: APP_DI_CONFIG,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(RevokeFunctionalitySubmissionPopoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
