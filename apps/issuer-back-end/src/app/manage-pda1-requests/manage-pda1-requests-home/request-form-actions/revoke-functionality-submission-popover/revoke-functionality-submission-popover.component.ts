import { Component, inject, Input, OnInit } from "@angular/core";
import {
  RequestFormActionEnum,
  requestFormActionEnumSchema,
  RequestFormStatusEnum,
} from "@esspass-web-wallet/domain-lib";
import { PopoverController, ToastController } from "@ionic/angular";
import { RequestFormActionsService } from "../request-form-actions.service";
import { mergeMap, Observable, tap } from "rxjs";
import { catchError, finalize } from "rxjs/operators";
import { SpinnerService } from "@esspass-web-wallet/common-web-lib";
import { PopoverDetectorService } from "../../request-form-table/application/popover-detector.service";

@Component({
  selector: "issuer-revoke-functionality-submission-popover",
  templateUrl: "./revoke-functionality-submission-popover.component.html",
  styleUrls: ["./revoke-functionality-submission-popover.component.scss"],
})
export class RevokeFunctionalitySubmissionPopoverComponent implements OnInit {
  _description: string;
  _actionValue: string;
  _action: keyof typeof RequestFormActionEnum;
  _email: string;

  isLoading$: Observable<boolean>;

  private popoverCtrl = inject(PopoverController);
  private toastController = inject(ToastController);
  private spinnerService = inject(SpinnerService);
  private requestFormActionsService = inject(RequestFormActionsService);

  ngOnInit(): void {
    this.isLoading$ = this.spinnerService.data$;
  }

  /**
   * @description id of the request form passed from parent component
   */
  @Input({ required: true }) id: string;

  /**
   * @description action of the request form passed from parent component
   * @param action keyof typeof RequestFormActionEnum
   */
  @Input({ required: true }) set action(action: keyof typeof RequestFormActionEnum) {
    if (!requestFormActionEnumSchema[action]) {
      throw new Error(`Invalid action: ${action}`);
    }

    this._actionValue = requestFormActionEnumSchema[action].value;
    this._action = action;
    this._description = requestFormActionEnumSchema[action].description;
  }

  /**
   * @description id of the request form passed from parent component
   */
  @Input({ required: true }) set email(email: string) {
    this._email = email;
  }

  /**
   * @description confirm the action , check if the action is valid and call the appropriate service registered through the actionRegister
   */
  async confirm() {
    this.spinnerService.load();
    this.requestFormActionsService
      .generateQrCode(this.id, this._email)
      .pipe(
        tap(async () => {
          await this.popoverCtrl.dismiss();
          await this.responseMessage(`Action ${this._action} submitted properly`);
        }),
        mergeMap(() =>
          this.requestFormActionsService.setPdaState(this.id, RequestFormStatusEnum.Downloaded)
        ),
        catchError(error => {
          this.responseMessage(error.message);
          return [];
        }),
        finalize(async () => {
          this.spinnerService.stop();
          await this.popoverCtrl.dismiss();
        })
      )
      .subscribe();
  }

  async cancel() {
    await this.popoverCtrl.dismiss();
  }

  private async responseMessage(message: string) {
    const toast = await this.toastController.create({
      message: message,
      position: "top",
      duration: 4000,
      cssClass: "custom-toast",
    });

    await toast.present();
  }
}
