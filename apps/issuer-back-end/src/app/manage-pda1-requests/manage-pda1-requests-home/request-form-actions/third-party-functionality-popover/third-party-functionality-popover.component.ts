import { Component, inject, Input } from "@angular/core";
import {
  RequestFormActionEnum,
  requestFormActionEnumSchema,
  RequestFormStatusEnum,
} from "@esspass-web-wallet/domain-lib";
import { from, mergeMap, Observable, tap } from "rxjs";
import { PopoverController, ToastController } from "@ionic/angular";
import { SpinnerService } from "@esspass-web-wallet/common-web-lib";
import { RequestFormActionsService } from "../request-form-actions.service";
import { catchError, finalize } from "rxjs/operators";

@Component({
  selector: "issuer-third-party-functionality-popover",
  templateUrl: "./third-party-functionality-popover.component.html",
  styleUrls: ["./third-party-functionality-popover.component.scss"],
})
export class ThirdPartyFunctionalityPopoverComponent {
  _description: string;
  _actionValue: string;
  /**
   * @description pin of the request form
   */
  pin: string | undefined;
  qrCode: string;
  isLoading$: Observable<boolean>;
  /**
   * @description id of the request form passed from parent component
   */
  @Input({ required: true }) id: string;
  private popoverCtrl = inject(PopoverController);
  private toastController = inject(ToastController);
  private spinnerService = inject(SpinnerService);
  private requestFormActionsService = inject(RequestFormActionsService);

  _action: keyof typeof RequestFormActionEnum;

  /**
   * @description action of the request form passed from parent component
   * @param action keyof typeof RequestFormActionEnum
   */
  @Input({ required: true }) set action(action: keyof typeof RequestFormActionEnum) {
    if (!requestFormActionEnumSchema[action]) {
      throw new Error(`Invalid action: ${action}`);
    }
    this._actionValue = requestFormActionEnumSchema[action].value;
    this._action = action;
    this._description = requestFormActionEnumSchema[action].description;
  }

  _email: string;

  /**
   * @description email of the request form passed from parent component
   * @param email
   */
  @Input({ required: true }) set email(email: string) {
    this._email = email;
  }

  ngOnInit(): void {
    this.isLoading$ = this.spinnerService.data$;
  }

  /**
   * @description confirm the action , check if the action is valid and call the appropriate service registered through the actionRegister
   */
  async confirm() {
    this.spinnerService.load();
    from(this.requestFormActionsService.generateQrCodeV3(this.id, this._email))
      .pipe(
        tap(async value => {
          console.log(value);
          await this.popoverCtrl.dismiss();
          await this.responseMessage(`Action ${this._action} submitted properly`);
        }),
        mergeMap(() =>
          this.requestFormActionsService.setPdaState(this.id, RequestFormStatusEnum.Downloaded)
        ),
        catchError(error => {
          console.log(error.message);

          this.responseMessage(error.message);
          return [];
        }),
        finalize(async () => {
          this.spinnerService.stop();
          await this.popoverCtrl.dismiss();
        })
      )
      .subscribe();
  }

  async cancel() {
    await this.popoverCtrl.dismiss();
  }

  private async responseMessage(message: string) {
    const toast = await this.toastController.create({
      message: message,
      position: "top",
      duration: 4000,
      cssClass: "custom-toast",
    });

    await toast.present();
  }
}
