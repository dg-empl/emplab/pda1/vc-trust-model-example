import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { PageSort, RequestFormStatusEnum } from "@esspass-web-wallet/domain-lib";

@Injectable({ providedIn: "root" })
export class PageSortService {
  readonly initialPageSort = {
    pageIndex: 0,
    pageSize: 5,
    sort: {
      active: "dateOfRegistration",
      direction: "desc",
    },
  } as PageSort;

  public pageSort = new BehaviorSubject<PageSort>(this.initialPageSort);
  public pageSort$ = this.pageSort.asObservable();
}
