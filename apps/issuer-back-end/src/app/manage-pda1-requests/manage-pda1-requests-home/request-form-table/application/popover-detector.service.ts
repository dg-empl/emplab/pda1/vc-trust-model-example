import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

type PopoverData = "opened" | "closed";

@Injectable({ providedIn: "root" })
export class PopoverDetectorService {
  public popoverOpened = new BehaviorSubject<PopoverData>("closed");
  public popoverOpened$ = this.popoverOpened.asObservable();
}
