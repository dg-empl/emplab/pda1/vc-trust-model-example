/**
 * @jest-environment jsdom
 */
import { TestBed } from "@angular/core/testing";
import { RequestFormCollectionsService } from "./request-form-collections.service";
import { RequestFormCollectionsRestService } from "../rest/request-form-collections-rest.service";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { APP_CONFIG, APP_DI_CONFIG } from "@esspass-web-wallet/security-web-lib";

describe("RequestFormCollectionsService", () => {
  let service: RequestFormCollectionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        RequestFormCollectionsRestService,
        {
          provide: APP_CONFIG,
          useValue: APP_DI_CONFIG,
        },
      ],
    });
    service = TestBed.inject(RequestFormCollectionsService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
