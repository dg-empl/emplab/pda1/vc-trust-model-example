import { BehaviorSubject, Observable, tap, throwError } from "rxjs";
import { catchError, finalize, first, map } from "rxjs/operators";
import { inject, Injectable } from "@angular/core";
import { RequestFormCollectionsRestService } from "../rest/request-form-collections-rest.service";

import { Sort } from "@angular/material/sort";
import { DataSource } from "@angular/cdk/table";
import { PageEvent } from "@angular/material/paginator";
import { CollectionViewer } from "@angular/cdk/collections";

import { ApiResponseType, PageSort, RequestFormsDbType } from "@esspass-web-wallet/domain-lib";
import { PageSortService } from "./page-sort.service";

@Injectable({ providedIn: "root" })
export class RequestFormCollectionsService implements DataSource<RequestFormsDbType> {
  private requestFormCollRestService: RequestFormCollectionsRestService = inject(
    RequestFormCollectionsRestService
  );

  private pageSortService = inject(PageSortService);

  /**
   * first call to backed to get the data
   * @private
   */
  private collectionsModel$: Observable<ApiResponseType> = this.requestFormCollRestService
    .findAllTestRequestCollection(this.pageSortService.initialPageSort)
    .pipe(
      first(),
      catchError(err => throwError(err))
    );

  connect(collectionViewer: CollectionViewer): Observable<RequestFormsDbType[]> {
    return this.collections$;
  }

  disconnect(collectionViewer: CollectionViewer): Record<string, unknown> {
    return {};
  }

  counter(): Observable<number> {
    return this.collectionsCounter.asObservable();
  }

  private collectionsSubject = new BehaviorSubject<RequestFormsDbType[]>({} as any);
  public collections$ = this.collectionsSubject.asObservable();

  private collectionsCounter = new BehaviorSubject<number>(0);

  private initialCollectionsSubject = new BehaviorSubject<RequestFormsDbType[]>([]);
  public initialCollections$ = this.initialCollectionsSubject.asObservable().pipe(first());

  /**
   * use for spinner loading
   * @private
   */
  private loadingSubject = new BehaviorSubject<boolean>(false);
  public loading$ = this.loadingSubject.asObservable();

  /**
   * on init page
   * @param initialSize
   * @param sort
   */
  findCollectionViewModels(initialSize: number, sort: Sort) {
    this.loadingSubject.next(true);

    return this.collectionsModel$
      .pipe(
        tap((requestFormCollection: ApiResponseType) => {
          const { items, size } = requestFormCollection;

          const initialCollection = [...items].splice(0, initialSize);

          this.collectionsSubject.next(initialCollection);
          this.initialCollectionsSubject.next(items);
          this.collectionsCounter.next(size);
        }),
        catchError(err => throwError(err)),
        finalize(() => {
          this.loadingSubject.next(false);
        })
      )
      .subscribe();
  }

  /**
   * on mat-paginator changes
   * @param $event
   * @param sort
   */
  pageCollections($event: PageEvent, sort: Sort): void {
    this.loadingSubject.next(true);

    this.pageSortService.pageSort.next({
      pageIndex: $event.pageIndex,
      pageSize: $event.pageSize,
      sort: {
        active: sort.active,
        direction: sort.direction,
      },
    } as PageSort);

    this.requestFormCollRestService
      .findAllTestRequestCollection(this.pageSortService.pageSort.value)
      .pipe(
        first(),
        map((response: ApiResponseType) => {
          const { items: collection, size: responseSize } = response;

          this.collectionsSubject.next(collection);
          this.collectionsCounter.next(responseSize);
        }),
        catchError(err => throwError(err)),
        finalize(() => this.loadingSubject.next(false))
      )
      .subscribe();
  }

  /**
   * on filter changes
   * @param filter
   * @param $event
   * @param sort
   */
  search(filter: string, $event: PageEvent, sort: Sort) {
    this.loadingSubject.next(true);

    this.pageSortService.pageSort.next({
      pageIndex: $event.pageIndex,
      pageSize: $event.pageSize,
      sort: {
        active: sort.active,
        direction: sort.direction,
      },
    } as PageSort);

    this.requestFormCollRestService
      .findAllTestRequestCollection(this.pageSortService.pageSort.value, filter)
      .pipe(
        first(),
        map((response: ApiResponseType) => {
          const { items: collection, size: responseSize } = response;

          this.collectionsSubject.next(collection);
          this.collectionsCounter.next(responseSize);
        }),
        catchError(err => throwError(err)),
        finalize(() => this.loadingSubject.next(false))
      )
      .subscribe();
  }
}
