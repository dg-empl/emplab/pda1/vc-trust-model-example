import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable({ providedIn: "root" })
export class SearchFilterService {
  public search = new BehaviorSubject<string>("");
  public search$ = this.search.asObservable();

  public clear = new BehaviorSubject<boolean>(false);
  public clear$ = this.clear.asObservable();
}
