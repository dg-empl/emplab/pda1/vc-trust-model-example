import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { RequestFormStatusEnum } from "@esspass-web-wallet/domain-lib";

@Injectable({ providedIn: "root" })
export class SubmitActionService {
  public submit = new BehaviorSubject<{ action: keyof typeof RequestFormStatusEnum | undefined }>({
    action: undefined,
  });
  public submit$ = this.submit.asObservable();
}
