import { JsonConverter, JsonCustomConvert } from "json2typescript";
import moment from "moment";

@JsonConverter
export class DateConverter implements JsonCustomConvert<Date | null> {
  serialize(date: Date): any {
    return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
  }

  deserialize(date: any): Date | null {
    return date ? moment(date).toDate() : null;
  }
}
