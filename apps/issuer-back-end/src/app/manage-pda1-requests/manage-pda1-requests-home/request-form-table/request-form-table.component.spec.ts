import { ComponentFixture, TestBed } from "@angular/core/testing";
import { RequestFormTableComponent } from "./request-form-table.component";
import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { MatSortModule } from "@angular/material/sort";
import { APP_CONFIG, APP_DI_CONFIG } from "@esspass-web-wallet/security-web-lib";

describe("RequestFormTableComponent", () => {
  let component: RequestFormTableComponent;
  let fixture: ComponentFixture<RequestFormTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        HttpClientTestingModule,
        NoopAnimationsModule,
      ],
      declarations: [RequestFormTableComponent],
      providers: [
        {
          provide: APP_CONFIG,
          useValue: APP_DI_CONFIG,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(RequestFormTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
