import { filter, Observable, Subscription } from "rxjs";
import {
  AfterViewInit,
  Component,
  inject,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from "@angular/core";
import { MatSort, Sort } from "@angular/material/sort";
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { SearchFilterService } from "./application/search-filter.service";
import { SubmitActionService } from "./application/submit-action.service";
import { SpinnerService } from "@esspass-web-wallet/common-web-lib";
import { RequestFormCollectionsService } from "./application/request-form-collections.service";

const unsubscribe = (subscription: Subscription) => {
  if (subscription) {
    subscription.unsubscribe();
  }
};

@Component({
  selector: "issuer-request-form-table",
  templateUrl: "./request-form-table.component.html",
  styleUrls: ["./request-form-table.component.scss"],
})
export class RequestFormTableComponent implements OnInit, OnDestroy, AfterViewInit {
  columnsToDisplay = [
    "index",
    "requestorEmail",
    "requestorFirstName",
    "requestorLastName",
    "dateOfRegistration",
    "requestFormStatus",
    "pdaStatus",
  ];

  data: any;
  pageSize = 5;
  collectionLength: number;
  requestFormCollectionsSubscription: Subscription;

  private spinnerService = inject(SpinnerService);
  private searchFilterService = inject(SearchFilterService);
  private submitActionService = inject(SubmitActionService);
  requestFormCollectionsDataSource = inject(RequestFormCollectionsService);

  @ViewChild(MatSort) sort: MatSort;
  @ViewChildren("paginator") paginator: QueryList<MatPaginator>;
  private sortData: Sort;

  private filterSearch$ = this.searchFilterService.search$.pipe(filter(data => !!data));
  private clearFilterSearch$ = this.searchFilterService.clear$.pipe(filter(data => data));
  private actionSubmit$ = this.submitActionService.submit$.pipe(filter(data => !!data.action));

  isLoading$: Observable<boolean>;

  ngOnInit() {
    this.requestFormCollectionsSubscription =
      this.requestFormCollectionsDataSource.findCollectionViewModels(this.pageSize, this.sortData);

    this.requestFormCollectionsDataSource
      .counter()
      .subscribe(data => (this.collectionLength = data));

    this.filterSearch$.subscribe(data => {
      this.filterSearch(data)();
    });

    this.clearFilterSearch$.subscribe(data => {
      this.paginator.first.pageSize = 5;
      this.requestFormCollectionsDataSource.findCollectionViewModels(this.pageSize, this.sortData);
    });

    this.isLoading$ = this.spinnerService.data$;
  }

  hasItems(): boolean {
    return this.collectionLength > 0;
  }

  /**
   * react on paging change
   */
  onPageChange($event: PageEvent) {
    const { direction, active } = this.sort;
    this.requestFormCollectionsDataSource.pageCollections($event, { direction, active });
  }

  ngAfterViewInit() {
    /**
     * react on sort change
     */
    this.sort.sortChange.subscribe(sort => {
      this.sortData = sort;
      this.paginator.first.pageIndex = 0;
      const { direction, active } = sort;

      const { value } = this.searchFilterService.search;
      const shouldClear = this.searchFilterService.clear.value;

      value ? this.filterSearch(value)() : this.querySearch(direction, active);
      shouldClear ? this.querySearch(direction, active) : null;
    });

    /**
     * reload page results after action submit
     */
    this.actionSubmit$.subscribe(() => {
      const { value } = this.searchFilterService.search;
      const shouldClear = this.searchFilterService.clear.value;

      value ? this.filterSearch(value)() : this.querySearch("desc", "dateOfRegistration");
      shouldClear ? this.querySearch("desc", "dateOfRegistration") : null;
    });
  }

  private querySearch(direction: "asc" | "desc" | "", active: string) {
    this.requestFormCollectionsDataSource.pageCollections(
      {
        pageIndex: 0,
        pageSize: this.paginator.first.pageSize,
        length: this.collectionLength,
      },
      { direction, active }
    );
  }

  private filterSearch(value: string) {
    return () =>
      this.requestFormCollectionsDataSource.search(
        value,
        { pageIndex: 0, pageSize: this.paginator.first.pageSize, length: this.collectionLength },
        this.sortData ? this.sortData : { direction: "asc", active: "dateOfRegistration" }
      );
  }

  ngOnDestroy() {
    unsubscribe(this.requestFormCollectionsSubscription);
  }
}
