import { inject, Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { catchError } from "rxjs/operators";
import { mapError } from "@esspass-web-wallet/common-web-lib";
import { HttpWrapperService } from "@esspass-web-wallet/web-utils-lib";
import {
  ApiResponseSchema,
  ApiResponseType,
  PageSort,
  RequestFormsDbType,
  RequestFormStatusEnum,
  requestFormStatusEnumAdapter,
} from "@esspass-web-wallet/domain-lib";
import { APP_CONFIG, AppConfigInterface } from "@esspass-web-wallet/security-web-lib";

@Injectable({ providedIn: "root" })
export class RequestFormCollectionsRestService {
  private httpWrapperService: HttpWrapperService = inject(HttpWrapperService);
  private config: AppConfigInterface = inject(APP_CONFIG);

  public findRequestForm(requestFormId: string): Observable<RequestFormsDbType> {
    return this.httpWrapperService
      .findRawFromGetMethod(`${this.config.serverUrl}/request-form/${requestFormId}`)
      .pipe(catchError(mapError()));
  }

  public findRequestFormFromPda1(requestFormId: string): Observable<RequestFormsDbType> {
    return this.httpWrapperService
      .findRawFromGetMethod(`${this.config.serverUrl}/request-form/pda1/${requestFormId}`)
      .pipe(catchError(mapError()));
  }

  public findAllTestRequestCollection(
    pageSort: PageSort,
    filter = ""
  ): Observable<ApiResponseType> {
    return this.httpWrapperService
      .findObjectWithParams(
        {
          filter: filter,
          pageIndex: pageSort.pageIndex,
          pageSize: pageSort.pageSize,
          ["sort.active"]: pageSort.sort.active,
          ["sort.direction"]: pageSort.sort.direction,
        },
        `${this.config.serverUrl}/request-form`
      )
      .pipe(catchError(mapError()));
  }

  public issueRequestForm(id: string): Observable<any> {
    return this.httpWrapperService
      .updateByPut(`${this.config.serverUrl}/request-form/${id}`, {
        action: "Issue",
      })
      .pipe(catchError(mapError()));
  }

  public rejectRequestForm(id: string): Observable<number> {
    return this.httpWrapperService
      .updateByPut(`${this.config.serverUrl}/request-form/${id}`, {
        action: "Reject",
      })
      .pipe(catchError(mapError()));
  }

  public deleteRequestForm(id: string): Observable<number> {
    return this.httpWrapperService
      .delete(`${this.config.serverUrl}/request-form/${id}`)
      .pipe(catchError(mapError()));
  }

  presentPdaForm(requestFormId: string) {
    return this.httpWrapperService.find(
      requestFormId,
      `${this.config.serverUrl}/request-form/pda1`,
      String
    );
  }

  generateQrCode(requestFormId: string, email: string): Observable<string> {
    return this.httpWrapperService
      .send(`${this.config.serverUrl}/v2/generate-qr-code/${requestFormId}/email/${email}`)
      .pipe(catchError(mapError()));
  }

  generateQrCodeV3(endPoint: string): Observable<string> {
    return this.httpWrapperService.sendWithTextResponse(`${endPoint}`).pipe(catchError(mapError()));
  }

  setPdaState(requestFormId: string, desiredState: RequestFormStatusEnum): Observable<any> {
    return this.httpWrapperService
      .updateByPut(`${this.config.serverUrl}/request-form/${requestFormId}`, {
        action: requestFormStatusEnumAdapter[desiredState],
      })
      .pipe(catchError(mapError()));
  }

  revokePdaVerifiableCredential(pdaId: any) {
    return this.httpWrapperService
      .updateByPut(`${this.config.serverUrl}/pda1/revoke/${pdaId}`, {
        action: "Revoke",
      })
      .pipe(catchError(mapError()));
  }
}
