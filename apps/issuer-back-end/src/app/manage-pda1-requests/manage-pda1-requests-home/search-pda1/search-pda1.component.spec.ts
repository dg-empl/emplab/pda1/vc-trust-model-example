import { ComponentFixture, TestBed } from "@angular/core/testing";
import { SearchPda1Component } from "./search-pda1.component";
import { NgSelectModule } from "@ng-select/ng-select";
import { IonicModule } from "@ionic/angular";

describe("SearchPda1Component", () => {
  let component: SearchPda1Component;
  let fixture: ComponentFixture<SearchPda1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NgSelectModule, IonicModule.forRoot()],
      declarations: [SearchPda1Component],
    }).compileComponents();

    fixture = TestBed.createComponent(SearchPda1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
