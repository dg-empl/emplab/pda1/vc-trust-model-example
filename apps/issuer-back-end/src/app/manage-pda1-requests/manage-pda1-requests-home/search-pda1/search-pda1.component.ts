import { AfterViewInit, Component, ElementRef, inject, ViewChild } from "@angular/core";
import { debounceTime, distinctUntilChanged, fromEvent, tap } from "rxjs";
import { SearchFilterService } from "../request-form-table/application/search-filter.service";

@Component({
  selector: "issuer-search-pda1",
  templateUrl: "./search-pda1.component.html",
  styleUrls: ["./search-pda1.component.scss"],
})
export class SearchPda1Component implements AfterViewInit {
  private searchFilterService: SearchFilterService = inject(SearchFilterService);

  @ViewChild("input") input: ElementRef;

  ngAfterViewInit(): void {
    this.input.nativeElement.focus();

    fromEvent(this.input.nativeElement, "keyup")
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        tap(value => {
          this.searchFilterService.search.next(this.input.nativeElement.value);
          this.emitWhenInputCleared(value);
        })
      )
      .subscribe();
  }

  private emitWhenInputCleared<B>(value: B) {
    if (this.input.nativeElement.value === "" && (value as KeyboardEvent).key === "Backspace") {
      this.searchFilterService.clear.next(true);
    }
  }
}
