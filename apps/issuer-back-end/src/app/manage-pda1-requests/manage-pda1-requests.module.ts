import { NgModule } from "@angular/core";
import { AsyncPipe, CommonModule, NgOptimizedImage } from "@angular/common";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatSortModule } from "@angular/material/sort";
import { MatInputModule } from "@angular/material/input";

import { IssuerManagePda1RequestsRoutingModule } from "./manage-pda1-requests.routes";
import { CommonWebLibModule } from "@esspass-web-wallet/common-web-lib";
import { SearchPda1Component } from "./manage-pda1-requests-home/search-pda1/search-pda1.component";
import { ManagePda1RequestsHomeComponent } from "./manage-pda1-requests-home/manage-pda1-requests-home.component";
import { RequestFormTableComponent } from "./manage-pda1-requests-home/request-form-table/request-form-table.component";
import { RequestFormActionsComponent } from "./manage-pda1-requests-home/request-form-actions/request-form-actions.component";
import { RequestFormActionPopoverComponent } from "./manage-pda1-requests-home/request-form-actions/request-form-action-popover/request-form-action-popover.component";
import { RequestFormActionSubmissionPopoverComponent } from "./manage-pda1-requests-home/request-form-actions/request-form-action-submission-popover/request-form-action-submission-popover.component";
import { RevokeFunctionalitySubmissionPopoverComponent } from "./manage-pda1-requests-home/request-form-actions/revoke-functionality-submission-popover/revoke-functionality-submission-popover.component";
import { IonicModule } from "@ionic/angular";
import { ThirdPartyFunctionalityPopoverComponent } from "./manage-pda1-requests-home/request-form-actions/third-party-functionality-popover/third-party-functionality-popover.component";
import { MatButtonModule } from "@angular/material/button";
import { MatStepperModule } from "@angular/material/stepper";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { QRCodeModule } from "angularx-qrcode";

@NgModule({
  declarations: [
    ManagePda1RequestsHomeComponent,
    SearchPda1Component,
    RequestFormTableComponent,
    RequestFormActionsComponent,
    RequestFormActionPopoverComponent,
    RequestFormActionSubmissionPopoverComponent,
    RevokeFunctionalitySubmissionPopoverComponent,
    ThirdPartyFunctionalityPopoverComponent,
  ],
  imports: [
    CommonModule,
    IssuerManagePda1RequestsRoutingModule,
    NgOptimizedImage,
    FormsModule,
    CommonWebLibModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    IonicModule,

    MatButtonModule,
    MatStepperModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    NgOptimizedImage,
    MatIconModule,
    AsyncPipe,
    QRCodeModule,
  ],
})
export class ManagePda1RequestsModule {}
