import { Route, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { ManagePda1RequestsHomeComponent } from "./manage-pda1-requests-home/manage-pda1-requests-home.component";

export const appRoutes: Route[] = [
  {
    path: "",
    component: ManagePda1RequestsHomeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule],
})
export class IssuerManagePda1RequestsRoutingModule {}
