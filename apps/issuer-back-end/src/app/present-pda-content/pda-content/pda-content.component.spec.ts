import { ComponentFixture, TestBed } from "@angular/core/testing";
import { PdaContentComponent } from "./pda-content.component";
import { MockComponent } from "ng2-mock-component";
import { ActivatedRoute, Data } from "@angular/router";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { RequestFormCollectionsRestService } from "../../manage-pda1-requests/manage-pda1-requests-home/request-form-table/rest/request-form-collections-rest.service";
import { of } from "rxjs";

describe("PdaContentComponent", () => {
  let component: PdaContentComponent;
  let fixture: ComponentFixture<PdaContentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [
        PdaContentComponent,
        MockComponent({ selector: "issuer-footer", inputs: ["officeName"] }),
      ],
      providers: [
        {
          provide: RequestFormCollectionsRestService,
          useValue: { findRequestForm: () => of({}) },
        },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              url: [
                {
                  path: "pda-content",
                },
              ],
            },
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(PdaContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
