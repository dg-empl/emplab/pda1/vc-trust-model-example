import { Component, inject } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { RequestFormCollectionsRestService } from "../../manage-pda1-requests/manage-pda1-requests-home/request-form-table/rest/request-form-collections-rest.service";

@Component({
  selector: "issuer-pda-content",
  templateUrl: "./pda-content.component.html",
  styleUrls: ["./pda-content.component.scss"],
})
export class PdaContentComponent {
  private route: ActivatedRoute = inject(ActivatedRoute);
  private requestFormCollRestService: RequestFormCollectionsRestService = inject(
    RequestFormCollectionsRestService
  );

  data: any;

  ngOnInit(): void {
    const requestFormId = this.route.snapshot.url[0].path;
    this.requestFormCollRestService.findRequestForm(requestFormId).subscribe(requestForm => {
      this.data = requestForm;
    });
  }
}
