import { NgModule } from "@angular/core";
import { CommonModule, NgOptimizedImage } from "@angular/common";
import { PdaContentComponent } from "./pda-content/pda-content.component";
import { PresentPdaContentRoutingModule } from "./present-pda-content.routes";
import { CommonWebLibModule } from "@esspass-web-wallet/common-web-lib";

@NgModule({
  declarations: [PdaContentComponent],
  imports: [CommonModule, PresentPdaContentRoutingModule, NgOptimizedImage, CommonWebLibModule],
})
export class PresentPdaContentModule {}
