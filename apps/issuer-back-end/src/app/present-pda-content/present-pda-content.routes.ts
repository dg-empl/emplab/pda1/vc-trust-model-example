import { Route, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { PdaContentComponent } from "./pda-content/pda-content.component";

export const appRoutes: Route[] = [
  {
    path: "present-pda1-content/:id",
    component: PdaContentComponent,
  },
  {
    path: "**",
    component: PdaContentComponent,
  },
  {
    path: "",
    component: PdaContentComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule],
})
export class PresentPdaContentRoutingModule {}
