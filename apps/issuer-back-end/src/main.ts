import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { AppModule } from "./app/app.module";
import { environment } from "./environments/environment";
import { enableProdMode } from "@angular/core";

if (environment.production) {
  enableProdMode();
}

console.log("ENV_NAME : ", process.env["ENVIRONMENT_NAME"]);
console.log("APP_VERSION : ", process.env["APP_VERSION"]);

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch(err => console.error(err));
