import * as qs from "qs";
import axios, { AxiosResponse } from "axios";
import { NotFoundError } from "@cef-ebsi/problem-details-errors";
import { logger } from "@esspass-web-wallet/logging-lib";
import IssuerService from "../application/issuer.service";

const log = logger("issuer-backoffice");

const issuerService: IssuerService = IssuerService.getInstance(log);

export type CredentialOffer = {
  credential_offer?: string; // The credential_offer is a stringified CredentialOfferPayload
  credential_offer_uri?: string; // URI to the credential offer
  user_pin?: string; // The user_pin is a string field UserPinPayload
};

export const getQrCodeAndPin = (response: string): { pin: string; qrCode: string } => {
  const responseSplit = response.split("");
  const { search } = new URL(response);

  const parsedCredentialOffer = qs.parse(search.slice(1)) as CredentialOffer;
  const pin = parsedCredentialOffer.user_pin;

  const userPinLength = "user_pin=0000".length;
  const qrCode = responseSplit.splice(0, responseSplit.length - userPinLength).join("");

  return {
    qrCode,
    pin,
  };
};

export type SendEmailContent = {
  pinCode?: string; // pin code
  qrCodeLocation?: string; // qr code location
  email?: string; // the user to receive the email
  pdaId?: string; // the pda1 document identified by pdaId to add to the email body
};

export const commandSendEmail = async (command: SendEmailContent): Promise<string> => {
  let response: AxiosResponse;
  const hostname = `${process.env["PDA_MANAGEMENT_URL"]}` + `/v3/send-emails`;

  const bearerToken = await issuerService.getBearerToken();

  try {
    response = await axios.post(hostname, command, {
      headers: {
        accept: "application/json",
        "Content-Type": "application/json",
        authorization: "Bearer " + bearerToken,
      },
    });
  } catch (e: any) {
    log.error("initiateCredentialOfferAdapter: commandSendEmail error : " + e.message);
    throw new NotFoundError(
      "initiateCredentialOfferAdapter: commandSendEmail error : " + e.message
    );
  }

  return await response.data;
};
