import IssuerService from "../application/issuer.service";
import GetInitiateCredentialOfferDto from "../../dto/get-initiate-credential-offer.dto";
import { logger } from "@esspass-web-wallet/logging-lib";
import {
  CredentialIssuerMetadata,
  CredentialOfferPayload,
  OAuth2Error,
} from "@esspass-web-wallet/crypto-lib";
import { Request, Response } from "express";
import { commandSendEmail, getQrCodeAndPin } from "./adapter-utils";
import { NotFoundError } from "@esspass-web-wallet/domain-lib";

const log = logger("issuer-backoffice");

const issuerService: IssuerService = IssuerService.getInstance(log);

// @Get("/initiate-credential-offer")
// @HttpCode(200)
// @Header("content-type", "text/plain; charset=utf-8")
export const initiateCredentialOfferAdapter = async (req: Request, res: Response) => {
  // log.debugJSON("initiateCredentialOfferAdapter entry : ", req.query);
  // log.debugJSON("initiateCredentialOfferAdapter entry headers : ", req.headers);

  const { query } = req;

  const dtoQuery = query as unknown as GetInitiateCredentialOfferDto;
  const { email, pdaId } = dtoQuery;

  const location = await issuerService.initiateCredentialOffer(dtoQuery);

  log.debugJSON("getQrCodeAndPin : ", getQrCodeAndPin(location));

  try {
    await commandSendEmail({
      ...getQrCodeAndPin(location),
      email: email,
      pdaId: pdaId,
    });
  } catch (e) {
    log.error("Error sending email for V3 credentials: " + e);
    throw new NotFoundError("Error sending email for V3 credentials: " + e);
  }

  /**
   * The HTTP 200 OK response to the Client MUST include a text/plain response body
   */
  log.debug("initiateCredentialOfferAdapter exit");
  res.send(location);
};

/**
 * Credential Offer by Reference
 * @see https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0-11.html#section-4.1.3
 */
// @Get("/offers/:credentialOfferId")
// @HttpCode(201)
export const credentialOfferAdapter = async (req, res): Promise<CredentialOfferPayload> => {
  log.debug("credentialOfferAdapter entry : " + JSON.stringify(req.params));
  const { params } = req;

  // grant.io DataWallet send data with extra quotes
  let credentialOfferId = params.credentialOfferId;
  if (credentialOfferId.endsWith('"')) {
    credentialOfferId = credentialOfferId.slice(0, -1);
  }

  const offer = await issuerService.getCredentialOffer(credentialOfferId);
  log.debug("credentialOfferAdapter exit :" + JSON.stringify(offer));
  return res.send(offer);
};

// @HttpCode(200)
// @Get("/.well-known/openid-credential-issuer")
export const credentialIssuerMetadataAdapter = async (
  req,
  res
): Promise<CredentialIssuerMetadata> => {
  log.debug("credentialIssuerMetadataAdapter entry");
  const metaData = issuerService.getCredentialIssuerMetadata();
  log.debug("credentialIssuerMetadataAdapter exit :" + JSON.stringify(metaData));
  return res.send(metaData);
};

// @HttpCode(200)
// @Post("/credential")
// postCredential(
// @Headers("content-type") contentType: string,
// @Headers("authorization") authorizationHeader: string,
// @Body() body: unknown
export const postAcceptanceTokenAdapter = async (req, res) => {
  log.debug("postAcceptanceTokenAdapter entry");
  // Only accept application/json
  // https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0-11.html#section-7.2
  const { headers, body } = req;
  const contentType = headers["content-type"];
  const authorizationHeader = headers["authorization"];

  // Only accept application/json
  // https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0-11.html#section-7.2
  if (!contentType.toLowerCase().includes("application/json")) {
    throw new OAuth2Error("invalid_request", {
      errorDescription: "Content-type must be application/json",
    });
  }

  const acceptanceToken = await issuerService.postCredential(authorizationHeader, body);
  log.debug("postAcceptanceTokenAdapter exit :" + JSON.stringify(acceptanceToken));
  return res.send(acceptanceToken);
};

// @HttpCode(200)
// @Post("/credential_deferred")
export const postCredentialDeferredAdapter = async (req, res) => {
  log.debug("postCredentialDeferredAdapter entry");
  const { headers } = req;
  const authorizationHeader = headers["authorization"];

  const jwt = await issuerService.postCredentialDeferred(authorizationHeader);
  log.debug("postCredentialDeferredAdapter exit :" + JSON.stringify(jwt));
  return res.send(jwt);
};
