import { randomUUID } from "node:crypto";
import { decodeJwt } from "jose";
import axios from "axios";
import { createVerifiablePresentationJwt } from "@cef-ebsi/verifiable-presentation";
import type { EbsiIssuer } from "@cef-ebsi/verifiable-presentation";
import type { PresentationSubmission } from "@sphereon/pex-models";
import type { Cache } from "cache-manager";
import { caching } from "cache-manager";

import { logger } from "@esspass-web-wallet/logging-lib";
import { logAxiosError } from "@esspass-web-wallet/backend-utils-lib";

// Refresh the token if it expires in less than 10 seconds
const REFRESH_LIMIT = 10 * 1000;
export const AUTH_API_PATH = "/authorisation/v3";

export class EBSIAuthorisationService {
  private readonly logger = logger("issuer-backoffice");
  private readonly authorisationApiUrl: string;
  private readonly domain: string;
  private cacheManager: Cache;

  constructor() {
    this.domain = process.env.DOMAIN;
    this.authorisationApiUrl = process.env.DOMAIN + AUTH_API_PATH;
  }

  /**
   * Generic function to request an access token from Authorisation API v3.
   */
  private async requestAccessToken(
    scope: "tir_write" | "didr_invite" | "didr_write" | "tir_invite",
    subject: EbsiIssuer,
    verifiableCredential: string[] = []
  ): Promise<string> {
    const nonce = randomUUID();
    const vpPayload = {
      "@context": ["https://www.w3.org/2018/credentials/v1"],
      type: ["VerifiablePresentation"],
      verifiableCredential,
      holder: subject.did,
    };

    if (!this.cacheManager) {
      this.cacheManager = await caching("memory", {
        max: 100,
        ttl: REFRESH_LIMIT /*milliseconds*/,
      });
    }

    const vpJwt = await createVerifiablePresentationJwt(
      vpPayload,
      subject,
      this.authorisationApiUrl,
      {
        ebsiAuthority: this.domain,
        skipValidation: true,
        nonce,
        ...(verifiableCredential.length === 0 && {
          // Manually add "exp" and "nbf" to the VP JWT because there's no VC to extract from
          exp: Math.floor(Date.now() / 1000) + 100,
          nbf: Math.floor(Date.now() / 1000) - 100,
        }),
      }
    );

    const presentationDefinitionsIds: Record<typeof scope, string> = {
      tir_write: "tir_write_presentation",
      didr_invite: "didr_invite_presentation",
      didr_write: "didr_write_presentation",
      tir_invite: "tir_invite_presentation",
    } as const;

    const presentationSubmission = {
      id: randomUUID(),
      definition_id: presentationDefinitionsIds[scope],
      descriptor_map: [] as PresentationSubmission["descriptor_map"],
    } satisfies PresentationSubmission;

    if (scope === "didr_invite") {
      presentationSubmission.descriptor_map.push({
        id: "didr_invite_credential",
        format: "jwt_vp",
        path: "$",
        path_nested: {
          id: "didr_invite_credential",
          format: "jwt_vc",
          path: "$.verifiableCredential[0]",
        },
      });
    } else if (scope === "tir_invite") {
      presentationSubmission.descriptor_map.push({
        id: "tir_invite_credential",
        format: "jwt_vp",
        path: "$",
        path_nested: {
          id: "tir_invite_credential",
          format: "jwt_vc",
          path: "$.verifiableCredential[0]",
        },
      });
    }

    const response = await axios.post<unknown>(
      `${this.authorisationApiUrl}/token`,
      new URLSearchParams({
        grant_type: "vp_token",
        scope: `openid ${scope}`,
        vp_token: vpJwt,
        presentation_submission: JSON.stringify(presentationSubmission),
      }).toString(),
      {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      }
    );

    const accessToken = (
      response.data as {
        access_token: string;
      }
    ).access_token;

    return accessToken;
  }

  /**
   * Method to get an access token from Authorisation API v3 or from cache if it's still valid.
   */
  async getAccessToken(
    scope: "tir_write" | "didr_invite" | "didr_write" | "tir_invite",
    subject: EbsiIssuer,
    verifiableCredential: string[] = []
  ) {
    const cacheKey = `access_token::${scope}::${subject.did}`;
    const cachedAccessToken = await this.cacheManager.get<string>(cacheKey);

    if (cachedAccessToken) {
      return cachedAccessToken;
    }

    try {
      const accessToken = await this.requestAccessToken(scope, subject, verifiableCredential);

      // Only cache "tir_write" and "didr_write" tokens
      if (scope === "tir_write" || scope === "didr_write") {
        // Decode access token
        const accessTokenExp = Number(decodeJwt(accessToken).exp);

        // TTL (in milliseconds)
        // JWT exp is expressed in seconds, Date.now() and REFRESH_LIMIT in milliseconds
        const ttl = accessTokenExp * 1000 - Date.now() - REFRESH_LIMIT;

        await this.cacheManager.set(cacheKey, accessToken, ttl);
      }

      return accessToken;
    } catch (err) {
      if (err instanceof Error) {
        if (axios.isAxiosError(err)) {
          logAxiosError(err, this.logger);
        } else {
          this.logger.errorObj(err.message, err.stack);
        }
      } else {
        this.logger.error(err);
      }
      throw err;
    }
  }
}

export default EBSIAuthorisationService;
