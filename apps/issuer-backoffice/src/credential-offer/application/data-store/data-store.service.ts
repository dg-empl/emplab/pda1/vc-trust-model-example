import type { Cache } from "cache-manager";
import type { IntentEvent, VCWithLinkedAttr } from "./data-store.interface";
import { logger } from "@esspass-web-wallet/logging-lib";
import { caching } from "cache-manager";

export class DataStoreService {
  private readonly log = logger("issuer-backoffice");
  private cacheManager: Cache;
  private readonly REFRESH_LIMIT = 10 * 1000;

  async setVCWithLinkedAttr(key: string, value: VCWithLinkedAttr) {
    this.log.debug(`Pushing data for ${key}: ${JSON.stringify(value)}}`);
    return this.cacheManager.set(key, value);
  }

  async getVCWithLinkedAttr(key: string) {
    return this.cacheManager.get<VCWithLinkedAttr>(key);
  }

  async pushEvent(key: string, event: IntentEvent) {
    if (!this.cacheManager) {
      this.cacheManager = await caching("memory", {
        max: 100,
        ttl: this.REFRESH_LIMIT /*milliseconds*/,
      });
    }
    const existingEvents = await this.getEvents(key);

    let events: IntentEvent[];
    if (existingEvents) {
      events = [event, ...existingEvents];
    } else {
      events = [event];
    }

    return this.cacheManager.set(key, events, 0);
  }

  getEvents(key: string) {
    return this.cacheManager.get<IntentEvent[]>(key);
  }

  private resetStoreData() {
    this.log.debug("Resetting data store.");
    return this.cacheManager.reset();
  }
}

export default DataStoreService;
