import { randomBytes, randomUUID } from "node:crypto";
import { decodeJwt, exportJWK, generateKeyPair, importJWK, SignJWT } from "jose";
import { util as keyDidHelpers } from "@cef-ebsi/key-did-resolver";
import GetInitiateCredentialOfferDto from "../../dto/get-initiate-credential-offer.dto";
import { Level } from "level";
import { NotFoundError } from "@cef-ebsi/problem-details-errors";
import { Resolver } from "did-resolver";
import {
  CredentialIssuerMetadata,
  HOLDER_WALLET_AUTHORIZATION_CODE_CREDENTIAL_TYPES,
  getKeyPair,
  KeyPair,
  LevelDbKeyIssuer,
  LevelDbObjectIssuer,
  CredentialOfferPayload,
  CredentialOffer,
  CredentialResponse,
  DeferredCredentialResponse,
  validatePostCredential,
  CT_WALLET_SAME_PRE_AUTHORISED,
  CT_WALLET_CROSS_PRE_AUTHORISED,
  issueCredential,
  CredentialError,
  CT_WALLET_CROSS_DEFERRED,
  CT_WALLET_SAME_DEFERRED,
  getUserPinV3,
} from "@esspass-web-wallet/crypto-lib";
import { getResolver as getEbsiDidResolver } from "@cef-ebsi/ebsi-did-resolver";
import { getResolver as getKeyDidResolver } from "@cef-ebsi/key-did-resolver";

import { Logger } from "@esspass-web-wallet/logging-lib";
import { EbsiVerifiableAttestation } from "@cef-ebsi/verifiable-credential";
import DataStoreService from "./data-store/data-store.service";
import { Cache, caching } from "cache-manager";
import axios, { AxiosResponse } from "axios";
import { IdpTokenService } from "@esspass-web-wallet/security-lib";
import {
  pda1Schema,
  Pda1Type,
  VerifiableCredential,
  verifiableCredentialSchema,
} from "@esspass-web-wallet/domain-lib";

export class IssuerService {
  private static issuerService: IssuerService;
  /**
   * Logger instance
   */
  private static log: Logger;
  private dataStoreService: DataStoreService;
  private cacheManager: Cache;
  private readonly REFRESH_LIMIT = 10 * 1000;
  /**
   * Issuer ES256 and ES256K key pairs (private + public key JWKs)
   */
  private issuerKeyPair: Record<"ES256" | "ES256K", KeyPair | undefined>;
  private readonly authUri: string;
  private readonly issuerUri: string;
  /**
   * No used as provided from issuer-wallet
   */
  // private readonly issuerPrivateKeyHex: string = process.env.ISSUER_PRIVATE_KEY ?? "";

  private readonly issuerKid: string;
  private readonly issuerDid: string;
  private db: Level<LevelDbKeyIssuer, LevelDbObjectIssuer>;
  private readonly DIDR_API_PATH = "/did-registry/v4/identifiers";
  private readonly TSR_API_PATH = "/trusted-schemas-registry/v2/schemas";
  private readonly TIR_API_PATH = "/trusted-issuers-registry/v4/issuers";
  private readonly TPR_API_PATH = "/trusted-policies-registry/v2/users";
  private readonly TIR_JSON_RPC_PATH = "/trusted-issuers-registry/v4/jsonrpc";
  /**
   * Auth ES256 key pair (private + public key JWKs)
   */
  private authKeyPair?: KeyPair;
  /**
   * Auth ES256 private key (hex)
   */
  private readonly authPrivateKeyHex: string;
  /**
   * EBSI DID v1 Resolver
   */
  private readonly ebsiResolver: Resolver;
  /**
   * Key DID v1 Resolver
   */
  private readonly keyResolver: Resolver;
  /**
   * Request timeout
   */
  private readonly timeout: number;
  /**
   * EBSI Authority
   */
  private readonly ebsiAuthority: string;
  /**
   * Issuer accreditation (URL of the attribute in TIR v4)
   */
  private readonly issuerAccreditationUrl: string;
  /**
   * PDA1 schema URI
   */
  private readonly pda1CredentialSchema: string;
  /**
   * EBSI VA schema URI
   */
  private readonly authorisationCredentialSchema: string;
  /**
   * TIR API /issuers endpoint
   */
  private readonly trustedIssuersRegistryApiUrl: string;
  /**
   * TPR API /users endpoint
   */
  private readonly trustedPoliciesRegistryApiUrl: string;
  /**
   * TIR API /jsonrpc endpoint
   */
  private readonly trustedIssuersRegistryApiJsonrpcUrl: string;
  /**
   * Pda1 document id
   */
  private pdaId: string;
  private cacheManagerLevel: {
    interval: ReturnType<typeof setInterval>;
    keys: { key: LevelDbKeyIssuer; expiration: number }[];
  };
  /**
   * DIDR API /identifiers endpoint
   */
  private readonly didRegistryApiUrl: string;
  /**
   * idpTokenService
   */
  private readonly idpTokenService: IdpTokenService;

  constructor() {
    this.authUri = process.env.AUTH_URI ?? "";
    this.issuerUri = process.env.ISSUER_URI ?? "";
    this.issuerKid = process.env.ISSUER_KID;
    this.authPrivateKeyHex = process.env.AUTH_PRIVATE_KEY;
    [this.issuerDid] = this.issuerKid.split("#") as [string];

    this.issuerKeyPair = {
      ES256: undefined,
      ES256K: undefined,
    };

    this.db = new Level<LevelDbKeyIssuer, LevelDbObjectIssuer>("db/issuer", {
      keyEncoding: "json",
      valueEncoding: "json",
    });

    this.cacheManagerLevel = {
      interval: setInterval(() => {
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        this.manageCache().catch(() => {});
      }, 120_000),
      keys: [],
    };

    this.dataStoreService = new DataStoreService();

    this.didRegistryApiUrl = process.env.DOMAIN + this.DIDR_API_PATH;

    this.ebsiResolver = new Resolver(getEbsiDidResolver({ registry: this.didRegistryApiUrl }));

    this.keyResolver = new Resolver(getKeyDidResolver());
    this.timeout = parseInt(process.env.REQUEST_TIMEOUT || "30000", 10);
    this.ebsiAuthority = process.env.DOMAIN.replace(/^https?:\/\//, ""); // remove http protocol scheme

    this.issuerAccreditationUrl = process.env.ISSUER_ACCREDITATION_URL;

    this.pda1CredentialSchema = `${process.env.DOMAIN}${this.TSR_API_PATH}/${process.env.PDA1_CREDENTIAL_SCHEMA_ID}`;

    this.authorisationCredentialSchema = `${process.env.DOMAIN}${this.TSR_API_PATH}/${process.env.AUTHORISATION_CREDENTIAL_SCHEMA_ID}`;

    this.trustedIssuersRegistryApiUrl = `${process.env.DOMAIN}${this.TIR_API_PATH}`;

    this.trustedPoliciesRegistryApiUrl = `${process.env.DOMAIN}${this.TPR_API_PATH}`;

    this.trustedIssuersRegistryApiJsonrpcUrl = `${process.env.DOMAIN}${this.TIR_JSON_RPC_PATH}`;

    /**
     * callback passed
     */
    this.idpTokenService = IdpTokenService.getInstance(IssuerService.log, () => false);
  }

  static getInstance(log: Logger): IssuerService {
    if (!this.issuerService) {
      this.log = log;
      this.issuerService = new IssuerService();
      this.issuerService.openDb().then(() => {
        this.log.info("db/issuer: memory db opened");
      });
    }
    return this.issuerService;
  }

  async openDb() {
    await this.db.open();
  }

  /**
   * Trigger a credential offer from the Issuer.
   *
   * @see https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0-11.html#name-credential-offer
   */
  async initiateCredentialOffer({
    credential_type: credentialType,
    credential_offer_endpoint: credentialOfferEndpoint,
    // TODO fix #73 2.
    // client_id: clientId,
    pdaId: pdaId,
  }: GetInitiateCredentialOfferDto) {
    // Open the database

    const offeredTypes = ["VerifiableCredential", "VerifiableAttestation", credentialType];

    // Define grants based on the flow related to the credential type
    let grants = {};
    const keyPair = await this.getIssuerKeyPair("ES256");
    const signingKey = await importJWK(keyPair.privateKeyJwk, "ES256");

    // Set the PDA ID to be issued to the client
    this.pdaId = pdaId;

    // TODO fix #73 3
    // Generate random client did for the pre-authorized flow - each request from client will have a different did
    const clientKeyPair = await generateKeyPair("ES256");
    const clientPublicKeyJwk = await exportJWK(clientKeyPair.publicKey);
    const clientId = keyDidHelpers.createDid(clientPublicKeyJwk);

    if (HOLDER_WALLET_AUTHORIZATION_CODE_CREDENTIAL_TYPES.includes(credentialType as any)) {
      // Generate a signed JWT as the issuer state
      const issuerState = await new SignJWT({
        client_id: clientId,
        credential_types: offeredTypes,
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: keyPair.publicKeyJwk.kid,
        })
        .setIssuedAt()
        .setExpirationTime("5m")
        .setIssuer(this.issuerUri)
        .setAudience(this.authUri)
        .setSubject(clientId)
        .sign(signingKey);

      grants = {
        authorization_code: {
          issuer_state: issuerState,
        },
      };
    } else {
      // For pre-authorized flow
      const preAuthorizedCode = await new SignJWT({
        client_id: clientId,
        authorization_details: [
          {
            type: "openid_credential",
            format: "jwt_vc",
            locations: [this.issuerUri],
            types: ["VerifiableCredential", "VerifiableAttestation", credentialType],
          },
        ],
      })
        .setProtectedHeader({
          typ: "JWT",
          alg: "ES256",
          kid: keyPair.publicKeyJwk.kid,
        })
        .setIssuedAt()
        .setExpirationTime("5m")
        .setIssuer(this.issuerUri)
        .setAudience(this.authUri)
        .setSubject(clientId)
        .sign(signingKey);

      grants = {
        "urn:ietf:params:oauth:grant-type:pre-authorized_code": {
          "pre-authorized_code": preAuthorizedCode,
          user_pin_required: true,
        },
        // TODO fix #73 1
        user_pin: getUserPinV3(clientId),
      };
    }

    const credentialOffer = {
      credential_issuer: this.issuerUri,
      credentials: [
        {
          format: "jwt_vc",
          types: offeredTypes,
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
        },
      ],
      grants,
    } satisfies CredentialOfferPayload;

    const location = `${credentialOfferEndpoint}?${new URLSearchParams({
      credential_offer: JSON.stringify(credentialOffer),
      // TODO fix #73 1
      user_pin: getUserPinV3(clientId),
    } satisfies CredentialOffer).toString()}`;

    // If the credential offer redirect URI is longer than 500 characters, use `credential_offer_uri` instead of `credential_offer`
    if (location.length > 500) {
      // Store request in cache
      const credentialOfferId = randomUUID();

      // Store credential offer in cache
      const cacheKey = { did: this.issuerDid, credentialOfferId };
      await this.db.put(cacheKey, credentialOffer);
      const ttl = 120_000; // Available for 2 minutes
      this.addKeyToCacheManager(cacheKey, ttl);

      return `${credentialOfferEndpoint}?${new URLSearchParams({
        credential_offer_uri: `${this.issuerUri}/offers/${credentialOfferId}`,
        // TODO fix #73 1
        user_pin: getUserPinV3(clientId),
      } satisfies CredentialOffer).toString()}`;
    }

    return location;
  }

  /**
   * Get Credential Offer by reference
   * @see https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0-11.html#section-4.1.3
   */
  async getCredentialOffer(credentialOfferId: string): Promise<CredentialOfferPayload> {
    const cacheKey = { did: this.issuerDid, credentialOfferId };
    IssuerService.log.debug(`Get Credential Offer from cache with key ${JSON.stringify(cacheKey)}`);
    try {
      return (await this.db.get(cacheKey)) as CredentialOfferPayload;
    } catch (error) {
      throw new NotFoundError(NotFoundError.defaultTitle, {
        detail: `No Credential Offer found with the ID ${credentialOfferId}`,
      });
    }
  }

  getCredentialIssuerMetadata(): CredentialIssuerMetadata {
    return {
      credential_issuer: this.issuerUri,
      authorization_server: this.authUri,
      credential_endpoint: `${this.issuerUri}/credential`,
      deferred_credential_endpoint: `${this.issuerUri}/credential_deferred`,
      credentials_supported: [
        {
          format: "jwt_vc",
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationToOnboard",
          ],
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          display: [
            {
              name: "Verifiable Authorisation to onboard",
              locale: "en-GB",
            },
          ],
        },
        {
          format: "jwt_vc",
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAccreditation",
            "VerifiableAccreditationToAttest",
          ],
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          display: [
            {
              name: "Verifiable Accreditation to attest",
              locale: "en-GB",
            },
          ],
        },
        {
          format: "jwt_vc",
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAccreditation",
            "VerifiableAccreditationToAccredit",
          ],
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          display: [
            {
              name: "Verifiable Accreditation to accredit",
              locale: "en-GB",
            },
          ],
        },
        {
          format: "jwt_vc",
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "VerifiableAuthorisationForTrustChain",
          ],
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          display: [
            {
              name: "Verifiable Authorisation to issue verifiable tokens",
              locale: "en-GB",
            },
          ],
        },
        {
          format: "jwt_vc",
          types: ["VerifiableCredential", "VerifiableAttestation", "CTAAQualificationCredential"],
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          display: [
            {
              name: "Verifiable Attestation Conformance Qualification To Accredit & Authorise",
              locale: "en-GB",
            },
          ],
        },
        {
          format: "jwt_vc",
          types: [
            "VerifiableCredential",
            "VerifiableAttestation",
            "CTWalletQualificationCredential",
          ],
          trust_framework: {
            name: "ebsi",
            type: "Accreditation",
            uri: "TIR link towards accreditation",
          },
          display: [
            {
              name: "Verifiable Attestation Conformance Qualification Holder Wallet",
              locale: "en-GB",
            },
          ],
        },
      ],
    };
  }

  /**
   * Load Auth key pair from environment.
   *
   * @returns The private and public key JWKs (including "kid")
   */
  async getAuthKeyPair() {
    if (!this.authKeyPair) {
      this.authKeyPair = await getKeyPair(this.authPrivateKeyHex);
    }

    return this.authKeyPair;
  }

  /**
   * Get bearer token from idp-token-service
   */
  async getBearerToken(): Promise<string> {
    return await this.idpTokenService.bearerToken();
  }

  /**
   * Get PDA1 from pda-management
   *
   * @returns The PDA1 document
   */
  async getPda1(requestFormId: string): Promise<string> {
    let response: AxiosResponse;
    const hostname = `${process.env["PDA_MANAGEMENT_URL"]}` + `/pda1/request-form/${requestFormId}`;

    IssuerService.log.debug("IssuerService: getPda1 hostname: " + hostname);

    const bearerToken = await this.idpTokenService.bearerToken();

    try {
      response = await axios({
        method: "GET",
        url: hostname,

        headers: {
          accept: "application/json",
          "Content-Type": "application/json",
          authorization: "Bearer " + bearerToken,
        },
      });
    } catch (e: any) {
      IssuerService.log.error("IssuerService: getPda1 error : " + e.message);
      throw new NotFoundError("IssuerService: pda-management connection error : " + e.message);
    }

    return await response.data;
  }

  /**
   * Sign PDA1 in issuer-wallet
   *
   * @returns The signed Pda1 document
   */
  async signVC(vcPda1Json: VerifiableCredential) {
    let response: AxiosResponse;

    const hostname = `${process.env.ISSUER_WALLET_URL}/create-vc-from-cs`;

    IssuerService.log.debug("IssuerService: signVC hostname: " + hostname);

    const bearerToken = await this.idpTokenService.bearerToken();

    try {
      response = await axios({
        method: "POST",
        url: hostname,
        data: JSON.stringify(vcPda1Json),
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + bearerToken,
        },
      });
    } catch (e: any) {
      IssuerService.log.error("IssuerService: signVC error : " + e.message);
      throw new NotFoundError("IssuerService: issuer-wallet connection error : " + e.message);
    }

    const result = await response.data;

    if (result && result.errors) {
      throw new CredentialError("invalid_or_missing_proof", {
        errorDescription: `issuer-wallet : signVC: ${result.errors[0].message}`,
      });
    }

    IssuerService.log.debug("signVC:response-status : " + response.status);
    IssuerService.log.debug("signVC:response : " + JSON.stringify(result));

    if (response.status !== 200) {
      throw new Error(
        `Unexpected response status : credential-generator-v2.service , signVC() response : ${response.status}`
      );
    }

    IssuerService.log.debug("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
    IssuerService.log.debug("signedVC : " + JSON.stringify(result));
    IssuerService.log.debug("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
    IssuerService.log.debug("signVC:finished");
    return result;
  }

  /**
   *
   * @param authorizationHeader
   * @param rawRequestBody
   */
  async postCredential(
    authorizationHeader: string,
    rawRequestBody: unknown
  ): Promise<CredentialResponse | DeferredCredentialResponse> {
    if (!this.cacheManager) {
      this.cacheManager = await caching("memory", {
        max: 100,
        ttl: this.REFRESH_LIMIT /*milliseconds*/,
      });
    }

    const { credentialRequest, accessTokenPayload, proofIssuerDid } = await validatePostCredential(
      this.issuerUri,
      (
        await this.getAuthKeyPair()
      ).publicKeyJwk,
      this.ebsiResolver,
      this.keyResolver,
      this.timeout,
      authorizationHeader,
      rawRequestBody
    );

    // Store c_nonce to prevent replay attacks
    const dbKey = {
      did: this.issuerDid,
      nonceAccessToken: accessTokenPayload.claims.c_nonce,
    };
    await this.db.put(dbKey, { nonce: accessTokenPayload.claims.c_nonce });
    // `accessTokenPayload.exp` is expressed in seconds, convert to milliseconds
    const ttl = accessTokenPayload.exp * 1000 - Date.now();
    this.addKeyToCacheManager(dbKey, ttl);

    const pdaObject = await this.getPda1(this.pdaId);

    let pdaPayload: Pda1Type;
    try {
      pdaPayload = pda1Schema.parse(pdaObject);
    } catch (e) {
      throw new NotFoundError("invalid_request", {
        detail: "PDA1 document not found or invalid : " + this.pdaId,
      });
    }

    IssuerService.log.debug("PDA1 Id: " + this.pdaId);

    const { vcJwt } = await issueCredential(
      this.idpTokenService.issuerDid,
      this.issuerAccreditationUrl,
      credentialRequest.types.includes("VerifiablePortableDocumentA1")
        ? this.pda1CredentialSchema
        : this.authorisationCredentialSchema,
      {},
      credentialRequest,
      // accessTokenPayload,
      pdaPayload,
      proofIssuerDid
    );

    const vcPayload = verifiableCredentialSchema.parse(JSON.parse(vcJwt));
    const vcJwtSigned = await this.signVC(vcPayload);

    if (credentialRequest.types.includes("CTWalletSamePreAuthorised")) {
      await this.dataStoreService.pushEvent(accessTokenPayload.sub, {
        success: true,
        timestamp: new Date().getTime(),
        intent: CT_WALLET_SAME_PRE_AUTHORISED,
      });
    } else if (credentialRequest.types.includes("CTWalletCrossPreAuthorised")) {
      await this.dataStoreService.pushEvent(accessTokenPayload.sub, {
        success: true,
        timestamp: new Date().getTime(),
        intent: CT_WALLET_CROSS_PRE_AUTHORISED,
      });
    }

    // Deferred issuance flows
    const deferredCredentials = [
      "VerifiableAuthorisationForTrustChain",
      "CTWalletCrossDeferred",
      "CTWalletSameDeferred",
      "VerifiablePortableDocumentA1",
    ] as const;
    if (credentialRequest.types.some(type => deferredCredentials.includes(type as any))) {
      // Create random acceptance token
      const acceptanceToken = Buffer.from(randomBytes(32)).toString("base64url");

      // Store VC JWT in cache
      const deferredCredentialId = this.getDeferredCredentialId(acceptanceToken);
      await this.cacheManager.set(
        deferredCredentialId,
        {
          // vcJwt,
          vcJwtSigned,
          notBefore: Date.now() + 3000, // Available in 3 seconds
        },
        300_000
      ); // ttl: 5 minutes (5 * 60 * 1000)

      // Return acceptance token
      return {
        acceptance_token: acceptanceToken,
      };
    }

    return {
      format: "jwt_vc",
      // credential: vcJwt,
      credential: vcJwtSigned,
    };
  }

  async postCredentialDeferred(authorizationHeader: string): Promise<CredentialResponse> {
    if (!authorizationHeader) {
      throw new CredentialError("invalid_token", {
        errorDescription: "Authorization header is missing",
      });
    }

    if (!authorizationHeader.startsWith("Bearer ")) {
      throw new CredentialError("invalid_token", {
        errorDescription: "Authorization header must contain a Bearer token",
      });
    }

    // Get the deferred credential from the cache
    const accessToken = authorizationHeader.replace("Bearer ", "");
    const deferredCredentialId = this.getDeferredCredentialId(accessToken);
    const deferredCredential = await this.cacheManager.get<{
      vcJwt: string;
      notBefore: number;
    }>(deferredCredentialId);

    if (!deferredCredential) {
      throw new CredentialError("invalid_token", {
        errorDescription: "Deferred credential not found",
      });
    }

    if (Date.now() < deferredCredential.notBefore) {
      throw new CredentialError("invalid_request", {
        errorDescription: "Deferred credential not available yet",
      });
    }

    await this.cacheManager.del(deferredCredentialId);

    const deferredCredentialPayload = decodeJwt(deferredCredential["vcJwtSigned"]["vc"]); // We assume the decoding won't fail
    const vc = deferredCredentialPayload["vc"] as EbsiVerifiableAttestation;

    if (vc.type.includes("CTWalletCrossDeferred")) {
      await this.dataStoreService.pushEvent(vc.credentialSubject.id as string, {
        success: true,
        timestamp: new Date().getTime(),
        intent: CT_WALLET_CROSS_DEFERRED,
      });
    } else if (vc.type.includes("CTWalletSameDeferred")) {
      await this.dataStoreService.pushEvent(vc.credentialSubject.id as string, {
        success: true,
        timestamp: new Date().getTime(),
        intent: CT_WALLET_SAME_DEFERRED,
      });
    }

    return {
      format: "jwt_vc",
      credential: deferredCredential["vcJwtSigned"]["vc"],
    };
  }

  /**
   * Load Issuer key pair from environment.
   *
   * @returns The private and public key JWKs (including "kid")
   */
  private async getIssuerKeyPair(alg: "ES256" | "ES256K"): Promise<KeyPair> {
    let keyPair = this.issuerKeyPair[alg];
    if (keyPair === undefined) {
      keyPair = await getKeyPair(this.authPrivateKeyHex, alg);
      this.issuerKeyPair[alg] = keyPair;
    }
    return keyPair;
  }

  /**
   * Expire key after TTL milliseconds
   * @param key - Key to remove from cache
   * @param ttl - Time to leave, in milliseconds
   */
  private addKeyToCacheManager(key: LevelDbKeyIssuer, ttl: number = Number.MAX_SAFE_INTEGER) {
    this.cacheManagerLevel.keys.push({ key, expiration: Date.now() + ttl });
    // Sort by expiration: The first ones to expire are at the beginning of the list
    this.cacheManagerLevel.keys.sort((a, b) => a.expiration - b.expiration);
  }

  private async manageCache() {
    const now = Date.now();
    // search which ID has not expired yet
    const id = this.cacheManagerLevel.keys.findIndex(a => now < a.expiration);

    if (!id) return;

    // remove the expired keys (from 0 until id-1)
    const expiredKeys = this.cacheManagerLevel.keys.splice(0, id);
    // remove data from database
    await this.db.batch(expiredKeys.map(({ key }) => ({ type: "del", key })));
  }

  /**
   * Helper function to deterministically compute the deferred credential ID (used as key in the cache)
   */
  private getDeferredCredentialId(accessToken: string): string {
    return `deferred-credential::${accessToken}`;
  }
}

export default IssuerService;
