import { IsIn, IsOptional, IsString, Matches } from "class-validator";
import { IsDid } from "./validators";
import {
  HOLDER_WALLET_CREDENTIAL_TYPES,
  PDA1_CREDENTIAL_TYPES,
  HolderWalletCredentialType,
  Pda1CredentialType,
} from "@esspass-web-wallet/crypto-lib";

export class GetInitiateCredentialOfferDto {
  @IsIn([...HOLDER_WALLET_CREDENTIAL_TYPES, ...PDA1_CREDENTIAL_TYPES])
  credential_type!: HolderWalletCredentialType | Pda1CredentialType;

  /**
   * credential_offer_endpoint: OPTIONAL. URL of the Credential Offer Endpoint of a Wallet.
   *
   * @see https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0-11.html#name-client-metadata
   */
  @IsOptional()
  @Matches(/^[a-z][a-z0-9+\-.]*:[a-z0-9+\-./@:_]*$$/, {
    message: "credential_offer_endpoint must be a valid endpoint",
  })
  credential_offer_endpoint = "openid-credential-offer://";

  // TODO fix #73
  @IsDid()
  client_id?: string;

  // "redirect" is only used with "CTWalletQualificationCredential"
  @IsOptional()
  @IsString()
  redirect?: string;

  // "pdaId" is id of the Pda1 credential client wants to download
  @IsOptional()
  @IsString()
  pdaId?: string;

  // "email" is the address of the client wants to download
  @IsOptional()
  @IsString()
  email?: string;
}

export default GetInitiateCredentialOfferDto;
