import { Tracer, Logger, logger } from "@esspass-web-wallet/logging-lib";
const applicationName = "issuer-backoffice";

/**
 * OpenTelemetry
 */
const tracer = new Tracer(applicationName);
import { api } from "@opentelemetry/sdk-node";
/**
 * OpenTelemetry
 */

import * as express from "express";
import "express-async-errors";
import "dotenv/config";

const log: Logger = logger(applicationName);
import { NotFoundError } from "@esspass-web-wallet/domain-lib";
import { errorHandler } from "@esspass-web-wallet/backend-utils-lib";
import {
  postAcceptanceTokenAdapter,
  credentialIssuerMetadataAdapter,
  credentialOfferAdapter,
  initiateCredentialOfferAdapter,
  postCredentialDeferredAdapter,
} from "./credential-offer";

import * as cors from "cors";
import * as bodyParser from "body-parser";

const app = express();

/* needed for TSL termination */
app.set("trust proxy", true);

app.use(cors());

app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);

app.use(bodyParser.json());

function setupRoutes() {
  /**
   * version - version deployed
   */
  app.get("/version", (req, res) => {
    const currentSpan = api.trace.getSpan(api.context.active());
    currentSpan ? log.info("In /version traceId: " + currentSpan) : null;

    return res.send({
      version: process.env.VERSION || "development",
      revision: process.env.GIT_REVISION || "1",
      branch: process.env.GIT_BRANCH || "master",
    });
  });

  /**
   * issuance flow for v3
   */
  app.get("/api/v1/issuer/initiate-credential-offer", initiateCredentialOfferAdapter);
  app.get("/offers/:credentialOfferId", credentialOfferAdapter);
  app.get("/.well-known/openid-credential-issuer", credentialIssuerMetadataAdapter);
  app.post("/credential", postAcceptanceTokenAdapter);
  app.post("/credential_deferred", postCredentialDeferredAdapter);

  /**
   * error handler
   */
  app.all("*", (req, res) => {
    log.debug("Not found route: " + req.url);
    throw new NotFoundError("Not found route");
  });
  app.use(errorHandler(log));
}

function startServer() {
  tracer.getTracer().startActiveSpan("startServer", span => {
    log.info("App name : " + applicationName);
    log.info("App version : " + process.env.VERSION || "development");
    log.info("Environment : " + process.env.ENVIRONMENT || "development");
  });

  const port = 4299;
  const server = app.listen(port, () => log.info(`Listening at port: ${port}`));
  server.on("error", log.error);
}

setupRoutes();
startServer();

export default app;
