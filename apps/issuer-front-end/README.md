```
formState(): any {
     return {
        section1: {
            personalIdentificationNumber: this.form.get("pin")!.value,
            sex: "M",
            surname: this.form.get("surname")!.value,
            forenames: this.form.get("forenames")!.value,
            dateBirth: this.form.get("dateOfBirth")!.value,
            surnameAtBirth: this.form.get("surnameAtBirth")!.value,
            countryOfBirth: this.form.get("countryOfBirth")!.value,
            placeOfBirth: {
                town: this.form.get("placeOfBirth")!.value,
                region: this.form.get("placeOfBirth")!.value,
                countryCode: this.uiSelectedCountry,
            },
            nationalities: [this.uiSelectedCountry],
            stateOfResidenceAddress: { ...this.addressComponent.get(0)!.address },
            stateOfStayAddress: { ...this.stayAddressComponent.get(0)!.address },
   }
 };
```

Version: DEV environment 1.0.0 before deployment

```angular2html
{
  "backend-web-wallet": {
    "version": "20230822-1.0.4-dev",
    "revision": "c6d4429",
    "branch": "main"
  },
  "issuer-backend": {
    "version": "20230821-1.0.1-dev",
    "revision": "308f562",
    "branch": "main"
  },
  "issuer-revocation": {
    "version": "20230822-1.0.2-dev",
    "revision": "c6d4429",
    "branch": "main"
  },
  "issuer-frontend": {
    "version": "20230821-1.0.1-dev",
    "revision": "308f562",
    "branch": "main"
  },
  "issuer-wallet": {
    "version": "20230822-1.0.2-dev",
    "revision": "c6d4429",
    "branch": "main"
  },
  "versions": {
    "version": "20230822-1.0.2-dev",
    "revision": "c6d4429",
    "branch": "main"
  }
}
```

```angular2html
❯ helm list
NAME              	NAMESPACE  	REVISION	UPDATED                                	STATUS  	CHART                   	APP VERSION
backend-web-wallet	dev-esspass	9       	2023-08-22 15:34:35.065784973 +0000 UTC	deployed	backend-web-wallet-0.1.0	1.0.0
issuer-be         	dev-esspass	7       	2023-08-21 15:10:56.803847702 +0000 UTC	deployed	issuer-be-0.1.0         	1.0.0
issuer-fe         	dev-esspass	4       	2023-08-21 14:43:59.582635911 +0000 UTC	deployed	issuer-fe-0.1.0         	1.0.0
issuer-revocation 	dev-esspass	18      	2023-08-22 15:34:04.267681844 +0000 UTC	deployed	issuer-revocation-0.1.0 	1.0.0
issuer-wallet     	dev-esspass	10      	2023-08-22 15:35:38.269372124 +0000 UTC	deployed	issuer-wallet-0.1.0     	1.0.0
keycloak          	dev-esspass	2       	2023-05-28 22:08:18.017335 +0200 CEST  	deployed	keycloak-15.1.2         	21.1.1
mongo-db          	dev-esspass	1       	2023-05-12 13:34:36.270831 +0200 CEST  	deployed	mongo-db-0.1.0          	1.0.0
mongo-express     	dev-esspass	1       	2023-05-15 14:16:52.627062 +0200 CEST  	deployed	mongo-express-0.1.0     	1.0.0
mongodb-configmap 	dev-esspass	6       	2023-06-23 14:34:52.639712 +0200 CEST  	deployed	mongo-config-map-0.1.0  	1.0.0
mongodb-secret    	dev-esspass	8       	2023-05-23 15:00:28.899683 +0200 CEST  	deployed	mongodb-secret-0.1.0    	1.0.0
verifier-be       	dev-esspass	29      	2023-07-19 11:54:08.228284 +0200 CEST  	deployed	verifier-be-0.1.0       	1.0.0
verifier-fe       	dev-esspass	12      	2023-07-19 15:09:36.30059 +0200 CEST   	deployed	verifier-fe-0.1.0       	1.0.0
versions          	dev-esspass	20      	2023-08-22 15:34:43.810936404 +0000 UTC	deployed	versions-0.1.0          	1.0.0
web-wallet        	dev-esspass	14      	2023-08-22 15:44:03.768704129 +0000 UTC	deployed	web-wallet-0.1.0        	1.0.0
~                                                                                                                                                                                                           04:58:01 PM
❯
```

Version: TEST environment 1.0.0 before deployment

```angular2html
❯ curl -sXGET https://versions.pda1.test.esspass-poc.eu/versions | jq
{
  "auth-server": {
    "version": "20230911-1.0.2-test",
    "revision": "df4eb4a",
    "branch": "feature/demo"
  },
  "backend-web-wallet": {
    "version": "20230613-1.0.1-test"
  },
  "issuer-backend": {
    "version": "20230811-1.0.1-test"
  },
  "issuer-backoffice": {
    "version": "20230817-1.0.1-test",
    "revision": "80118b2",
    "branch": "feature/demo-v3"
  },
  "issuer-frontend": {
    "version": "20230605-1.0.2-test"
  },
  "issuer-wallet": {
    "version": "20230604-1.0.3-demo"
  },
  "versions": {
    "version": "20230915-1.0.1-test",
    "revision": "4ce47ee",
    "branch": "feature/no-client-id"
  }
}
```

```angular2html
❯ helm list
NAME              	NAMESPACE   	REVISION	UPDATED                              	STATUS  	CHART                   	APP VERSION
auth-server       	test-esspass	6       	2023-09-15 13:51:43.619728 +0200 CEST	deployed	auth-server-0.1.0       	1.0.0
backend-web-wallet	test-esspass	44      	2023-06-13 11:26:21.255173 +0200 CEST	deployed	backend-web-wallet-0.1.0	1.0.0
issuer-backoffice 	test-esspass	22      	2023-09-15 13:50:56.423733 +0200 CEST	deployed	issuer-backoffice-0.1.0 	1.0.0
issuer-be         	test-esspass	15      	2023-09-15 13:52:14.850336 +0200 CEST	deployed	issuer-be-0.1.0         	1.0.0
issuer-fe         	test-esspass	4       	2023-06-05 12:06:23.801695 +0200 CEST	deployed	issuer-fe-0.1.0         	1.0.0
issuer-wallet     	test-esspass	1       	2023-06-04 14:42:04.988971 +0200 CEST	deployed	issuer-wallet-0.1.0     	1.0.0
keycloak          	test-esspass	1       	2023-05-30 18:07:52.851528 +0200 CEST	deployed	keycloak-15.1.2         	21.1.1
mongo-config-map  	test-esspass	7       	2023-08-08 14:37:36.032545 +0200 CEST	deployed	mongo-config-map-0.1.0  	1.0.0
mongo-db          	test-esspass	1       	2023-05-31 14:34:08.488859 +0200 CEST	deployed	mongo-db-0.1.0          	1.0.0
mongo-express     	test-esspass	1       	2023-05-31 14:35:32.07637 +0200 CEST 	deployed	mongo-express-0.1.0     	1.0.0
mongodb-secret    	test-esspass	8       	2023-08-31 10:05:21.272752 +0200 CEST	deployed	mongodb-secret-0.1.0    	1.0.0
verifier-be       	test-esspass	25      	2023-09-13 10:50:17.282928 +0200 CEST	deployed	verifier-be-0.1.0       	1.0.0
verifier-fe       	test-esspass	22      	2023-09-13 14:27:17.385646 +0200 CEST	deployed	verifier-fe-0.1.0       	1.0.0
versions          	test-esspass	5       	2023-09-15 09:11:25.979074 +0200 CEST	deployed	versions-0.1.0          	1.0.0
web-wallet        	test-esspass	22      	2023-06-13 14:31:12.721292 +0200 CEST	deployed	web-wallet-0.1.0        	1.0.0
~/development/esspass/github                                                                                                                                                                                04:50:32 PM
❯
```
