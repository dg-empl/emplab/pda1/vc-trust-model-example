import { TestBed } from "@angular/core/testing";
import { AppComponent } from "./app.component";
import { RouterTestingModule } from "@angular/router/testing";
import { MockComponent } from "ng2-mock-component";

describe("AppComponent", () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [
        AppComponent,
        MockComponent({ selector: "issuer-header-menu", inputs: ["id"] }),
        MockComponent({ selector: "issuer-footer", inputs: ["officeName"] }),
      ],
    }).compileComponents();
  });

  it("should render section tag", () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    // const compiled = fixture.nativeElement as HTMLElement;
    // expect(compiled.querySelector("section")?.textContent).toContain("Welcome issuer-front-end");
  });

  // it(`should have as title 'issuer-front-end'`, () => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   const app = fixture.componentInstance;
  //   expect(app.title).toEqual("issuer-front-end");
  // });
});
