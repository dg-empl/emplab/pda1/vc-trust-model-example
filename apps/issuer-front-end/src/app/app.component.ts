import { Component } from "@angular/core";

@Component({
  providers: [],
  selector: "issuer-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  title = "issuer-front-end";
}
