import { PreloadAllModules, Route, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

export const appRoutes: Route[] = [
  {
    path: "home",
    loadChildren: () => import("./main-page/main-page.module").then(m => m.MainPageModule),
  },
  {
    path: "",
    redirectTo: "home",
    pathMatch: "full",
  },
  {
    path: "**",
    pathMatch: "full",
    loadChildren: () => import("./main-page/main-page.module").then(m => m.MainPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
})
export class IssuerFrontEndRoutingModule {}
