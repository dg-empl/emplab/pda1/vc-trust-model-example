import { ComponentFixture, TestBed } from "@angular/core/testing";
import { HeaderMenuComponent } from "./header-menu.component";
import { KeycloakService } from "keycloak-angular";
import { APP_CONFIG } from "@esspass-web-wallet/security-web-lib";
import { MockComponent } from "ng2-mock-component";
import { IonicModule } from "@ionic/angular";

describe("HeaderMenuComponent", () => {
  let component: HeaderMenuComponent;
  let fixture: ComponentFixture<HeaderMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [IonicModule.forRoot()],
      declarations: [
        HeaderMenuComponent,
        MockComponent({ selector: "issuer-login", inputs: ["isLoggedIn"] }),
      ],
      providers: [KeycloakService, { provide: APP_CONFIG, useValue: APP_CONFIG }],
    }).compileComponents();

    fixture = TestBed.createComponent(HeaderMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
