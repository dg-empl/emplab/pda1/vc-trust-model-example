import { Component, inject } from "@angular/core";
import { Router } from "@angular/router";
import { LoginService, ScrollerService } from "@esspass-web-wallet/common-web-lib";

@Component({
  selector: "issuer-header-menu",
  templateUrl: "./header-menu.component.html",
  styleUrls: ["./header-menu.component.scss"],
})
export class HeaderMenuComponent {
  isLoggedIn = false;
  private loginService = inject(LoginService);
  private router = inject(Router);
  private scroller = inject(ScrollerService);

  public async ngOnInit() {
    this.isLoggedIn = await this.loginService.isAuthenticated();

    if (this.isLoggedIn) {
      await this.router.navigate(["/"]);
    }
  }

  async onLoginEvent($event: boolean) {
    if (!$event) {
      await this.loginService.logout();
      return;
    }
    await this.loginService.login();
  }

  requestPda() {
    const selector = document.querySelector("#requestPda1Form");
    if (selector !== null) {
      selector.scrollIntoView({ behavior: "smooth" });
      this.scroller.scroll.set("#requestPda1Form");
    }
  }
}
