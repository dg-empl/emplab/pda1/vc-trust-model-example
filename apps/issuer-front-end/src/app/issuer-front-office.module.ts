import { APP_INITIALIZER, NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { HttpClientModule } from "@angular/common/http";
import { KeycloakAngularModule, KeycloakService } from "keycloak-angular";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppComponent } from "./app.component";
import { RouterModule } from "@angular/router";
import { IssuerFrontEndRoutingModule } from "./app.routes";
import {
  APP_CONFIG,
  APP_DI_CONFIG,
  initializeKeycloak,
} from "@esspass-web-wallet/security-web-lib";
import { IonicModule } from "@ionic/angular";
import { NgOptimizedImage } from "@angular/common";
import { CommonWebLibModule } from "@esspass-web-wallet/common-web-lib";
import { HeaderMenuComponent } from "./header-menu/header-menu.component";

@NgModule({
  declarations: [AppComponent, HeaderMenuComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IssuerFrontEndRoutingModule,
    IonicModule.forRoot(),
    RouterModule.forRoot([], { initialNavigation: "enabledNonBlocking" }),
    HttpClientModule,
    KeycloakAngularModule,
    NgOptimizedImage,
    CommonWebLibModule,
  ],
  providers: [
    {
      provide: APP_CONFIG,
      useValue: APP_DI_CONFIG,
    },
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
    },
  ],
  bootstrap: [AppComponent],
})
export class IssuerFrontOfficeModule {}
