import { ComponentFixture, TestBed } from "@angular/core/testing";
import { AboutA1FormComponent } from "./about-a1-form.component";

describe("AboutA1FormComponent", () => {
  let component: AboutA1FormComponent;
  let fixture: ComponentFixture<AboutA1FormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AboutA1FormComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(AboutA1FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
