import { Component } from "@angular/core";
import { CommonModule } from "@angular/common";

@Component({
  selector: "issuer-about-a1-form",
  templateUrl: "./about-a1-form.component.html",
  styleUrls: ["./about-a1-form.component.scss"],
})
export class AboutA1FormComponent {}
