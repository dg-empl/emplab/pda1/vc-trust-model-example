import { NgModule } from "@angular/core";
import { CommonModule, NgOptimizedImage } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { MainPageComponent } from "./main-page/main-page.component";
import { MainPageRoutingModule } from "./main-page-routing.module";
import { AboutA1FormComponent } from "./about-a1-form/about-a1-form.component";
import { RequestA1FormComponent } from "./request-a1-form/request-a1-form.component";
import { Pda1FormComponent } from "./request-a1-form/pda1-form/pda1-form.component";
import { Pda1PersonalDetailsComponent } from "./request-a1-form/pda1-form/1-pda1-personal-details/pda1-personal-details.component";
import { Pda1TocComponent } from "./request-a1-form/pda1-form/pda1-toc/pda1-toc.component";
import { Pda1AddressComponent } from "./request-a1-form/pda1-form/1-pda1-personal-details/pda1-address/pda1-address.component";
import { Pda1StayAddressComponent } from "./request-a1-form/pda1-form/1-pda1-personal-details/pda1-stay-address/pda1-stay-address.component";
import { Pda1MemberStateWhichAppliedComponent } from "./request-a1-form/pda1-form/2-pda1-member-state-which-applied/pda1-member-state-which-applied.component";
import { Pda1StatusConfirmationComponent } from "./request-a1-form/pda1-form/3-pda1-status-confirmation/pda1-status-confirmation.component";
import { Pda1DetailsEmployerComponent } from "./request-a1-form/pda1-form/4-pda1-details-employer/pda1-details-employer.component";
import { Pda1DetailsEmployerAddressComponent } from "./request-a1-form/pda1-form/4-pda1-details-employer/pda1-details-employer-address/pda1-details-employer-address.component";
import { Pda1ActivityPursuedComponent } from "./request-a1-form/pda1-form/5-pda1-activity-pursued/pda1-activity-pursued.component";
import { Pda1InstitutionCompletingComponent } from "./request-a1-form/pda1-form/6-pda1-institution-completing/pda1-institution-completing.component";
import { NgSelectModule } from "@ng-select/ng-select";
import { Pda1SubmitComponent } from "./request-a1-form/pda1-form/pda1-submit/pda1-submit.component";
import { CommonWebLibModule } from "@esspass-web-wallet/common-web-lib";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MainPageRoutingModule,
    NgOptimizedImage,
    ReactiveFormsModule,
    NgSelectModule,
    CommonWebLibModule,
  ],
  declarations: [
    MainPageComponent,
    AboutA1FormComponent,
    RequestA1FormComponent,
    Pda1FormComponent,
    Pda1PersonalDetailsComponent,
    Pda1AddressComponent,
    Pda1StayAddressComponent,
    Pda1TocComponent,
    Pda1MemberStateWhichAppliedComponent,
    Pda1StatusConfirmationComponent,
    Pda1DetailsEmployerComponent,
    Pda1DetailsEmployerAddressComponent,
    Pda1ActivityPursuedComponent,
    Pda1InstitutionCompletingComponent,
    Pda1SubmitComponent,
  ],
})
export class MainPageModule {}
