import { ComponentFixture, TestBed } from "@angular/core/testing";
import { MainPageComponent } from "./main-page.component";
import { MockComponent } from "ng2-mock-component";
import { KeycloakService } from "keycloak-angular";
import { APP_CONFIG } from "@esspass-web-wallet/security-web-lib";
import { LoginService } from "@esspass-web-wallet/common-web-lib";

describe("MainPageComponent", () => {
  let component: MainPageComponent;
  let fixture: ComponentFixture<MainPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        MainPageComponent,
        MockComponent({ selector: "issuer-header-menu" }),
        MockComponent({ selector: "issuer-about-a1-form" }),
        MockComponent({ selector: "issuer-request-a1-form" }),
        MockComponent({ selector: "issuer-footer", inputs: ["officeName"] }),
        MockComponent({ selector: "issuer-login-button", inputs: ["isLoggedIn", "loggedLabel"] }),
      ],
      providers: [
        LoginService,
        KeycloakService,
        {
          provide: APP_CONFIG,
          useValue: {},
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(MainPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
