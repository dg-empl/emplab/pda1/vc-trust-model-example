import { Component, inject } from "@angular/core";
import { Router } from "@angular/router";
import { LoginService, ScrollerService } from "@esspass-web-wallet/common-web-lib";

@Component({
  selector: "issuer-main-page",
  templateUrl: "./main-page.component.html",
  styleUrls: ["./main-page.component.scss"],
})
export class MainPageComponent {
  isLoggedIn = false;
  private router = inject(Router);
  private loginService = inject(LoginService);
  private scroller = inject(ScrollerService);

  public async ngOnInit() {
    this.isLoggedIn = await this.loginService.isAuthenticated();

    if (this.isLoggedIn) {
      await this.router.navigate(["/"]);
    }
  }

  async login() {
    await this.loginService.login();
  }

  requestPda() {
    const selector = document.querySelector("#requestPda1Form");
    if (selector !== null) {
      selector.scrollIntoView({ behavior: "smooth" });
      this.scroller.scroll.set("#requestPda1Form");
    }
  }
}
