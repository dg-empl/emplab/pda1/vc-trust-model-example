import { ComponentFixture, TestBed } from "@angular/core/testing";
import { Pda1AddressComponent } from "./pda1-address.component";
import { IonicModule } from "@ionic/angular";
import { FormsModule } from "@angular/forms";

describe("Pda1AddressComponent", () => {
  let component: Pda1AddressComponent;
  let fixture: ComponentFixture<Pda1AddressComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Pda1AddressComponent],
      imports: [IonicModule.forRoot(), FormsModule],
    }).compileComponents();

    fixture = TestBed.createComponent(Pda1AddressComponent);
    component = fixture.componentInstance;
    component.address = {
      streetName: "",
      city: "",
      postCode: "",
      countryCode: "",
    } as any;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
