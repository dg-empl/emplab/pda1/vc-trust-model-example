import { Component, Input, OnChanges, SimpleChanges } from "@angular/core";
import { AddressType } from "@esspass-web-wallet/common-web-lib";

@Component({
  selector: "issuer-pda1-address",
  templateUrl: "./pda1-address.component.html",
  styleUrls: ["./pda1-address.component.scss"],
})
export class Pda1AddressComponent implements OnChanges {
  @Input({ required: true }) address: AddressType;

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.address) {
      this.address = {
        streetName: "",
        city: "",
        postCode: "",
        countryCode: "",
      } as AddressType;
    }
  }
}
