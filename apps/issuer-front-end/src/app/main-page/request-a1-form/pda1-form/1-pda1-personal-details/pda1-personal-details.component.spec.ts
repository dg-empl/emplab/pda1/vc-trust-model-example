import { ComponentFixture, TestBed, waitForAsync } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { Pda1PersonalDetailsComponent } from "./pda1-personal-details.component";
import { MockComponent } from "ng2-mock-component";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { NgSelectModule } from "@ng-select/ng-select";
import { KeycloakService } from "keycloak-angular";

describe("Pda1PersonalDetailsPage", () => {
  let component: Pda1PersonalDetailsComponent;
  let fixture: ComponentFixture<Pda1PersonalDetailsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [
        Pda1PersonalDetailsComponent,
        MockComponent({ selector: "issuer-pda1-address" }),
        MockComponent({ selector: "issuer-pda1-stay-address" }),
      ],
      imports: [IonicModule.forRoot(), HttpClientTestingModule, NgSelectModule],
      providers: [
        {
          provide: KeycloakService,
          useValue: {
            isLoggedIn: () => true,
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(Pda1PersonalDetailsComponent);
    component = fixture.componentInstance;
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
