import { random } from "lodash";
import { countries } from "../countries";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ChangeDetectionStrategy, Component, inject, QueryList, ViewChildren } from "@angular/core";
import { GetFormState } from "../get-form-state";
import {
  AddressType,
  DataPasserService,
  FakeDataByCountry,
  generateDate,
  ScrollerService,
  SubmitterService,
} from "@esspass-web-wallet/common-web-lib";
import { Pda1AddressComponent } from "./pda1-address/pda1-address.component";
import { Pda1StayAddressComponent } from "./pda1-stay-address/pda1-stay-address.component";
import { KeycloakService } from "keycloak-angular";
import { filter } from "rxjs";
import { SelectedCountryService } from "../pda1-ui-services/selected-country.service";

@Component({
  selector: "issuer-pda1-personal-details",
  templateUrl: "./pda1-personal-details.component.html",
  styleUrls: ["./pda1-personal-details.component.scss"],
  changeDetection: ChangeDetectionStrategy.Default,
})
export class Pda1PersonalDetailsComponent extends GetFormState {
  form: FormGroup;

  @ViewChildren(Pda1AddressComponent) addressComponent!: QueryList<Pda1AddressComponent>;
  @ViewChildren(Pda1StayAddressComponent)
  stayAddressComponent!: QueryList<Pda1StayAddressComponent>;

  //address data to pass down to address component
  address: AddressType;
  //countries dropdown
  protected readonly countries = countries;

  private scroller = inject(ScrollerService);
  private dataPasser = inject(DataPasserService);
  private keycloakService = inject(KeycloakService);
  private submitterService = inject(SubmitterService);
  private selectedCountryService = inject(SelectedCountryService);

  private uiSelectedCountry: string;
  private userEmail: string | undefined;

  private readonly MAX_NAT_ID: number = 100_000_000;

  private formControls = {
    pin: null,
    surname: [null, [Validators.required]],
    forenames: [null, [Validators.required]],
    surnameAtBirth: null,
    dateOfBirth: null,
    countryOfBirth: [null, [Validators.required]],
    placeOfBirth: null,
  };

  constructor(private fb: FormBuilder) {
    super();
  }

  async ngOnInit() {
    this.initForm();

    // get user email from keycloak context ( currently logged-in user ) and set it to the form
    const { email } = await this.keycloakService.loadUserProfile();
    this.userEmail = email;

    //select test data based on country
    this.form.get("countryOfBirth")!.valueChanges.subscribe(() => {
      const uiSelectedCountry = this.form.get("countryOfBirth")!.value;

      this.selectedCountryService.country = uiSelectedCountry;

      if (uiSelectedCountry) {
        const { city, streetName, postcode } = this.dataPasser.dataToPass[
          uiSelectedCountry
        ] satisfies FakeDataByCountry;

        this.pushData(streetName, city, postcode, uiSelectedCountry);

        this.uiSelectedCountry = uiSelectedCountry;
      } else {
        this.clearForm();
      }
    });

    this.form.valueChanges.subscribe(() => {
      this.submitterService.validData = this.form.valid;
      this.submitterService.clearSubmit();
    });

    this.submitterService
      .getSubmit()
      .pipe(filter(data => data.submit))
      .subscribe(() => {
         if (this.form) {
          //scroll to error on form and force error
          for (const controlsKey in this.form.controls) {
            if (!this.form.controls[controlsKey].valid) {
              //1st. mark control as dirty
              this.form.controls[controlsKey].markAsDirty();
              //2nd. scroll to error
              this.scroller.scrollToElement(`#${controlsKey}`);
              //3rd. stop submit
              this.submitterService.validData = false;
              //4th. clear toc
              this.scroller.clearAllSelected();
              this.scroller.addCssSelectedClassToElement(`#requestPda1Form`);
              return;
            }
          }
        }
      });
  }

  /**
   * push data down to address component
   * @param streetName
   * @param city
   * @param postcode
   * @param uiSelectedCountry
   * @private
   */
  private pushData(streetName: string, city: string, postcode: string, uiSelectedCountry: string) {
    this.address = this.addAddressData(streetName, city, postcode, uiSelectedCountry);
    this.form.get("pin")!.setValue(uiSelectedCountry + random(this.MAX_NAT_ID));
    this.form.get("dateOfBirth")!.setValue(generateDate());
    this.form.get("placeOfBirth")!.setValue(city);
  }

  formState(): Partial<any> {
    return {
      part1: {
        personalIdentificationNumber: this.form.get("pin")!.value,
        surname: this.form.get("surname")!.value,
        forenames: this.form.get("forenames")!.value,
        surnameAtBirth: this.form.get("surnameAtBirth")!.value ?? "---",
        gender: "01",
        dateOfBirth: this.form.get("dateOfBirth")!.value,
        nationality: this.uiSelectedCountry,
        placeOfBirth: this.form.get("placeOfBirth")!.value,
        email: this.userEmail ?? "",
        address: {
          street: this.addressComponent.get(0)!.address.streetName,
          number: "",
          town: this.addressComponent.get(0)!.address.city,
          postCode: this.addressComponent.get(0)!.address.postCode,
          countryCode: this.uiSelectedCountry,
        },
      },
    };
  }

  private addAddressData(
    streetName: string,
    city: string,
    postCode: string,
    uiSelectedCountry: string
  ): AddressType {
    return {
      streetName: streetName ? streetName : "",
      city: city ? city : "",
      postCode: postCode ? postCode : "",
      countryCode: uiSelectedCountry ? uiSelectedCountry : "",
    } satisfies AddressType;
  }

  private initForm() {
    this.form = this.fb.group(this.formControls);
  }

  /**
   * clear form
   * @private
   */
  private clearForm() {
    this.form.get("pin")!.setValue(null);
    this.form.get("surnameAtBirth")!.setValue(null);
    this.form.get("dateOfBirth")!.setValue(null);

    this.form.get("countryOfBirth")!.setValue(null, { emitEvent: false });
    this.form.get("placeOfBirth")!.setValue(null);

    this.address = this.addAddressData("", "", "", "");
  }
}
