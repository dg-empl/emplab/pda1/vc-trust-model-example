import { ComponentFixture, TestBed } from "@angular/core/testing";
import { Pda1StayAddressComponent } from "./pda1-stay-address.component";
import { IonicModule } from "@ionic/angular";
import { FormsModule } from "@angular/forms";

describe("Pda1StayAddressComponent", () => {
  let component: Pda1StayAddressComponent;
  let fixture: ComponentFixture<Pda1StayAddressComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Pda1StayAddressComponent],
      imports: [IonicModule.forRoot(), FormsModule],
    }).compileComponents();

    fixture = TestBed.createComponent(Pda1StayAddressComponent);
    component = fixture.componentInstance;
    component.address = {
      streetName: "",
      city: "",
      postCode: "",
      countryCode: "",
    } as any;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
