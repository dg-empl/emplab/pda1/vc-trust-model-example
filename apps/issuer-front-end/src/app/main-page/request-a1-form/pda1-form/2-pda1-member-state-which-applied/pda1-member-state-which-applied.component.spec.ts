import { ComponentFixture, TestBed } from "@angular/core/testing";
import { Pda1MemberStateWhichAppliedComponent } from "./pda1-member-state-which-applied.component";
import { IonicModule } from "@ionic/angular";
import { FormsModule } from "@angular/forms";

describe("Pda1MemberStateWhichAppliedComponent", () => {
  let component: Pda1MemberStateWhichAppliedComponent;
  let fixture: ComponentFixture<Pda1MemberStateWhichAppliedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Pda1MemberStateWhichAppliedComponent],
      imports: [IonicModule.forRoot(), FormsModule],
    }).compileComponents();

    fixture = TestBed.createComponent(Pda1MemberStateWhichAppliedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
