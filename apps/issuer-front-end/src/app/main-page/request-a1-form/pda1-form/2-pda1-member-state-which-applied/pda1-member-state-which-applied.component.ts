import { GetFormState } from "../get-form-state";
import { Component, inject } from "@angular/core";
import {
  ScrollerService,
  generateEndDate,
  generateStartDate,
} from "@esspass-web-wallet/common-web-lib";
import { random } from "lodash";
import { z } from "zod";

@Component({
  selector: "issuer-pda1-member-state-which-applied",
  templateUrl: "./pda1-member-state-which-applied.component.html",
  styleUrls: ["./pda1-member-state-which-applied.component.scss"],
})
export class Pda1MemberStateWhichAppliedComponent extends GetFormState {
  private scroller = inject(ScrollerService);
  /**
   * for the DEMO 29/th Sep 2023, verification country is IT
   * @private
   */
  private readonly VERIFICATION_COUNTRY = "IT";

  memberState = {
    country: this.VERIFICATION_COUNTRY,
    startingDate: `${generateStartDate()}`,
    endingDate: `${generateEndDate()}`,
  };

  onClick($event: MouseEvent) {
    this.scroller.scroll.set("#top1");
  }

  formState(): Partial<any> {
    const data = {
      part2: {
        memberState: this.memberState.country,
        startingDate: this.memberState.startingDate,
        endingDate: this.memberState.endingDate,
      },
    };
    return data;
  }
}
