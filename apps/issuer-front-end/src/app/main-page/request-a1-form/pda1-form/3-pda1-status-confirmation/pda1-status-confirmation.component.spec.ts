import { ComponentFixture, TestBed } from "@angular/core/testing";
import { Pda1StatusConfirmationComponent } from "./pda1-status-confirmation.component";
import { IonicModule } from "@ionic/angular";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { NgSelectModule } from "@ng-select/ng-select";

describe("Pda1StatusConfirmationComponent", () => {
  let component: Pda1StatusConfirmationComponent;
  let fixture: ComponentFixture<Pda1StatusConfirmationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Pda1StatusConfirmationComponent],
      imports: [
        IonicModule.forRoot(),
        ReactiveFormsModule,
        HttpClientTestingModule,
        NgSelectModule,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(Pda1StatusConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
