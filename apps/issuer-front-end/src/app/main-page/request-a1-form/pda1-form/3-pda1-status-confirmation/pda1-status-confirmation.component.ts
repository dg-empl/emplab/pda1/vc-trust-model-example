import { Component, inject, OnInit } from "@angular/core";
import { ScrollerService, SubmitterService } from "@esspass-web-wallet/common-web-lib";
import { GetFormState } from "../get-form-state";
import { AbstractControl, FormBuilder, FormGroup, Validators } from "@angular/forms";

type Answer = "yes" | "no";

function answerValidator(control: AbstractControl): { [key: string]: any } | null {
  const validAnswers: Answer[] = ["yes" as Answer, "no" as Answer];
  return validAnswers.includes(control.value.toLowerCase())
    ? null
    : { invalidAnswer: { value: control.value } };
}

@Component({
  selector: "issuer-pda1-status-confirmation",
  templateUrl: "./pda1-status-confirmation.component.html",
  styleUrls: ["./pda1-status-confirmation.component.scss"],
})
export class Pda1StatusConfirmationComponent extends GetFormState {
  private scroller = inject(ScrollerService);
  private submitterService = inject(SubmitterService);

  form: FormGroup;

  statusConfirmation = {
    postedEmployedPerson: ["no", [answerValidator]],
    employedInTwoOrMoreMemberStates: ["no", [answerValidator]],
    postedSelfEmployedPerson: ["yes", [answerValidator]],
    selfEmployedInTwoOrMoreMemberStates: ["no", [answerValidator]],
    civilServant: ["no", [answerValidator]],
    contractStaff: ["no", [answerValidator]],
    mariners: ["no", [answerValidator]],
    employedAndSelfEmployed: ["no", [answerValidator]],
    civilServantAndEmployedOrSelfEmployed: ["no", [answerValidator]],
    flightCrewMember: ["no", [answerValidator]],
    exception: ["no", [answerValidator]],
    exceptionDescription: ["no exceptions", Validators.required],
    workingInStateUnder21: ["no", [answerValidator]],
  };

  constructor(private fb: FormBuilder) {
    super();
    this.form = this.fb.group(this.statusConfirmation);
  }

  onClick($event: MouseEvent) {
    this.scroller.scroll.set("#top2");
  }

  formState(): Partial<any> {
    const data = {
      part3: {
        postedEmployedPerson: this.form.get("postedEmployedPerson")?.value,
        employedInTwoOrMoreMemberStates: this.form.get("employedInTwoOrMoreMemberStates")?.value,
        postedSelfEmployedPerson: this.form.get("postedSelfEmployedPerson")?.value,
        selfEmployedInTwoOrMoreMemberStates: this.form.get("selfEmployedInTwoOrMoreMemberStates")
          ?.value,
        civilServant: this.form.get("civilServant")?.value,
        contractStaff: this.form.get("contractStaff")?.value,
        mariners: this.form.get("mariners")?.value,
        employedAndSelfEmployed: this.form.get("employedAndSelfEmployed")?.value,
        civilServantAndEmployedOrSelfEmployed: this.form.get(
          "civilServantAndEmployedOrSelfEmployed"
        )?.value,
        flightCrewMember: this.form.get("flightCrewMember")?.value,
        exception: this.form.get("exception")?.value,
        exceptionDescription: this.form.get("exceptionDescription")?.value,
        employedOrSelfEmployedInState21: this.form.get("workingInStateUnder21")?.value,
      },
    };

    return data;
  }
}
