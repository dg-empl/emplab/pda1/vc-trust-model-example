import { ComponentFixture, TestBed } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";
import { Pda1DetailsEmployerAddressComponent } from "./pda1-details-employer-address.component";
import { FormsModule } from "@angular/forms";

describe("Pda1DetailsEmployerAddressComponent", () => {
  let component: Pda1DetailsEmployerAddressComponent;
  let fixture: ComponentFixture<Pda1DetailsEmployerAddressComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Pda1DetailsEmployerAddressComponent],
      imports: [IonicModule.forRoot(), FormsModule],
    }).compileComponents();

    fixture = TestBed.createComponent(Pda1DetailsEmployerAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
