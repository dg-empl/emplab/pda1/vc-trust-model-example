import { Component, inject, OnInit } from "@angular/core";
import { GetFormState } from "../../get-form-state";
import { AddressType, DataPasserService } from "@esspass-web-wallet/common-web-lib";
import { SelectedCountryService } from "../../pda1-ui-services/selected-country.service";
import { filter } from "rxjs";

@Component({
  selector: "issuer-pda1-details-employer-address",
  templateUrl: "./pda1-details-employer-address.component.html",
  styleUrls: ["./pda1-details-employer-address.component.scss"],
})
export class Pda1DetailsEmployerAddressComponent extends GetFormState implements OnInit {
  private selectedCountryService = inject(SelectedCountryService);
  private dataPasser = inject(DataPasserService);

  registeredAddress = {
    streetName: "",
    postCode: "",
    city: "",
    countryCode: "",
  };

  formState(): AddressType {
    return this.registeredAddress;
  }

  ngOnInit(): void {
    this.selectedCountryService.countryChanged$.subscribe(country => {
      if (!country) {
        this.registeredAddress = {
          countryCode: "",
          streetName: "",
          postCode: "",
          city: "",
        };
      } else {
        const { streetName, postcode, city } = this.dataPasser.dataToPass[country];
        this.registeredAddress = {
          countryCode: country,
          streetName: streetName,
          postCode: postcode,
          city: city,
        };
      }
    });
  }
}
