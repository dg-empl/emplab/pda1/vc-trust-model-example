import { ComponentFixture, TestBed } from "@angular/core/testing";
import { Pda1DetailsEmployerComponent } from "./pda1-details-employer.component";
import { IonicModule } from "@ionic/angular";
import { MockComponent } from "ng2-mock-component";
import { FormsModule } from "@angular/forms";

describe("Pda1DetailsEmployerComponent", () => {
  let component: Pda1DetailsEmployerComponent;
  let fixture: ComponentFixture<Pda1DetailsEmployerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        Pda1DetailsEmployerComponent,
        MockComponent({ selector: "issuer-pda1-details-employer-address" }),
      ],
      imports: [IonicModule.forRoot(), FormsModule],
    }).compileComponents();

    fixture = TestBed.createComponent(Pda1DetailsEmployerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
