import { Component, inject, QueryList, ViewChildren } from "@angular/core";
import {
  ScrollerService,
  AddressType,
  DataPasserService,
} from "@esspass-web-wallet/common-web-lib";
import { GetFormState } from "../get-form-state";
import { random } from "lodash";
import { SelectedCountryService } from "../pda1-ui-services/selected-country.service";
import { z } from "zod";

@Component({
  selector: "issuer-pda1-details-employer",
  templateUrl: "./pda1-details-employer.component.html",
  styleUrls: ["./pda1-details-employer.component.scss"],
})
export class Pda1DetailsEmployerComponent extends GetFormState {
  private scroller = inject(ScrollerService);
  private dataPasser = inject(DataPasserService);
  private selectedCountryService = inject(SelectedCountryService);

  @ViewChildren("detailsEmployerAddress") detailsEmployerAddress: QueryList<GetFormState>;
  private uiSelectedCountry: string;

  onClick($event: MouseEvent) {
    this.scroller.scroll.set("#top3");
  }

  detailsEmployer = {
    employee: "",
    selfEmployedActivity: "",
    employerSelfEmployedActivityCodes: [random(1000000000)],
    nameBusinessName: "",
    postcode: "",
  };

  ngOnInit(): void {
    this.selectedCountryService.countryChanged$.subscribe(country => {
      if (!country) {
        this.detailsEmployer = {
          employee: "",
          selfEmployedActivity: "yes",
          employerSelfEmployedActivityCodes: [0],
          nameBusinessName: "",
          postcode: "",
        };
      } else {
        const { company, postcode } = this.dataPasser.dataToPass[country];
        this.uiSelectedCountry = country;

        this.detailsEmployer = {
          employee: "no",
          selfEmployedActivity: "yes",
          employerSelfEmployedActivityCodes: [random(1000000000)],
          nameBusinessName: company,
          postcode: postcode,
        };
      }
    });
  }

  formState(): Partial<any> {
    const formState = this.detailsEmployerAddress.first.formState() as AddressType;

    const data = {
      part4: {
        employee: this.detailsEmployer.employee,
        selfEmployedActivity: this.detailsEmployer.selfEmployedActivity,
        employedSelfEmployedActivityCode:
          "" + this.detailsEmployer.employerSelfEmployedActivityCodes[0],
        nameOrBusinessName: this.detailsEmployer.nameBusinessName,
        registeredAddress: {
          street: formState.streetName,
          number: "",
          town: formState.city,
          postCode: this.detailsEmployer.postcode,
          countryCode: this.uiSelectedCountry,
        },
      },
    };

    return data;
  }
}
