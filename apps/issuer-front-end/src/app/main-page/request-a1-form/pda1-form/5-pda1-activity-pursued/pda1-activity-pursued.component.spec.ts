import { ComponentFixture, TestBed } from "@angular/core/testing";
import { Pda1ActivityPursuedComponent } from "./pda1-activity-pursued.component";
import { IonicModule } from "@ionic/angular";
import { FormsModule } from "@angular/forms";

describe("Pda1ActivityPursuedComponent", () => {
  let component: Pda1ActivityPursuedComponent;
  let fixture: ComponentFixture<Pda1ActivityPursuedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Pda1ActivityPursuedComponent],
      imports: [IonicModule.forRoot(), FormsModule],
    }).compileComponents();

    fixture = TestBed.createComponent(Pda1ActivityPursuedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
