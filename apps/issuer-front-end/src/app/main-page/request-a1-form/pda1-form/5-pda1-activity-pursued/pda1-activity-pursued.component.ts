import { random } from "lodash";
import { GetFormState } from "../get-form-state";
import { Component, inject } from "@angular/core";
import { ScrollerService } from "@esspass-web-wallet/common-web-lib";

@Component({
  selector: "issuer-pda1-activity-pursued",
  templateUrl: "./pda1-activity-pursued.component.html",
  styleUrls: ["./pda1-activity-pursued.component.scss"],
})
export class Pda1ActivityPursuedComponent extends GetFormState {
  activityPursued = {
    workPlaceName: {
      pursuedBusinessName: "Assicurazioni Generali S.p.A",
    },
    workPlaceAddresses: {
      street: "Piazza Duca degli Abruzzi",
      number: "2",
      town: "Trieste",
      postCode: "" + random(10000),
      countryCode: "IT",
    },
    noFixedAddress: "no",
  };

  address = `${this.activityPursued.workPlaceAddresses.street} ${this.activityPursued.workPlaceAddresses.number}, ${this.activityPursued.workPlaceAddresses.town}, ${this.activityPursued.workPlaceAddresses.postCode}, ${this.activityPursued.workPlaceAddresses.countryCode}`;

  private scroller = inject(ScrollerService);

  onClick($event: MouseEvent) {
    this.scroller.scroll.set("#top4");
  }

  formState(): Partial<any> {
    const data = {
      part5: {
        pursuedActivityName: "Software Development",
        pursuedBusinessName: this.activityPursued.workPlaceName.pursuedBusinessName,
        addressInHostMemberState: this.activityPursued.workPlaceAddresses,
        noFixedAddress: this.activityPursued.noFixedAddress,
      },
    };

    return data;
  }
}
