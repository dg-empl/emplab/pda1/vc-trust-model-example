import { ComponentFixture, TestBed } from "@angular/core/testing";
import { Pda1InstitutionCompletingComponent } from "./pda1-institution-completing.component";
import { IonicModule } from "@ionic/angular";
import { FormsModule } from "@angular/forms";

describe("Pda1InstitutionCompletingComponent", () => {
  let component: Pda1InstitutionCompletingComponent;
  let fixture: ComponentFixture<Pda1InstitutionCompletingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Pda1InstitutionCompletingComponent],
      imports: [IonicModule.forRoot(), FormsModule],
    }).compileComponents();

    fixture = TestBed.createComponent(Pda1InstitutionCompletingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
