import { Component, inject } from "@angular/core";
import { ScrollerService } from "@esspass-web-wallet/common-web-lib";
import { GetFormState } from "../get-form-state";
import { z } from "zod";

@Component({
  selector: "issuer-pda1-institution-completing",
  templateUrl: "./pda1-institution-completing.component.html",
  styleUrls: ["./pda1-institution-completing.component.scss"],
})
export class Pda1InstitutionCompletingComponent extends GetFormState {
  private scroller = inject(ScrollerService);

  institutionCompleting = {
    name: "Office National de Sécurité Sociale / Rijksdienst voor Sociale Zekerheid",
    street: "Main Street 1",
    postCode: "1000",
    town: "Brussels",
    countryCode: "BE",
    institutionId: "NSSI-BE-01",
    officePhoneNumber: "0800 12345",
    officeFaxNumber: "0800 98765",
    email: "esspass.noreply@gmail.com",
    date: "2023-09-14",
    signature: "Official signature",
  };

  onClick($event: MouseEvent) {
    this.scroller.scroll.set("#top5");
  }

  formState(): Partial<any> {
    return {
      part6: this.institutionCompleting,
      part7: {
        dataProcessingConsent: "yes",
        declarationOfHonor:
          "I hereby confirm that the information provided is accurate and complete.",
      },
    };
  }
}
