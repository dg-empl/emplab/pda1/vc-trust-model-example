import { AddressType } from "@esspass-web-wallet/common-web-lib";

export abstract class GetFormState {
  abstract formState(): URLSearchParams | AddressType | Partial<any>;
}
