import { ComponentFixture, TestBed } from "@angular/core/testing";
import { Pda1FormComponent } from "./pda1-form.component";
import { MockComponent } from "ng2-mock-component";
import { KeycloakService } from "keycloak-angular";
import { APP_CONFIG, APP_DI_CONFIG } from "@esspass-web-wallet/security-web-lib";
import { HttpClientTestingModule } from "@angular/common/http/testing";

describe("Pda1FormComponent", () => {
  let component: Pda1FormComponent;
  let fixture: ComponentFixture<Pda1FormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [
        Pda1FormComponent,
        MockComponent({ selector: "issuer-pda1-personal-details", inputs: ["submitCommand"] }),
        MockComponent({ selector: "issuer-pda1-toc" }),
        MockComponent({ selector: "issuer-pda1-member-state-which-applied" }),
        MockComponent({ selector: "issuer-pda1-status-confirmation" }),
        MockComponent({ selector: "issuer-pda1-details-employer" }),
        MockComponent({ selector: "issuer-pda1-activity-pursued" }),
        MockComponent({ selector: "issuer-pda1-institution-completing" }),
        MockComponent({ selector: "issuer-pda1-submit" }),
      ],
      providers: [
        {
          provide: KeycloakService,
          useValue: {
            isLoggedIn: () => true,
          },
        },
        {
          provide: APP_CONFIG,
          useValue: APP_DI_CONFIG,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(Pda1FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
