import { Component, effect, inject, QueryList, ViewChild, ViewChildren } from "@angular/core";
import { ScrollerService, SubmitterService } from "@esspass-web-wallet/common-web-lib";
import { GetFormState } from "./get-form-state";
import { ToastController } from "@ionic/angular";
import { RequestFormRestService } from "./pda1-ui-services/request-form.service";
import { HttpHeaders } from "@angular/common/http";
import { filter, firstValueFrom } from "rxjs";

@Component({
  selector: "issuer-pda1-form",
  templateUrl: "./pda1-form.component.html",
  styleUrls: ["./pda1-form.component.scss"],
})
export class Pda1FormComponent {
  submitCommand: { submit: boolean };
  @ViewChildren("personalDetails") personalDetails: QueryList<GetFormState>;
  @ViewChildren("memberState") memberState: QueryList<GetFormState>;
  @ViewChildren("statusConfirmation") statusConfirmation: QueryList<GetFormState>;
  @ViewChildren("detailsEmployer") detailsEmployer: QueryList<GetFormState>;
  @ViewChildren("activityPursued") activityPursued: QueryList<GetFormState>;
  @ViewChildren("institutionCompleting") institutionCompleting: QueryList<GetFormState>;

  private scroller = inject(ScrollerService);
  private submitter = inject(SubmitterService);
  private toastController = inject(ToastController);
  private requestFormRestService = inject(RequestFormRestService);

  constructor() {
    effect(() => {
      this.scroller.clearAllSelected();

      if (this.scroller.scroll().toString().indexOf("#top") > -1) {
        this.scroller.addCssSelectedClassToElement("#requestPda1Form");
        this.scroller.scrollToElement("#requestPda1Form");
        return;
      }

      this.scroller.addCssSelectedClassToElement(this.scroller.scroll());
      this.scroller.scrollToElement(this.scroller.scroll());
    });

    this.submitter
      .getSubmit()
      .pipe(filter(data => data.submit))
      .subscribe(async data => {
        // submit only if submit signal is true and form data is valid
        if (String(this.submitter.validData) === "true") {
          const requestForm = {
            // part1
            ...this.personalDetails.first.formState(),
            // part2
            ...this.memberState.first.formState(),
            // part3
            ...this.statusConfirmation.first.formState(),
            // part4
            ...this.detailsEmployer.first.formState(),
            // part5
            ...this.activityPursued.first.formState(),
            // part6
            ...this.institutionCompleting.first.formState(),
          };    
            const message = await this.sendToServer(requestForm);
            this.scrollToTopWithMsg(message).then(() => {
              this.submitter.clearSubmit();
            });
        }
      });
  }

  private async scrollToTopWithMsg(message: string) {
    this.scroller.scrollToElement("#top");
    const toast = await this.toastController.create({
      message: message,
      position: "top",
      duration: 4000,
      cssClass: "custom-toast",
    });

    await toast.present();
  }

  private async sendToServer(requestForm: Record<string, any>) {
    let message;
    try {
      /*
      Carefully with the headers, the content-type is mandatory for the backend to understand the request
      Axios library is not used because it does not allow to automatically refresh the token
     */
      const options = {
        headers: new HttpHeaders().append("content-type", "application/json"),
      };

      const response = await firstValueFrom(
        this.requestFormRestService.postRequestA1Form(requestForm, options)
      );

      response.status === 200
        ? (message = "PDA1 successfully submitted.")
        : (message = "Errors when submitting PDA1 form");
    } catch (e: any) {
      console.log(e.message);
      throw new Error(`Errors when submitting PDA1 form : ${e.message}`);
    }
    return message;
  }
}
