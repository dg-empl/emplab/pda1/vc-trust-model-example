import { ComponentFixture, TestBed } from "@angular/core/testing";
import { Pda1SubmitComponent } from "./pda1-submit.component";

describe("Pda1SubmitComponent", () => {
  let component: Pda1SubmitComponent;
  let fixture: ComponentFixture<Pda1SubmitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Pda1SubmitComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(Pda1SubmitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
