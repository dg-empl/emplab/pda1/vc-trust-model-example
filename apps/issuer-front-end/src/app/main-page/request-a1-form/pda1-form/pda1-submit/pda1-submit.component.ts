import { Component, effect, inject, signal } from "@angular/core";
import { SubmitterService } from "@esspass-web-wallet/common-web-lib";
import { filter } from "rxjs";

@Component({
  selector: "issuer-pda1-submit",
  templateUrl: "./pda1-submit.component.html",
  styleUrls: ["./pda1-submit.component.scss"],
})
export class Pda1SubmitComponent {
  private submitter = inject(SubmitterService);
    isSubmitting = false;


  constructor(){
    effect(()=>{
      this.isSubmitting = this.submitter.dataSignalValue;
    });
  }

  onSubmit() {
    if (this.isSubmitting) {
      return; // Prevent multiple submissions
    }
    this.submitter.submit();
  }
}