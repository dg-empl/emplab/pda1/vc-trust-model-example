import { ComponentFixture, TestBed } from "@angular/core/testing";
import { Pda1TocComponent } from "./pda1-toc.component";

describe("Pda1TocComponent", () => {
  let component: Pda1TocComponent;
  let fixture: ComponentFixture<Pda1TocComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [Pda1TocComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(Pda1TocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
