import { AfterViewInit, Component, ElementRef, inject, ViewChild } from "@angular/core";
import { ScrollElementId, ScrollerService } from "@esspass-web-wallet/common-web-lib";

@Component({
  selector: "issuer-pda1-toc",
  templateUrl: "./pda1-toc.component.html",
  styleUrls: ["./pda1-toc.component.scss"],
})
export class Pda1TocComponent implements AfterViewInit {
  private scroller = inject(ScrollerService);

  @ViewChild("requestPda1Form") requestPda1Form: ElementRef;
  @ViewChild("memberState") memberState: ElementRef;
  @ViewChild("statusConfirmation") statusConfirmation: ElementRef;
  @ViewChild("detailsEmployer") detailsEmployer: ElementRef;
  @ViewChild("activityPursued") activityPursued: ElementRef;
  @ViewChild("institutionCompleting") institutionCompleting: ElementRef;

  ngAfterViewInit(): void {
    this.scroller.addElement("#requestPda1Form", this.requestPda1Form);
    this.scroller.addElement("#memberState", this.memberState);
    this.scroller.addElement("#statusConfirmation", this.statusConfirmation);
    this.scroller.addElement("#detailsEmployer", this.detailsEmployer);
    this.scroller.addElement("#detailsEmployer", this.detailsEmployer);
    this.scroller.addElement("#activityPursued", this.activityPursued);
    this.scroller.addElement("#institutionCompleting", this.institutionCompleting);
  }

  onClick($event: MouseEvent, id: ScrollElementId) {
    this.scroller.scroll.set(id);
    this.scroller.scrollToElement(id);
  }
}
