import { Inject, Injectable } from "@angular/core";
import { HttpWrapperService } from "@esspass-web-wallet/web-utils-lib";
import { Observable } from "rxjs";
import { APP_CONFIG, AppConfigInterface } from "@esspass-web-wallet/security-web-lib";

@Injectable({
  providedIn: "root",
})
export class RequestFormRestService {
  constructor(
    private httpService: HttpWrapperService,
    @Inject(APP_CONFIG) private config: AppConfigInterface
  ) {}

  public postRequestA1Form(body: any, options: any): Observable<any> {
    return this.httpService.createWithParams(
      `${this.config.serverUrl}/request-form`,
      body,
      options
    );
  }
}
