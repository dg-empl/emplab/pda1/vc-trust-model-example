import { Injectable, signal, Signal } from "@angular/core";
import { BehaviorSubject, Subject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class SelectedCountryService {
  private readonly countryChanged = new BehaviorSubject("");
  public readonly countryChanged$ = this.countryChanged.asObservable();

  set country(country: string) {
    this.countryChanged.next(country);
  }

  get country(): string {
    return this.countryChanged.value;
  }
}
