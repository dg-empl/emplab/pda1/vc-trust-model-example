import { ComponentFixture, TestBed } from "@angular/core/testing";
import { RequestA1FormComponent } from "./request-a1-form.component";
import { LoginService } from "@esspass-web-wallet/common-web-lib";
import { HttpClientTestingModule } from "@angular/common/http/testing";

describe("RequestA1FormComponent", () => {
  let component: RequestA1FormComponent;
  let fixture: ComponentFixture<RequestA1FormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RequestA1FormComponent],
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: LoginService,
          useValue: { login: jest.fn(), isAuthenticated: jest.fn().mockReturnValue(true) },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(RequestA1FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should calculate signal when logged in", async () => {
    //given
    const loginService = TestBed.inject(LoginService);

    //when
    await component.login();

    //then
    expect(loginService.login).toHaveBeenCalled();
    expect(await loginService.isAuthenticated()).toBeTruthy();
  });
});
