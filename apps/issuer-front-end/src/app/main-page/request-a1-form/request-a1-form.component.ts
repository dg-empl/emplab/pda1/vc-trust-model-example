import { Component, inject, OnDestroy } from "@angular/core";
import {
  LoginService,
  DataPasserService,
  dataSchema,
  FakeData,
} from "@esspass-web-wallet/common-web-lib";
import { HttpClient } from "@angular/common/http";
import { Subscription } from "rxjs";

@Component({
  selector: "issuer-request-a1-form",
  templateUrl: "request-a1-form.component.html",
  styleUrls: ["./request-a1-form.component.scss"],
})
export class RequestA1FormComponent implements OnDestroy {
  isLoggedIn = false;
  private loginService = inject(LoginService);
  private httpClient = inject(HttpClient);
  private dataPasserService = inject(DataPasserService);
  private subscription: Subscription;

  async ngOnInit() {
    this.isLoggedIn = await this.loginService.isAuthenticated();
    this.subscription = this.httpClient
      .get<FakeData>("/assets/demo-data/resident-addresses.json")
      .subscribe(data => {
        dataSchema.parse(data);
        this.dataPasserService.dataToPass = data;
      });
  }

  async login() {
    await this.loginService.login();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
