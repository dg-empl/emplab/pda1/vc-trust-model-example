import "dotenv/config";
import { logger } from "@esspass-web-wallet/logging-lib";
import {
  createCredentialStatus,
  setCredentialStatus,
  getCredentialStatus,
  getCredentialStatusItems,
  createOrGetStatusListStatus,
  returnJWT,
} from "../../applications";
import { nssiBEACFail, NSSI_BE_AC_OK } from "../../schemas/errors/errorCatalog";
import { CredentialStatusRequestItem } from "../../domain";
import { credentialStatusSchema, NotFoundError } from "@esspass-web-wallet/domain-lib";

import { Request, Response } from "express";

const log = logger("issuer-revocation");

async function returnService(req: Request, res: Response, resultObject) {
  try {
    log.trace("returnService: returnService:started");
    res.set({ "Content-Type": "application/json; charset=utf-8" });
    if (resultObject.returnCode !== NSSI_BE_AC_OK) {
      res.status(418);
    } else {
      res.status(200);
    }
    res.json(resultObject);

    log.trace("returnService: services:Done ");
  } catch (ex) {
    log.exception("returnService:An exception Occurred:[" + ex + "]");
    log.trace("returnService:Done");
    const result = nssiBEACFail;
    result.body = "An exception occurred: RETURN SERVICE[" + ex + "].";
    await returnService(req, res, result);
  }
}

const createCredentialStatusAdapter = async (req: Request, res: Response) => {
  log.trace("createCredentialStatusAdapter:started");
  const credentialStatusObject = await createCredentialStatus();
  credentialStatusSchema.parse(credentialStatusObject.body);
  log.trace("createCredentialStatusAdapter:finished");
  res.send(credentialStatusObject.body);
};

const setCredentialStatusAdapter = async (req: Request, res: Response) => {
  let credentialStatusObject: Record<string, any>;
  try {
    log.trace(":setCredentialStatusAdapter:started");

    credentialStatusObject = await setCredentialStatus(req.body);

    log.trace(":setCredentialStatusAdapter:finished");
    await returnService(req, res, credentialStatusObject);
  } catch (ex) {
    log.exception(":setCredentialStatusAdapter:An exception Occurred:[" + ex + "]");
    log.trace(":setCredentialStatusAdapter:Done");
    const result = nssiBEACFail;
    result.body = "An exception occurred:[" + ex + "].";
    await returnService(req, res, result);
  }
};

const credentialStatusAdapter = async (req: Request, res: Response) => {
  try {
    log.trace(":getCredentialStatusAdapter:started");

    const credentialStatusObject = await getCredentialStatus(req.body);

    log.trace(":getCredentialStatusAdapter:finished");
    await returnService(req, res, credentialStatusObject);
  } catch (ex) {
    log.exception(":getCredentialStatusAdapter:An exception Occurred:[" + ex + "]");
    log.trace(":getCredentialStatusAdapter:Done");
    const result = nssiBEACFail;
    result.body = "An exception occurred:[" + ex + "].";
    await returnService(req, res, result);
  }
};

const credentialStatusItemsAdapter = async (req: Request, res: Response) => {
  log.trace("getCredentialStatusItemsAdapter:started");
  const credentialStatusObject = await getCredentialStatusItems(
    req.body as CredentialStatusRequestItem[]
  );
  log.trace("getCredentialStatusItemsAdapter:finished");

  if (!credentialStatusObject || credentialStatusObject.length === 0) {
    throw new NotFoundError("Credential status not found");
  } else {
    res.status(200);
    res.json(credentialStatusObject);
  }
};

/**
 * Beware this function returns text/plain to the EBSI client as EBSI does not support application/json
 * @param req
 * @param res
 * @returns Promise<?>
 */
const createOrGetCredentialStatusListAdapter = async (req: Request, res: Response) => {
  const statusListID = req.params.id;
  log.trace("createOrGetCredentialStatusListAdapter:starting");
  const credentialStatusObject = await createOrGetStatusListStatus(statusListID);
  log.trace("createOrGetCredentialStatusListAdapter:finished");
  await returnJWT(req, res, credentialStatusObject);
};

export {
  createCredentialStatusAdapter,
  createOrGetCredentialStatusListAdapter,
  credentialStatusAdapter,
  credentialStatusItemsAdapter,
  setCredentialStatusAdapter,
};
