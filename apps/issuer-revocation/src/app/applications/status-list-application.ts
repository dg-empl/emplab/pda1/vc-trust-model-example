import {
  CredentialStatusItem,
  CredentialStatusRequestItem,
  Result,
  StatusListResult,
} from "../domain";
import fetch from "node-fetch";
import { gzip, ungzip } from "node-gzip";
import "dotenv/config";
import { Request, Response } from "express";
import { statusList } from "../schemas/ebsi/statusList";
import { nssiBEACException, nssiBEACFail, nssiBEACOk } from "../schemas/errors/errorCatalog";
import {
  NotFoundError,
  statusCredentialListSchema,
  StatusCredentialListType,
} from "@esspass-web-wallet/domain-lib";
import { OAuth2Error } from "@esspass-web-wallet/crypto-lib";
import { logger } from "@esspass-web-wallet/logging-lib";
import { IdpTokenService } from "@esspass-web-wallet/security-lib";

const LIST_SIZE_IN_BITS = 131072;

const log = logger("issuer-revocation");

async function checkAvailableStatusList() {
  try {
    try {
      log.trace("status-list-application:checkAvailableStatusList:starting");

      const statusLists = await statusList.find();
      const result: StatusListResult = { availability: null, statusListID: null };

      if (statusLists.length === 0) {
        result.availability = 0;
        result.statusListID = 0;
        log.debug("status-list-application:checkAvailableStatusList:No statusList defined");
        log.trace("status-list-application:checkAvailableStatusList:Done");
        return result;
      } else {
        if (
          statusLists[statusLists.length - 1].listSize - 1 <=
          statusLists[statusLists.length - 1].lastUsedIndex
        ) {
          result.availability = 0;
          result.statusListID = statusLists[statusLists.length - 1].statusListID;
          log.debug("status-list-application:checkAvailableStatusList:last statusList Full");
          log.trace("status-list-application:checkAvailableStatusList:Done");
          return result;
        } else {
          result.availability = 1;
          result.statusListID = statusLists[statusLists.length - 1].statusListID;
          log.debug("status-list-application:checkAvailableStatusList:last statusList Not Full");
          log.trace("status-list-application:checkAvailableStatusList:Done");
          return result;
        }
      }
    } catch (ex) {
      const result: any = {};
      result.availability = 0;
      result.statusListID = 0;
      if (ex.message.includes("Cannot access 'statusList' before initialization")) {
        log.debug("status-list-application:checkAvailableStatusList:No statusList defined");
        log.error("No statusList defined : " + ex);
      } else {
        log.exception(
          "status-list-application:checkAvailableStatusList:An Exception occurred:[" + ex + "]"
        );
        log.error(ex);
      }
      return result;
    }
  } catch (ex) {
    log.exception(
      "status-list-application:checkAvailableStatusList:An Exception occurred:[" + ex + "]"
    );
    log.error(ex);
  }
}

async function getListAndIndex(statusListID) {
  try {
    log.trace("status-list-application:getListAndIndex:starting");

    const availableStatusListResult = await checkAvailableStatusList();

    if (availableStatusListResult.availability === 0) {
      const statusListReference = await getNewStatusList();
      statusListID = statusListReference.statusListID;
    } else {
      statusListID = availableStatusListResult.statusListID;
    }

    type ListResult = {
      availableStatusListID: number | null;
      statusListIndex: number | null;
    };
    const result: ListResult = { availableStatusListID: null, statusListIndex: null };

    result.availableStatusListID = statusListID;
    result.statusListIndex = await reserveIndex();

    log.trace("status-list-application:getListAndIndex:Done");

    return result;
  } catch (ex) {
    log.exception("status-list-application:getListAndIndex:An Exception occurred:[" + ex + "]");
  }
}

async function getNewStatusList() {
  try {
    log.trace("status-list-application:getNewStatusList:starting");

    const checkAvailableStatusListResult = await checkAvailableStatusList();

    type NewStatusList = {
      dateOfCreation: any;
      modificationDate: any;
      statusListID: any;
      listSize: any;
      lastUsedIndex: any;
      statusList: any;
    };

    const newStatusList: NewStatusList = {
      dateOfCreation: null,
      modificationDate: null,
      statusListID: null,
      listSize: null,
      lastUsedIndex: null,
      statusList: null,
    };

    const date = new Date();

    console.log(newStatusList);

    newStatusList.dateOfCreation = date;
    newStatusList.modificationDate = date;
    newStatusList.statusListID = checkAvailableStatusListResult.statusListID + 1;
    newStatusList.listSize = LIST_SIZE_IN_BITS;
    newStatusList.lastUsedIndex = -1;
    const statusListArray = new Uint8Array(LIST_SIZE_IN_BITS / 8);
    const temp = await gzip(statusListArray);
    newStatusList.statusList = temp.toString("base64");
    const dbRecord = await statusList.create({ ...newStatusList });

    log.trace("status-list-application:getNewStatusList:Done");

    return dbRecord;
  } catch (ex) {
    log.exception("status-list-application:getNewStatusList:An Exception occurred:[" + ex + "]");
  }
}

async function reserveIndex() {
  try {
    log.trace("status-list-application:reserveIndex:starting");

    const retVal = await checkAvailableStatusList();
    const result = -1;

    if (retVal.availability !== 0) {
      const statusLists = await statusList.find();
      const ptr = statusLists.length - 1;

      if (statusLists[ptr].statusListID === retVal.statusListID) {
        log.debug("status-list-application:reserveIndex:At last list.");
        if (statusLists[ptr].lastUsedIndex < 0) {
          log.debug("status-list-application:reserveIndex:Starting with New List.");
          statusLists[ptr].lastUsedIndex = 0;
        } else {
          statusLists[ptr].lastUsedIndex = statusLists[ptr].lastUsedIndex + 1;
        }
        statusLists[ptr].modificationDate = new Date().getMilliseconds();
        await statusList.findByIdAndUpdate(
          statusLists[ptr]._id,
          { ...statusLists[ptr] },
          {
            useFindAndModify: false,
            new: true,
          }
        );
      } else {
        log.error("status-list-application:reserveIndex:Wierd things happeing in the Kitchen");
      }
      return statusLists[ptr].lastUsedIndex;
    }

    log.trace("status-list-application:reserveIndex:Done");

    return result;
  } catch (ex) {
    log.exception("status-list-application:reserveIndex:An Exception occurred:[" + ex + "]");
  }
}

async function generatedStatusListJSON(statusListID: string): Promise<StatusCredentialListType> {
  log.trace("status-list-application:generatedStatusListJSON:Starting");
  const idpTokenService = IdpTokenService.getInstance(log);

  const statusListObject = await statusList.findOne({ statusListID: statusListID });

  if (!statusListObject) {
    throw new NotFoundError("Status List not found");
  }

  const issuanceDate = new Date(statusListObject.dateOfCreation);

  const retVal = {
    "@context": [
      "https://www.w3.org/2018/credentials/v1",
      "https://w3id.org/vc/status-list/2021/v1",
    ],
    id: idpTokenService.issuerDid,
    type: ["VerifiableCredential", "VerifiableAttestation", "StatusList2021Credential"],
    issuer: idpTokenService.issuerDid,
    issuanceDate: issuanceDate,
    validFrom: new Date(statusListObject.modificationDate),
    expirationDate: new Date(statusListObject.modificationDate + 365 * 24 * 60 * 60 * 1000),
    issued: new Date(statusListObject.modificationDate),
    credentialSubject: {
      id: idpTokenService.issuerDid,
      type: "StatusList2021",
      statusPurpose: "revocation",
      encodedList: statusListObject.statusList,
    },
    credentialSchema: {
      id: process.env.VERIFIABLE_ATTESTATION_SCHEMA_URL,
      type: "FullJsonSchemaValidator2021",
    },
  };
  log.trace("status-list-application:generatedStatusListJSON:Done");

  return statusCredentialListSchema.parse(retVal);
}

async function createStatusListVC(statusList: StatusCredentialListType) {
  log.trace("status-list-application:createStatusListVC:starting");
  const apiPath = "/create-vc-from-cs";
  const hostname = process.env.ISSUER_WALLET_URL + apiPath;
  const idpTokenService = IdpTokenService.getInstance(log);

  const payLoad = JSON.stringify(statusList);

  const result: Result = { response: null };
  const bearerToken = await idpTokenService.getRefreshedAccessToken();

  const requestObj = {
    method: "POST",
    body: payLoad,
    headers: {
      accept: "application/json",
      "Content-Type": "application/json",
      authorization: "Bearer " + bearerToken,
    },
  };
  const response = await fetch(hostname, requestObj);
  const data = await response.json();

  if (data && data.errors) {
    throw new Error(JSON.stringify(data.errors));
  }

  result.response = JSON.stringify(data.vc);

  log.trace("status-list-application:createStatusListVC:finished");
  return result;
}

const createOrGetStatusListStatus = async (statusListID: string) => {
  log.trace("status-list-application:getStatus:Starting");
  const result: StatusCredentialListType = await generatedStatusListJSON(statusListID);
  const vcStatusList = await createStatusListVC(result);
  const returnValue = JSON.parse(vcStatusList.response);

  log.trace("status-list-application:getStatus:Done");

  return returnValue;
};

const createCredentialStatus = async () => {
  const idpTokenService = IdpTokenService.getInstance(log);
  const result = { ...nssiBEACOk };

  const credentialStatusType = "StatusList2021Entry";
  const credentialStatusPurpose = "revocation";

  log.trace("status-list-application:createCredentialStatus:started");

  const getListAndIndexResult = await getListAndIndex(credentialStatusPurpose);

  if (!idpTokenService.issuerDid) {
    throw new OAuth2Error("invalid_request", {
      errorDescription: "Token mismatch : invalid issuerDid",
      statusCode: 418,
    });
  }

  result.body = {
    id: idpTokenService.issuerDid,
    type: credentialStatusType,
    statusPurpose: "revocation",
    statusListIndex: JSON.stringify(getListAndIndexResult.statusListIndex),
    statusListCredential:
      process.env.STATUS_LIST_CREDENTIAL +
      "/" +
      getListAndIndexResult.availableStatusListID +
      "#list",
  };

  log.trace("status-list-application:createCredentialStatus:finished");
  log.trace("status-list-application:createCredentialStatus:body " + JSON.stringify(result.body));
  return result;
};

const getCredentialStatusItems = async (credentialStatusList: CredentialStatusRequestItem[]) => {
  log.trace("status-list-application:getCredentialStatusItems:Starting");

  const resultList = [];
  for (const credentialStatus of credentialStatusList) {
    const credentialStatusItem: CredentialStatusItem = {
      credentialStatusObject: null,
      revocationStatus: null,
    };
    credentialStatusItem.credentialStatusObject = credentialStatus;
    credentialStatusItem.revocationStatus = await getCredentialStatus(credentialStatus);
    resultList.push(credentialStatusItem);
  }
  log.trace("status-list-application:getCredentialStatusItems:Done");
  return resultList;
};

/*
Note missing checking is index is in range needs check if index > lastIndex from database and errorhandling : DSAK
*/
const getCredentialStatus = async (credentialStatusObject: CredentialStatusRequestItem) => {
  try {
    let result = { ...nssiBEACOk };
    const revocationStatus = ["Not revoked", "Revoked"];
    const strings = credentialStatusObject.statusListCredential.split("/");
    const temp = strings.pop();
    const statusListID = Number(temp.split("#")[0]);
    const statusListsArray = await statusList.find({ statusListID: statusListID });

    if (statusListsArray.length == 0) {
      result = { ...nssiBEACFail };
      result.returnMsg = "Status List with ID: [" + statusListID + "] does not exist";
      log.error(
        "status-list-application:getCredentialStatus:An Error occurred:[" + result.returnMsg + "]"
      );
      log.trace("status-list-application:getCredentialStatus:Done");
      return result;
    }

    const statusListIndex = statusListsArray[0].statusList;

    if (Number(credentialStatusObject.statusListIndex) > statusListsArray[0].lastUsedIndex) {
      result = { ...nssiBEACFail };
      result.returnMsg =
        "statusListIndex[" +
        credentialStatusObject.statusListIndex +
        "] > stored index [" +
        statusListsArray[0].lastUsedIndex +
        "]";
      log.error(
        "status-list-application:getCredentialStatus:An Error occurred:[" + result.returnMsg + "]"
      );
      log.trace("status-list-application:getCredentialStatus:Done");
      return result;
    }
    /*insert check ion index here and error handling */
    const base64decoded = Buffer.from(statusListIndex, "base64");
    const inflated = await ungzip(base64decoded);
    const byteIndex = Math.trunc(Number(credentialStatusObject.statusListIndex) / 8);
    const retVal = inflated[byteIndex];
    const bitIndex = Math.pow(2, 7 - (Number(credentialStatusObject.statusListIndex) % 8)) % 255;
    const credentialStatus = (retVal & bitIndex) === 0 ? 0 : 1;
    result.body = revocationStatus[credentialStatus];
    log.trace("status-list-application:getCredentialStatus:Done");
    return result;
  } catch (ex) {
    log.exception("status-list-application:getCredentialStatus:An Exception occurred:[" + ex + "]");
    const result = { ...nssiBEACException };
    result.returnMsg = ":An Exception occurred:[" + ex + "]";
    log.trace("status-list-application:getCredentialStatus:Done ");
    return result;
  }
};

const setCredentialStatus = async credentialStatusObject => {
  try {
    let response;

    log.trace("status-list-application:setCredentialStatus:Starting");

    let result = { ...nssiBEACOk };

    const strings = credentialStatusObject.statusListCredential.split("/");
    const temp = strings.pop();
    const statusListID = Number(temp.split("#")[0]);
    const statusListIndex = Number(credentialStatusObject.statusListIndex);
    log.trace(
      "status-list-application:setCredentialStatus:statusListIndex:[" + statusListIndex + "]."
    );
    log.trace("status-list-application:setCredentialStatus:statusListID: [" + statusListID + "].");

    const statusListsArray = await statusList.find({ statusListID: statusListID });

    if (statusListsArray.length == 1 && statusListIndex <= statusListsArray[0].lastUsedIndex) {
      const statusListObject = statusListsArray[0];

      const base64decoded = Buffer.from(statusListObject.statusList, "base64");
      const inflated = await ungzip(base64decoded);
      const byteIndex = Math.trunc(statusListIndex / 8);
      const bitIndex = Math.pow(2, 7 - (statusListIndex % 8)) % 255;
      inflated[byteIndex] = inflated[byteIndex] | bitIndex;
      const temp2 = await gzip(inflated);
      statusListObject.statusList = temp2.toString("base64");
      response = await statusList.findByIdAndUpdate(
        statusListObject._id,
        { ...statusListObject },
        {
          useFindAndModify: false,
          new: true,
        }
      );

      if (response.statusListID !== statusListID) {
        log.error(
          "status-list-application:setCredentialStatus:Cannot update statusListIndex + [" +
            statusListIndex +
            "]:statusListID [" +
            statusListID +
            "] !"
        );
        result = { ...nssiBEACFail };
        result.returnMsg =
          "Error cannot update statusListIndex + [" +
          statusListIndex +
          "]:statusListID [" +
          statusListID +
          "] !";
        return result;
      }

      result.body = {};

      log.trace("status-list-application:setCredentialStatus:Done");
      return result;
    } else {
      result = { ...nssiBEACFail };
      result.returnMsg =
        "Error cannot find statusListIndex + [" +
        statusListIndex +
        "]:statusListID [" +
        statusListID +
        "] !";
      log.error("status-list-application:setCredentialStatus:" + result.returnMsg);
      log.trace("status-list-application:setCredentialStatus:Done");
      return result;
    }
  } catch (ex) {
    log.exception("status-list-application:setCredentialStatus:An Exception occurred:[" + ex + "]");
    const result = { ...nssiBEACException };
    result.body = ":An Exception occurred:[" + ex + "]";
    await log.trace("status-list-application:setCredentialStatus:Done ");
    return result;
  }
};

const returnJWT = async (req: Request, res: Response, credentialStatusJwt) => {
  res.set({ "Content-Type": "text/plain; charset=utf-8" });
  res.status(200);
  res.send(credentialStatusJwt);

  if (!credentialStatusJwt) {
    throw new NotFoundError("JWT not found");
  }
};

export {
  createOrGetStatusListStatus,
  createCredentialStatus,
  getCredentialStatusItems,
  getCredentialStatus,
  setCredentialStatus,
  returnJWT,
};
