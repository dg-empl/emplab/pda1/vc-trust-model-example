interface Result {
  response: any | null;
}

interface CredentialStatusItem {
  credentialStatusObject: any | null;
  revocationStatus: any | null;
}

type StatusListResult = {
  availability: number | null;
  statusListID: number | null;
};

type CredentialStatusRequestItem = {
  statusListCredential: string;
  statusListIndex: string;
};

export { Result, CredentialStatusItem, StatusListResult, CredentialStatusRequestItem };
