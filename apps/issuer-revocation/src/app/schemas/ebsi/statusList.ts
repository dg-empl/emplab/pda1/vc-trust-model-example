import * as mongoose from 'mongoose';

const statusListSchema = new mongoose.Schema({
  dateOfCreation: Number,
  modificationDate: Number,
  statusListID: Number,
  statusList: String,
  listSize: Number,
  lastUsedIndex: Number
});

const statusList = mongoose.model(
  'statusList',
  statusListSchema
);

export { statusList };