import { Tracer, logger } from "@esspass-web-wallet/logging-lib";
const applicationName = "issuer-revocation";

/**
 * OpenTelemetry
 */
const tracer = new Tracer(applicationName);
import { api } from "@opentelemetry/sdk-node";
/**
 * OpenTelemetry
 */

import express from "express";
import "express-async-errors";
import cors from "cors";
import "dotenv/config";

import * as bodyParser from "body-parser";

import { Database, errorHandler } from "@esspass-web-wallet/backend-utils-lib";
import { NotFoundError } from "@esspass-web-wallet/domain-lib";

import {
  createCredentialStatusAdapter,
  createOrGetCredentialStatusListAdapter,
  credentialStatusAdapter,
  credentialStatusItemsAdapter,
  setCredentialStatusAdapter,
} from "./app/adapters/rest/credentials-adapter";
import { keycloakSetupInterop } from "./app/utils/keycloakInterop";
import { IdpTokenService } from "@esspass-web-wallet/security-lib";

const app = express();
app.use(cors());

/* needed for TSL termination */
app.set("trust proxy", true);

app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);

app.use(bodyParser.json());

//keycloak
const keycloak = keycloakSetupInterop(app);
app.use(keycloak.middleware());

const log = logger(applicationName);

function setupRoutes() {
  /**
   * version - version deployed
   */
  app.get("/version", (req, res) => {
    return res.send({
      version: process.env.VERSION || "development",
      revision: process.env.GIT_REVISION || "1",
      branch: process.env.GIT_BRANCH || "master",
    });
  });

  //get specific credential status
  app.get("/credential-status", credentialStatusAdapter);
  //get credential statuses for specific items
  app.get("/credential-status-item", credentialStatusItemsAdapter);
  //create new credential status
  app.post("/create-credential-status", createCredentialStatusAdapter);
  //get specific credential status list
  app.get("/credentials/status/:id", createOrGetCredentialStatusListAdapter);
  //set credential status to new value (i.e. not revoked -> revoked)
  app.put("/credential-status", setCredentialStatusAdapter);

  /**
   * error handler
   */
  app.all("*", (req, res) => {
    log.trace(req.path);
    throw new NotFoundError("Not found route");
  });
  app.use(errorHandler(log));
}

function startServer() {
  log.info("Application : " + applicationName);
  log.info("App version : " + process.env.VERSION || "development");
  log.info("Environment : " + process.env.ENVIRONMENT || "development");

  IdpTokenService.getInstance(log);

  /**
   * connect to database
   */
  Database.getInstance(
    process.env.DB_HOST,
    process.env.DB_USERNAME,
    process.env.DB_PASSWORD,
    process.env.DB_PORT,
    "esspass-nssi-revocation",
    log
  );

  const port = 20080;
  const server = app.listen(port, () => log.info(`Listening at: http://localhost:${port}`));
  server.on("error", log.error);
}

setupRoutes();
startServer();
