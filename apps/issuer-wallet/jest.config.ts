/* eslint-disable */
export default {
  displayName: "issuer-wallet",
  preset: "../../jest.preset.js",
  testEnvironment: "node",
  setupFiles: ["<rootDir>/src/app/.env.js"],
  transform: {
    "^.+\\.[tj]s$": [
      "ts-jest",
      {
        tsconfig: "<rootDir>/tsconfig.spec.json",
      },
    ],
  },
  moduleFileExtensions: ["ts", "js", "html"],
  coverageDirectory: "../../coverage/apps/issuer-wallet",
};
