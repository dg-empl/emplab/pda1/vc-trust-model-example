import mongoose from "mongoose";
import { MongoClient } from "mongodb";
import { MongoMemoryServer } from "mongodb-memory-server";

import { logger } from "@esspass-web-wallet/logging-lib";
const log = logger("mongo-mem-server");

// This is an Example test, do not merge it with others and do not delete this file
mongoose.set("strictQuery", true);

describe("Single MongoMemoryServer", () => {
  let con: MongoClient, mongoServer: MongoMemoryServer;

  beforeAll(async () => {
    log.info("DB_PORT " + process.env.DB_PORT);
    log.info("DB_USERNAME " + process.env.DB_USERNAME);
    log.info("DB_PASSWORD " + process.env.DB_PASSWORD);
    log.info("DB_NAME " + process.env.DB_NAME);

    mongoServer = await MongoMemoryServer.create({
      instance: {
        ip: "127.0.0.1",
        port: +process.env.DB_PORT,
        dbName: process.env.DB_NAME,
      },
      auth: {
        enable: true,
        customRootName: "admin",
        customRootPwd: "admin",
      },
    });

    // // 1.
    // function buildDbUrl(
    //   dbHost: string,
    //   user: string,
    //   password: string,
    //   port: string,
    //   databaseName?: string
    // ) {
    //   return `mongodb://${user}:${password}@${dbHost}:${port}/${databaseName}?authSource=admin`;
    // }
    //
    // const connection = buildDbUrl(
    //   process.env.DB_HOST,
    //   "admin",
    //   "admin",
    //   process.env.DB_PORT,
    //   process.env.DB_NAME
    // );
    //
    // try {
    //   con = await mongoose.connect(connection);
    //   console.info("Mongoose opened connection");
    // } catch (e) {
    //   console.error("Mongoose error opening connection " + e);
    // }

    //2.
    con = await MongoClient.connect(mongoServer.getUri(), {
      auth: { username: "admin", password: "admin" },
    });
  });

  afterAll(async () => {
    if (con) {
      await con.close();
    }
    if (mongoServer) {
      await mongoServer.stop();
    }
  });

  it("should successfully connect to the database", async () => {
    expect(true).toBeTruthy();
  });

  it("should successfully set & get information from the database", async () => {
    const db = con.db(mongoServer.instanceInfo!.dbName);

    expect(db).toBeDefined();
    const col = db.collection("test");
    const result = await col.insertMany([{ a: 1 }, { b: 1 }]);
    expect(result.insertedCount).toStrictEqual(2);
    expect(await col.countDocuments({})).toBe(2);
  });
});
