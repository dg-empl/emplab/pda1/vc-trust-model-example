import { Request, Response } from "express";
import { createVCFromCSAdapter, issuerDidAdapter } from "./services-adapter";
import { disconnect } from "../../utils/open-db-connection";
import { logger } from "@esspass-web-wallet/logging-lib";
import { MongoMemoryServer } from "mongodb-memory-server";
import { schema1 } from "./utils/schema-1";
import * as process from "process";
import { issuerIdentity } from "./utils/issuer-identity";

import * as dbUtils from "@esspass-web-wallet/backend-utils-lib";

const log = logger("integration");

describe("ServiceAdapters", () => {
  jest.setTimeout(20_000);

  let mongoServer: MongoMemoryServer;
  let mockRequest: Partial<Request>;
  let mockResponse: Partial<Response>;

  const mongoDbName = process.env.DB_NAME;
  const schemaCollectionName = process.env.SCHEMA_COLLECTION_NAME;
  const identityCollectionName = process.env.IDENTITY_COLLECTION_NAME;

  beforeAll(async () => {
    //open mongo in memory
    mongoServer = await dbUtils.createMongoMemoryServer();
    log.debug("Mongo Memory Server Started()");
    log.debug("mongoServer.started()");

    // create test ebsi-schema
    await dbUtils.createTestData(mongoServer.getUri(), mongoDbName, {
      collection: schemaCollectionName,
      data: schema1,
      id: "64a7bac07620689a8e085cd5",
      check: {
        schemaURL:
          "https://api-pilot.ebsi.eu/trusted-schemas-registry/v2/schemas/0xe5e53f7bcebe95fc9576e27298de64bfacf592dd9be673eb6b8edbc4795ac96a",
      },
    });

    log.debug("Test data added : ebsi-schema");

    // create test issuer-identity
    await dbUtils.createTestData(mongoServer.getUri(), mongoDbName, {
      collection: identityCollectionName,
      data: issuerIdentity,
      id: "63d5333379a10084e441b6be",
      check: {
        did: "did:ebsi:z21NqAkAUjTgG9FazC6LzG35",
      },
    });

    log.debug("Test data added : issuer-identity");
  });

  beforeEach(async () => {
    mockRequest = {};
    mockResponse = {
      json: jest.fn(),
      send: jest.fn(),
    };

    dbUtils.connect(mongoDbName, logger("esspass-nssi-wallet-unit-test"));
  });

  afterAll(async () => {
    await dbUtils.purgeTestData(mongoServer.getUri(), mongoDbName, {
      collection: schemaCollectionName,
    });

    await dbUtils.purgeTestData(mongoServer.getUri(), mongoDbName, {
      collection: identityCollectionName,
    });
    //
    log.debug("Test data purged");

    await disconnect(logger("esspass-nssi-wallet-unit-test"));
    await mongoServer.stop();
    log.debug("mongoServer.stop()");
  });

  it("should sign jwt verifiable credential", async () => {
    const proxyEndPoint =
      "https://api-pilot.ebsi.eu/trusted-issuers-registry/v4/issuers/did:ebsi:zeex3EqMqZQTpuqM7j1krDK/proxies/0xcfb2313e90007db4e517398cd762a18d85e686f78de6fd960a79600c58115f71/credentials/status/1";

    mockRequest = {
      body: {
        "@context": ["https://www.w3.org/2018/credentials/v1"],
        id: "did:ebsi:zhm9k4DeCPGjdrXy3SrZxPEvFJSXm2R551VpfLaRrZ3gZ",
        type: ["VerifiableCredential", "VerifiableAttestation", "VerifiablePortableDocumentA1"],
        issuer: "did:ebsi:z21NqAkAUjTgG9FazC6LzG35",
        issuanceDate: "2023-12-04T06:45:58.054Z",
        validFrom: "2023-12-04T06:45:58.054Z",
        issued: "2023-12-04T06:45:58.054Z",
        credentialStatus: {
          id: "did:ebsi:zeex3EqMqZQTpuqM7j1krDK",
          type: "StatusList2021Entry",
          statusPurpose: "revocation",
          statusListIndex: "1",
          statusListCredential: proxyEndPoint,
        },
        credentialSubject: {
          id: "did:ebsi:zhm9k4DeCPGjdrXy3SrZxPEvFJSXm2R551VpfLaRrZ3gZ",
          section1: {
            personalIdentificationNumber: "PL20511546",
            sex: "01",
            surname: "zawadzki-1",
            forenames: "marcin-1",
            dateBirth: "1977-04-01",
            surnameAtBirth: "null",
            placeBirth: {
              town: "Varsovie",
              region: "Varsovie",
              countryCode: "PL",
            },
            nationalities: ["PL"],
            stateOfResidenceAddress: {
              streetNo: "Juliana Tuwima 7",
              postCode: "90-010",
              town: "Varsovie",
              countryCode: "PL",
            },
            stateOfStayAddress: {
              streetNo: "Juliana Tuwima 7",
              postCode: "90-010",
              town: "Varsovie",
              countryCode: "PL",
            },
          },
          section2: {
            memberStateWhichLegislationApplies: "AT",
            startingDate: "2025-01-16",
            endingDate: "2026-02-14",
            certificateForDurationActivity: true,
            determinationProvisional: true,
            transitionRulesApplyAsEC8832004: true,
          },
          section3: {
            postedEmployedPerson: true,
            employedTwoOrMoreStates: true,
            postedSelfEmployedPerson: true,
            selfEmployedTwoOrMoreStates: true,
            civilServant: true,
            contractStaff: true,
            mariner: true,
            employedAndSelfEmployed: true,
            civilAndEmployedSelfEmployed: true,
            flightCrewMember: true,
            exception: true,
            exceptionDescription: "no exceptions",
            workingInStateUnder21: true,
          },
          section4: {
            employee: true,
            selfEmployedActivity: true,
            employerSelfEmployedActivityCodes: [""],
            nameBusinessName: "955163362",
            registeredAddress: {
              streetNo: "Trabrennstraße 6-8, 213 A",
              postCode: "1020",
              town: "Vienne",
              countryCode: "AT",
            },
          },
          section5: {
            noFixedAddress: false,
          },
          section6: {
            name: "Office National de Sécurité Sociale / Rijksdienst voor Sociale Zekerheid",
            address: {
              streetNo: "Main Street 1",
              postCode: "1000",
              town: "Brussels",
              countryCode: "BE",
            },
            institutionID: "NSSI-BE-01",
            officePhoneNo: "0800 12345",
            officeFaxNo: "0800 98765",
            email: "esspass.noreply@gmail.com",
            date: "2023-12-04",
            signature: "Official signature",
          },
        },
        credentialSchema: {
          id: "https://api-pilot.ebsi.eu/trusted-schemas-registry/v2/schemas/0x47c61667281cc3883cca001bcac7038237c85d0e8f61f139652375c330a99063",
          type: "FullJsonSchemaValidator2021",
        },
        expirationDate: "2024-12-03T06:45:58.000Z",
      },
    };

    //when
    await createVCFromCSAdapter(mockRequest as Request, mockResponse as Response);

    //then
    expect(mockResponse.send).toBeCalledWith({ vc: expect.any(String) });
  });

  it("should sign credential list credential", async () => {
    //given
    const mockRequest = {
      body: {
        "@context": [
          "https://www.w3.org/2018/credentials/v1",
          "https://w3id.org/vc/status-list/2021/v1",
        ],
        id: "did:ebsi:z21NqAkAUjTgG9FazC6LzG35",
        type: ["VerifiableCredential", "VerifiableAttestation", "StatusList2021Credential"],
        issuer: "did:ebsi:z21NqAkAUjTgG9FazC6LzG35",
        issuanceDate: "2023-12-04T15:29:44.055Z",
        validFrom: "2023-12-04T15:29:44.055Z",
        expirationDate: "2024-12-03T15:29:44.000Z",
        issued: "2023-12-04T15:29:44.055Z",
        credentialSubject: {
          id: "did:ebsi:z21NqAkAUjTgG9FazC6LzG35",
          type: "StatusList2021",
          statusPurpose: "revocation",
          encodedList:
            "H4sIAAAAAAAAE+3BMQEAAAjAoBWwfz2jGMMH2JoAAAAAAAAAAAAAAAAAAACAVwfzx6asAEAAAA==",
        },
        credentialSchema: {
          id: "https://api-conformance.ebsi.eu/trusted-schemas-registry/v2/schemas/z6HkKUAhWgMQqGQEcc1kewTWJrY4nrtadncmid7hsfGV8",
          type: "FullJsonSchemaValidator2021",
        },
      },
    };

    //when
    await createVCFromCSAdapter(mockRequest as Request, mockResponse as Response);

    //then
    expect(mockResponse.send).toBeCalledWith({ vc: expect.any(String) });
  });

  it("should return a wallet", async () => {
    //given when
    await issuerDidAdapter(mockRequest as Request, mockResponse as Response);

    //then
    expect(mockResponse.send).toBeCalledWith({ did: expect.any(String) });
  });

  it("should throw exception when wallet not properly formatted", async () => {
    //given

    await dbUtils.purgeTestData(mongoServer.getUri(), mongoDbName, {
      collection: identityCollectionName,
    });

    const record = {
      dateOfRegistration: "1674916659924",
      wallet:
        '{"wallet":{"phrase":"aerobic draw copy butter filter grit upper twenty material congress cube unaware","path":"m/44\'/60\'/0\'/0/0","locale":"en"}}',
      did: "did:ebsi:zhkNKqkC86BZuzrdGEU9acH",
      ebsiWallet:
        '{"did":"did:ebsi:zhkNKqkC86BZuzrdGEU9acH","ethWallet":{"_isSigner":true,"address":"0xE6E3af5Dd8f93e04444b4d013542ae458a668194","provider":null},"didVersion":1,"keys":{"ES256":{"id":"did:ebsi:zhkNKqkC86BZuzrdGEU9acH#keys-1","privateKeyJwk":{"kty":"EC","x":"gLdRRAIbckT4nCqnuB7WACIGdzBBCtrR9_rFNkbTwE8","y":"K5ahHODwj8R-fu9hiTd_HyDNWCAfQOBCmlchARPB2RM","crv":"P-256","d":"QG6l80VvoCkwpMxhmBlw0fnXtRqLaDp0Ddqo73a4Rks"},"publicKeyJwk":{"kty":"EC","x":"gLdRRAIbckT4nCqnuB7WACIGdzBBCtrR9_rFNkbTwE8","y":"K5ahHODwj8R-fu9hiTd_HyDNWCAfQOBCmlchARPB2RM","crv":"P-256"},"privateKeyEncryptionJwk":{"kty":"EC","x":"gLdRRAIbckT4nCqnuB7WACIGdzBBCtrR9_rFNkbTwE8","y":"K5ahHODwj8R-fu9hiTd_HyDNWCAfQOBCmlchARPB2RM","crv":"P-256","d":"QG6l80VvoCkwpMxhmBlw0fnXtRqLaDp0Ddqo73a4Rks"},"publicKeyEncryptionJwk":{"kty":"EC","x":"gLdRRAIbckT4nCqnuB7WACIGdzBBCtrR9_rFNkbTwE8","y":"K5ahHODwj8R-fu9hiTd_HyDNWCAfQOBCmlchARPB2RM","crv":"P-256"}},"ES256K":{"id":"did:ebsi:zhkNKqkC86BZuzrdGEU9acH#keys-2","privateKeyJwk":{"kty":"EC","crv":"secp256k1","x":"OlCN7s_yjEdGjV-jbYr3A_DvEe3kByuJFpedCRCsjhI","y":"0dMvYln_TW-s_twPF57KSlS9lAdMOiApcK53Yhs5oEE","d":"5uOvXdj5PgRES00BNUKuRYpmgZQ"},"publicKeyJwk":{"kty":"EC","crv":"secp256k1","x":"OlCN7s_yjEdGjV-jbYr3A_DvEe3kByuJFpedCRCsjhI","y":"0dMvYln_TW-s_twPF57KSlS9lAdMOiApcK53Yhs5oEE"},"privateKeyEncryptionJwk":{"kty":"EC","crv":"secp256k1","x":"OlCN7s_yjEdGjV-jbYr3A_DvEe3kByuJFpedCRCsjhI","y":"0dMvYln_TW-s_twPF57KSlS9lAdMOiApcK53Yhs5oEE","d":"5uOvXdj5PgRES00BNUKuRYpmgZQ"},"publicKeyEncryptionJwk":{"kty":"EC","crv":"secp256k1","x":"OlCN7s_yjEdGjV-jbYr3A_DvEe3kByuJFpedCRCsjhI","y":"0dMvYln_TW-s_twPF57KSlS9lAdMOiApcK53Yhs5oEE"}}}}',
      __v: 0,
    };

    // create test issuer-identity
    await dbUtils.createTestData(mongoServer.getUri(), mongoDbName, {
      collection: identityCollectionName,
      data: record,
      id: "63d5333379a10084e441b6be",
      check: {
        did: "did:ebsi:zhkNKqkC86BZuzrdGEU9acH",
      },
    });

    log.debug("Test data added : issuer-identity");

    //when
    try {
      await issuerDidAdapter(mockRequest as Request, mockResponse as Response);
    } catch (e) {
      //then
      expect(e.message).toBe(
        "getWallet: ebsi wallet not found or invalid format : " +
          '{"issues":[{"code":"invalid_type","expected":"string","received":"undefined","path":' +
          '["keys","ES256K","kid"],"message":"Required"},{"code":"invalid_type","expected":"string",' +
          '"received":"undefined","path":["keys","ES256","kid"],"message":"Required"}],' +
          '"name":"ZodError"}'
      );
    }
  });
});
