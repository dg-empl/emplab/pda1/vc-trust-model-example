import { logger } from "@esspass-web-wallet/logging-lib";

import {
  getEBSIIdentifierByID,
  getEBSIIdentifiers,
  getEBSISchemaByID,
  getEBSISchemaByURL,
  getEBSISchemas,
} from "../../applications/ebsi-services-application";

import {
  generateMockHolderWallet,
  generateWallet,
  getDidDocument,
  getIssuerDid,
  getIssuerWallets,
  getJwks,
  signVC,
  validateVC,
  validateVCBySchema,
} from "../../applications/wallet-service";

import { Request, Response } from "express";

import {
  nssiBEACFail,
  nssiBEACOk,
  nssiIWDIDNotPresentOnEBSI,
} from "../../schemas/errors/errorCatalog";

import { signedVC } from "../../schemas/mongoose/signedVCS";
import { check, validationResult } from "express-validator";
import {
  statusCredentialListSchema,
  VerifiableCredential,
  verifiableCredentialSchema,
} from "@esspass-web-wallet/domain-lib";
import { SafeParseReturnType } from "zod/lib/types";
import { JsonWebKeySet } from "@esspass-web-wallet/crypto-lib";

const log = logger("issuer-wallet");

async function ebsiSchemaByIDAdapter(req, res) {
  try {
    let result: any = {};

    log.trace("service:ebsiSchemaByID:started");

    result.returnCode = 1;

    result = await getEBSISchemaByID(req.query.id);

    if (result.returnCode !== nssiBEACOk) {
      await returnServiceError(req, res, result);
      log.trace("service:ebsiSchemaByID:finished");
    }
  } catch (ex) {
    log.exception("service:ebsiSchemaByID:An exception Occurred:[" + ex + "]");
    log.trace("service:ebsiSchemaByID:Done");
    const result = nssiBEACFail;
    result.body = "An exception occurred:[" + ex + "].";
    await returnServiceError(req, res, result);
  }
}

async function ebsiSchemasAdapter(req, res) {
  try {
    let result: any = {};

    log.trace("service:getEBSISchemas:started");

    result.returnCode = 1;

    result = await getEBSISchemas();

    if (result.returnCode !== nssiBEACOk) {
      await returnServiceError(req, res, result);
      log.trace("service:getEBSISchemas:finished");
    }
  } catch (ex) {
    log.exception("service:getEBSISchemas:An exception Occurred:[" + ex + "]");
    log.trace("service:getEBSISchemas:Done");
    const result = nssiBEACFail;
    result.body = "An exception occurred:[" + ex + "].";
    await returnServiceError(req, res, result);
  }
}

async function ebsiIdentifiersAdapter(req, res) {
  try {
    let result: any = {};

    log.trace("service:getEBSIIdentifiers:started");

    result.returnCode = 1;

    result = await getEBSIIdentifiers();

    if (result.returnCode !== nssiBEACOk) {
      await returnServiceError(req, res, result);
      log.trace("service:getEBSIIdentifiers:finished");
    }
  } catch (ex) {
    log.exception("service:getEBSIIdentifiers:An exception Occurred:[" + ex + "]");
    log.trace("service:getEBSIIdentifiers:Done");
    const result = nssiBEACFail;
    result.body = "An exception occurred:[" + ex + "].";
    await returnServiceError(req, res, result);
  }
}

async function ebsiIdentifierByIDAdapter(req, res) {
  try {
    let result: any = {};

    log.trace("service:getEBSIIdentifierByID:started");

    result.returnCode = 1;

    result = await getEBSIIdentifierByID(req.query.id);

    await returnServiceError(req, res, result);
    log.trace("service:getEBSIIdentifierByID:finished");
  } catch (ex) {
    log.exception("service:getEBSIIdentifiers:An exception Occurred:[" + ex + "]");
    log.trace("service:getEBSIIdentifiers:Done");
    const result = nssiBEACFail;
    result.body = "An exception occurred:[" + ex + "].";
    await returnServiceError(req, res, result);
  }
}

async function issuerDidAdapter(req, res) {
  log.trace("services:issuerDidAdapter:started");
  const did = await getIssuerDid();
  log.trace("services:issuerDidAdapter:finished");
  return res.send({ did: did });
}

async function createHolderWalletAdapter(req, res) {
  try {
    let result: any = {};

    log.trace("service:createHolderWallet :started");

    result = await generateMockHolderWallet();
    if (
      !(
        result.returnCode === nssiBEACOk.returnCode ||
        result.returnCode === nssiIWDIDNotPresentOnEBSI.returnCode
      )
    ) {
      log.error("EBSIServices: createHolderWallet:Errors occurred" + result.body);
      log.trace("EBSIServices: createHolderWallet:finished");
      await returnServiceError(req, res, result);
      return result;
    }
    log.debug("EBSIServices: createHolderWallet:Issuer did is : " + result.body);
    log.trace("EBSIServices: getcreateHolderWalletIssuerDid:finished");
    await returnServiceError(req, res, result);
  } catch (ex) {
    log.exception("service:createHolderWallet:An exception Occurred:[" + ex + "]");
    log.trace("service:createHolderWallet:Done");
    const result = nssiBEACFail;
    result.body = "An exception occurred:[" + ex + "].";
    await returnServiceError(req, res, result);
  }
}

async function validateParams(req: Request) {
  await check("id").isString().run(req);
  await check("type").isArray().run(req);

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    errors.array().forEach(error => {
      console.log(error);
    });
    throw new Error("Invalid parameters");
  }
}

export const createVCFromCSAdapter = async (
  req: Request,
  res: Response
): Promise<Response<{ vc: string }>> => {
  log.trace("service:createVCFromCS:started");
  /**
   * validate entry to createVCFromCSAdapter
   */
  await validateParams(req);

  const credentialSubject = req.body;

  const verifiableCredential: SafeParseReturnType<any, any> =
    verifiableCredentialSchema.safeParse(credentialSubject);
  let statusCredentialList: SafeParseReturnType<any, any>;

  log.debug("credentialSubject: " + JSON.stringify(credentialSubject));

  if (!verifiableCredential.success) {
    statusCredentialList = statusCredentialListSchema.safeParse(credentialSubject);
  }

  if (statusCredentialList && !statusCredentialList.success) {
    const desc = statusCredentialList["error"].issues
      .map(issue => `[${issue.code}] in 'credential.${issue.path.join(".")}': ${issue.message}`)
      .join("\n");

    throw new Error(
      "createVCFromCSAdapter error:verifiableCredentialSchema : " + JSON.stringify(desc)
    );
  }

  let validationBySchemaResponse: {
    returnCode?: any;
    body: any;
    returnMsg?: string;
    resultClass?: number;
  };

  if (verifiableCredential.success) {
    validationBySchemaResponse = await validateVCBySchema(verifiableCredential.data);
  } else if (statusCredentialList.success) {
    validationBySchemaResponse = await validateVCBySchema(statusCredentialList.data);
  }

  if (validationBySchemaResponse.returnCode !== 0) {
    validationBySchemaResponse.body.errors.forEach(error => {
      log.debug("credentialSubjectError: " + error["errorMessage"]);
    });
    throw new Error("createVCFromCSAdapter error:validateVCBySchema error");
  }

  const vc = await signVC(credentialSubject);

  if (!vc) {
    throw new Error("createVCFromCSAdapter:signVC:error");
  }

  try {
    await validateVC(vc);
  } catch (e) {
    throw new Error("createVCFromCSAdapter:validateVC:error " + e);
  }

  return res.send({ vc });
};

async function getSignedVCAdapter(req: Request, res: Response) {
  try {
    log.trace("service:getSignedVC :started");

    const recordID = req.body.id;
    const storedVC = await signedVC.find({ _id: recordID });

    log.trace("service:signVC: Done ");
    await returnServiceError(req, res, storedVC);
  } catch (ex) {
    log.exception("service:signVC:An exception Occurred:[" + ex + "]");
    log.trace("service:signVC:Done");
    const result = nssiBEACFail;
    result.body = "An exception occurred:[" + ex + "].";
    await returnServiceError(req, res, result);
  }
}

async function ebsiSchemaByURLAdapter(req, res) {
  log.trace("ebsiSchemaByURLAdapter:started");
  const result = await getEBSISchemaByURL(req.query.schemaURL);
  log.trace("ebsiSchemaByURLAdapter:finished");
  res.send(result);
}

async function returnServiceError(req, res, resultCode) {
  try {
    log.trace("returnServiceError: returnServiceError:started");

    res.set({ "Content-Type": "application/json; charset=utf-8" });
    res.json(resultCode);

    log.trace("returnServiceError: services:finished");
  } catch (ex) {
    log.exception("service:returnServiceError:An exception Occurred:[" + ex + "]");
    log.trace("service:returnServiceError:Done");
    const result = nssiBEACFail;
    result.body = "An exception occurred:[" + ex + "].";
    await returnServiceError(req, res, result);
  }
}

async function generateWalletAdapter(req, res) {
  try {
    log.trace("service:generateWallet :started");

    const wallet = await generateWallet();

    log.trace("service:generateWallet: Done ");
    await returnServiceError(req, res, wallet);
  } catch (ex) {
    log.exception("service:generateWallet:An exception Occurred:[" + ex + "]");
    log.trace("service:generateWallet:Done");
    const result = nssiBEACFail;
    result.body = "An exception occurred:[" + ex + "].";
    await returnServiceError(req, res, result);
  }
}

/* take first registered wallet or specific wallet based on DID */
async function getIssuerWalletAdapter(req, res) {
  try {
    log.trace("service:getIssuerWallet :started");

    let result: any = {};
    result.returnCode = 1;

    result = await getIssuerWallets(req.query.did);

    if (
      !(
        result.returnCode === nssiBEACOk.returnCode ||
        result.returnCode === nssiIWDIDNotPresentOnEBSI.returnCode
      )
    ) {
      log.error(
        "service:getIssuerWallet:Errors occurred aftern TIwalletServices.getIssuerWallets()" +
          result.body
      );
      log.trace("service:getIssuerWallet:finished");
      await returnServiceError(req, res, result);
      return result;
    }
    const response = nssiBEACOk;

    response.body.ebsiWalletObject = JSON.parse(result.body[0].ebsiWallet);

    response.body.didDocument = await getDidDocument(req.query.did);

    log.debug("service:getIssuerWallet:Issuer did is : " + response.body);
    log.trace("service:getIssuerWallet:finished");
    await returnServiceError(req, res, response);
  } catch (ex) {
    log.exception("service:getIssuerWallet:An exception Occurred:[" + ex + "]");
    log.trace("service:getIssuerWallet:Done");
    const result = nssiBEACFail;
    result.body = "An exception occurred:[" + ex + "].";
    await returnServiceError(req, res, result);
  }
}

// async function createVPAdapter(req: Request, res: Response) {
//   log.trace("createVPAdapter:started");
//
//   const vc = { ...req.body };
//
//   const issuerKey = await getWallet();
//   const keyObject = issuerKey.ebsiWallet.keys.ES256K;
//   const vp = await createVP(JSON.stringify(vc), keyObject, issuerKey.body.did);
//
//   log.debug("service:createVP: vp is : " + vp);
//   log.trace("service:createVP:finished");
//   return vp;
// }

export const getPublicJwksAdapter = async (req, res): Promise<JsonWebKeySet> => {
  log.debug("jwksAdapter entry");
  const jwks = await getJwks();
  res.setHeader("Content-type", "application/jwk-set+json");
  log.debug("jwksAdapter exit :" + JSON.stringify(jwks));
  return res.send(jwks);
};

export {
  ebsiSchemasAdapter,
  ebsiSchemaByIDAdapter,
  ebsiIdentifiersAdapter,
  ebsiIdentifierByIDAdapter,
  issuerDidAdapter,
  createHolderWalletAdapter,
  getSignedVCAdapter,
  // createVPAdapter,
  ebsiSchemaByURLAdapter,
  getIssuerWalletAdapter,
  generateWalletAdapter,
};
