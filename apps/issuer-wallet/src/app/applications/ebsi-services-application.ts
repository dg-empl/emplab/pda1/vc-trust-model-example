import fetch from "node-fetch";

import "dotenv/config";

import { logger } from "@esspass-web-wallet/logging-lib";

import {
  nssiBEACOk,
  nssiBEACBadInputParameters,
  nssiIWBadHtmlResponse,
  nssiBEACrequestFormInvalid,
} from "../schemas/errors/errorCatalog";

import { ebsiSchema } from "../schemas/mongoose/ebsiSchema";
import { NotFoundError } from "@esspass-web-wallet/domain-lib";

const ebsiHostUrl = process.env.EBSI_ENV_DOMAIN;

const log = logger("issuer-wallet");
const applicationName = "esspass-web-wallet";

async function getEBSIIdentifiers() {
  try {
    let result, nextUrl, calledURL, resultObject;

    log.trace(applicationName + ":ebsi-services-application: getEBSIIdentifiers:Started ");

    result = { ...nssiBEACOk };

    calledURL = "----------------";
    nextUrl = ebsiHostUrl + "/did-registry/v3/identifiers";
    const allSchemaIDs = [];

    if (arguments.length !== 0) {
      result = { ...nssiBEACBadInputParameters };
      result.body = "Bad number of input parameters:[" + arguments.length + "]";
      log.error(
        applicationName + ":ebsi-services-application: getEBSIIdentifiers:" + result.body + "."
      );
      log.trace(applicationName + ":ebsi-services-application: getEBSIIdentifiers:Done ");
      return result;
    }

    while (!calledURL.includes(nextUrl)) {
      const response = await fetch(nextUrl);

      if (response.status !== 200) {
        result = { ...nssiIWBadHtmlResponse };
        result.body = "Call to " + nextUrl + "return as status:" + response.status + ".";
        log.error(
          applicationName + ":ebsi-services-application: getEBSIIdentifiers:" + result.body
        );
        log.trace(applicationName + ":ebsi-services-application: getEBSIIdentifiers:Done ");
        return result;
      }

      result.body = await response.text();
      resultObject = JSON.parse(result.body);

      calledURL = nextUrl;
      nextUrl = resultObject.links.next;

      allSchemaIDs.push(...resultObject.items);
      resultObject = {};
    }

    result.body = JSON.stringify(allSchemaIDs);

    log.trace(applicationName + ":ebsi-services-application: getEBSIIdentifiers:Done ");
    return result;
  } catch (ex) {
    log.exception(
      applicationName +
        ":ebsi-services-application:getEBSIIdentifiers:Bad request: An Exception occurred:[" +
        ex +
        "]"
    );
    log.trace(applicationName + ":ebsi-services-application:getEBSIIdentifiers:Done ");
  }
}

async function getEBSIIdentifierByID(DID) {
  try {
    let result;

    log.trace(applicationName + ":ebsi-services-application:getEBSIIdentifierByID:Started ");

    const getDIDUrl = ebsiHostUrl + "/did-registry/v3/identifiers/";

    if (arguments.length !== 1) {
      result = { ...nssiBEACrequestFormInvalid };
      result.body = "Bad number of Arguments:[" + arguments.length + "]";
      log.error(
        applicationName +
          ":ebsi-services-application: getEBSIIdentifierByID:Bad request:" +
          result.body +
          "."
      );
      log.trace(applicationName + ":ebsi-services-application: getEBSIIdentifierByID:Done ");
      return result;
    }

    const response = await fetch(getDIDUrl + DID);

    if (response.status !== 200) {
      result = { ...nssiIWBadHtmlResponse };
      result.body = "Call to " + getDIDUrl + DID + "return as status:" + response.status + ".";
      log.error(
        applicationName + ":ebsi-services-application: getEBSIIdentifierByID:" + result.body
      );
      log.trace(applicationName + ":ebsi-services-application: getEBSIIdentifierByID:Done ");
      return result;
    }

    result = { ...nssiBEACOk };
    result.body = await response.text();

    log.trace(applicationName + ":ebsi-services-application: getEBSIIdentifierByID:Done ");
    return result;
  } catch (ex) {
    log.exception(
      applicationName +
        ":ebsi-services-application:getEBSIIdentifierByID:Bad request: An Exception occurred:[" +
        ex +
        "]"
    );
    log.trace(applicationName + ":ebsi-services-application:getEBSIIdentifierByID:Done ");
  }
}

async function getEBSISchemas() {
  try {
    let result, calledURL, response, nextUrl, resultObject;

    log.trace(applicationName + ":ebsi-services-application: getEBSISchemas:Started ");

    result = { ...nssiBEACOk };

    calledURL = "----------------";
    nextUrl = ebsiHostUrl + "/trusted-schemas-registry/v2/schemas";
    const allSchemaIDs = [];

    if (arguments.length !== 0) {
      result = { ...nssiBEACrequestFormInvalid };
      result.body = "Bad number of Arguments:[" + arguments.length + "]";
      log.error(
        applicationName +
          ":ebsi-services-application: getEBSISchemas:Bad request:" +
          result.body +
          "."
      );
      log.trace(applicationName + ":ebsi-services-application: getEBSISchemas:Done ");
      return result;
    }

    while (!calledURL.includes(nextUrl)) {
      response = await fetch(nextUrl);

      if (response.status !== 200) {
        result = { ...nssiIWBadHtmlResponse };
        result.body = "Call to " + nextUrl + "return as status:" + response.status + ".";
        log.error(applicationName + ":ebsi-services-application: getEBSISchemas:" + result.body);
        log.trace(applicationName + ":ebsi-services-application: getEBSISchemas:Done ");
        return result;
      }

      result.body = await response.text();
      resultObject = JSON.parse(result.body);

      calledURL = nextUrl;
      nextUrl = resultObject.links.next;

      allSchemaIDs.push(...resultObject.items);
      resultObject = {};
    }
    result.body = JSON.stringify(allSchemaIDs);

    log.trace(applicationName + ":ebsi-services-application: getEBSISchemas:Done ");
    return result;
  } catch (ex) {
    log.exception(
      applicationName +
        ":ebsi-services-application:getEBSISchemas:Bad request: An Exception occurred:[" +
        ex +
        "]"
    );
    log.trace(applicationName + ":ebsi-services-application:getEBSISchemas:Done ");
  }
}

export const getEBSISchemaByID = async (schemaId: string): Promise<string> => {
  log.trace(applicationName + ":ebsi-services-application:getEBSISchemaByID:Started");
  const getSchemaUrl = ebsiHostUrl + "/trusted-schemas-registry/v2/schemas/";
  const response = await fetch(getSchemaUrl + schemaId);

  if (response.status !== 200) {
    throw new NotFoundError("Schema not found: " + getSchemaUrl);
  }
  return response.text();
};

async function getEBSISchemaByURL(schemaUrl: string) {
  let result: any = {};
  log.trace(applicationName + ":ebsi-services-application:getEBSISchemaByURL:started");
  log.trace(applicationName + ":ebsi-services-application:getEBSISchemaByURL:" + schemaUrl);

  const cachedSchema = await ebsiSchema.findOne({ schemaURL: schemaUrl });

  if (!cachedSchema) {
    log.trace(
      applicationName +
        "ebsi-services-application:getEBSISchemaByURL:Not found in cache " +
        schemaUrl
    );
    const response = await fetch(schemaUrl);

    if (response.status !== 200) {
      throw new NotFoundError("Schema not found: " + schemaUrl);
    }

    result = await response.text();
    const now = new Date();

    const ebsiSchemaRecord = {
      dateOfRegistration: now.getTime(),
      schemaURL: schemaUrl,
      schemaBody: result,
    };

    await ebsiSchema.create(ebsiSchemaRecord);

    log.trace(applicationName + ":ebsi-services-application:getEBSISchemaByURL:finished");
    return result;
  }

  return cachedSchema.schemaBody;
}

async function getOnboardingAuthRequest() {
  try {
    log.trace(applicationName + ":ebsi-services-application:onboardingAuthRequest:starting");

    const apiPath = "users-onboarding/v1/authentication-requests";
    const hostname = ebsiHostUrl + `/${apiPath}`;
    const body = '{ "scope"' + ":" + '"ebsi users onboarding"} ';
    const payLoad = `${body}`;

    const result: any = {};
    const requestObj = {
      method: "POST",
      body: payLoad,
      headers: {
        accept: "application/json",
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(hostname, requestObj);
    const data = await response.json();
    result.response = JSON.stringify(data);
    log.trace(applicationName + ":ebsi-services-application:onboardingAuthRequest:Done");
    return result;
  } catch (ex) {
    log.exception(
      applicationName +
        ":ebsi-services-application:onboardingAuthRequest:An Exception occurred:[" +
        ex +
        "]"
    );
  }
}

async function getOnboardingAuthResponse(bearerToken, idToken) {
  try {
    log.trace(applicationName + ":ebsi-services-application:getOnboardingAuthResponse:starting");

    const apiPath = "users-onboarding/v2/authentication-responses";
    const hostname = ebsiHostUrl + `/${apiPath}`;
    const body = idToken;
    const payLoad = `${body}`;

    const result: any = {};
    const requestObj = {
      method: "POST",
      body: payLoad,
      headers: {
        accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded",
        authorization: "Bearer " + bearerToken,
      },
    };
    const response = await fetch(hostname, requestObj);
    const data = await response.json();
    result.response = JSON.stringify(data);

    log.trace(applicationName + ":ebsi-services-application:getOnboardingAuthResponse:Done");
    return result;
  } catch (ex) {
    log.exception(
      applicationName +
        ":ebsi-services-application:getOnboardingAuthResponse:An Exception occurred:[" +
        ex +
        "]"
    );
  }
}

async function getAuthorisationAuthResponse(bearerToken) {
  try {
    log.trace(applicationName + ":ebsi-services-application:getOnboardingAuthResponse:starting");

    const apiPath = "authorisation/v2/authentication-requests";
    const hostname = ebsiHostUrl + `/${apiPath}`;
    const payLoad = '{ "scope": "openid did_authn" } ';

    const result: any = {};
    const requestObj = {
      method: "POST",
      body: payLoad,
      headers: {
        accept: "application/json",
        "Content-Type": "application/json",
        authorization: "Bearer " + bearerToken,
      },
    };

    const response = await fetch(hostname, requestObj);
    const data = await response.text();

    result.response = JSON.stringify(data);

    log.trace(applicationName + ":ebsi-services-application:getOnboardingAuthResponse:Done");
    return result;
  } catch (ex) {
    log.exception(
      applicationName +
        ":ebsi-services-application:getOnboardingAuthResponse:An Exception occurred:[" +
        ex +
        "]"
    );
  }
}

export {
  getEBSISchemas,
  getEBSISchemaByURL,
  getEBSIIdentifiers,
  getEBSIIdentifierByID,
  getOnboardingAuthRequest,
  getOnboardingAuthResponse,
  getAuthorisationAuthResponse,
};

/* ----------------------------------End External functions ------------------*/

/* LOG:
 */
