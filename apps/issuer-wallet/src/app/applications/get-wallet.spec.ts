import { getWallet, WalletType } from "./get-wallet";
import * as dbUtils from "@esspass-web-wallet/backend-utils-lib";
import { MongoMemoryServer } from "mongodb-memory-server";
import { logger } from "@esspass-web-wallet/logging-lib";
import { disconnect } from "../utils/open-db-connection";

const log = logger("get-wallet.spec.ts");

describe("GetWallet", () => {
  jest.setTimeout(20_000);

  let mongoServer: MongoMemoryServer;

  const mongoDbName = process.env.DB_NAME;
  const identityCollectionName = process.env.IDENTITY_COLLECTION_NAME;
  process.env.DB_PORT = "37019";

  beforeAll(async () => {
    //open mongo in memory
    mongoServer = await dbUtils.createMongoMemoryServer();
    log.debug("Mongo Memory Server Started()");
    log.debug("mongoServer.started()");
  });

  beforeEach(async () => {
    dbUtils.connect(mongoDbName, log);
  });

  afterEach(async () => {
    await dbUtils.purgeTestData(mongoServer.getUri(), mongoDbName, {
      collection: identityCollectionName,
    });
    log.debug("Test data purged");
  });

  afterAll(async () => {
    await disconnect(logger("esspass-nssi-wallet-unit-test"));
    await mongoServer.stop();
    log.debug("mongoServer.stop()");
  });

  it("should return properly formatted crypto wallet", async () => {
    //given
    const record = {
      //   _id: ObjectId('63d5333379a10084e441b6be'),
      dateOfRegistration: "1674916659924",
      wallet:
        '{"wallet":{"phrase":"aerobic draw copy butter filter grit upper twenty material congress cube unaware","path":"m/44\'/60\'/0\'/0/0","locale":"en"}}',
      did: "did:ebsi:z21NqAkAUjTgG9FazC6LzG35",
      ebsiWallet:
        '{"did":"did:ebsi:z21NqAkAUjTgG9FazC6LzG35","ethWallet":{"_isSigner":true,"address":"0xE6E3af5Dd8f93e04444b4d013542ae458a668194","provider":null},"didVersion":1,"keys":{"ES256K":{"id":"9m6sXoI-_rEiNQRmhRwJuVz2uIJ9S8eXoTyfEV6-c2c","kid":"did:ebsi:z21NqAkAUjTgG9FazC6LzG35#9m6sXoI-_rEiNQRmhRwJuVz2uIJ9S8eXoTyfEV6-c2c","privateKeyJwk":{"kty":"EC","crv":"secp256k1","x":"TGMXN5-CEbVF_j_Qhl1VoCB4x4ZiWarnKIdH4Rc7hcU","y":"Yn6UdHDqwVtlXyngevhGgMO31YyHgBqKiCiO_2962hk","d":"mTIeawXWSMlKyiAUd6p9oDeov20JD2_7BvdoQHwwVec"},"publicKeyJwk":{"kty":"EC","crv":"secp256k1","x":"TGMXN5-CEbVF_j_Qhl1VoCB4x4ZiWarnKIdH4Rc7hcU","y":"Yn6UdHDqwVtlXyngevhGgMO31YyHgBqKiCiO_2962hk"},"privateKeyEncryptionJwk":{"kty":"EC","crv":"secp256k1","x":"TGMXN5-CEbVF_j_Qhl1VoCB4x4ZiWarnKIdH4Rc7hcU","y":"Yn6UdHDqwVtlXyngevhGgMO31YyHgBqKiCiO_2962hk","d":"mTIeawXWSMlKyiAUd6p9oDeov20JD2_7BvdoQHwwVec"},"publicKeyEncryptionJwk":{"kty":"EC","crv":"secp256k1","x":"TGMXN5-CEbVF_j_Qhl1VoCB4x4ZiWarnKIdH4Rc7hcU","y":"Yn6UdHDqwVtlXyngevhGgMO31YyHgBqKiCiO_2962hk"}},"ES256":{"id":"iu1Mav6kmbMnkh11-cx7v0A7KERwdfqCpsXZRwPtfg4","kid":"did:ebsi:z21NqAkAUjTgG9FazC6LzG35#iu1Mav6kmbMnkh11-cx7v0A7KERwdfqCpsXZRwPtfg4","privateKeyJwk":{"kty":"EC","x":"6NKhtR9tlbKA5F8h6psq5N-NNB-ty_Qb5rnUlWBpe3Y","y":"CDNr__fFocwmMy0EkXFnOwFr1Q0HVzvlgyx1gf8InIY","crv":"P-256","d":"rr9v1Il3LDrklHNVW0gT18qFZlz5j_QCbBgffVE9EDU"},"publicKeyJwk":{"kty":"EC","x":"6NKhtR9tlbKA5F8h6psq5N-NNB-ty_Qb5rnUlWBpe3Y","y":"CDNr__fFocwmMy0EkXFnOwFr1Q0HVzvlgyx1gf8InIY","crv":"P-256"},"privateKeyEncryptionJwk":{"kty":"EC","x":"v8lptC9N4WWUJyUnzLp114yU9zQ1QqNYaqUGGi_IFqs","y":"i-NqDfKDSq6-tDf9dEglfPDTaTpBqvUrBGxiSkZEofI","crv":"P-256","d":"E3vlc5rpxukGjYj5FIMSP2XMYAo-90jI2dL-j4__ytc"},"publicKeyEncryptionJwk":{"kty":"EC","x":"v8lptC9N4WWUJyUnzLp114yU9zQ1QqNYaqUGGi_IFqs","y":"i-NqDfKDSq6-tDf9dEglfPDTaTpBqvUrBGxiSkZEofI","crv":"P-256"}}}}',
      __v: 0,
    };

    // create test issuer-identity
    await dbUtils.createTestData(mongoServer.getUri(), mongoDbName, {
      collection: identityCollectionName,
      data: record,
      id: "63d5333379a10084e441b6be",
      check: {
        did: "did:ebsi:z21NqAkAUjTgG9FazC6LzG35",
      },
    });

    log.debug("Test data added : issuer-identity");

    //when
    const wallet = await getWallet();

    //then
    expect(wallet).toBeTruthy();
    expect((wallet as WalletType).ebsiWallet.did).toEqual("did:ebsi:z21NqAkAUjTgG9FazC6LzG35");
  });

  it("should throw an error when crypto wallet is not properly formatted", async () => {
    //given
    const record = {
      dateOfRegistration: "1674916659924",
      wallet:
        '{"wallet":{"phrase":"aerobic draw copy butter filter grit upper twenty material congress cube unaware","path":"m/44\'/60\'/0\'/0/0","locale":"en"}}',
      did: "did:ebsi:zhkNKqkC86BZuzrdGEU9acH",
      ebsiWallet:
        '{"did":"did:ebsi:zhkNKqkC86BZuzrdGEU9acH","ethWallet":{"_isSigner":true,"address":"0xE6E3af5Dd8f93e04444b4d013542ae458a668194","provider":null},"didVersion":1,"keys":{"ES256":{"id":"did:ebsi:zhkNKqkC86BZuzrdGEU9acH#keys-1","privateKeyJwk":{"kty":"EC","x":"gLdRRAIbckT4nCqnuB7WACIGdzBBCtrR9_rFNkbTwE8","y":"K5ahHODwj8R-fu9hiTd_HyDNWCAfQOBCmlchARPB2RM","crv":"P-256","d":"QG6l80VvoCkwpMxhmBlw0fnXtRqLaDp0Ddqo73a4Rks"},"publicKeyJwk":{"kty":"EC","x":"gLdRRAIbckT4nCqnuB7WACIGdzBBCtrR9_rFNkbTwE8","y":"K5ahHODwj8R-fu9hiTd_HyDNWCAfQOBCmlchARPB2RM","crv":"P-256"},"privateKeyEncryptionJwk":{"kty":"EC","x":"gLdRRAIbckT4nCqnuB7WACIGdzBBCtrR9_rFNkbTwE8","y":"K5ahHODwj8R-fu9hiTd_HyDNWCAfQOBCmlchARPB2RM","crv":"P-256","d":"QG6l80VvoCkwpMxhmBlw0fnXtRqLaDp0Ddqo73a4Rks"},"publicKeyEncryptionJwk":{"kty":"EC","x":"gLdRRAIbckT4nCqnuB7WACIGdzBBCtrR9_rFNkbTwE8","y":"K5ahHODwj8R-fu9hiTd_HyDNWCAfQOBCmlchARPB2RM","crv":"P-256"}},"ES256K":{"id":"did:ebsi:zhkNKqkC86BZuzrdGEU9acH#keys-2","privateKeyJwk":{"kty":"EC","crv":"secp256k1","x":"OlCN7s_yjEdGjV-jbYr3A_DvEe3kByuJFpedCRCsjhI","y":"0dMvYln_TW-s_twPF57KSlS9lAdMOiApcK53Yhs5oEE","d":"5uOvXdj5PgRES00BNUKuRYpmgZQ"},"publicKeyJwk":{"kty":"EC","crv":"secp256k1","x":"OlCN7s_yjEdGjV-jbYr3A_DvEe3kByuJFpedCRCsjhI","y":"0dMvYln_TW-s_twPF57KSlS9lAdMOiApcK53Yhs5oEE"},"privateKeyEncryptionJwk":{"kty":"EC","crv":"secp256k1","x":"OlCN7s_yjEdGjV-jbYr3A_DvEe3kByuJFpedCRCsjhI","y":"0dMvYln_TW-s_twPF57KSlS9lAdMOiApcK53Yhs5oEE","d":"5uOvXdj5PgRES00BNUKuRYpmgZQ"},"publicKeyEncryptionJwk":{"kty":"EC","crv":"secp256k1","x":"OlCN7s_yjEdGjV-jbYr3A_DvEe3kByuJFpedCRCsjhI","y":"0dMvYln_TW-s_twPF57KSlS9lAdMOiApcK53Yhs5oEE"}}}}',
      __v: 0,
    };

    // create test issuer-identity
    await dbUtils.createTestData(mongoServer.getUri(), mongoDbName, {
      collection: identityCollectionName,
      data: record,
      id: "63d5333379a10084e441b6be",
      check: {
        did: "did:ebsi:zhkNKqkC86BZuzrdGEU9acH",
      },
    });

    log.debug("Test data added : issuer-identity");

    let wallet: any;
    try {
      //when
      wallet = await getWallet();
    } catch (e) {
      //then
      expect(wallet).toBeFalsy();
      expect(e.message).toEqual(
        "getWallet: ebsi wallet not found or invalid format : " +
          '{"issues":[{"code":"invalid_type","expected":"string","received":"undefined","path":' +
          '["keys","ES256K","kid"],"message":"Required"},{"code":"invalid_type",' +
          '"expected":"string","received":"undefined","path":["keys","ES256","kid"],' +
          '"message":"Required"}],"name":"ZodError"}'
      );
    }
  });
});
