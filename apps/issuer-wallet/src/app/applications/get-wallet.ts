import { NotFoundError } from "@esspass-web-wallet/domain-lib";
import { Wallet } from "ethers";
import { logger } from "@esspass-web-wallet/logging-lib";
import { issuerIdentity } from "../schemas/mongoose/issuerIdentity";
import { ebsiSchemaWallet, EbsiWalletType } from "../model/interface/ebsi-wallet-type";
import { CredentialError } from "@esspass-web-wallet/crypto-lib";

const log = logger("issuer-wallet");

export type WalletType = {
  did: string;
  wallet: Wallet;
  ebsiWallet: EbsiWalletType;
};

export async function getWallet(did = ""): Promise<WalletType | any> {
  log.debug("issuer-wallet:getWallet:Started ");

  const pWallet: any = {};
  let wallets: string | any[];
  did
    ? (wallets = await issuerIdentity.find({ did: did }))
    : (wallets = await issuerIdentity.find());

  if (wallets.length < 1) {
    throw new NotFoundError("getWallet:: issuer identity not found");
  } else if (wallets.length > 1) {
    throw new NotFoundError("getWallet:: more than one issuer identity found");
  }

  const wallet = wallets[0];
  const sWallet = JSON.parse(wallet.wallet);
  pWallet.did = wallet.did;
  pWallet.wallet = Wallet.fromMnemonic(sWallet.wallet.phrase, sWallet.wallet.path);

  const parsing = ebsiSchemaWallet.safeParse({
    ...JSON.parse(wallet.ebsiWallet),
  });
  if (parsing.success === false) {
    throw new CredentialError("invalid_crypto_wallet", {
      errorDescription:
        "getWallet: ebsi wallet not found or invalid format : " + JSON.stringify(parsing.error),
    });
  }

  pWallet.ebsiWallet = parsing.data;

  log.debug("issuer-wallet:getWallet:finished ");

  return pWallet;
}
