import { logger } from "@esspass-web-wallet/logging-lib";
import { issuerIdentity } from "../schemas/mongoose/issuerIdentity";

const log = logger("issuer-wallet");

async function getRecords() {
  try {
    let response;

    log.debug(":manage-isssuer-identity-application:getRecords:Started ");
    log.debug(":manage-isssuer-identity-application:getRecords:Finding record.");

    response = [];

    response = await issuerIdentity.find();
    //console.log( response );
    log.debug(":manage-isssuer-identity-application:getRecords:Done.");
    return response;
  } catch (ex) {
    log.exception(
      ":manage-isssuer-identity-application:getRecords:An exception occurred: [" + ex + "]."
    );
  }
}

async function init(req, res) {
  try {
    log.debug(":manage-isssuer-identity-application:init:Starting");
    log.debug(":manage-isssuer-identity-application:init:Done");
    return 0;
  } catch (ex) {
    log.exception("manage-isssuer-identity-application:init:An exception Occurred:[" + ex + "]");
    log.debug("manage-isssuer-identity-application:init:Done");
  }
}

export { init, getRecords };
