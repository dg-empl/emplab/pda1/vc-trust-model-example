import * as ethers from "ethers";
import { EbsiWallet } from "@cef-ebsi/wallet-lib";
import { calculateJwkThumbprint, exportJWK, generateKeyPair } from "jose";
import { base64url } from "multiformats/bases/base64";
import { logger } from "@esspass-web-wallet/logging-lib";
import { createVerifiablePresentationJwt, EbsiIssuer } from "@cef-ebsi/verifiable-presentation";

import { randomUUID, randomBytes } from "crypto";
import * as mfBasics from "multiformats/basics";

const applicationName = "esspass-web-wallet";
const log = logger("issuer-wallet");

function generateDidDocument(nssiWallet: any) {
  const context = ["https://www.w3.org/ns/did/v1", "https://w3id.org/security/suites/jws-2020/v1"];

  const didDocument = {
    "@context": context,
    id: nssiWallet.did,
    verificationMethod: [] as any[],
    authentication: [] as string[],
    assertionMethod: [] as string[],
  };

  //const ebsiWallet = JSON.parse(nssiWallet.ebsiWallet);

  didDocument.verificationMethod.push({
    id: nssiWallet.ebsiWallet.keys.ES256.id,
    type: "JsonWebKey2020", // all type are the same string so made it easy for now.
    controller: nssiWallet.did,
    publicKeyJwk: nssiWallet.ebsiWallet.keys.ES256.publicKeyJwk,
  });

  didDocument.authentication.push(nssiWallet.ebsiWallet.keys.ES256.id);
  didDocument.assertionMethod.push(nssiWallet.ebsiWallet.keys.ES256.id);
  return didDocument;
}

function newKeyId() {
  const ids = ["keys-1", "keys-2", "keys-3", "keys-4", "keys-5"];
  /* check isf key is already used => doesn't work in client.js*/
  return ids[0];
}

function getPublicKeyES256KJwk(jwk) {
  const { d, ...publicJwk } = jwk;
  return publicJwk;
}

function getES256KJWK(did: any, privateKeyJwk: any) {
  type T = { [keys: string | number]: string };
  const keyStruct: T | any = {};

  try {
    keyStruct.id = did + "#" + newKeyId();
    keyStruct.privateKeyJwk = privateKeyJwk;
    keyStruct.publicKeyJwk = getPublicKeyES256KJwk(privateKeyJwk);
    keyStruct.privateKeyEncryptionJwk = privateKeyJwk;
    keyStruct.publicKeyEncryptionJwk = keyStruct.publicKeyJwk;
  } catch (ex) {
    log.exception(
      applicationName +
        ":wallet-crypto-functions-application:getES256KJWK:An Exception occurred [" +
        ex +
        "]."
    );
  }

  return keyStruct;
}

function getPrivateKeyJwk(privateKeyHex: any) {
  try {
    const publicKeyJWK = new EbsiWallet(privateKeyHex).getPublicKey({
      format: "jwk",
    });
    const d = Buffer.from(removePrefix0x(privateKeyHex), "hex")
      .toString("base64")
      .replace(/\+/g, "-")
      .replace(/\//g, "_")
      .replace(/=/g, "");
    return { publicKeyJWK, d };
  } catch (ex) {
    log.exception(
      applicationName +
        ":wallet-crypto-functions-application:getPrivateKeyJwk:An Exception occurred [" +
        ex +
        "]."
    );
  }
  throw new Error("wallet-crypto-functions-application:getPrivateKeyJwk:An Exception occurred");
}

async function createES256KJWK(did: any, privateKey: any) {
  try {
    const ES256K: any = {};

    log.trace(applicationName + ":wallet-crypto-functions-application:createES256KJWK:Started ");

    const privateKeyJwk = getPrivateKeyJwk(privateKey);
    ES256K.ES256K = getES256KJWK(did, privateKeyJwk);

    log.trace(applicationName + ":wallet-crypto-functions-application:createES256KJWK:Done ");
    return ES256K;
  } catch (ex) {
    log.exception(
      applicationName +
        ":wallet-crypto-functions-application:createES256KJWK:An Exception occurred [" +
        ex +
        "]."
    );
  }
  throw new Error("wallet-crypto-functions-application:createES256KJWK:An Exception occurred");
}

async function createES256JWK(did, privateKey) {
  try {
    log.trace(applicationName + ":wallet-crypto-functions-application:createES256JWK:Started ");

    const alg = "ES256";
    const { privateKey, publicKey } = await generateKeyPair(alg);

    const ES256: any = {};
    const tmp: any = {};
    tmp.id = did + "#" + newKeyId();
    tmp.privateKeyJwk = await exportJWK(privateKey);
    tmp.publicKeyJwk = await exportJWK(publicKey);
    tmp.privateKeyEncryptionJwk = tmp.privateKeyJwk;
    tmp.publicKeyEncryptionJwk = tmp.publicKeyJwk;
    ES256.ES256 = tmp;

    log.trace(applicationName + ":wallet-crypto-functions-application:createES256JWK:Done ");
    return ES256;
  } catch (ex) {
    log.exception(
      applicationName +
        ":wallet-crypto-functions-application:createES256JWK:An Exception occurred [" +
        ex +
        "]."
    );
  }
}

async function init() {
  try {
    log.trace(applicationName + ":wallet-crypto-functions-application:init:Started ");
    log.trace(applicationName + ":wallet-crypto-functions-application:init:Done ");
  } catch (ex) {
    log.exception(
      applicationName +
        ":wallet-crypto-functions-application:init:An Exception occurred [" +
        ex +
        "]."
    );
  }
}

function removePrefix0x(key: any) {
  try {
    log.trace(applicationName + ":wallet-crypto-functions-application:removePrefix0x:Started ");

    const output = key.startsWith("0x") ? key.slice(2) : key;

    log.debug(
      applicationName +
        ":wallet-crypto-functions-application:removePrefix0x:output:[" +
        output +
        "] "
    );
    log.trace(applicationName + ":wallet-crypto-functions-application:removePrefix0x:Done ");
    return output;
  } catch (ex) {
    log.exception(
      applicationName +
        ":wallet-crypto-functions-application:removePrefix0x:An Exception occurred [" +
        ex +
        "]."
    );
  }
}

function fromHexString(hexString: any) {
  try {
    log.trace(applicationName + ":wallet-crypto-functions-application:fromHexString:Started ");

    const strippedhexString = removePrefix0x(hexString);
    const output = Buffer.from(strippedhexString, "hex");

    log.debug(
      applicationName +
        ":wallet-crypto-functions-application:fromHexString:output:[" +
        output +
        "] "
    );
    log.trace(applicationName + ":wallet-crypto-functions-application:fromHexString:Done ");
    return output;
  } catch (ex) {
    log.exception(
      applicationName +
        ":wallet-crypto-functions-application:fromHexString:An Exception occurred [" +
        ex +
        "]."
    );
  }
  throw new Error("wallet-crypto-functions-application:fromHexString:An Exception occurred");
}

function multibaseEncode(base, input) {
  try {
    log.trace(applicationName + ":wallet-crypto-functions-application:multibaseEncode:Started ");

    const buffer = typeof input === "string" ? fromHexString(input) : input;
    const output = mfBasics.bases[base].encode(buffer).toString();
    log.debug(
      applicationName +
        ":wallet-crypto-functions-application:multibaseEncode:output:[" +
        output +
        "] "
    );
    log.trace(applicationName + ":wallet-crypto-functions-application:multibaseEncode:Done ");
    return output;
  } catch (ex) {
    log.exception(
      applicationName +
        ":wallet-crypto-functions-application:multibaseEncode:An Exception occurred [" +
        ex +
        "]."
    );
  }
}

async function createRandomEBSIDID() {
  try {
    let did, tmpBuffer;

    log.trace(
      applicationName + ":wallet-crypto-functions-application:createRandomEBSIDID:Started "
    );

    did = "";
    const buffer = randomBytes(17);
    buffer[0] = 1;

    const base58Did = multibaseEncode("base58btc", buffer);
    did = "did:ebsi:" + base58Did;
    log.trace(applicationName + ":wallet-crypto-functions-application:createRandomEBSIDID:Done ");
    return did;
  } catch (ex) {
    log.exception(
      applicationName +
        ":wallet-crypto-functions-application:createRandomEBSIDID:An Exception Occurred [" +
        ex +
        "]."
    );
  }
}

/**
 *  Produce NPDID v2
 */
async function createRandomNPDID(client: any) {
  try {
    const ALG = "ES256";
    await log.trace(
      applicationName + ":wallet-crypto-functions-application:createRandomNPDID:Started "
    );

    const response = {} as any;
    const jwk = client.keys[ALG].publicKeyJwk;
    const thumbprint = await calculateJwkThumbprint(jwk, "sha256");

    log.traceObj("thumbprint=>", thumbprint);
    const subjectIdentifier = base64url.baseDecode(thumbprint);
    log.traceObj("subjectIdentifier=>", subjectIdentifier);
    response.did = EbsiWallet.createDid("NATURAL_PERSON", subjectIdentifier);
    log.traceObj("response.did=>", response.did);
    response.id = `${response.did}#${thumbprint}`;
    await log.trace(
      applicationName + ":wallet-crypto-functions-application:createRandomNPDID:Done "
    );
    log.traceObj("response=>", response);
    return response;
  } catch (ex) {
    await log.exception(
      applicationName +
        ":wallet-crypto-functions-application:createRandomNPDID:An Exception Occurred [" +
        ex +
        "]."
    );
  }

  await log.error(
    applicationName + ":wallet-crypto-functions-application:createRandomNPDID:An Exception Occurred"
  );

  throw new Error("wallet-crypto-functions-application:createRandomNPDID:An Exception occurred");
}

async function createRandomWallet() {
  try {
    return ethers.Wallet.createRandom();
  } catch (ex) {
    log.exception(
      applicationName +
        ":wallet-crypto-functions-application:createRandomWallet: An Exception Occurred [" +
        ex +
        "]."
    );
  }

  throw new Error(
    "':wallet-crypto-functions-application:createRandomWallet: An Exception Occurred"
  );
}

async function createVP(vc, keys, did: string) {
  const vpIssuerKid = keys.id;
  const vpIssuerDid = did;

  if (!vpIssuerDid) {
    throw Error("No vpIssuerDid");
  }

  const alg = "ES256K";
  const publicKeyJwk = keys.privateKeyJwk;
  const privateKeyJwk = keys.publicKeyJwk;

  const vcJwt = vc;

  const audience = "authorisation-api";
  const EBSI_AUTHORITY = "test";

  const vpPayload = {
    id: randomUUID(), // `urn:did:${( 0, uuid_1.v4 )()}`,
    "@context": ["https://www.w3.org/2018/credentials/v1"],
    type: ["VerifiablePresentation"],
    holder: vpIssuerDid,
    verifiableCredential: [vcJwt],
  };

  const vpIssuer: EbsiIssuer = {
    did: vpIssuerDid,
    kid: vpIssuerKid,
    publicKeyJwk,
    privateKeyJwk,
    alg: alg,
  };

  return await createVerifiablePresentationJwt(vpPayload, vpIssuer, audience, {
    skipValidation: true,
    ebsiAuthority: EBSI_AUTHORITY,
    exp: Math.floor(Date.now() / 1000) + 900,
  });
}

function getIDToken(openIDAuthorisationString) {
  const result = openIDAuthorisationString.split("&request=");
  const result2 = result[1].split('"');
  return result2[0];
}

export {
  init,
  createRandomWallet,
  createES256KJWK,
  generateDidDocument,
  createRandomNPDID,
  createRandomEBSIDID,
  createVP,
  getPrivateKeyJwk,
  createES256JWK,
};
