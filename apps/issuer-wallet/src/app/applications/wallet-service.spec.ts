import * as verifiableCredential from "@cef-ebsi/verifiable-credential";
import { decodeJwt } from "jose";

describe("ti-wallet-service", () => {
  //given
  const jwt =
    "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImRpZDplYnNpOnpzcUJZUXVLdW5OZHIzWGZHZlBVdlU3I0NrOGNIZXRrcC1vNjJtQjBaSDRRSU1ib1R6QlVMaGhnUGFRbnZ0U09WQlUifQ.eyJqdGkiOiJ1cm46dXVpZDplY2UyZmM2OS00NGFiLTRlOWUtODE3Yi0xNDU2OTU5ZWI4ZTIiLCJzdWIiOiJkaWQ6ZWJzaTp6cXB4QmlXeWZHQjJHSjlqSmN4eXB0ZmNKZ1pmMmpucktCcHg5cGduZXVrSGoiLCJpc3MiOiJkaWQ6ZWJzaTp6c3FCWVF1S3VuTmRyM1hmR2ZQVXZVNyIsIm5iZiI6MTY5MjYxMzg1NywiaWF0IjoxNjkyNjEzODU3LCJleHAiOjE3MjQxNDk4NTcsInZjIjp7IkBjb250ZXh0IjpbImh0dHBzOi8vd3d3LnczLm9yZy8yMDE4L2NyZWRlbnRpYWxzL3YxIl0sImlkIjoidXJuOnV1aWQ6ZWNlMmZjNjktNDRhYi00ZTllLTgxN2ItMTQ1Njk1OWViOGUyIiwidHlwZSI6WyJWZXJpZmlhYmxlQ3JlZGVudGlhbCIsIlZlcmlmaWFibGVBdHRlc3RhdGlvbiJdLCJpc3N1YW5jZURhdGUiOiIyMDIzLTA4LTIxVDEwOjMwOjU3LjIxNloiLCJ2YWxpZEZyb20iOiIyMDIzLTA4LTIxVDEwOjMwOjU3LjIxNloiLCJpc3N1ZWQiOiIyMDIzLTA4LTIxVDEwOjMwOjU3LjIxNloiLCJjcmVkZW50aWFsU3RhdHVzIjp7ImlkIjoiZGlkOmVic2k6enNxQllRdUt1bk5kcjNYZkdmUFV2VTciLCJ0eXBlIjoiU3RhdHVzTGlzdDIwMjFFbnRyeSIsInN0YXR1c1B1cnBvc2UiOiJyZXZvY2F0aW9uIiwic3RhdHVzTGlzdEluZGV4IjoiMzAiLCJzdGF0dXNMaXN0Q3JlZGVudGlhbCI6Imh0dHBzOi8vbG9jYWxob3N0OjIwMDgwL2NyZWRlbnRpYWxzL3N0YXR1cy8xI2xpc3QifSwiY3JlZGVudGlhbFN1YmplY3QiOnsic2VjdGlvbjEiOnsicGVyc29uYWxJZGVudGlmaWNhdGlvbk51bWJlciI6InRlc3QiLCJzZXgiOiIwMSIsInN1cm5hbWUiOiJaYXdhZHpraSIsImZvcmVuYW1lcyI6Ik1hcmNpbiIsImRhdGVCaXJ0aCI6IjE5MDAtMDEtMDEiLCJzdXJuYW1lQXRCaXJ0aCI6InRlc3QiLCJwbGFjZUJpcnRoIjp7InRvd24iOiJ0ZXN0IiwicmVnaW9uIjoidGVzdCIsImNvdW50cnlDb2RlIjoiQVQifSwibmF0aW9uYWxpdGllcyI6WyJBVCJdLCJzdGF0ZU9mUmVzaWRlbmNlQWRkcmVzcyI6eyJzdHJlZXRObyI6InRlc3QsIHRlc3QgIiwicG9zdENvZGUiOiJ0ZXN0IiwidG93biI6InRlc3QiLCJjb3VudHJ5Q29kZSI6IkJHIn0sInN0YXRlT2ZTdGF5QWRkcmVzcyI6eyJzdHJlZXRObyI6InRlc3QsIHRlc3QgIiwicG9zdENvZGUiOiJ0ZXN0IiwidG93biI6InRlc3QiLCJjb3VudHJ5Q29kZSI6IkJHIn19LCJzZWN0aW9uMiI6eyJtZW1iZXJTdGF0ZVdoaWNoTGVnaXNsYXRpb25BcHBsaWVzIjoiQ1oiLCJzdGFydGluZ0RhdGUiOiIyMDIzLTA5LTAxIiwiZW5kaW5nRGF0ZSI6IjIwMjMtMTItMzEiLCJjZXJ0aWZpY2F0ZUZvckR1cmF0aW9uQWN0aXZpdHkiOnRydWUsImRldGVybWluYXRpb25Qcm92aXNpb25hbCI6ZmFsc2UsInRyYW5zaXRpb25SdWxlc0FwcGx5QXNFQzg4MzIwMDQiOmZhbHNlfSwic2VjdGlvbjMiOnsicG9zdGVkRW1wbG95ZWRQZXJzb24iOmZhbHNlLCJlbXBsb3llZFR3b09yTW9yZVN0YXRlcyI6ZmFsc2UsInBvc3RlZFNlbGZFbXBsb3llZFBlcnNvbiI6dHJ1ZSwic2VsZkVtcGxveWVkVHdvT3JNb3JlU3RhdGVzIjp0cnVlLCJjaXZpbFNlcnZhbnQiOnRydWUsImNvbnRyYWN0U3RhZmYiOmZhbHNlLCJtYXJpbmVyIjpmYWxzZSwiZW1wbG95ZWRBbmRTZWxmRW1wbG95ZWQiOmZhbHNlLCJjaXZpbEFuZEVtcGxveWVkU2VsZkVtcGxveWVkIjp0cnVlLCJmbGlnaHRDcmV3TWVtYmVyIjpmYWxzZSwiZXhjZXB0aW9uIjpmYWxzZSwiZXhjZXB0aW9uRGVzY3JpcHRpb24iOiIiLCJ3b3JraW5nSW5TdGF0ZVVuZGVyMjEiOmZhbHNlfSwic2VjdGlvbjQiOnsiZW1wbG95ZWUiOmZhbHNlLCJzZWxmRW1wbG95ZWRBY3Rpdml0eSI6dHJ1ZSwiZW1wbG95ZXJTZWxmRW1wbG95ZWRBY3Rpdml0eUNvZGVzIjpbIjEyMyJdLCJuYW1lQnVzaW5lc3NOYW1lIjoiMTIzIiwicmVnaXN0ZXJlZEFkZHJlc3MiOnsic3RyZWV0Tm8iOiIxMjMsIDEyMyAiLCJwb3N0Q29kZSI6IjEyMyIsInRvd24iOiJyIiwiY291bnRyeUNvZGUiOiJBVCJ9fSwic2VjdGlvbjUiOnsibm9GaXhlZEFkZHJlc3MiOnRydWV9LCJzZWN0aW9uNiI6eyJuYW1lIjoiT2ZmaWNlIE5hdGlvbmFsIGRlIFPDqWN1cml0w6kgU29jaWFsZSAvIFJpamtzZGllbnN0IHZvb3IgU29jaWFsZSBaZWtlcmhlaWQiLCJhZGRyZXNzIjp7InN0cmVldE5vIjoiTWFpbiBTdHJlZXQgMSIsInBvc3RDb2RlIjoiMTAwMCIsInRvd24iOiJCcnVzc2VscyIsImNvdW50cnlDb2RlIjoiQkUifSwiaW5zdGl0dXRpb25JRCI6Ik5TU0ktQkUtMDEiLCJvZmZpY2VQaG9uZU5vIjoiMDgwMCAxMjM0NSIsIm9mZmljZUZheE5vIjoiMDgwMCA5ODc2NSIsImVtYWlsIjoiaW5mb0Buc3NpLWJlLmV1IiwiZGF0ZSI6IjIwMjMtMDgtMjEiLCJzaWduYXR1cmUiOiJPZmZpY2lhbCBzaWduYXR1cmUifSwiaWQiOiJkaWQ6ZWJzaTp6cXB4QmlXeWZHQjJHSjlqSmN4eXB0ZmNKZ1pmMmpucktCcHg5cGduZXVrSGoifSwiY3JlZGVudGlhbFNjaGVtYSI6eyJpZCI6Imh0dHBzOi8vYXBpLXBpbG90LmVic2kuZXUvdHJ1c3RlZC1zY2hlbWFzLXJlZ2lzdHJ5L3YyL3NjaGVtYXMvMHg0N2M2MTY2NzI4MWNjMzg4M2NjYTAwMWJjYWM3MDM4MjM3Yzg1ZDBlOGY2MWYxMzk2NTIzNzVjMzMwYTk5MDYzIiwidHlwZSI6IkZ1bGxKc29uU2NoZW1hVmFsaWRhdG9yMjAyMSJ9LCJpc3N1ZXIiOiJkaWQ6ZWJzaTp6c3FCWVF1S3VuTmRyM1hmR2ZQVXZVNyIsImV4cGlyYXRpb25EYXRlIjoiMjAyNC0wOC0yMFQxMDozMDo1Ny4wMDBaIn19.cCLTiC-_i8ivEWmWruJ7XwcJ7d3aGsTn-vtr6myzdeYH_LcaSyd04qnZX-L1ZuxbFBaGEJvCn_qAkRewFEHoag";

  const EBSI_ENV_DOMAIN = "https://api-pilot.ebsi.eu";
  const DIDR_API_PATH = "/did-registry/v4/identifiers";
  const TIR_API_PATH = "/trusted-issuers-registry/v4/issuers";
  const TPR_API_PATH = "/trusted-policies-registry/v2/users";

  // verify VC
  const ebsiAuthority = EBSI_ENV_DOMAIN.replace(/^https?:\/\//, ""); // remove http protocol scheme
  const didRegistry = `${EBSI_ENV_DOMAIN}${DIDR_API_PATH}`;
  const trustedIssuersRegistry = `${EBSI_ENV_DOMAIN}${TIR_API_PATH}`;
  const trustedPoliciesRegistry = `${EBSI_ENV_DOMAIN}${TPR_API_PATH}`;

  const options = {
    ebsiAuthority,
    ebsiEnvConfig: {
      didRegistry,
      trustedIssuersRegistry,
      trustedPoliciesRegistry,
    },
  };

  it("should decode jwt", async () => {
    //decode jwt to get VC
    const decodedJwt = decodeJwt(jwt);

    expect(decodedJwt).toBeTruthy();
  });

  it("should verify jwt against EBSI pilot env", async () => {
    const verifiedVc = await verifiableCredential.verifyCredentialJwt(jwt, {
      ...options,
      /**
       * Skip status validation due to jwt's statusListCredential:"https://localhost:20080/credentials/status/1#list" is not available in EBSI pilot env
       */ skipStatusValidation: true,
    });

    //then
    expect(verifiedVc).toBeTruthy();
  });

  it("should throw error when verify jwt against EBSI pilot env", async () => {
    //when
    try {
      await verifiableCredential.verifyCredentialJwt(jwt, {
        ...options,
        skipStatusValidation: false,
      });
    } catch (e) {
      //then
      expect(e.toString()).toEqual(
        'ValidationError: The credentialStatus "statusListCredential" property must be a URL from the Trusted Issuers Registry (https://api-pilot.ebsi.eu/trusted-issuers-registry/v4/issuers)'
      );
    }
  });
});
