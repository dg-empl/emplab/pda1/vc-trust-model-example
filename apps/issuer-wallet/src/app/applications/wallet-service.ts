import { Wallet } from "ethers";
import { logger } from "@esspass-web-wallet/logging-lib";
import { v4 as uuidv4 } from "uuid";
import * as verifiableCredential from "@cef-ebsi/verifiable-credential";
import { calculateJwkThumbprint, importJWK, SignJWT } from "jose";

import {
  nssiBEACOk,
  nssiBEACException,
  nssiIWEBSIValidationIncorrect,
} from "../schemas/errors/errorCatalog";

import {
  createRandomWallet,
  createES256KJWK,
  generateDidDocument,
  createRandomNPDID,
  createRandomEBSIDID,
  createES256JWK,
} from "./wallet-crypto-functions-application";

import { getEBSISchemaByURL } from "./ebsi-services-application";

import * as jsonschema from "jsonschema";
import { $RefParser } from "@apidevtools/json-schema-ref-parser";

import { issuerIdentity } from "../schemas/mongoose/issuerIdentity";
import { holderIdentity } from "../schemas/mongoose/holderIdentity";
import { Schema } from "jsonschema";
import {
  NotFoundError,
  StatusCredentialListType,
  VerifiableCredential,
  verifiableCredentialSchema,
} from "@esspass-web-wallet/domain-lib";
import { IssuerResponse } from "../model/interface/interface-type-common";
import { ebsiSchema } from "../schemas/mongoose/ebsiSchema";
import { JsonWebKeySet, KeyPair } from "@esspass-web-wallet/crypto-lib";
import { getWallet, WalletType } from "./get-wallet";
import * as process from "process";
import type { VerifyCredentialOptions } from "@cef-ebsi/verifiable-credential/dist/types";

const log = logger("issuer-wallet");

async function init() {
  try {
    log.debug("wallet-service: init:Started ");

    const getIssuerWalletsResponse = await getIssuerWallets();
    if (getIssuerWalletsResponse.returnCode === nssiBEACOk.returnCode) {
      if (getIssuerWalletsResponse.body.length > 0) {
        log.debug("setupWallet:setupWallet:Wallet setup skipped");
      } else {
        log.debug("wallet-service:setupWallet:Setting up wallet");
        await generateWallet();
      }
    } else {
      log.exception(
        "wallet-service: init:An error occurred after calling  getIssuerWallets()" +
          getIssuerWalletsResponse.body
      );
      log.debug("wallet-service: init:finished ");
      return getIssuerWalletsResponse;
    }

    log.debug("wallet-service: init:finished ");
    return nssiBEACOk;
  } catch (ex) {
    const exception = { ...nssiBEACException };

    log.exception("wallet-service: init:An Exception occurred [" + ex + "].");
    exception.body = "An Exception occurred [" + ex + "].";
    log.debug("wallet-service: init:finished ");
    return exception;
  }
}

/**
 * Expose Issuer public keys.
 * Used in "Issue & Revoke" tests.
 *
 * @returns Issuer JWKS
 */
async function getJwks(alg = "ES256"): Promise<JsonWebKeySet> {
  const wallet: WalletType = await getWallet();

  if (!(wallet instanceof Object)) {
    throw new NotFoundError("getJwks:Issuer wallet not found");
  }

  const walletPubKey = wallet.ebsiWallet.keys.ES256.publicKeyJwk;

  const jwk = {
    kty: "EC",
    crv: alg === "ES256" ? "P-256" : "secp256k1",
    alg,
    x: walletPubKey.x,
    y: walletPubKey.y,
  };

  const thumbprint = await calculateJwkThumbprint(jwk);

  const publicKeyJwk = {
    ...jwk,
    kid: thumbprint,
  };

  return {
    keys: [{ ...publicKeyJwk, alg: "ES256" }],
  };
}

async function getIssuerWallets(did?: string) {
  try {
    const responseCode = { ...nssiBEACOk };
    log.debug("wallet-service:getIssuerWallets:started");
    console.log("(DID): ", did);
    if (typeof did !== "undefined") {
      log.debug("wallet-service:getIssuerWallets:DID is defined: " + did);
      responseCode.body = await issuerIdentity.find({ did: did });
    } else {
      log.debug("wallet-service:getIssuerWallets:DID is NOT! defined: " + did);
      responseCode.body = await issuerIdentity.find();
    }

    log.debug("wallet-service:getIssuerWallets:finished");
    return responseCode;
  } catch (ex) {
    log.exception("wallet-service:getIssuerWallets:An Exception occurred [" + ex + "].");
  }
}

function updateClientKeyIDs(Client) {
  try {
    log.debug("wallet-service:generateWallet:started");

    let keyID = 1;
    Object.keys(Client.keys).forEach(item => {
      Client.keys[item].id = Client.did + "#keys-" + keyID;
      keyID++;
    });
    log.debug("wallet-service:generateWallet:finished");
  } catch (ex) {
    log.exception("wallet-service:updateClientKeyIDs:An Exception occurred [" + ex + "].");
  }
}

async function generateWallet() {
  try {
    const responseCode = { ...nssiBEACOk };
    log.debug("wallet-service:generateWallet:Started ");

    const trecord = {} as any;
    const record = {} as any;
    const Client = {} as any;
    const Client2 = {} as any;
    const did = await createRandomEBSIDID();
    const wallet = await createRandomWallet();
    const now = new Date();
    Client.did = did;
    Client.ethWallet = wallet;
    Client.didVersion = 1;
    Client.keys = await createES256JWK(did, wallet);

    Client2.keys = await createES256KJWK(did, wallet.address);
    Client.keys.ES256K = Client2.keys.ES256K;

    updateClientKeyIDs(Client);
    console.log(Client);
    record.dateOfRegistration = now.getTime();
    record.did = Client.did;
    trecord.wallet = wallet._mnemonic();
    record.ebsiWallet = JSON.stringify(Client);
    record.wallet = JSON.stringify(trecord);

    responseCode.body = await issuerIdentity.create(record);

    log.debug("wallet-service:generateWallet:finished ");
    return responseCode;
  } catch (ex) {
    const responseCode = { ...nssiBEACException };
    responseCode.body = "An Exception occurred [" + ex + "].";
    log.exception("wallet-service:generateWallet:" + responseCode.body);
    log.debug("wallet-service:generateWallet:finished ");
    return responseCode;
  }
}

async function getDidDocument(did: string) {
  return await getWallet(did);
}

async function getIssuerDid() {
  log.debug("wallet-service:getIssuerDid:Started ");

  const wallet = await getWallet();

  if (!(wallet instanceof Object)) {
    throw new NotFoundError("getIssuerDid:Issuer wallet not found");
  }

  const didDocumentResponse = generateDidDocument(wallet);
  if (!didDocumentResponse) {
    throw new NotFoundError("getIssuerDid:Issuer DID not generated");
  }

  return didDocumentResponse.id;
}

async function validateVCBySchema(
  credential: VerifiableCredential | StatusCredentialListType
): Promise<{
  returnCode?: number;
  returnMsg?: string;
  resultClass?: number;
  body: { msg: string; errors: string[] };
}> {
  try {
    log.debug("wallet-service:validateVCBySchema:Started ");

    const getEBSISchemaByURLResponse = await getEBSISchemaByURL(credential.credentialSchema.id);

    const credentialSubject = { ...credential };

    const validator = new jsonschema.Validator();
    const rawSchemaObject = JSON.parse(getEBSISchemaByURLResponse);

    log.debug("wallet-service:rawSchemaObject: " + getEBSISchemaByURLResponse);

    const schemaObject = (await $RefParser.dereference(rawSchemaObject)) as Schema;

    log.debug("credentialSubject : " + JSON.stringify(credentialSubject));

    const valResult = validator.validate(credentialSubject, schemaObject);

    if (valResult.errors.length > 0) {
      const result = { ...nssiIWEBSIValidationIncorrect };
      const errorList = [];

      for (const validationError of valResult.errors) {
        const errorObj: any = {};
        errorObj.recordProperty =
          typeof validationError.property !== "undefined" ? validationError.property : "";
        errorObj.errorMessage =
          typeof validationError.message !== "undefined" ? validationError.message : "";
        errorObj.recordPropertyValue =
          typeof validationError.instance === "object"
            ? "Displaying Object supressed"
            : validationError.instance;
        errorList.push(errorObj);
      }
      result.body = {
        msg: "The CredentialSubject is not valid",
        errors: errorList,
      };

      log.debug("wallet-service:validateVCBySchema:errors:finished");
      return result;
    }

    const result = { ...nssiBEACOk };
    result.body = "No Error: No errors occurred during process";
    log.debug("wallet-service:validateVCBySchema:finished");
    return result;
  } catch (ex) {
    throw new Error(ex);
  }
}

async function signVC(vc: Record<string, any>, alg = "ES256"): Promise<string> {
  log.debug("wallet-service:getSignedVC:started");
  const issuerWallet = await getWallet();

  const privateKey = await importJWK(issuerWallet.ebsiWallet.keys[alg].privateKeyJwk, alg);
  const typ = "JWT";
  const kid = issuerWallet.ebsiWallet.keys[alg].kid;
  // const kid = issuerWallet.ebsiWallet.keys[alg].id;
  const nonce = uuidv4();
  const jti = typeof vc.id !== "undefined" ? vc.id : "urn:did:" + nonce;
  const ts = new Date();

  vc.issuanceDate = ts;
  vc.validFrom = ts;
  vc.issued = ts;
  vc.issuer = issuerWallet.did;

  const now = Math.trunc(ts.getTime() / 1000);
  const exp = now + 365 * 24 * 3600;
  const expStringDate = new Date(exp * 1000);

  const options = {
    year: "numeric",
    month: "numeric",
    day: "numeric",
  };
  vc.expirationDate = expStringDate; //.toLocaleString("nl-NL", options);

  const header = {
    alg,
    typ,
    kid,
  };

  const vcPayload = {
    jti,
    sub: vc.credentialSubject.id,
    iss: issuerWallet.did,
    nbf: now,
    iat: now,
    exp,
    vc,
  };

  const signedJwt = await new SignJWT(vcPayload)
    .setProtectedHeader(header)
    .setIssuer(issuerWallet.did)
    .sign(privateKey);

  log.debug("wallet-service:getSignedVC:finished");
  return signedJwt;
}

async function generateMockHolderWallet() {
  try {
    log.debug("wallet-service:generateMockHolderWallet:Started ");

    const alg = "ES256";
    const didv2 = "";
    const wallet = await createRandomWallet();
    //handleErrrors
    const now = new Date();
    const Client: any = {};
    Client.keys = await createES256JWK(didv2, wallet);

    Client.ethWallet = wallet;
    const createRandomNPDIDResult = await createRandomNPDID(Client);

    Client.did = createRandomNPDIDResult.did;
    Client.keys[alg].id = createRandomNPDIDResult.id;

    const record: any = {};
    record.dateOfRegistration = now.getTime();
    record.did = JSON.stringify(Client.did);
    record.ebsiWallet = JSON.stringify(Client);
    record.wallet = JSON.stringify(wallet._mnemonic());
    await holderIdentity.create(record);

    const result = { ...nssiBEACOk };
    result.body = record;

    log.debug("wallet-service:generateMockHolderWallet:finished ");
    return result;
  } catch (ex) {
    const result = { ...nssiBEACException };
    result.body = "An Exception occurred [" + ex + "].";
    log.exception("wallet-service:generateMockHolderWallet:" + result.body);
    log.debug("wallet-service:generateMockHolderWallet::finished ");
    return result;
  }
}

async function validateVC(jwt: string) {
  log.debug("wallet-service:validateVC:Started");

  // const options = { ebsiAuthority: process.env.EBSI_AUTHORITY, skipStatusValidation: true };

  const DIDR_API_PATH = "/did-registry/v4/identifiers";
  const TIR_API_PATH = "/trusted-issuers-registry/v4/issuers";
  const TPR_API_PATH = "/trusted-policies-registry/v2/users";

  // verify VC
  const ebsiAuthority = process.env.EBSI_ENV_DOMAIN.replace(/^https?:\/\//, ""); // remove http protocol scheme
  const didRegistry = `${process.env.EBSI_ENV_DOMAIN}${DIDR_API_PATH}`;
  const trustedIssuersRegistry = `${process.env.EBSI_ENV_DOMAIN}${TIR_API_PATH}`;
  const trustedPoliciesRegistry = `${process.env.EBSI_ENV_DOMAIN}${TPR_API_PATH}`;

  let options: VerifyCredentialOptions = {
    ebsiAuthority,
    ebsiEnvConfig: {
      didRegistry,
      trustedIssuersRegistry,
      trustedPoliciesRegistry,
    },
  };

  /**
   * for local development, we skip the validation of the issuer's DID
   */
  if (process.env.ENVIRONMENT === "development") {
    options = {
      ...options,
      skipAccreditationsValidation: true,
      skipStatusValidation: true,
    };
  }

  await verifiableCredential.verifyCredentialJwt(jwt, options);
  log.debug("wallet-service:validateVC:JWT:finished:" + jwt);
}

export {
  init,
  getJwks,
  getIssuerWallets,
  generateWallet,
  getDidDocument,
  getIssuerDid,
  generateMockHolderWallet,
  validateVCBySchema,
  signVC,
  validateVC,
};
