import { z } from "zod";

export const ebsiSchemaWallet = z.object({
  did: z.string(),
  ethWallet: z.object({
    _isSigner: z.boolean(),
    address: z.string(),
    provider: z.null(),
  }),
  didVersion: z.number(),
  keys: z.object({
    ES256K: z.object({
      id: z.string(),
      kid: z.string(),
      privateKeyJwk: z.object({
        kty: z.string(),
        crv: z.string(),
        x: z.string(),
        y: z.string(),
        d: z.string(),
      }),
      publicKeyJwk: z.object({
        kty: z.string(),
        crv: z.string(),
        x: z.string(),
        y: z.string(),
      }),
      privateKeyEncryptionJwk: z.object({
        kty: z.string(),
        crv: z.string(),
        x: z.string(),
        y: z.string(),
        d: z.string(),
      }),
      publicKeyEncryptionJwk: z.object({
        kty: z.string(),
        crv: z.string(),
        x: z.string(),
        y: z.string(),
      }),
    }),
    ES256: z.object({
      id: z.string(),
      kid: z.string(),
      privateKeyJwk: z.object({
        kty: z.string(),
        x: z.string(),
        y: z.string(),
        crv: z.string(),
        d: z.string(),
      }),
      publicKeyJwk: z.object({
        kty: z.string(),
        x: z.string(),
        y: z.string(),
        crv: z.string(),
      }),
      privateKeyEncryptionJwk: z.object({
        kty: z.string(),
        x: z.string(),
        y: z.string(),
        crv: z.string(),
        d: z.string(),
      }),
      publicKeyEncryptionJwk: z.object({
        kty: z.string(),
        x: z.string(),
        y: z.string(),
        crv: z.string(),
      }),
    }),
  }),
});

export type EbsiWalletType = z.infer<typeof ebsiSchemaWallet>;
