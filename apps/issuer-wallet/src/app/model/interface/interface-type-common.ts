export type ResponseRecord = {
  createRec: any | null;
  histRec: any | null;
};

export type IssuerResponse = {
  body: {
    msg: string;
    errors: string[];
  };
  returnCode: number;
  resultClass: number;
  returnMsg: string;
};
