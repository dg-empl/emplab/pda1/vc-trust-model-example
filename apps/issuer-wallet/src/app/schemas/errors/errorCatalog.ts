const ERRORCATALOG_VERSION = 0x02;

const CLASS_NO_ERROR = 0x00;
const CLASS_ERROR = 0x10;
const CLASS_EXCEPTION = 0x20;
const CLASS_INFO = 0x30;

const NSSI_BE_AC_OK = 0x00;
const NSSI_BE_AC_FAIL = 0x90;
const NSSI_BE_AC_EXCEPTION = 0x91;

const NSSI_BE_AC_USERNAME_UNKNOWN = 0x01;
const NSSI_BE_AC_USERNAME_NOT_UNIQUE = 0x02;
const NSSI_BE_AC_USERNAME_FORMAT_INVALID = 0x03;
const NSSI_BE_AC_PASSWORD_FORMAT_INVALID = 0x04;
const NSSI_BE_AC_PASSWORD_UNKNOWN = 0x05;
const NSSI_BE_AC_PASSWORD_INVALID = 0x06;
const NSSI_BE_AC_EMAIL_NOT_PRESENT = 0x07;
const NSSI_BE_AC_EMAIL_FORMAT_INVALID = 0x08;
const NSSI_BE_AC_EMAIL_ADDRESS_INVALID = 0x09;
const NSSI_BE_AC_REQ_FORM_INVALID = 0x10;
const NSSI_BE_AC_BAD_INPUT_PARAMS = 0x11;
const NSSI_BE_AC_BAD_METHOD = 0x12;

const NSSI_IW_BAD_HTML_RESPONSE = 0x21;
const NSSI_IW_WALLET_NOT_INITIALIZED = 0x22;
const NSSI_IW_DID_NOT_CREATED = 0x23;

const NSSI_IW_DID_NOT_PRESENT_ON_EBSI = 0x30;
const NSSI_IW_DID_NOT_VALID_IN_EBSI = 0x31;
const NSSI_IW_EBSI_SESSIONTOKEN_NOT_VALID = 0x41;
const NSSI_IW_EBSI_SESSIONTOKEN_EXPIRED = 0x42;
const NSSI_IW_EBSI_INPUTDATAINCORRECT = 0x50;
const NSSI_IW_VC_SCHEMAINCORRECT = 0x60;
const NSSI_IW_CS_DATAINCORRECT = 0x61;
const NSSI_IW_EBSI_VALIDATIONINCORRECT = 0x62;
const NSSI_IW_WALLET_NOT_CREATED = 0x70;

interface ErrorInterface {
  returnCode: number;
  resultClass: number;
  returnMsg: string;
  body: any;
}

const nssiErrorCatalogVersion: ErrorInterface = {
  returnCode: NSSI_BE_AC_OK,
  resultClass: CLASS_NO_ERROR,
  returnMsg: "'Version of Error Catalog is :' + ERRORCATALOG_VERSION  + '.' ",
  body: "Version of Error Catalog is :" + ERRORCATALOG_VERSION + ".",
};

const nssiBEACOk: ErrorInterface = {
  returnCode: NSSI_BE_AC_OK,
  resultClass: CLASS_NO_ERROR,
  returnMsg: "No Error: No errors occurred during process.",
  body: {},
};

const nssiBEACFail: ErrorInterface = {
  returnCode: NSSI_BE_AC_FAIL,
  resultClass: CLASS_ERROR,
  returnMsg: "General Error: Errors occurred during process.",
  body: {},
};

const nssiBEACException: ErrorInterface = {
  returnCode: NSSI_BE_AC_EXCEPTION,
  resultClass: CLASS_EXCEPTION,
  returnMsg: "General Exception: Exceptions occurred during process.",
  body: {},
};

const nssiBEACUsernameUnknown: ErrorInterface = {
  returnCode: NSSI_BE_AC_USERNAME_UNKNOWN,
  resultClass: CLASS_ERROR,
  returnMsg: "Error: The user is not known in the system.",
  body: {},
};

const nssiBEACUsernameNotUnique: ErrorInterface = {
  returnCode: NSSI_BE_AC_USERNAME_NOT_UNIQUE,
  resultClass: CLASS_ERROR,
  returnMsg: "Error: The user is not known in the system.",
  body: {},
};

const nssiBEACUsernameInvalidFormat: ErrorInterface = {
  returnCode: NSSI_BE_AC_USERNAME_FORMAT_INVALID,
  resultClass: CLASS_ERROR,
  returnMsg: "Error: The user is not known in the system.",
  body: {},
};

const nssiBEACPasswordInvalidFormat: ErrorInterface = {
  returnCode: NSSI_BE_AC_PASSWORD_FORMAT_INVALID,
  resultClass: CLASS_ERROR,
  returnMsg: "Error: The user is not known in the system.",
  body: {},
};

const nssiBEACPasswordUnknown: ErrorInterface = {
  returnCode: NSSI_BE_AC_PASSWORD_UNKNOWN,
  resultClass: CLASS_ERROR,
  returnMsg: "Error: The user is not known in the system.",
  body: {},
};

const nssiBEACPasswordInvalid: ErrorInterface = {
  returnCode: NSSI_BE_AC_PASSWORD_INVALID,
  resultClass: CLASS_ERROR,
  returnMsg: "Error: The user is not known in the system.",
  body: {},
};

const nssiBEACEmailNotPResent: ErrorInterface = {
  returnCode: NSSI_BE_AC_EMAIL_NOT_PRESENT,
  resultClass: CLASS_ERROR,
  returnMsg: "Error: The user has not passed his/her emailaddress.",
  body: {},
};

const nssiBEACEmailInvalidFormat: ErrorInterface = {
  returnCode: NSSI_BE_AC_EMAIL_FORMAT_INVALID,
  resultClass: CLASS_ERROR,
  returnMsg: "Error: The user is not known in the system.",
  body: {},
};

const nssiBEACEmailInvalidAddress: ErrorInterface = {
  returnCode: NSSI_BE_AC_EMAIL_ADDRESS_INVALID,
  resultClass: CLASS_ERROR,
  returnMsg: "Error: The user gave an Address that doesn't match his/her registered email address.",
  body: {},
};

const nssiBEACrequestFormInvalid: ErrorInterface = {
  returnCode: NSSI_BE_AC_REQ_FORM_INVALID,
  resultClass: CLASS_ERROR,
  returnMsg: "Error: The Frontend sent an invalid request form.",
  body: {},
};

const nssiBEACBadMethod: ErrorInterface = {
  returnCode: NSSI_BE_AC_BAD_METHOD,
  resultClass: CLASS_ERROR,
  returnMsg: "Error: METHOD NOT supported for this service",
  body: {},
};

const nssiBEACBadInputParameters: ErrorInterface = {
  returnCode: NSSI_BE_AC_BAD_INPUT_PARAMS,
  resultClass: CLASS_ERROR,
  returnMsg: "Error: There is an error with the inputparamters",
  body: {},
};

const nssiIWBadHtmlResponse: ErrorInterface = {
  returnCode: NSSI_IW_BAD_HTML_RESPONSE,
  resultClass: CLASS_ERROR,
  returnMsg: "Error: Request returned reponse other than status '200'",
  body: {},
};

const nssiIWWalletNotInitialized: ErrorInterface = {
  returnCode: NSSI_IW_WALLET_NOT_INITIALIZED,
  resultClass: CLASS_ERROR,
  returnMsg: "Error: Wallet is not Initialized",
  body: {},
};

const nssiIWDIDNotCreated: ErrorInterface = {
  returnCode: NSSI_IW_DID_NOT_CREATED,
  resultClass: CLASS_ERROR,
  returnMsg: "Error: Did is not created in wallet",
  body: {},
};

const nssiIWDIDNotPresentOnEBSI: ErrorInterface = {
  returnCode: NSSI_IW_DID_NOT_PRESENT_ON_EBSI,
  resultClass: CLASS_ERROR,
  returnMsg: "Error: Did is not present in EBSI",
  body: {},
};

const nssiIWDIDNotValidInEBSI: ErrorInterface = {
  returnCode: NSSI_IW_DID_NOT_VALID_IN_EBSI,
  resultClass: CLASS_ERROR,
  returnMsg: "Error: Did is not valid in EBSI",
  body: {},
};

const nssiIWEBSISessionTokenNotValidIn: ErrorInterface = {
  returnCode: NSSI_IW_EBSI_SESSIONTOKEN_NOT_VALID,
  resultClass: CLASS_ERROR,
  returnMsg: "Error: EBSI Session Token  is not valid in EBSI",
  body: {},
};

const nssiIWEBSISessionTokenExpired: ErrorInterface = {
  returnCode: NSSI_IW_EBSI_SESSIONTOKEN_EXPIRED,
  resultClass: CLASS_ERROR,
  returnMsg: "Error: EBSI Session Token  has expired",
  body: {},
};
const nssiIWEBSIInputdataIncorrect: ErrorInterface = {
  returnCode: NSSI_IW_EBSI_INPUTDATAINCORRECT,
  resultClass: CLASS_ERROR,
  returnMsg: "Error: INputdata is not correct",
  body: {},
};

const nssiIWVCSchemaIncorrect = {
  returnCode: NSSI_IW_VC_SCHEMAINCORRECT,
  resultClass: CLASS_ERROR,
  returnMsg: "Error: Schema for VC is not correct",
  body: {},
};

const nssiIWCSDataIncorrect = {
  returnCode: NSSI_IW_VC_SCHEMAINCORRECT,
  resultClass: CLASS_ERROR,
  returnMsg: "Error: Schema for VC is not correct",
  body: {},
};

const nssiIWEBSIValidationIncorrect = {
  returnCode: NSSI_IW_CS_DATAINCORRECT,
  resultClass: CLASS_ERROR,
  returnMsg: "Error: Credential Subject Data Incorrect format",
  body: {
    msg: "Credential Subject Data Incorrect format",
    errors: [],
  },
};

const nssiIWWalletNotCreated = {
  returnCode: NSSI_IW_WALLET_NOT_CREATED,
  resultClass: CLASS_INFO,
  returnMsg: "Result: No wallet was created",
  body: {},
};

export {
  nssiBEACOk,
  nssiBEACFail,
  nssiBEACException,
  nssiBEACUsernameUnknown,
  nssiBEACUsernameNotUnique,
  nssiBEACUsernameInvalidFormat,
  nssiBEACPasswordInvalidFormat,
  nssiBEACPasswordUnknown,
  nssiBEACPasswordInvalid,
  nssiBEACEmailNotPResent,
  nssiBEACEmailInvalidFormat,
  nssiBEACEmailInvalidAddress,
  nssiBEACrequestFormInvalid,
  nssiBEACBadInputParameters,
  nssiIWBadHtmlResponse,
  nssiIWWalletNotInitialized,
  nssiIWDIDNotCreated,
  nssiIWDIDNotPresentOnEBSI,
  nssiIWDIDNotValidInEBSI,
  nssiIWEBSISessionTokenNotValidIn,
  nssiIWEBSISessionTokenExpired,
  nssiIWEBSIInputdataIncorrect,
  nssiIWEBSIValidationIncorrect,
  nssiErrorCatalogVersion,
  nssiIWCSDataIncorrect,
  nssiIWVCSchemaIncorrect,
  nssiIWWalletNotCreated,
  nssiBEACBadMethod,
  NSSI_BE_AC_OK,
  NSSI_BE_AC_FAIL,
  NSSI_BE_AC_EXCEPTION,
  NSSI_BE_AC_USERNAME_UNKNOWN,
  NSSI_BE_AC_USERNAME_NOT_UNIQUE,
  NSSI_BE_AC_USERNAME_FORMAT_INVALID,
  NSSI_BE_AC_PASSWORD_FORMAT_INVALID,
  NSSI_BE_AC_PASSWORD_UNKNOWN,
  NSSI_BE_AC_PASSWORD_INVALID,
  NSSI_BE_AC_EMAIL_NOT_PRESENT,
  NSSI_BE_AC_EMAIL_FORMAT_INVALID,
  NSSI_BE_AC_EMAIL_ADDRESS_INVALID,
  NSSI_BE_AC_REQ_FORM_INVALID,
  NSSI_BE_AC_BAD_INPUT_PARAMS,
  NSSI_IW_BAD_HTML_RESPONSE,
  NSSI_IW_WALLET_NOT_INITIALIZED,
  NSSI_IW_DID_NOT_CREATED,
  NSSI_IW_DID_NOT_PRESENT_ON_EBSI,
  NSSI_IW_DID_NOT_VALID_IN_EBSI,
  NSSI_IW_EBSI_SESSIONTOKEN_NOT_VALID,
  NSSI_IW_EBSI_SESSIONTOKEN_EXPIRED,
  NSSI_IW_EBSI_INPUTDATAINCORRECT,
  NSSI_IW_EBSI_VALIDATIONINCORRECT,
  NSSI_IW_WALLET_NOT_CREATED,
  NSSI_IW_CS_DATAINCORRECT,
  NSSI_BE_AC_BAD_METHOD,
};

/* ----------------------------------End External functions ------------------*/

/* LOG:

*/
