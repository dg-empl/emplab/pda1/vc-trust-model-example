import * as mongoose from "mongoose";

const ebsiSchemaSchema = new mongoose.Schema({
  dateOfRegistration: String,
  schemaURL: String,
  schemaBody: String,
});

// const  db = mongoose.connection.useDb('esspass-web-wallet');

export const ebsiSchema = mongoose.model("ebsiSchema", ebsiSchemaSchema);
