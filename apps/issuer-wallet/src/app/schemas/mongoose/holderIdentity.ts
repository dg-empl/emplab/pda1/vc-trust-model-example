import * as mongoose from "mongoose";

const holderIdentitySchema = new mongoose.Schema({
  dateOfRegistration: String,
  wallet: String,
  did: String,
  ebsiWallet: String,
});

// const  db = mongoose.connection.useDb('esspass-web-wallet');

export const holderIdentity = mongoose.model("holderIdentity", holderIdentitySchema);
