import * as mongoose from "mongoose";

const issuerIdentitySchema = new mongoose.Schema({
  dateOfRegistration: String,
  wallet: String,
  did: String,
  ebsiWallet: String,
});

// const  db = mongoose.connection.useDb('esspass-web-wallet');

export const issuerIdentity = mongoose.model("issuerIdentity", issuerIdentitySchema);
