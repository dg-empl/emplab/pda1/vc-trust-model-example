import * as mongoose from "mongoose";

const signedVCSchema = new mongoose.Schema({
  dateOfRegistration: String,
  signedVC: String,
});

// const  db = mongoose.connection.useDb('esspass-web-wallet');

export const signedVC = mongoose.model("signedVC", signedVCSchema);
