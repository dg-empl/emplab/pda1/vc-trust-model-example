import * as core from "express-serve-static-core";
import Keycloak from "keycloak-connect";
import session from "express-session";

import "dotenv/config";

const secret = "C0SbeTsTA5n2!m4^7@QYqA@!asdl2ttbI";

const config = {
  realm: process.env["IDP_REALM"] || "realm",
  "auth-server-url": process.env["IDP"] + "/" || "auth-server-url",
  "ssl-required": "ssl-required",
  resource: process.env["CLIENT_ID"] || "resource",
  credentials: {
    secret: "TO ADD",
  },
  "confidential-port": 0,
};

/*------------------ Keycloak ------------------------------------------------*/
export function keycloakSetupInterop(app: core.Express): Keycloak.Keycloak {
  const memoryStore = new session.MemoryStore();

  app.use(
    session({
      secret: secret,
      resave: false,
      saveUninitialized: true,
      store: memoryStore,
    })
  );

  return new Keycloak({ store: memoryStore }, config);
}
/*----------------------------------------------------------------------------*/
