//open Mongoose
import mongoose from "mongoose";
import { Logger } from "@esspass-web-wallet/logging-lib";

mongoose.set("strictQuery", true);

function buildDbUrl(dbHost, user, password, port, databaseName: string) {
  return `mongodb://${user}:${password}@${dbHost}:${port}/${databaseName}?authSource=admin`;
}

export function connect(databaseName: string, log: Logger) {
  if (!databaseName) {
    throw new Error("Database name is not defined");
  }
  log.info("process.env.DB_HOST : " + process.env.DB_HOST);
  log.info("process.env.DB_PORT : " + process.env.DB_PORT);
  log.info("process.env.DB_USERNAME : " + process.env.DB_USERNAME);
  log.info("process.env.DB_PASSWORD : " + process.env.DB_PASSWORD);

  const connection = buildDbUrl(
    process.env.DB_HOST,
    process.env.DB_USERNAME,
    process.env.DB_PASSWORD,
    process.env.DB_PORT,
    databaseName
  );

  mongoose.connect(connection).then(
    () => log.info("Mongoose opened connection"),
    err => {
      log.error("Mongoose error opening connection " + err);
    }
  );
}

export async function disconnect(log: Logger) {
  log.info("Mongoose disconnecting");
  await mongoose.disconnect();
}
