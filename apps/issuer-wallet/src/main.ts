import { Tracer, Logger, logger } from "@esspass-web-wallet/logging-lib";
const applicationName = "issuer-wallet";

/**
 * OpenTelemetry
 */
const tracer = new Tracer(applicationName);
import { api } from "@opentelemetry/sdk-node";
/**
 * OpenTelemetry
 */

import express from "express";
import "express-async-errors";
import cors from "cors";
import "dotenv/config";

import * as bodyParser from "body-parser";

import { Database, errorHandler } from "@esspass-web-wallet/backend-utils-lib";
import { NotFoundError } from "@esspass-web-wallet/domain-lib";

import { keycloakSetupInterop } from "./app/utils/keycloakInterop";

import {
  ebsiSchemasAdapter,
  ebsiSchemaByIDAdapter,
  ebsiIdentifiersAdapter,
  ebsiIdentifierByIDAdapter,
  issuerDidAdapter,
  createHolderWalletAdapter,
  getSignedVCAdapter,
  ebsiSchemaByURLAdapter,
  getIssuerWalletAdapter,
  generateWalletAdapter,
  createVCFromCSAdapter,
  getPublicJwksAdapter,
} from "./app/adapters/rest/services-adapter";

const app = express();
app.use(cors());

/* needed for TSL termination */
app.set("trust proxy", true);

app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);

app.use(bodyParser.json());

const log = logger("issuer-wallet");

//keycloak
const keycloak = keycloakSetupInterop(app);
app.use(keycloak.middleware());

function setupRoutes() {
  /**
   * version - version deployed
   */
  app.get("/version", (req, res) => {
    return res.send({
      version: process.env.VERSION || "development",
      revision: process.env.GIT_REVISION || "1",
      branch: process.env.GIT_BRANCH || "master",
    });
  });

  app.post("/create-holder-wallet", keycloak.protect(), createHolderWalletAdapter);
  app.post("/create-vc-from-cs", keycloak.protect(), createVCFromCSAdapter);

  app.get("/get-issuer-wallet", getIssuerWalletAdapter);
  app.post("/generate-wallet", keycloak.protect(), generateWalletAdapter);

  app.get("/ebsi-schemas", keycloak.protect(), ebsiSchemasAdapter);
  app.get("/ebsi-schema-by-id", keycloak.protect(), ebsiSchemaByIDAdapter); // change to get method
  app.get("/ebsi-identifiers", keycloak.protect(), ebsiIdentifiersAdapter); // change to get method
  app.get("/ebsi-identifier-by-id", keycloak.protect(), ebsiIdentifierByIDAdapter); // change to get method
  app.get("/issuer-did", keycloak.protect(), issuerDidAdapter); // change to get method
  app.get("/signed-vc", keycloak.protect(), getSignedVCAdapter); // change to get method
  // app.post("/create-vp", keycloak.protect(), createVPAdapter);
  app.get("/ebsi-schema-by-url", keycloak.protect(), ebsiSchemaByURLAdapter); // change to get method

  app.get("/conformance/v3/auth/jwks", getPublicJwksAdapter);

  /**
   * error handler
   */
  app.all("*", (req, res) => {
    log.trace(req.path);
    throw new NotFoundError("Not found route");
  });
  app.use(errorHandler(log));
}

function startServer() {
  log.info("App : " + applicationName);
  log.info("App version : " + process.env.VERSION || "development");
  log.info("Environment : " + process.env.ENVIRONMENT || "development");

  /**
   * connect to database
   */
  Database.getInstance(
    process.env.DB_HOST,
    process.env.DB_USERNAME,
    process.env.DB_PASSWORD,
    process.env.DB_PORT,
    "esspass-nssi-wallet",
    log
  );

  const port = 20090;
  const server = app.listen(port, () => log.info(`Listening at: http://localhost:${port}`));
  server.on("error", log.error);
}

setupRoutes();
startServer();

export default app;
