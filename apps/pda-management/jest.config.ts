/* eslint-disable */
export default {
  displayName: "pda-management",
  preset: "../../jest.preset.js",
  testEnvironment: "node",
  setupFiles: [
    "<rootDir>/src/app/adapters/rest/v2/.env.js",
    "<rootDir>/src/app/application/issuance-flow-v2/.env.js",
    "<rootDir>/src/app/application/pda1/.env.js",
  ],
  transform: {
    "^.+\\.[tj]s$": ["ts-jest", { tsconfig: "<rootDir>/tsconfig.spec.json" }],
  },
  moduleFileExtensions: ["ts", "js", "html"],
  coverageDirectory: "../../coverage/apps/pda-management",
};
