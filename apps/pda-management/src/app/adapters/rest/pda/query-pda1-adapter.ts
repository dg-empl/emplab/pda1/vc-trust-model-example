import { Request, Response } from "express";
import { logger } from "@esspass-web-wallet/logging-lib";
import { Pda1Service } from "../../../application";
import { NotFoundError, Pda1DbType } from "@esspass-web-wallet/domain-lib";

const log = logger("pda-management");

const pda1Service = Pda1Service.getInstance(log);

export const queryPda1Adapter = async (req: Request, res: Response) => {
  const requestFormId = req.params["id"];
  let pda1: Pda1DbType;

  if (!requestFormId) {
    res.status(400).send("Bad Request");
    return;
  }

  log.debug("queryPda1Adapter:started");

  try {
    pda1 = await pda1Service.findPdaByRequestFormById(requestFormId);
  } catch (e) {
    throw new NotFoundError("Find Pda1 failed : " + e);
  }

  log.debug("queryPda1Adapter:finished");
  res.send(pda1.pda1Data);
};
