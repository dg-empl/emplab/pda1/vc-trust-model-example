import { Request, Response } from "express";
import { logger } from "@esspass-web-wallet/logging-lib";
import { Pda1Service, RequestFormService } from "../../../application";
import { RequestFormActionEnum } from "@esspass-web-wallet/domain-lib";
import { check, param, validationResult } from "express-validator";

const log = logger("pda-management");

const pdaService = Pda1Service.getInstance(log);
const requestFormService = RequestFormService.getInstance(log);

/**
 * validateParams for updateRequestFormAdapter
 * @param req - Express request
 * @param id - pdalId
 * @param action - action to perform on pda1 - revoke
 */
async function validateParams(req: Request) {
  await param("id").isString().run(req);
  await check("action")
    .custom(value => value.toString() === RequestFormActionEnum.Revoke)
    .run(req);

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const msg: string[] = [""];
    errors.array().forEach(error => {
      msg.push(" " + `${error.param} : ${error.msg}`);
    });
    throw new Error("Invalid request" + msg.toString());
  }
}

export const revokePda1Adapter = async (req: Request, res: Response) => {
  /**
   * validate action is valid
   */
  await validateParams(req);

  const requestFormId = req.params["id"];
  const action = req.body;

  await pdaService.revokePDA1(requestFormId);
  await requestFormService.updateRequestFormState(requestFormId, action);

  res.send({ status: 200 });
};
