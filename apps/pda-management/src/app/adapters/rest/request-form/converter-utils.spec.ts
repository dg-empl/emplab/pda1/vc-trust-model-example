import { businessRequestFormToPda } from "./converter-utils";
import { pda1Schema, Pda1Type } from "@esspass-web-wallet/domain-lib";
import * as uuid from "uuid";
import { ConversionError } from "@esspass-web-wallet/backend-utils-lib";
import { z } from "zod";
import { RequestFormType } from "../../../domain/request-form-zod-schema";
import { describe, it, expect } from "@jest/globals";

//generate example data from the schema
export const requestForm: RequestFormType = {
  part1: {
    personalIdentificationNumber: "1234567890",
    surname: "Doe",
    forenames: "John",
    surnameAtBirth: "Doe",
    gender: "01",
    dateOfBirth: "1990-01-01",
    nationality: "Poland",
    placeOfBirth: "Warsaw",
    address: {
      street: "20 Lower Hatch Street",
      number: "",
      town: "Dublin",
      postCode: "2",
      countryCode: "IE",
    },
  },
  part2: {
    memberState: "Belgium",
    startingDate: "2021-01-01",
    endingDate: "2021-12-31",
  },
  part3: {
    postedEmployedPerson: "Yes",
    employedInTwoOrMoreMemberStates: "Yes",
    postedSelfEmployedPerson: "No",
    selfEmployedInTwoOrMoreMemberStates: "No",
    civilServant: "No",
    contractStaff: "No",
    mariners: "No",
    employedAndSelfEmployed: "No",
    civilServantAndEmployedOrSelfEmployed: "No",
    flightCrewMember: "No",
    exception: "Yes",
    exceptionDescription: "no exception",
    employedOrSelfEmployedInState21: "No",
  },
  part4: {
    employee: "No",
    selfEmployedActivity: "Yes",
    employedSelfEmployedActivityCode: "1234",
    nameOrBusinessName: "Doe",
    registeredAddress: {
      street: "20 Lower Hatch Street",
      number: "",
      town: "Dublin",
      postCode: "2",
      countryCode: "IE",
    },
  },
  part5: {
    pursuedBusinessName: "Company Abroad s.p.r.l",
    addressInHostMemberState: {
      street: "Extravaganza",
      number: "99",
      town: "Trieste",
      postCode: "902-22",
      countryCode: "IT",
    },
    noFixedAddress: "No",
  },
  part6: {
    name: "Doe",
    street: "Rue de la Loi",
    town: "Brussels",
    postCode: "1000",
    countryCode: "BE",
    institutionId: "1234",
    officeFaxNumber: "123456789",
    officePhoneNumber: "123456789",
    email: "223@test.com",
    date: "2021-01-01",
    signature: "signature",
  },
};

describe("converter-utils", () => {
  it("should convert request form to pda", () => {
    expect(typeof businessRequestFormToPda(requestForm) as string).toBeTruthy();
  });

  it("should match request-form to pda fields", () => {
    //given
    const pda1Type = businessRequestFormToPda(requestForm);

    //when
    const pda1SafeParse = pda1Schema.safeParse({ ...pda1Type, id: `urn:uuid:${uuid.v4()}` });

    if (!pda1SafeParse.success) {
      const errorDesc = pda1SafeParse["error"].issues
        .map(issue => `'${issue.path.join(".")}': ${issue.message}`)
        .join("\n");

      throw new ConversionError("invalid_conversion_action", {
        errorDescription: `Error in conversion from Request Form to Pda1Type model : ${errorDesc}`,
      });
    }

    //then
    expect(pda1SafeParse.success).toBeTruthy();
    expect(pda1SafeParse.data).toBeTruthy();

    const pda1: Pda1Type = pda1SafeParse.data;
    expect(pda1).toBeTruthy();

    // 1.6 Nationality
    expect(pda1.section1.nationalities[0]).toEqual(requestForm.part1.nationality);

    // 1.7 Place of birth
    expect(pda1.section1.placeBirth.town).toEqual("Warsaw");
    expect(pda1.section1.placeBirth.town).toEqual(requestForm.part1.placeOfBirth);
    expect(pda1.section1.placeBirth.region).toEqual(requestForm.part1.placeOfBirth);

    // 1.8.1 Street, N°
    expect(pda1.section1.stateOfResidenceAddress.streetNo).toEqual(
      `${requestForm.part1.address.street} ${requestForm.part1.address.number}`
    );

    // 1.8.2 Town
    expect(pda1.section1.stateOfResidenceAddress.town).toEqual(`${requestForm.part1.address.town}`);

    // 1.9.1 Street, N°
    expect(pda1.section1.stateOfStayAddress.streetNo).toEqual(
      `${requestForm.part1.address.street} ${requestForm.part1.address.number}`
    );

    // 1.9.2 Town
    expect(pda1.section1.stateOfStayAddress.town).toEqual(`${requestForm.part1.address.town}`);

    // 1.9.3 Post code
    expect(pda1.section1.stateOfStayAddress.postCode).toEqual(
      `${requestForm.part1.address.postCode}`
    );

    // 1.9.4 Country code
    expect(pda1.section1.stateOfStayAddress.countryCode).toEqual(
      `${requestForm.part1.address.countryCode}`
    );

    // 3.11.1 Exception Description
    expect(pda1.section3.exceptionDescription).toEqual(requestForm.part3.exceptionDescription);

    // 4.4.1 Street, N°
    expect(pda1.section4.registeredAddress.streetNo).toEqual(
      `${requestForm.part4.registeredAddress.street} ${requestForm.part4.registeredAddress.number}`
    );

    // 4.4.4 Post Code
    expect(pda1.section4.registeredAddress.postCode).toEqual(
      `${requestForm.part4.registeredAddress.postCode}`
    );

    // 5.2 Address in the host State
    // expect(pda1.section5.workPlaceAddresses).toEqual(
    //   `${requestForm.part5.addressInHostMemberState.street} ${requestForm.part5.addressInHostMemberState.number}, ${requestForm.part5.addressInHostMemberState.town}, ${requestForm.part5.addressInHostMemberState.postCode}, ${requestForm.part5.addressInHostMemberState.countryCode}`
    // );
    expect(pda1.section5.workPlaceAddresses[0].address.streetNo).toEqual(
      `${requestForm.part5.addressInHostMemberState.street} ${requestForm.part5.addressInHostMemberState.number}`
    );

    // 6.10 Date
    expect(pda1.section6.date).toEqual(requestForm.part6.date);
  });

  it("should parse uuid", () => {
    //given
    const test = {
      id: "did:ebsi:zn3XhtwmdomBcspSnYPcCMqBy8XP5VMZ4CLcq5auwGC4m",
    };

    const schema = z.object({ id: z.string().url() });

    //when
    const response = schema.safeParse(test);

    //then
    expect(response.success).toBeTruthy();
  });
});
