import { logger } from "@esspass-web-wallet/logging-lib";
import { RequestFormType } from "../../../domain/request-form-zod-schema";
import { Pda1Type } from "@esspass-web-wallet/domain-lib";
import { requestForm } from "./converter-utils.spec";

const log = logger("pda-management");

function formatDate(date: Date): string {
  const isoString = date.toISOString();
  const splitString = isoString.split("T");
  return splitString[0];
}

function stringToBoolean(str: string): boolean {
  return str.toLowerCase() === "yes";
}

type GenderType = "M" | "F" | "X";

export const businessRequestFormToPda = (data: RequestFormType): Pda1Type => {
  log.debug("businessRequestFormToPda : started");
  try {
    const { part1, part3, part4, part5, part7, part2, part6 } = data;

    const pda1: Pda1Type = {};
    pda1.section1 = {
      personalIdentificationNumber: part1.personalIdentificationNumber,
      sex: part1.gender,
      surname: part1.surname,
      forenames: part1.forenames,
      dateBirth: part1.dateOfBirth,
      surnameAtBirth: part1.surnameAtBirth !== "null" ? part1.surnameAtBirth : "---",
      placeBirth: {
        region: part1.placeOfBirth,
        countryCode: part1.address.countryCode,
        town: part1.placeOfBirth,
      },
      nationalities: [part1.nationality],
      stateOfResidenceAddress: {
        streetNo: `${part1.address.street} ${part1.address.number}`,
        postCode: part1.address.postCode,
        town: part1.address.town,
        countryCode: part1.address.countryCode,
      },
      stateOfStayAddress: {
        streetNo: `${part1.address.street} ${part1.address.number}`,
        postCode: part1.address.postCode,
        town: part1.address.town,
        countryCode: part1.address.countryCode,
      },
    };
    pda1.section2 = {
      memberStateWhichLegislationApplies: part2.memberState,
      startingDate: part2.startingDate,
      endingDate: part2.endingDate,
      certificateForDurationActivity: true,
      determinationProvisional: true,
      transitionRulesApplyAsEC8832004: true,
    };
    pda1.section3 = {
      postedEmployedPerson: stringToBoolean(part3.postedEmployedPerson),
      employedTwoOrMoreStates: stringToBoolean(part3.employedInTwoOrMoreMemberStates),
      postedSelfEmployedPerson: stringToBoolean(part3.postedSelfEmployedPerson),
      selfEmployedTwoOrMoreStates: stringToBoolean(part3.selfEmployedInTwoOrMoreMemberStates),
      civilServant: stringToBoolean(part3.civilServant),
      contractStaff: stringToBoolean(part3.contractStaff),
      mariner: stringToBoolean(part3.mariners),
      employedAndSelfEmployed: stringToBoolean(part3.employedAndSelfEmployed),
      civilAndEmployedSelfEmployed: stringToBoolean(part3.civilServantAndEmployedOrSelfEmployed),
      flightCrewMember: stringToBoolean(part3.flightCrewMember),
      exception: stringToBoolean(part3.exception),
      exceptionDescription: part3.exceptionDescription,
      workingInStateUnder21: stringToBoolean(part3.employedOrSelfEmployedInState21),
    };
    pda1.section4 = {
      employee: stringToBoolean(part4.employee),
      selfEmployedActivity: stringToBoolean(part4.selfEmployedActivity),
      employerSelfEmployedActivityCodes: [part4.employedSelfEmployedActivityCode],
      nameBusinessName: part4.nameOrBusinessName,
      registeredAddress: {
        streetNo: `${part4.registeredAddress.street} ${part4.registeredAddress.number}`,
        postCode: part4.registeredAddress.postCode,
        town: part4.registeredAddress.town,
        countryCode: part4.registeredAddress.countryCode,
      },
    };
    pda1.section5 = {
      workPlaceNames: [
        {
          companyNameVesselName: part5.pursuedBusinessName,
        },
      ],
      workPlaceAddresses: [
        {
          address: {
            streetNo: `${part5.addressInHostMemberState.street} ${part5.addressInHostMemberState.number}`,
            town: part5.addressInHostMemberState.town,
            postCode: part5.addressInHostMemberState.postCode,
            countryCode: part5.addressInHostMemberState.countryCode,
          },
        },
      ],
      noFixedAddress: stringToBoolean(part5.noFixedAddress),
    };
    pda1.section6 = {
      name: "Office National de Sécurité Sociale / Rijksdienst voor Sociale Zekerheid",
      address: {
        streetNo: "Main Street 1",
        postCode: "1000",
        town: "Brussels",
        countryCode: "BE",
      },
      institutionID: "NSSI-BE-01",
      officePhoneNo: "0800 12345",
      officeFaxNo: "0800 98765",
      email: "esspass.noreply@gmail.com",
      date: part6.date,
      signature: "Official signature",
    };

    return pda1;
  } catch (e) {
    throw new Error("Error in conversion from Request Form to Pda1 type");
  } finally {
    log.debug("businessRequestFormToPda : finished");
  }
};
