import { logger } from "@esspass-web-wallet/logging-lib";
import { RequestFormService } from "../../../application/create-request-form/request-form.service";
import { Request, Response } from "express";
import { Pda1Service } from "../../../application/pda1/pda1.service";
import { check, validationResult } from "express-validator";

const log = logger("pda-management");
const requestFormService = RequestFormService.getInstance(log);
const pda1Service = Pda1Service.getInstance(log);

async function validateParams(req: Request) {
  await check("id").isString().run(req);

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    throw new Error("Invalid email");
  }
}

export const deleteRequestFormAdapter = async (req: Request, res: Response) => {
  /**
   * validation function
   */
  await validateParams(req);

  const documentId = req.params["id"];

  try {
    const requestFormId = await requestFormService.deleteRequestForm(documentId);
    log.debug("deleteRequestFormAdapter requestFormId : " + requestFormId);
    const pda1Id = await pda1Service.deleteRecordByRequestFormId(requestFormId);
    log.debug("deleteRequestFormAdapter pda1Id : " + pda1Id);
  } catch (e) {
    res.send(e).status(418);
  }

  res.send({ status: 200 });
};
