import { logger } from "@esspass-web-wallet/logging-lib";
import { RequestFormService } from "../../../application/create-request-form/request-form.service";
import { Request, Response } from "express";

const log = logger("pda-management");
const requestFormService = RequestFormService.getInstance(log);

export const postRequestFormAdapter = async (req: Request, res: Response) => {
  const resourceId = await requestFormService.createRequestForm(req.body);
  log.debug("resourceId : " + resourceId);
  res.send({ resourceId: resourceId, status: 200 });
};
