import { Request, Response } from "express";
import { RequestFormsDbType } from "@esspass-web-wallet/domain-lib";
import { logger } from "@esspass-web-wallet/logging-lib";
import { RequestFormService } from "../../../application";

const log = logger("pda-management");
const requestFormService = RequestFormService.getInstance(log);

export const queryRequestFormAdapter = async (req: Request, res: Response) => {
  log.info(`queryRequestFormAdapter: ${req.params.id}`);
  const requestFormsDbType: RequestFormsDbType = await requestFormService.findRequestFormById(
    req.params.id as string
  );
  log.info(`queryRequestFormAdapter: ${JSON.stringify(requestFormsDbType)}`);
  res.send(requestFormsDbType);
};
