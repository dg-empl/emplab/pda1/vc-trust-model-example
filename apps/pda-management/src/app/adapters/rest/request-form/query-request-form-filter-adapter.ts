import { logger } from "@esspass-web-wallet/logging-lib";
import { RequestFormService } from "../../../application/create-request-form/request-form.service";
import { Request, Response } from "express";
import { ApiResponseSchema, PageSort, RequestFormsDbType } from "@esspass-web-wallet/domain-lib";

const log = logger("pda-management");
const requestFormService = RequestFormService.getInstance(log);

function extractFilter(req: Request) {
  if (Object.keys(req.query).length > 0 && req.query["filter"]) {
    const { filter } = req.query;
    return filter as string;
  }
  return "";
}

export const queryRequestFormFilterAdapter = async (req: Request, res: Response) => {
  const dbResponse: RequestFormsDbType[] = await requestFormService.findRequestFormByQuery(
    extractFilter(req),
    extractPageSort(req)
  );

  const collection = ApiResponseSchema.parse({
    items: dbResponse,
    size: extractFilter(req)
      ? requestFormService.responseLengthValue
      : await requestFormService.countRequestFormByQuery(),
  });

  res.send(collection);
};

function extractPageSort(req: Request) {
  let pageSort = {} as PageSort;

  if (Object.keys(req.query).length > 0) {
    const active = req.query["sort.active"];
    const direction = req.query["sort.direction"];

    pageSort = {
      pageIndex: req.query.pageIndex as unknown as number,
      pageSize: req.query.pageSize as unknown as number,
      sort: {
        active: active as string,
        direction: direction as "asc" | "desc",
      },
    };
  }
  return pageSort;
}
