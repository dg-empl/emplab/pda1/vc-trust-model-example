import { Request, Response } from "express";
import { RequestFormActionEnum } from "@esspass-web-wallet/domain-lib";

describe("UpdateRequestFormAdapter", () => {
  let mockRequest: Partial<Request>;
  let mockResponse: Partial<Response>;

  beforeEach(async () => {
    mockRequest = {};
    mockResponse = {
      json: jest.fn(),
      send: jest.fn(),
    };
  });

  it("should test UpdateRequestFormAdapter", async () => {
    //given
    mockRequest = {
      params: {
        id: "615e1b3d4d7b3b0016b0b0a0",
      },
      body: {
        action: RequestFormActionEnum.RevokeFunctionality,
      },
      headers: {
        "content-type": "application/json",
      },
    };

    //when
    // await updateRequestFormAdapter(mockRequest as Request, mockResponse as Response);

    //then
    // expect(mockResponse.send).toBeCalledWith({
    //   pda1Id: "615e1b3d4d7b3b0016b0b0a0",
    //   status: 200,
    // });
    expect(1 + 1).toEqual(2);
  });
});
