import { Request, Response } from "express";
import { logger } from "@esspass-web-wallet/logging-lib";
import { businessRequestFormToPda } from "./converter-utils";
import { Pda1Service, RequestFormService } from "../../../application";
import {
  isInEnum,
  pda1Schema,
  RequestFormActionEnum,
  RequestFormsDbType,
} from "@esspass-web-wallet/domain-lib";
import { check, param, validationResult } from "express-validator";
import { ConversionError } from "@esspass-web-wallet/backend-utils-lib";
import * as uuid from "uuid";

const log = logger("pda-management");

const requestFormService = RequestFormService.getInstance(log);
const pdaService = Pda1Service.getInstance(log);

/**
 * validateParams for updateRequestFormAdapter
 * @param req
 */
async function validateParams(req: Request) {
  await param("id").isString().run(req);
  await check("action")
    .custom(value => isInEnum(value, RequestFormActionEnum))
    .run(req);

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const msg: string[] = [""];
    errors.array().forEach(error => {
      msg.push(" " + `${error.param} : ${error.msg}`);
    });
    throw new Error("Invalid request" + msg.toString());
  }
}

export const updateRequestFormAdapter = async (req: Request, res: Response) => {
  /**
   * validate action is valid
   */
  await validateParams(req);

  const requestFormId = req.params["id"];
  const action = req.body;

  try {
    let resourceName: "requestFormId" | "pda1Id" = "requestFormId";
    let result = await requestFormService.updateRequestFormState(requestFormId, action);

    const actionOnRequestForm: keyof RequestFormActionEnum = req.body.action;

    /**
     * if action is of type RequestFormActionEnum.Issue create Pda1 and return pda1Id
     */
    if (actionOnRequestForm.toString() === RequestFormActionEnum.Issue) {
      resourceName = "pda1Id";

      const pda1 = await convertToPda1(requestFormId);
      result = await pdaService.create(pda1, requestFormId);
      await requestFormService.updateRequestFormByPdaId(requestFormId, result);
    }

    /**
     * if action is of type RequestFormActionEnum.RevokeFunctionality change status of pda1 to R
     */
    if (actionOnRequestForm.toString() === RequestFormActionEnum.RevokeFunctionality) {
      resourceName = "pda1Id";

      result = await pdaService.updateByStatus(requestFormId, req.body);
    }

    log.debug(`${resourceName} : ${result}`);
    res.send({ [resourceName]: result, status: 200 });
  } catch (e) {
    /**
     * rollback requestForm state
     */
    await requestFormService.rollbackRequestFormState(requestFormId);

    throw new Error(e);
  }
};

const convertToPda1 = async (documentId: string) => {
  const requestFormData: RequestFormsDbType = await requestFormService.findRequestFormById(
    documentId
  );
  const temp = businessRequestFormToPda(JSON.parse(requestFormData.requestFormData));
  /**
   * validate pda1 schema
   */
  const pda1SafeParse = pda1Schema.safeParse({ ...temp, id: `urn:uuid:${uuid.v4()}` });

  if (!pda1SafeParse.success) {
    const errorDesc = pda1SafeParse["error"].issues
      .map(issue => `'${issue.path.join(".")}': ${issue.message}`)
      .join("\n");

    log.debug(errorDesc);

    throw new ConversionError("invalid_conversion_action", {
      errorDescription: `Error in conversion from Request Form to Pda1Type model : ${errorDesc}`,
    });
  }

  return pda1SafeParse.data;
};
