process.env.DB_HOST = "localhost";
process.env.DB_USERNAME = "root";
process.env.DB_PASSWORD = "password";
process.env.DB_PORT = "27017";

process.env.PDA_MANAGEMENT_PRIVATE_KEY =
  "736f625c9dda78a94bb16840c82779bb7bc18014b8ede52f0f03429902fc4ba8";
process.env.PDA_MANAGEMENT_URL = "http://localhost:3100";
process.env.ISSUER_WALLET_URL = "http://localhost:20090";
process.env.IDP = "https://idp.dev.esspass-poc.eu";
process.env.IDP_REALM = "dev-esspass";
process.env.CLIENT_ID = "issuer-revocation-local";
process.env.CLIENT_SECRET = "dJC1B9LKxHEVqkuU4Nysy4nuCBzDfnpJ";
process.env.IDP_USERNAME = "issuer_revocation_local";
process.env.IDP_PASSWORD = "Hkbzs9nU^68Nr";
process.env.EBSI_PDA1_TSR_ID =
  "https://api-pilot.ebsi.eu/trusted-schemas-registry/v2/schemas/0x47c61667281cc3883cca001bcac7038237c85d0e8f61f139652375c330a99063";
process.env.REVOCATION_URL = "http://localhost:20080";
