import { Request, Response } from "express";
import { logger } from "@esspass-web-wallet/logging-lib";
import { NotFoundError, Pda1DbType } from "@esspass-web-wallet/domain-lib";
import {
  generateDocx,
  generatePaperQRCode,
  generateQrCode,
  sendEmailPDA1,
  sendEmailPin,
} from "@esspass-web-wallet/async-comm-lib";
import { check, validationResult } from "express-validator";
import { getUserPin, PreAuthCodeGrants } from "@esspass-web-wallet/crypto-lib";
import { Pda1Service, QrCodeGeneratorV2Service } from "../../../application";

const log = logger("pda-management");

const pda1Service = Pda1Service.getInstance(log);
const qrCodeService = QrCodeGeneratorV2Service.getInstance(log);

async function validateParams(req: Request) {
  await check("email").isEmail().run(req);
  await check("requestFormId").isString().run(req);

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    throw new Error("Invalid email");
  }
}

async function sendEmails(
  email: string,
  docxPath: string,
  qrCodePath: string,
  grant: PreAuthCodeGrants
) {
  try {
    await sendEmailPDA1(
      "esspass.noreply@gmail.com",
      email,
      email,
      docxPath,
      qrCodePath,
      new Date().toISOString()
    );

    const userPin = getUserPin(
      grant["urn:ietf:params:oauth:grant-type:pre-authorized_code"]["pre-authorized_code"]
    );

    await sendEmailPin(
      "esspass.noreply@gmail.com",
      email,
      email, // sent to requestorEmail or the configured email if any
      String(userPin),
      new Date().toISOString()
    );
  } catch (e) {
    throw new Error("Error in sending email : " + e);
  }
}

export const createQrCodeAdapter = async (req: Request, res: Response) => {
  let pda1: Pda1DbType,
    grant: PreAuthCodeGrants,
    qrCodePath: string,
    paperQRCodePath: string,
    docxPath: string;

  const pdaManagementUrl = process.env.PDA_MANAGEMENT_URL;

  /**
   * validation function
   */
  await validateParams(req);

  const requestFormId = req.params["requestFormId"] as string;
  const email = req.params["email"] as string;

  log.trace("createQrCodeAdapter:started");

  try {
    pda1 = await pda1Service.findPdaByRequestFormById(requestFormId);
  } catch (e) {
    throw new NotFoundError("Find Pda1 failed : " + e);
  }

  const pdaId = pda1._id.toString();

  try {
    grant = await qrCodeService.generatePreAuthGrant(pdaId, pdaManagementUrl, email);
  } catch (e) {
    throw new NotFoundError("Error in generating PreAuthGrant token");
  }

  try {
    qrCodePath = await generateQrCode(grant, pda1._id.toString());
  } catch (e) {
    throw new NotFoundError("Error in generating QRCode : " + e);
  }

  try {
    paperQRCodePath = await generatePaperQRCode(JSON.parse(pda1.pda1Data as string), email, pdaId);
    docxPath = await generateDocx(JSON.parse(pda1.pda1Data as string), paperQRCodePath, pdaId);

    log.debug("docxPath : " + docxPath);
  } catch (e) {
    throw new NotFoundError("Error in generating PaperQRCode : " + e);
  }

  /**
   * send emails with the qr code
   */
  const sendQrCodesBylEmail = process.env.PDA_SEND_V2_RESULT_BY_EMAIL;

  if (sendQrCodesBylEmail) {
    await sendEmails(email, docxPath, qrCodePath, grant);
  }

  res.send({ status: 200 });
  log.trace("createQrCodeAdapter:finished");
};
