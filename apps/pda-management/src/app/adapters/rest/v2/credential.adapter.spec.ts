import { Request, Response } from "express";
import { credentialAdapter } from "./credential.adapter";
import { logger } from "@esspass-web-wallet/logging-lib";

import { TokenGeneratorV2Service } from "../../../application/issuance-flow-v2/token-generator-v2.service";

const log = logger("credential-adapter-unit-test");

describe("CredentialAdapter", () => {
  jest.setTimeout(20_000);
  let mockRequest: Partial<Request>;
  let mockResponse: Partial<Response>;

  beforeEach(async () => {
    mockRequest = {};
    mockResponse = {
      json: jest.fn(),
      send: jest.fn(),
    };

    const tokenService = TokenGeneratorV2Service.getInstance(log);
    await tokenService.generateTokenResponse("AmUWNavOaa", "1234", "5678");
  });

  afterAll(async () => {
    //mongoose disconnect
    // await disconnect();
  });

  // it("should test credential.adapter", async () => {
  //   //given
  //   mockRequest = {
  //     body: {
  //       format: "jwt_vc_json",
  //       types: ["VerifiableCredential"],
  //       proof: {
  //         proof_type: "jwt",
  //         jwt: "eyJhbGciOiJFUzI1NiIsImtpZCI6ImRpZDplYnNpOnplc05BeXZjZldzbjh1RWJVdWN1Q1Rmb2gzU0FNM3hSUDVjMXR4TWdNcVJldCJ9.eyJpc3MiOiJob2xkZXItd2ViLXdhbGxldCIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzEwMCIsIm5vbmNlIjoiQW1VV05hdk9hYSIsInBkYUlkIjoiNjUzMTEzMGRkNDYyZjhhOGFlNTIwNTQwIiwiaWF0IjoxNjk4MDY0NDQ1NjYyfQ.qdkF6w6Ovn_yuLkQd_60euDc5eaXGIgVeCTZYa_exfiWTof3XcvU6K8vk30cvdBT1kjJPAuka8JgQ8YeS-C4vQ",
  //       },
  //     },
  //     headers: {
  //       "content-type": "application/json",
  //     },
  //   };
  //
  //   CredentialGeneratorV2Service.getInstance(log);
  //
  //   //mongoose
  //   connect("esspass-nssi-be", log);
  //
  //   //when
  //   await credentialAdapter(mockRequest as Request, mockResponse as Response);
  //
  //   //then
  //   expect(mockResponse.send).toBeCalledWith({
  //     vc: expect.stringContaining("eyJh"),
  //     did: expect.stringContaining("did:ebsi:"),
  //     id: expect.anything(),
  //   });
  // });

  it("should throw error with wrong content-type in request header", async () => {
    mockRequest = {
      body: {
        format: "jwt_vc_json",
        types: ["VerifiableCredential"],
        proof: {
          proof_type: "jwt",
          jwt: `proofJwt`,
        },
      },
      headers: {
        "content-type": "application/x-www-form-urlencoded",
      },
    };

    try {
      //when
      await credentialAdapter(mockRequest as Request, mockResponse as Response);
    } catch (e) {
      //then
      expect(e.message).toEqual("Invalid request, content-type : Invalid value");
    }
  });

  it("should throw error with missing format in request", async () => {
    mockRequest = {
      body: {
        // format: "jwt_vc_json",
        types: ["VerifiableCredential"],
        proof: {
          proof_type: "jwt",
          jwt: `proofJwt`,
        },
      },
      headers: {
        "content-type": "application/json",
      },
    };

    try {
      //when
      await credentialAdapter(mockRequest as Request, mockResponse as Response);
    } catch (e) {
      //then
      expect(e.message).toEqual("Invalid request, format : Invalid value");
    }
  });

  it("should throw error with wrong nonce string", async () => {
    const tokenService = TokenGeneratorV2Service.getInstance(log);
    await tokenService.generateTokenResponse("AmUWNavOaa1", "1234", "5678");

    mockRequest = {
      body: {
        format: "jwt_vc_json",
        types: ["VerifiableCredential"],
        proof: {
          proof_type: "jwt",
          jwt: "eyJhbGciOiJFUzI1NiIsImtpZCI6ImRpZDplYnNpOnplc05BeXZjZldzbjh1RWJVdWN1Q1Rmb2gzU0FNM3hSUDVjMXR4TWdNcVJldCJ9.eyJpc3MiOiJob2xkZXItd2ViLXdhbGxldCIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzEwMCIsIm5vbmNlIjoiQW1VV05hdk9hYSIsInBkYUlkIjoiNjUzMTEzMGRkNDYyZjhhOGFlNTIwNTQwIiwiaWF0IjoxNjk4MDY0NDQ1NjYyfQ.qdkF6w6Ovn_yuLkQd_60euDc5eaXGIgVeCTZYa_exfiWTof3XcvU6K8vk30cvdBT1kjJPAuka8JgQ8YeS-C4vQ",
        },
      },
      headers: {
        "content-type": "application/json",
      },
    };

    try {
      //when
      await credentialAdapter(mockRequest as Request, mockResponse as Response);
    } catch (e) {
      //then
      expect(e.message).toEqual("Invalid Token Request payload. Nonce mismatch.");
    }
  });
});
