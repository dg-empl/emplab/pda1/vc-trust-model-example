import { Request, Response } from "express";
import { logger } from "@esspass-web-wallet/logging-lib";
import { check, header, validationResult } from "express-validator";
import { decodeJwt, decodeProtectedHeader, JWTPayload, ProtectedHeaderParameters } from "jose";
import { CredentialGeneratorV2Service } from "../../../application/issuance-flow-v2/credential-generator-v2.service";
import { checkSchema } from "express-validator/src/middlewares/schema";
import { Pda1Service } from "../../../application";

const log = logger("pda-management");
const credentialService = CredentialGeneratorV2Service.getInstance(log);
const pdaService = Pda1Service.getInstance(log);

/**
 * validateParams for credentialAdapter
 * @param req
 */
async function validateParams(req: Request) {
  await check("format").equals("jwt_vc_json").run(req);
  await check("types").isArray().contains("VerifiableCredential").run(req);
  await check("proof").isObject().run(req);
  await checkSchema({ proof_types: { in: "body" } }).run(req);
  await checkSchema({ jwt: { in: "body" } }).run(req);
  await header("Content-Type").equals("application/json").run(req);

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const msg: string[] = [""];
    errors.array().forEach(error => {
      msg.push(" " + `${error.param} : ${error.msg}`);
    });
    throw new Error("Invalid request" + msg.toString());
  }
}

export const credentialAdapter = async (req: Request, res: Response): Promise<Response> => {
  log.trace("credentialAdapter:started");
  /**
   * validation req params
   */
  await validateParams(req);

  /**
   * validation proof jwt
   */
  const proofJwt = req.body.proof.jwt;
  await credentialService.validateCredentialRequest(proofJwt as string);

  const jwtHeader: ProtectedHeaderParameters = decodeProtectedHeader(proofJwt as string);
  const jwt: JWTPayload = decodeJwt(proofJwt as string);

  // build vc pda1
  const { vcPda1Json, requestFormId, pdaId, did } =
    await credentialService.createVerifiableCredential(jwt.pdaId as string, jwtHeader.kid);

  // save vc pda1 in mongoDB
  await pdaService.createVerifiableCredential(vcPda1Json, requestFormId, pdaId);

  // sign vc pda1
  const signedVC = await credentialService.signVC(vcPda1Json);

  log.trace("credentialAdapter:finished");
  return res.send({ ...signedVC, id: pdaId, did: did });
};
