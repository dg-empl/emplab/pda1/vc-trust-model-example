import { Request, Response } from "express";
import { logger } from "@esspass-web-wallet/logging-lib";
import { QrCodeGeneratorV2Service } from "../../../application";

const log = logger("pda-management");
const qrCodeService = QrCodeGeneratorV2Service.getInstance(log);

export const jwksAdapter = async (req: Request, res: Response) => {
  log.debug("jwksAdapter entry");
  const jwks = await qrCodeService.getJwks();
  res.setHeader("Content-type", "application/jwk-set+json");
  log.debug("jwksAdapter exit :" + JSON.stringify(jwks));
  return res.send(jwks);
};
