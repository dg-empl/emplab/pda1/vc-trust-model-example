import { tokenAdapter } from "./tokenAdapter";
import { Request, Response } from "express";
import { QrCodeGeneratorV2Service } from "../../../application";
import { logger } from "@esspass-web-wallet/logging-lib";
import { getUserPin } from "@esspass-web-wallet/crypto-lib";

describe("TokenAdapter", () => {
  let mockRequest: Partial<Request>;
  let mockResponse: Partial<Response>;
  let code: string;
  let userPin: string;

  beforeEach(async () => {
    const pdaId = "652915d2537303ca788e40ba";
    const issuerUri = "http://localhost:3100";
    const email = "email@test.zc";

    const qrCodeService = QrCodeGeneratorV2Service.getInstance(logger("token-adapter-unit-test"));

    code = (await qrCodeService.generatePreAuthGrant(pdaId, issuerUri, email, "1s"))[
      "urn:ietf:params:oauth:grant-type:pre-authorized_code"
    ]["pre-authorized_code"];

    userPin = getUserPin(code);

    mockRequest = {};
    mockResponse = {
      json: jest.fn(),
      send: jest.fn(),
    };
  });

  it("should test tokenAdapter", async () => {
    mockRequest = {
      query: {
        grant_type: "urn:ietf:params:oauth:grant-type:pre-authorized_code",
        "pre-authorized_code": code,
        user_pin: userPin,
      },
      headers: {
        "content-type": "application/x-www-form-urlencoded",
      },
    };

    //when
    await tokenAdapter(mockRequest as Request, mockResponse as Response);

    //then
    expect(mockResponse.send).toBeCalledWith({
      access_token: expect.any(String),
      c_nonce: expect.any(String),
      c_nonce_expires_in: 86400,
      expires_in: 86400,
      token_type: "bearer",
    });
  });

  it("should throw error with missing grant_type in request", async () => {
    mockRequest = {
      query: {
        "pre-authorized_code": code,
        user_pin: "436523",
      },
      headers: {
        "content-type": "application/x-www-form-urlencoded",
      },
    };

    try {
      //when
      await tokenAdapter(mockRequest as Request, mockResponse as Response);
    } catch (e) {
      //then
      expect(e.message).toEqual("Invalid request, grant_type : Invalid value");
    }
  });

  it("should throw error with wrong user_pin format in request", async () => {
    mockRequest = {
      query: {
        grant_type: "urn:ietf:params:oauth:grant-type:pre-authorized_code",
        "pre-authorized_code": code,
        user_pin: "436523s",
      },
      headers: {
        "content-type": "application/x-www-form-urlencoded",
      },
    };

    try {
      //when
      await tokenAdapter(mockRequest as Request, mockResponse as Response);
    } catch (e) {
      //then
      expect(e.message).toEqual("Invalid request, user_pin : Invalid value");
    }
  });

  it("should throw error with wrong user_pin given in request", async () => {
    mockRequest = {
      query: {
        grant_type: "urn:ietf:params:oauth:grant-type:pre-authorized_code",
        "pre-authorized_code": code,
        user_pin: "422222",
      },
      headers: {
        "content-type": "application/x-www-form-urlencoded",
      },
    };

    try {
      //when
      await tokenAdapter(mockRequest as Request, mockResponse as Response);
    } catch (e) {
      //then
      expect(e.message).toEqual("Invalid PreAuthCode Token: 422222 is invalid");
    }
  });
});
