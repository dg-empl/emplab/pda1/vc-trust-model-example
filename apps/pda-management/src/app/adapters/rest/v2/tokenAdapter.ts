import { Request, Response } from "express";
import { logger } from "@esspass-web-wallet/logging-lib";
import { header, query, validationResult } from "express-validator";
import { TokenGeneratorV2Service } from "../../../application/issuance-flow-v2/token-generator-v2.service";
import { generateRandomAlphaString } from "../../../application/issuance-flow-v2/commons";
import {
  PreAuthCodeGrantsPayload,
  preAuthCodeGrantsPayloadSchema,
} from "@esspass-web-wallet/crypto-lib";
import { decodeJwt } from "jose";
import { AccessTokenType } from "@esspass-web-wallet/domain-lib";

const log = logger("pda-management");
const tokenService = TokenGeneratorV2Service.getInstance(log);

/**
 * validateParams for tokenAdapter
 * @param req
 */
async function validateParams(req: Request) {
  await query("grant_type").isString().run(req);
  await query("pre-authorized_code").isString().run(req);
  await query("user_pin").isString().run(req);
  await query("user_pin")
    .matches(/^[0-9]+$/)
    .run(req);
  await header("Content-Type").equals("application/x-www-form-urlencoded").run(req);

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const msg: string[] = [""];
    errors.array().forEach(error => {
      msg.push(" " + `${error.param} : ${error.msg}`);
    });
    throw new Error("Invalid request" + msg.toString());
  }
}

export const tokenAdapter = async (req: Request, res: Response) => {
  log.trace("tokenAdapter:started");
  /**
   * validation req params
   */
  await validateParams(req);

  /**
   * validation pre auth token
   */
  const pre_authorized_code = req.query["pre-authorized_code"];
  const user_pin = req.query["user_pin"];
  await tokenService.validateTokenRequest(pre_authorized_code as string, user_pin as string);

  const preAuthTokenPayload: PreAuthCodeGrantsPayload = preAuthCodeGrantsPayloadSchema.parse(
    decodeJwt(pre_authorized_code as string)
  );

  /**
   * generate access_token
   */
  const accessToken: AccessTokenType = await tokenService.generateTokenResponse(
    generateRandomAlphaString(10),
    preAuthTokenPayload.aud,
    preAuthTokenPayload.sub
  );

  log.trace("tokenAdapter:finished");
  return res.send(accessToken);
};
