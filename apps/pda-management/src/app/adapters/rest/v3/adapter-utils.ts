import { getUserPin, PreAuthCodeGrants } from "@esspass-web-wallet/crypto-lib";
import {
  generateQrCodeToFile,
  sendEmailPDA1,
  sendEmailPin,
} from "@esspass-web-wallet/async-comm-lib";
import path from "path";
import { NotFoundError } from "@esspass-web-wallet/domain-lib";

export async function sendEmails(email: string, docxPath: string, qrCodePath: string, pin: string) {
  try {
    await sendEmailPDA1(
      "esspass.noreply@gmail.com",
      email,
      email,
      docxPath,
      qrCodePath,
      new Date().toISOString()
    );

    await sendEmailPin(
      "esspass.noreply@gmail.com",
      email,
      email, // sent to requestorEmail or the configured email if any
      String(pin),
      new Date().toISOString()
    );
  } catch (e) {
    throw new NotFoundError("Error in sending email : " + e);
  }
}

export async function generateV3QrCodeToFile(
  version = "v3",
  pda1Id: string,
  encodedTxt: string,
  color = "#00f"
): Promise<string> {
  const outputType = "png";

  const qrCodePath = path.resolve(
    __dirname,
    `./generated/${version}/${pda1Id}/qrcode_paper_${pda1Id}.png`
  );

  try {
    await generateQrCodeToFile({ qrCodePath, encodedTxt, outputType }, color);
  } catch (e) {
    throw new NotFoundError("Error in generating QR code for V3: " + e);
  }

  return qrCodePath;
}
