import { Request, Response } from "express";
import { logger } from "@esspass-web-wallet/logging-lib";
import { header, validationResult } from "express-validator";
import { body } from "express-validator/src/middlewares/validation-chain-builders";
import { Pda1Service } from "../../../application";
import { NotFoundError, Pda1DbType } from "@esspass-web-wallet/domain-lib";
import { generateV3QrCodeToFile, sendEmails } from "./adapter-utils";
import { generateDocx, generatePaperQRCode } from "@esspass-web-wallet/async-comm-lib";

const log = logger("pda-management");
const pdaService = Pda1Service.getInstance(log);

/**
 * validate req params - pin, qrCode, pdaId
 * pin - to be sent in the 1st email
 * qrCode - to be sent in the 2nd email
 * pdaId - to be used to produce the qrCode email content
 */
async function validateParams(req: Request) {
  const bearerValidator = (str: string) => str.startsWith("Bearer ") && str.length > 7;

  await body("pin").isString().run(req);
  await body("qrCode").isString().run(req);
  await body("pdaId").isString().run(req);
  await body("email").isString().run(req);
  await header("authorization").custom(bearerValidator).run(req);

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const msg: string[] = [""];
    errors.array().forEach(error => {
      msg.push(" " + `${error.param} : ${error.msg}`);
    });
    throw new Error(`Invalid / lack of : ${msg.toString()}`);
  }
}

export const sendQrCodeByEmail = async (req: Request, res: Response) => {
  log.trace("sendQrCodeByEmail:started");
  log.debugJSON("req.body : ", req.body);

  /**
   * validation req params
   */
  await validateParams(req);

  const { pin, qrCode, pdaId, email } = req.body;

  /**
   * send emails with the qr code
   */
  const sendQrCodesBylEmail = process.env.PDA_SEND_V2_RESULT_BY_EMAIL;

  if (sendQrCodesBylEmail) {
    let docxPath: string;
    let qrCodePath: string;

    /**
     * get pda1 content
     */
    const pda1: Pda1DbType = await pdaService.findPdaByRequestFormById(pdaId);

    try {
      /**
       * generate qrCode for the paper version of the pda1 data and save it to the file system
       */
      const paperQRCodePath = await generatePaperQRCode(
        JSON.parse(pda1.pda1Data as string),
        email,
        pdaId,
        "#000",
        "v3"
      );

      /**
       * generate docx attachment for the pda1 data and save it to the file system
       */
      docxPath = await generateDocx(
        JSON.parse(pda1.pda1Data as string),
        paperQRCodePath,
        pdaId,
        "v3"
      );

      /**
       * generate qrCode for the pda1 data and save it to the file system
       */
      qrCodePath = await generateV3QrCodeToFile("v3", pdaId, qrCode);
    } catch (e) {
      throw new NotFoundError("Error in generating PaperQRCode : " + e);
    }

    await sendEmails(email, docxPath, qrCodePath, pin);
  }

  log.trace("sendQrCodeByEmail:finished");
  return res.send({ message: "Emails sent successfully" });
};
