import { logger, Logger } from "@esspass-web-wallet/logging-lib";
import { requestFormSchemaModel } from "../../domain/request-form-schema";
import { manageRequestForm } from "../utils";
import {
  PageSort,
  RequestFormActionEnum,
  requestFormActionEnumAdapter,
  RequestFormsDbType,
} from "@esspass-web-wallet/domain-lib";

const log = logger("pda-management");

interface RequestForm {
  findRequestFormByQuery(filter: string, query: PageSort): Promise<any>;
  createRequestForm(req: Request, res: Response): Promise<string>;
}

export class RequestFormService implements RequestForm {
  private static requestFormService = new RequestFormService();
  private static log: Logger;
  private responseLength: number;

  private constructor() {
    RequestFormService.log
      ? RequestFormService.log.debug("Creating new instance of RequestFormService")
      : null;
  }

  get responseLengthValue() {
    return this.responseLength;
  }

  public static getInstance(log: Logger): RequestFormService {
    this.log = log;
    if (!this.requestFormService) {
      this.requestFormService = new RequestFormService();
    } else {
      return this.requestFormService;
    }
  }

  async createRequestForm(requestForm: any): Promise<string> {
    const result = await manageRequestForm(requestForm);
    return result._id.toString();
  }

  async deleteRequestForm(requestFormId: any): Promise<string> {
    try {
      const result = await requestFormSchemaModel.findByIdAndDelete(requestFormId);
      return result._id.toString();
    } catch (error) {
      throw new Error(error);
    }
  }

  async updateRequestFormState(
    requestFormId: string,
    action: {
      action: keyof RequestFormActionEnum;
    }
  ): Promise<string> {
    try {
      requestFormSchemaModel.findById(requestFormId, (err, requestForm) => {
        if (err) {
          throw new Error("Invalid request");
        }

        requestForm.previousFormStatus = requestForm.requestFormStatus;
        requestForm.requestFormStatus = requestFormActionEnumAdapter[action.action];

        // Save the requestForm object and handle the error.
        requestForm.save((err: any) => {
          if (err) {
            throw new Error("Invalid request");
          } else {
            return requestFormId;
          }
        });
      });

      return requestFormId;
    } catch (error) {
      throw new Error(error);
    }
  }

  async updateRequestFormByPdaId(requestFormId: string, pda1Id: string): Promise<string> {
    if (!requestFormId || !pda1Id) {
      throw new Error("Invalid request");
    }

    try {
      requestFormSchemaModel.findById(requestFormId, (err, requestForm) => {
        if (err) {
          throw new Error("Invalid request");
        }

        requestForm.pda1Id = pda1Id;

        requestForm.save(err => {
          if (err) {
            throw new Error("Invalid request");
          } else {
            return requestFormId;
          }
        });
      });

      return requestFormId;
    } catch (error) {
      throw new Error(error);
    }
  }

  async rollbackRequestFormState(requestFormId: string): Promise<void> {
    try {
      requestFormSchemaModel.findById(requestFormId, (err, requestForm) => {
        if (err) {
          throw new Error("Invalid request");
        }
        requestForm.requestFormStatus = requestForm.previousFormStatus;

        requestForm.save(err => {
          if (err) {
            throw new Error("RollbackRequestFormState request failed");
          } else {
            return requestFormId;
          }
        });
      });
    } catch (error) {
      throw new Error(error);
    }
  }

  async findRequestFormByQuery(filter: string, pageSort: PageSort): Promise<RequestFormsDbType[]> {
    let dbResponse: any;

    const skip = pageSort.pageIndex * pageSort.pageSize;

    if (this.hasFilter(filter)) {
      log.debugJSON("filter: ", filter);
      const skip = pageSort.pageIndex * pageSort.pageSize;
      const limit = pageSort.pageSize;

      this.responseLength = await this.getQuery(filter).countDocuments();

      const query2 = this.getQuery(filter)
        .skip(skip)
        .limit(limit)
        .sort({ [pageSort.sort.active]: pageSort.sort.direction == "asc" ? 1 : -1 });

      dbResponse = await query2.exec();
    } else if (this.hasQuery(pageSort)) {
      /**
       * get the request form data from the database with the query
       */
      log.debugJSON("pageSort :", pageSort);

      const skip = pageSort.pageIndex * pageSort.pageSize;
      const limit = pageSort.pageSize;

      dbResponse = await requestFormSchemaModel
        .find()
        .skip(skip)
        .limit(limit)
        .sort({ [pageSort.sort.active]: pageSort.sort.direction == "asc" ? 1 : -1 });
    } else {
      dbResponse = await requestFormSchemaModel.find();
    }

    return (dbResponse as any[]).map((element, index) => {
      const {
        requestFormData,
        requestorEmail,
        requestorFirstName,
        requestorLastName,
        dateOfRegistration,
        validationResult,
        requestFormStatus,
      } = element;
      return {
        index: ++index + skip,
        id: element._id.toString(),
        requestFormData,
        requestorEmail,
        requestorFirstName,
        requestorLastName,
        dateOfRegistration,
        validationResult,
        requestFormStatus,
      };
    });
  }

  async findRequestFormById(requestFormId: string): Promise<RequestFormsDbType> {
    return requestFormSchemaModel.findById(requestFormId);
  }

  async countRequestFormByQuery() {
    return requestFormSchemaModel.countDocuments();
  }

  private getQuery(filter: string) {
    return requestFormSchemaModel.find({
      $or: [
        { requestorEmail: { $regex: new RegExp(`^${filter}`), $options: "i" } },
        { requestorFirstName: { $regex: new RegExp(`^${filter}`), $options: "i" } },
        { requestorLastName: { $regex: new RegExp(`^${filter}`), $options: "i" } },
      ],
    });
  }

  private hasQuery(query: any) {
    return Object.keys(query).length > 0;
  }

  private hasFilter(filter: string) {
    return filter;
  }
}
