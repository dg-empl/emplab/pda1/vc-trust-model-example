import { getKeyPair, issuerKeyPair, KeyPair } from "@esspass-web-wallet/crypto-lib";
import { randomBytes } from "node:crypto";

/**
 * Load Issuer key pair from environment.
 *
 * @returns The private and public key JWKs (including "kid")
 */
export async function getIssuerKeyPair(alg: "ES256" | "ES256K"): Promise<KeyPair> {
  let keyPair = issuerKeyPair[alg];
  if (keyPair === undefined) {
    keyPair = await getKeyPair(process.env.PDA_MANAGEMENT_PRIVATE_KEY, alg);
    issuerKeyPair[alg] = keyPair;
  }
  return keyPair;
}

/**
 * Generate random alpha string with given length
 * @param length
 */
export function generateRandomAlphaString(length: number): string {
  const alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  const randomString = randomBytes(length).toString("hex").slice(0, length);
  return randomString.replace(
    /[^a-zA-Z]/g,
    char => alphabet[Math.floor(Math.random() * alphabet.length)]
  );
}
