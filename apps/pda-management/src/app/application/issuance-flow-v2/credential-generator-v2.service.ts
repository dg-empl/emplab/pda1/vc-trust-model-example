import { Logger } from "@esspass-web-wallet/logging-lib";
import { CredentialError, validateCredentialRequest } from "@esspass-web-wallet/crypto-lib";
import { TokenGeneratorV2Service } from "./token-generator-v2.service";
import {
  CredentialStatus,
  credentialStatusSchema,
  Pda1DbType,
  pda1Schema,
  Pda1Type,
  VerifiableCredential,
} from "@esspass-web-wallet/domain-lib";
import * as uuid from "uuid";
import { pda1SchemaDbModel } from "../../domain/pda1-schema";
import { ObjectId } from "mongodb";
import { IdpTokenService } from "@esspass-web-wallet/security-lib";

export class CredentialGeneratorV2Service {
  private static log: Logger;
  private static credentialGeneratorV2Service: CredentialGeneratorV2Service;
  private static tokenGeneratorV2Service: TokenGeneratorV2Service;
  private static idpTokenService: IdpTokenService;

  private constructor() {
    CredentialGeneratorV2Service.log
      ? CredentialGeneratorV2Service.log.debug(
          "Creating new instance of CredentialGeneratorV2Service"
        )
      : null;
  }

  public static getInstance(log: Logger): CredentialGeneratorV2Service {
    this.log = log;
    this.tokenGeneratorV2Service = TokenGeneratorV2Service.getInstance(log);
    this.idpTokenService = IdpTokenService.getInstance(log);

    if (!this.credentialGeneratorV2Service) {
      this.credentialGeneratorV2Service = new CredentialGeneratorV2Service();
    }
    return this.credentialGeneratorV2Service;
  }

  public async validateCredentialRequest(proofJwt: string) {
    const nonceValue = CredentialGeneratorV2Service.tokenGeneratorV2Service.nonceValue;

    await validateCredentialRequest(proofJwt, nonceValue);
  }

  public async createVerifiableCredential(
    pdaId: string,
    holderDid: string
  ): Promise<Record<string, any>> {
    CredentialGeneratorV2Service.log.debug("createVerifiableCredential:started");

    /**
     * find the pda1 record
     */
    const pda1: Pda1DbType = await pda1SchemaDbModel.findById(pdaId);

    /**
     * find and update the pda1 record with the holderDid ( clientDid )
     */
    const { pda1Data } = await this.updateCredentialSubjectIdWithHolderDid(pda1, holderDid);

    /**
     * Validate updated pda1 record
     */
    const parsedSafePda1Content = pda1Schema.safeParse(JSON.parse(pda1Data as string));

    if (!parsedSafePda1Content.success) {
      const errorDesc = parsedSafePda1Content["error"].issues
        .map(issue => `'${issue.path.join(".")}': ${issue.message}`)
        .join("\n");
      throw new CredentialError("invalid_or_missing_proof", {
        errorDescription: `Invalid Pda1 type: ${errorDesc}`,
      });
    }

    if (parsedSafePda1Content.data.id.startsWith("urn:uuid:")) {
      throw new CredentialError("invalid_or_missing_proof", {
        errorDescription: `Invalid Pda1 type: id cannot start with urn:uuid:`,
      });
    }

    /**
     * Generate VC
     */
    const vcPda1Json = await this.generateVcPda1Json(parsedSafePda1Content.data, holderDid);

    if (!vcPda1Json) {
      throw new CredentialError("invalid_or_missing_proof", {
        errorDescription: `pda-management : createVerifiableCredential : unable to generate VC from Pda1`,
      });
    }

    CredentialGeneratorV2Service.log.debug(JSON.stringify(vcPda1Json));

    CredentialGeneratorV2Service.log.debug("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
    CredentialGeneratorV2Service.log.debug("vcPda1Json issuer: " + vcPda1Json.issuer);
    CredentialGeneratorV2Service.log.debug("+++++++++++++++++++++++++++++++++++++++++++++++++++++");

    CredentialGeneratorV2Service.log.debug("vcPda1Json : " + JSON.stringify(vcPda1Json));
    CredentialGeneratorV2Service.log.debug("createVerifiableCredential:finished");

    return { vcPda1Json, requestFormId: pda1.requestFormId, pdaId, did: holderDid };
  }

  public async signVC(vcPda1Json: VerifiableCredential) {
    CredentialGeneratorV2Service.log.debug("signVC:started");
    const bearerToken = await CredentialGeneratorV2Service.idpTokenService.bearerToken();
    const endPointUrl = `${process.env.ISSUER_WALLET_URL}/create-vc-from-cs`;
    const response = await fetch(endPointUrl, {
      method: "POST",
      body: JSON.stringify(vcPda1Json),
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + bearerToken,
      },
    });
    const result = await response.json();

    if (result && result.errors) {
      throw new CredentialError("invalid_or_missing_proof", {
        errorDescription: `issuer-wallet : signVC: ${result.errors[0].message}`,
      });
    }

    CredentialGeneratorV2Service.log.debug("signVC:response-status : " + response.status);
    CredentialGeneratorV2Service.log.debug("signVC:response : " + JSON.stringify(result));

    if (response.status !== 200) {
      throw new Error(
        `Unexpected response status : credential-generator-v2.service , signVC() response : ${response.status}`
      );
    }

    if (!result) {
      throw new Error(`Unable to sign VC`);
    }

    CredentialGeneratorV2Service.log.debug("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
    CredentialGeneratorV2Service.log.debug("result : " + JSON.stringify(result));
    CredentialGeneratorV2Service.log.debug("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
    CredentialGeneratorV2Service.log.debug("signVC:finished");
    return result;
  }

  private async generateVcPda1Json(pda1Content: Pda1Type, holderDid: string) {
    const vcPda1Json: VerifiableCredential = {};
    vcPda1Json["@context"] = ["https://www.w3.org/2018/credentials/v1"];
    vcPda1Json.id = `urn:uuid:${uuid.v4()}`;
    vcPda1Json.type = [
      "VerifiableCredential",
      "VerifiableAttestation",
      "VerifiablePortableDocumentA1",
    ];

    vcPda1Json.issuer = await this.issuerDid(
      await CredentialGeneratorV2Service.idpTokenService.bearerToken()
    );

    const d = new Date().toLocaleString("fr-BE");
    vcPda1Json.issuanceDate = `${d.slice(6, 10)}-${d.slice(3, 5)}-${d.slice(0, 2)}T${d.slice(-8)}Z`;
    vcPda1Json.validFrom = vcPda1Json.issuanceDate;
    vcPda1Json.issued = vcPda1Json.issuanceDate;

    let credentialStatusParsed: CredentialStatus, credentialStatus: CredentialStatus;
    try {
      credentialStatus = await this.createCredentialStatus();
      credentialStatusParsed = credentialStatusSchema.parse(credentialStatus);
    } catch (e) {
      CredentialGeneratorV2Service.log.debug(e);
      throw new CredentialError("invalid_or_missing_proof", {
        errorDescription: `Invalid credential status`,
      });
    }

    CredentialGeneratorV2Service.log.debug(
      "credentialStatus : " + JSON.stringify(credentialStatus)
    );

    vcPda1Json.credentialStatus = credentialStatusParsed;

    vcPda1Json.credentialSubject = pda1Content;

    CredentialGeneratorV2Service.log.debug(
      "vcPda1Json.credentialSubject : " + JSON.stringify(vcPda1Json.credentialSubject)
    );

    vcPda1Json.id = holderDid;
    vcPda1Json.credentialSchema = {};

    CredentialGeneratorV2Service.log.debug(process.env.EBSI_PDA1_TSR_ID);

    vcPda1Json.credentialSchema.id = process.env.EBSI_PDA1_TSR_ID;
    vcPda1Json.credentialSchema.type = "FullJsonSchemaValidator2021";
    vcPda1Json.expirationDate = "2027-01-01T00:00:00Z";

    return vcPda1Json;
  }

  private async createCredentialStatus() {
    CredentialGeneratorV2Service.log.debug("createCredentialStatus:starting");
    const hostname = process.env.REVOCATION_URL + "/create-credential-status";

    const requestObj = {
      method: "POST",
      headers: {
        accept: "application/json",
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(hostname, requestObj);
    const data = await response.json();

    CredentialGeneratorV2Service.log.debug("createCredentialStatus : " + JSON.stringify(data));
    CredentialGeneratorV2Service.log.trace("createCredentialStatus:finished");

    return data;
  }

  private async issuerDid(bearerToken: string) {
    const endPointUrl = `${process.env.ISSUER_WALLET_URL}/issuer-did`;
    const response = await fetch(endPointUrl, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + bearerToken,
      },
    });

    if (response.status !== 200) {
      CredentialGeneratorV2Service.log.error("issuerDid : " + JSON.stringify(response));
      throw new CredentialError("invalid_token", {
        errorDescription: `Unexpected issuer-wallet response /issuer-did: ${response.status}`,
        statusCode: response.status,
      });
    }

    const resp = await response.json();

    CredentialGeneratorV2Service.log.debug("issuerDid : " + JSON.stringify(resp.did));

    return resp.did;
  }

  /**
   * Update the pda1 record with the holderDid ( clientDid )
   * @param pda
   * @param holderDid
   * @private
   */
  private async updateCredentialSubjectIdWithHolderDid(
    pda: Pda1DbType,
    holderDid: string
  ): Promise<any> {
    const filter = {
      _id: new ObjectId(pda._id),
    };

    const update = {
      $set: {
        pda1Data: JSON.stringify({
          ...pda1Schema.parse(JSON.parse(pda.pda1Data as string)),
          id: holderDid,
        }),
      },
    };

    const result = await pda1SchemaDbModel.findOneAndUpdate(filter, update, { new: true });
    await pda1SchemaDbModel.ensureIndexes();
    return result;
  }
}
