import { JWK, calculateJwkThumbprint, decodeJwt, decodeProtectedHeader } from "jose";
import {
  getUserPin,
  PreAuthCodeGrants,
  preAuthCodeGrantsPayloadSchema,
  validatePreAuthCode,
} from "@esspass-web-wallet/crypto-lib";

import { QrCodeGeneratorV2Service } from "./qr-code-generator-v2.service";
import { logger } from "@esspass-web-wallet/logging-lib";
import { getIssuerKeyPair } from "./commons";

describe("QrCodeGeneratorV2Service", () => {
  let grants: PreAuthCodeGrants;
  let qrCodeService: QrCodeGeneratorV2Service;

  beforeAll(async () => {
    qrCodeService = QrCodeGeneratorV2Service.getInstance(logger("qr-code-generator-v2"));
  });

  it("should encode pre-auth-code in form of jwt", async () => {
    //given
    const pdaId = "652915d2537303ca788e40ba";
    const issuerUri = "http://localhost:3100";
    const email = "dharsinisuriya@gmail.com";

    //when
    grants = await qrCodeService.generatePreAuthGrant(pdaId, issuerUri, email, "1s");

    //then
    expect(grants).toBeDefined();
  });

  it("should generate 4-digit code from pre-auth-code", async () => {
    //given
    const pdaId = "65212331235d2537303ca788e40ba";
    const issuerUri = "http://localhost:3100";
    const email = "dharsiaasd123asnisuriya@gmdsda.com";

    //when
    grants = await qrCodeService.generatePreAuthGrant(pdaId, issuerUri, email, "1s");

    expect(
      getUserPin(
        grants["urn:ietf:params:oauth:grant-type:pre-authorized_code"]["pre-authorized_code"]
      ).split("").length
    ).toEqual(4);
  });

  it("should decode grant containing pre-authorized_code with the public key from /jwks end=point", async () => {
    //given
    const issuerUrl = "http://localhost:3100";
    const jwtHeaders = decodeProtectedHeader(
      grants["urn:ietf:params:oauth:grant-type:pre-authorized_code"]["pre-authorized_code"]
    );

    const keys = await qrCodeService.getJwks();
    const key: JsonWebKey = keys.keys.find((key: { kid: any }) => key.kid === jwtHeaders.kid);

    const thumbprint = await calculateJwkThumbprint(key as JWK);

    const publicKeyJwk = {
      ...key,
      kid: thumbprint,
    };

    //when then
    await validatePreAuthCode(grants, issuerUrl, publicKeyJwk);
  });

  it("should throw exception when jwt expired", async () => {
    // Wait 2 seconds (> 2 seconds)
    // when
    await new Promise(r => {
      setTimeout(r, 2000);
    });

    const issuerUrl = "http://localhost:3100";
    const keyPair = await getIssuerKeyPair("ES256");

    try {
      //given
      await validatePreAuthCode(grants, issuerUrl, keyPair.publicKeyJwk);
    } catch (e) {
      //then
      expect(e.message).toEqual("The PreAuthCode token is expired");
    }
  });

  it("should generate 4-Digit pin code from pre auth code jwt", async () => {
    //given
    const EXPECTED_PIN_CODE = "6523";

    const { sub, authorization_details, iss, aud } = preAuthCodeGrantsPayloadSchema.parse(
      decodeJwt(
        grants["urn:ietf:params:oauth:grant-type:pre-authorized_code"]["pre-authorized_code"]
      )
    );

    const id = { sub, authorization_details, iss, aud };

    //when
    const pinCode = getUserPin(`${id}`.toString());

    //then
    expect(pinCode).toEqual(EXPECTED_PIN_CODE);
  });

  it("should generate 4-Digit pin code from with leading zero pre auth code jwt", async () => {
    const preAuthCode =
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiIsImtpZCI6IkROeVh6Tmx0V1FMSjhnNVM3UTBTUWE1enNrWk92RlhUdkVvMENxUFZKVXcifQ.eyJhdXRob3JpemF0aW9uX2RldGFpbHMiOlt7InR5cGVzIjpbIlZlcmlmaWFibGVQb3J0YWJsZURvY3VtZW50QTEiXX1dLCJpYXQiOjE3MDQzODE2ODIsImV4cCI6MTcwNDQxMDQ4MiwiaXNzIjoiaHR0cHM6Ly9wZGEtbWFuYWdlbWVudC5wZGExLnNhbmRib3guZW1wbGFiLmV1L2p3a3MiLCJhdWQiOiJkaGFyc2luaXN1cml5YUBnbWFpbC5jb20iLCJzdWIiOiI2NTk2Y2I2YjAwMGRiYjRhNDM4NGE0OTUifQ.vDRuPqmRemAobLTp2cZu0llnpE7xNqXTNOVYZPF1pP9sFaO5iDYb-whFii5otwh14avQNncOkc56fSv0dUyk1g";

    const pinCode = getUserPin(preAuthCode);

    expect(pinCode).toEqual("0507");
  });
});
