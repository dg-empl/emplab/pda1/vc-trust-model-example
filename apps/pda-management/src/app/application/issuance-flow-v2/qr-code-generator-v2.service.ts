import { Logger } from "@esspass-web-wallet/logging-lib";
import { importJWK, SignJWT } from "jose";
import {
  getKeyPair,
  getUserPin,
  issuerKeyPair,
  JsonWebKeySet,
  KeyPair,
  PreAuthCodeGrants,
} from "@esspass-web-wallet/crypto-lib";
import { getIssuerKeyPair } from "./commons";

export class QrCodeGeneratorV2Service {
  private static log: Logger;
  private static qrCodeGeneratorV2Service: QrCodeGeneratorV2Service;
  private readonly privateKey = process.env.PDA_MANAGEMENT_PRIVATE_KEY;

  /**
   * ES256 and ES256K key pairs (private + public key JWKs)
   */
  private readonly issuerKeyPair: Record<"ES256" | "ES256K", KeyPair | undefined>;

  private constructor() {
    QrCodeGeneratorV2Service.log
      ? QrCodeGeneratorV2Service.log.debug("Creating new instance of QrCodeGeneratorV2Service")
      : null;

    this.issuerKeyPair = {
      ES256: undefined,
      ES256K: undefined,
    };
  }

  public static getInstance(log: Logger): QrCodeGeneratorV2Service {
    this.log = log;
    if (!this.qrCodeGeneratorV2Service) {
      this.qrCodeGeneratorV2Service = new QrCodeGeneratorV2Service();
    }
    return this.qrCodeGeneratorV2Service;
  }

  /**
   * Generate a pre-authorized code grant.
   * @param pdaId
   * @param issuerUri
   * @param email
   * @param lifetime
   * @returns {Promise<PreAuthCodeGrants>}
   */
  public async generatePreAuthGrant(
    pdaId: string,
    issuerUri: string,
    email: string,
    lifetime = "8h"
  ): Promise<PreAuthCodeGrants> {
    const keyPair = await getIssuerKeyPair("ES256");
    const publicKid = keyPair.publicKeyJwk.kid;
    const signingPrivateKey = await importJWK(keyPair.privateKeyJwk);

    const preAuthorizedCode = await new SignJWT({
      authorization_details: [
        {
          // type: "openid_credential",
          // format: "jwt_vc",
          // locations: [issuerUri],
          types: ["VerifiablePortableDocumentA1"],
        },
      ],
    })
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: publicKid,
      })
      .setIssuedAt()
      .setExpirationTime(lifetime)
      .setIssuer(`${issuerUri}/jwks`)
      .setAudience(email) //email needed for further dispatching information in v2 exchange
      .setSubject(pdaId) //pdaId needed for further dispatching information in v2 exchange -> generation of vc content
      .sign(signingPrivateKey);

    QrCodeGeneratorV2Service.log.debug("Pre-authorized pin code: " + getUserPin(preAuthorizedCode));

    return {
      "urn:ietf:params:oauth:grant-type:pre-authorized_code": {
        "pre-authorized_code": preAuthorizedCode,
        user_pin_required: true,
      },
    };
  }

  /**
   * Expose Issuer public keys.
   * Used in "Issue & Revoke" tests.
   *
   * @returns Issuer JWKS
   */
  async getJwks(): Promise<JsonWebKeySet> {
    const { publicKeyJwk } = await getIssuerKeyPair("ES256");
    return {
      keys: [publicKeyJwk],
    };
  }
}
