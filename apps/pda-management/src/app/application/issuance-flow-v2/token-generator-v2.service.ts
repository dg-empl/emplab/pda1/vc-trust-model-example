import { getIssuerKeyPair } from "./commons";
import { Logger } from "@esspass-web-wallet/logging-lib";
import { validateTokenRequest } from "@esspass-web-wallet/crypto-lib";
import { importJWK, SignJWT } from "jose";
import { AccessTokenType } from "@esspass-web-wallet/domain-lib";

export class TokenGeneratorV2Service {
  private static log: Logger;
  private static tokenGeneratorV2Service: TokenGeneratorV2Service;
  private readonly hostUrl = process.env.PDA_MANAGEMENT_URL;

  //nonce storage for v2
  //TODO move to leveldb
  private nonce = "";
  private nonceExpirationTime = 86_400;

  private constructor() {
    TokenGeneratorV2Service.log
      ? TokenGeneratorV2Service.log.debug("Creating new instance of TokenGenenratorV2Service")
      : null;
  }

  public static getInstance(log: Logger): TokenGeneratorV2Service {
    this.log = log;

    if (!this.tokenGeneratorV2Service) {
      this.tokenGeneratorV2Service = new TokenGeneratorV2Service();
    }
    return this.tokenGeneratorV2Service;
  }

  public async validateTokenRequest(preAuthorizedCode: string, userPin: string) {
    const keyPair = await getIssuerKeyPair("ES256");
    await validateTokenRequest(preAuthorizedCode, keyPair.publicKeyJwk, userPin);
  }

  /**
   * Generate token response of type AccessTokenType
   * @param nonce - random generated session string
   * @param audience - email of the requestor
   * @param pda1Id - requested pdaId
   * @return Promise<AccessTokenType>
   */
  public async generateTokenResponse(
    nonce: string,
    audience: string,
    pda1Id: string
  ): Promise<AccessTokenType> {
    const issuer = process.env.PDA_MANAGEMENT_URL;
    const keyPair = await getIssuerKeyPair("ES256");
    const publicKid = keyPair.publicKeyJwk.kid;
    const signingPrivateKey = await importJWK(keyPair.privateKeyJwk);

    this.nonce = nonce;

    const jwt = await new SignJWT({
      nonce: nonce,
      doc_id: pda1Id,
    })
      .setProtectedHeader({
        typ: "JWT",
        alg: "ES256",
        kid: publicKid,
      })
      .setIssuedAt()
      .setExpirationTime("24h")
      .setAudience(audience)
      .setIssuer(`${issuer}`)
      .setIssuedAt(new Date().getTime())
      .sign(signingPrivateKey);

    return {
      access_token: jwt,
      token_type: "bearer",
      expires_in: 86_400,
      c_nonce: nonce,
      c_nonce_expires_in: 86_400,
    };
  }

  get nonceValue() {
    return this.nonce;
  }
}
