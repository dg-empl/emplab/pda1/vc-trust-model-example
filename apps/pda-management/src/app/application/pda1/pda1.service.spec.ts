import { Pda1Service } from "./pda1.service";
import { MongoMemoryServer } from "mongodb-memory-server";
import { pda1Data } from "./pda1data";
import { logger } from "@esspass-web-wallet/logging-lib";
import * as dbUtils from "@esspass-web-wallet/backend-utils-lib";
import { RequestFormActionEnum } from "@esspass-web-wallet/domain-lib";

const log = logger("pda1Service-test");

describe("Pda1Service", () => {
  let pda1Service: Pda1Service, mongoServer: MongoMemoryServer;

  const pda1Id = "6531130dd462f8a8ae520541";
  const mongoDbName = "esspass-nssi-be";

  beforeAll(async () => {
    // mongoose.set("strictQuery", true);

    mongoServer = await dbUtils.createMongoMemoryServer();
    log.debug("Mongo Memory Server Started()");
    log.debug("mongoServer.started()");

    await dbUtils.createTestData(mongoServer.getUri(), mongoDbName, {
      collection: "pda1documents",
      data: pda1Data,
      id: { id: pda1Id },
      check: { requestFormId: "653112fcd462f8a8ae520537" },
    });

    /**
     * Connect to the in-memory database through the mongoose connection as the Schema uses mongoose
     */
    await dbUtils.memoryServerConnect(mongoServer);
    pda1Service = Pda1Service.getInstance(log);
  });

  it("should update status of pda1 document by pda1Id", async () => {
    //when
    const requestFormId = await pda1Service.updateByStatus(pda1Id, {
      action: RequestFormActionEnum.RevokeFunctionality,
    });

    //then
    expect(requestFormId).toEqual(pda1Id);
  });

  afterAll(async () => {
    await dbUtils.disconnect(log);
    await dbUtils.closeDatabase(mongoServer);
    log.debug("MongoServer stopped");
  });
});
