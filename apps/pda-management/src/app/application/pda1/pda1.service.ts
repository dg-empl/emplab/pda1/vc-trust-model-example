import { Logger } from "@esspass-web-wallet/logging-lib";
import { getSchema, validatePayload } from "../utils";
import { pda1SchemaDbModel, vcPda1SchemaDbModel } from "../../domain/pda1-schema";
import {
  NotFoundError,
  Pda1DbType,
  Pda1Type,
  RequestFormActionEnum,
  requestFormActionEnumAdapter,
  RequestFormStatusEnum,
  VerifiableCredential,
} from "@esspass-web-wallet/domain-lib";
import axios from "axios";

interface Pda1Request {
  create(pda1: Pda1Type, requestFormId: string): Promise<string>;
}

export class Pda1Service implements Pda1Request {
  private static pda1Service = new Pda1Service();
  private static log: Logger;

  private constructor() {
    Pda1Service.log ? Pda1Service.log.debug("Creating new instance of RequestFormService") : null;
  }

  public static getInstance(log: Logger): Pda1Service {
    this.log = log;
    if (!this.pda1Service) {
      this.pda1Service = new Pda1Service();
    } else {
      return this.pda1Service;
    }
  }

  public async create(pda1: Pda1Type, requestFormId: string): Promise<string> {
    Pda1Service.log.trace("create:started");
    const schemaName = "PDA1Schema";
    const pda1ValidationSchema = await getSchema(schemaName);
    const validatorResult = await validatePayload(pda1, pda1ValidationSchema, schemaName);

    if (!validatorResult.valid) {
      throw new Error(JSON.stringify(validatorResult.errors));
    }

    const storeResult = await this.storePda1(pda1, requestFormId, validatorResult.valid);
    Pda1Service.log.trace("create:finished");

    return storeResult._id.toString();
  }

  public async createVerifiableCredential(
    vcPda1Json: VerifiableCredential,
    requestFormId: string,
    pda1Id: string
  ): Promise<string> {
    let storeResult;
    Pda1Service.log.trace("createVerifiableCredential:started");

    try {
      const vcPda1 = {
        vcPda1Data: JSON.stringify(vcPda1Json),
        requestFormId: requestFormId,
        pda1Id: pda1Id,
        dateOfRegistration: new Date(),
        vcPda1Status: "Active",
      };
      storeResult = await vcPda1SchemaDbModel.create(vcPda1);
      Pda1Service.log.trace("createVerifiableCredential:finished");
    } catch (ex) {
      throw new Error(ex);
    }

    return storeResult._id.toString();
  }

  public async deleteRecordByRequestFormId(requestFormId: string): Promise<string> {
    if (!requestFormId) {
      throw new Error("Invalid request");
    }

    let result: any;

    try {
      result = await pda1SchemaDbModel.findOneAndDelete({ requestFormId: requestFormId });
      await vcPda1SchemaDbModel.findOneAndDelete({ requestFormId: requestFormId });
    } catch (e) {
      throw new NotFoundError("No record found for requestFormId: " + requestFormId);
    }
    return result._id.toString();
  }

  public async findPdaByRequestFormById(requestFormId: string): Promise<Pda1DbType> {
    return pda1SchemaDbModel.findOne({ requestFormId: requestFormId });
  }

  public async updateByStatus(
    requestFormId: string,
    action: Record<"action", RequestFormActionEnum>
  ): Promise<string> {
    const update = {
      $set: {
        pda1Status: requestFormActionEnumAdapter[action.action] as string,
      },
    };

    // Optional options (for example, to return the updated document)
    const options = {
      new: true, // Return the updated document
      runValidators: true, // Run validators defined in the schema
    };

    pda1SchemaDbModel.findOneAndUpdate(
      { requestFormId: requestFormId },
      update,
      options,
      (err: any, updatedDocument) => {
        if (err) {
          throw new Error(err);
        } else {
          console.log("Updated request-form / pda1 document : " + updatedDocument);

          Pda1Service.log.trace(
            "Updated request-form / pda1 document : " + updatedDocument.requestFormId
          );
        }
      }
    );

    return requestFormId;
  }

  public async revokePDA1(requestFormId: string) {
    Pda1Service.log.trace("revokePDA1:started");
    const vcJson = await vcPda1SchemaDbModel.findOne({ requestFormId: requestFormId });

    if (!vcJson) {
      throw new NotFoundError("No vcJson record found for requestFormId: " + requestFormId);
    }

    const { vcPda1Data } = vcJson;

    const theVcPda1NotString = JSON.parse(vcPda1Data);
    const credentialStatusJson = theVcPda1NotString.credentialStatus;

    const credStatus = await this.getCredentialStatus(credentialStatusJson);

    const responseJson = JSON.parse(credStatus.response);

    if (responseJson.body === "Not revoked") {
      Pda1Service.log.trace("Document not revoked... revoking....");
      Pda1Service.log.trace("-------------------revoking in DB-------------------------------");
      await this.setCredentialStatus(credentialStatusJson);

      const update = {
        $set: {
          vcPda1Status: "Revoked",
        },
      };

      // Optional options (for example, to return the updated document)
      const options = {
        new: true, // Return the updated document
        runValidators: true, // Run validators defined in the schema
      };

      vcPda1SchemaDbModel.findOneAndUpdate(
        { requestFormId: requestFormId },
        update,
        options,
        (err: any, updatedDocument) => {
          if (err) {
            throw new Error(err);
          } else {
            Pda1Service.log.trace("Updated vc pda1 document : " + updatedDocument.requestFormId);
          }
        }
      );

      return requestFormId;
    } else if (responseJson.body === "Revoked") {
      Pda1Service.log.trace("-------------------already revoked-------------------------------");
    }

    Pda1Service.log.trace("revokePDA1:finished");
  }

  private async getCredentialStatus(credentialSubject, apiPath = "/credential-status") {
    try {
      Pda1Service.log.trace("getCredentialStatus:started");
      const hostname = process.env.REVOCATION_URL + `${apiPath}`;
      const result: { response: string } = { response: "" };
      const response = await axios({ method: "get", url: hostname, data: credentialSubject });
      const data = response.data;
      result.response = JSON.stringify(data);
      Pda1Service.log.trace("getCredentialStatus:finished");
      return result;
    } catch (ex) {
      throw new Error(ex);
    }
  }

  private async setCredentialStatus(credentialJson: any) {
    try {
      Pda1Service.log.trace("setCredentialStatus:started");

      const apiPath = "credential-status";
      const hostname = process.env.REVOCATION_URL + `/${apiPath}`;
      const body = JSON.stringify(credentialJson);
      const payLoad = `${body}`;

      const result = { response: "" };
      const requestObj = {
        method: "PUT",
        body: payLoad,
        headers: {
          accept: "application/json",
          "Content-Type": "application/json",
        },
      };

      const response = await fetch(hostname, requestObj);
      const data = await response.json();
      result.response = JSON.stringify(data.body);
      Pda1Service.log.trace("setCredentialStatus:finished");
      return result;
    } catch (ex) {
      throw new Error(ex);
    }
  }

  private async storePda1(payload: Pda1Type, requestFormId: string, valid: boolean): Promise<any> {
    try {
      Pda1Service.log.trace("storePda1Document:started");

      const record = {
        pda1Data: JSON.stringify(payload),
        requestFormId: requestFormId,
        validationResult: valid ? "VALID" : "INVALID",
        pda1Status: RequestFormStatusEnum.Issued,
        emailedTo: "",
      };

      const result = await pda1SchemaDbModel.create({ ...record });

      Pda1Service.log.trace("storePda1Document:finished");
      return result;
    } catch (ex) {
      throw new Error(ex);
    }
  }
}
