export const pda1Data = {
  pda1Data:
    '{"section1":{"personalIdentificationNumber":"AT53724332","sex":"01","surname":"asd","forenames":"asd","dateBirth":"1973-11-13","surnameAtBirth":"null","placeBirth":{"region":"Vienna","countryCode":"AT","town":"Vienna"},"nationalities":["AT"],"stateOfResidenceAddress":{"streetNo":"Trabrennstraße 6-8","postCode":"1020","town":"Vienna","countryCode":"AT"},"stateOfStayAddress":{"streetNo":"Trabrennstraße 6-8","postCode":"1020","town":"Vienna","countryCode":"AT"}},"section2":{"memberStateWhichLegislationApplies":"CZ","startingDate":"2024-11-11","endingDate":"2027-12-12","certificateForDurationActivity":true,"determinationProvisional":true,"transitionRulesApplyAsEC8832004":true},"section3":{"postedEmployedPerson":true,"employedTwoOrMoreStates":true,"postedSelfEmployedPerson":true,"selfEmployedTwoOrMoreStates":true,"civilServant":true,"contractStaff":true,"mariner":true,"employedAndSelfEmployed":true,"civilAndEmployedSelfEmployed":true,"flightCrewMember":true,"exception":true,"exceptionDescription":"no exceptions","workingInStateUnder21":true},"section4":{"employee":true,"selfEmployedActivity":true,"employerSelfEmployedActivityCodes":[""],"nameBusinessName":"221139152","registeredAddress":{"streetNo":"Trabrennstraße 6-8, 213 A","postCode":"1020","town":"Vienne","countryCode":"CZ"}},"section5":{"noFixedAddress":false},"section6":{"name":"Office National de Sécurité Sociale / Rijksdienst voor Sociale Zekerheid","address":{"streetNo":"Main Street 1","postCode":"1000","town":"Brussels","countryCode":"BE"},"institutionID":"NSSI-BE-01","officePhoneNo":"0800 12345","officeFaxNo":"0800 98765","email":"esspass.noreply@gmail.com","date":"2023-10-19","signature":"Official signature"}}',
  requestFormId: "653112fcd462f8a8ae520537",
  vcPda1Id: "",
  dateOfRegistration: "2023-10-19T11:29:17.529Z",
  validationResult: "VALID",
  pda1Status: "Issued",
  emailedTo: "",
  __v: 0,
};
