import { convertObjectFromBracketNotation, convertStringToDate, validatePayload } from "./utils";
import { requestFormZodSchema } from "../domain/request-form-zod-schema";
import { ConversionError } from "@esspass-web-wallet/backend-utils-lib";
import { logger } from "@esspass-web-wallet/logging-lib";
import { readFile } from "fs/promises";
import * as path from "path";

const log = logger("utils.spec");

const data = {
  part1: {
    personalIdentificationNumber: "PL37441974",
    surname: "a",
    forenames: "a",
    surnameAtBirth: "---",
    gender: "01",
    dateOfBirth: "1985-11-28",
    nationality: "PL",
    placeOfBirth: "Warszawa",
    email: "marcin.zawadzki@outlook.com",
    address: {
      street: "Juliana Tuwima 7",
      number: "196",
      town: "Brussels",
      postCode: "90-010",
      countryCode: "PL",
    },
  },
  part2: { memberState: "IT", startingDate: "2026-02-09", endingDate: "2027-06-18" },
  part3: {
    postedEmployedPerson: "yes",
    employedInTwoOrMoreMemberStates: "no",
    postedSelfEmployedPerson: "no",
    selfEmployedInTwoOrMoreMemberStates: "no",
    civilServant: "no",
    contractStaff: "no",
    mariners: "no",
    employedAndSelfEmployed: "no",
    civilServantAndEmployedOrSelfEmployed: "no",
    flightCrewMember: "no",
    exception: "no",
    exceptionDescription: "no exceptions",
    employedOrSelfEmployedInState21: "no exceptions",
  },
  part4: {
    employee: "no",
    selfEmployedActivity: "Oil and gas",
    employedSelfEmployedActivityCode: "470442435",
    nameOrBusinessName: "PKN Orlen",
    registeredAddress: {
      street: "Juliana Tuwima 7",
      number: "160",
      town: "Warszawa",
      postCode: "63873",
      countryCode: "PL",
    },
  },
  part5: {
    pursuedActivityName: "Software development",
    pursuedBusinessName: "Assicurazioni Generali S.p.A",
    addressInHostMemberState: {
      street: "Piazza Duca degli Abruzzi 2, 932",
      number: "2A",
      town: "Trieste",
      postCode: "1877",
      countryCode: "IT",
    },
    noFixedAddress: "no",
  },
  part6: {
    name: "Office National de Sécurité Sociale / Rijksdienst voor Sociale Zekerheid",
    street: "Main Street 1",
    number: "1A",
    postCode: "1000",
    town: "Brussels",
    countryCode: "BE",
    institutionId: "NSSI-BE-01",
    officePhoneNumber: "0800 12345",
    officeFaxNumber: "0800 98765",
    email: "esspass.noreply@gmail.com",
    date: "2023-09-14",
    signature: "Official signature",
  },
  part7: {
    dataProcessingConsent: "yes",
    declarationOfHonor: "I hereby confirm that the information provided is accurate and complete.",
  },
};

async function getSchema(schemaName: string) {
  try {
    log.trace("getSchema:Started ");

    const schemaPath = path.join(__dirname, "..", "..", "schemas", schemaName);

    const result = await readFile(`${schemaPath}.json`, "utf8");

    log.trace("getSchema:Done ");
    return result;
  } catch (ex) {
    throw new Error(ex);
  }
}

describe("utils class", () => {
  it("should create proper object from request form urlencoded", () => {
    const obj = {
      "part1[requestorNatRegNR]": "AT61983125",
      "part1[requestorLastName]": "a",
      "part1[requestorFirstName]": "a",
      "part2[requestorFirstName2]": "a",
      "part3[requestorFirstName3]": "3",
      "part3[requestorFirstName4]": "4",
    };

    const res = convertObjectFromBracketNotation(obj);

    expect(res).toEqual({
      part1: {
        requestorNatRegNR: "AT61983125",
        requestorLastName: "a",
        requestorFirstName: "a",
      },
      part2: { requestorFirstName2: "a" },
      part3: { requestorFirstName3: "3", requestorFirstName4: "4" },
    });
  });

  it("should convert from string fields to date fields", () => {
    //given
    const requestForm: Record<string, any> = {
      part1: {
        personalIdentificationNumber: "1234567890",
        surname: "Doe",
        forenames: "John",
        surnameAtBirth: "Doe",
        gender: "01",
        dateOfBirth: "1990-01-01",
        nationality: "Belgian",
        placeOfBirth: "Brussels",
        address: {
          street: "Rue de la Loi",
          number: "16",
          town: "Brussels",
          postCode: "1000",
          countryCode: "BE",
          asdDate: "2021-01-01",
          test: {
            aSssdate: "2021-01-01",
          },
        },
      },
    };

    //when
    const ret = convertStringToDate(requestForm, "date");

    //then
    expect(ret["part1"].dateOfBirth).toBeInstanceOf(Date);
    expect(ret["part1"].address.asdDate).toBeInstanceOf(Date);
    expect(ret["part1"].address.test.aSssdate).toBeInstanceOf(Date);
  });

  it("should zod safe parse request form", () => {
    //given
    const testData = { ...data };
    // const convertedData = convertStringToDate(testData, "date");

    //when
    const result = requestFormZodSchema.safeParse(data);

    if (!result.success) {
      const errorDesc = result["error"].issues
        .map(issue => `'${issue.path.join(".")}': ${issue.message}`)
        .join("\n");

      log.trace(`testData : ${JSON.stringify(data, null, 2)}`);

      throw new ConversionError("invalid_conversion_action", {
        errorDescription: `Error in conversion from rest post to Request Form model : ${errorDesc}`,
      });
    }

    //then
    expect(result.success).toBe(true);
  });

  it("should json schema parse request form", async () => {
    //given
    const schemaName = "PDA1RequestFormSchema";
    const requestFormSchema = await getSchema(schemaName);

    //when
    const validatorResult = await validatePayload({ ...data }, requestFormSchema, schemaName);

    validatorResult.errors?.forEach(err => {
      log.error(`Error: ${err}`);
    });

    expect(validatorResult.valid).toBe(true);
  });
});
