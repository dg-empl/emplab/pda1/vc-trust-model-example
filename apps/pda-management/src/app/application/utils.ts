import { logger } from "@esspass-web-wallet/logging-lib";
import { readFile } from "fs/promises";
import { dirname } from "path";
import * as jsonschema from "jsonschema";
import { requestFormSchemaModel } from "../domain/request-form-schema";
import { RequestFormType, requestFormZodSchema } from "../domain/request-form-zod-schema";
import { RequestFormStatusEnum } from "@esspass-web-wallet/domain-lib";
import { ConversionError } from "@esspass-web-wallet/backend-utils-lib";

const log = logger("pda-management");

export const getSchema = async (schemaName: string) => {
  try {
    log.trace("getSchema:Started ");

    const result = await readFile(
      `${dirname(__dirname)}/pda-management/schemas/${schemaName}.json`,
      "utf8"
    );

    log.trace("getSchema:Done ");
    return result;
  } catch (ex) {
    throw new Error(ex);
  }
};

export const validatePayload = async (payload: any, schema: string, schemaName?: string) => {
  try {
    log.trace("validatePayload: started for schema : " + schemaName);

    const validator = new jsonschema.Validator();
    const schemaObject = JSON.parse(schema);
    const result = validator.validate(payload, schemaObject);

    log.trace("validatePayload:finished");
    return result;
  } catch (ex) {
    throw new Error(ex);
  }
};

async function createRecord(record: any) {
  try {
    log.trace("createRecord:started");

    record.dateOfRegistration = new Date().getTime();
    log.trace("createRecord:record :" + record);
    const response = await requestFormSchemaModel.create({ ...record });

    log.trace("createRecord:finished");

    return response;
  } catch (ex) {
    throw new Error(ex);
  }
}

async function storeRequestForm(payload: RequestFormType, valid: boolean) {
  try {
    log.trace("storeRequestForm:Started ");

    const { email, forenames, surname } = payload.part1;

    const record = {
      requestFormData: JSON.stringify(payload),
      validationResult: valid ? "VALID" : "INVALID",
      previousFormStatus: RequestFormStatusEnum.Accepted,
      requestFormStatus: RequestFormStatusEnum.Accepted,
      requestorEmail: email,
      requestorFirstName: forenames,
      requestorLastName: surname,
    };

    const result = await createRecord(record);

    log.trace("storeRequestForm:Done ");
    return result;
  } catch (ex) {
    throw new Error(ex);
  }
}

export async function manageRequestForm(requestFormData: string) {
  log.trace("manageRequestForm : started");

  // convert raw string data to object e.g dates
  // const dateConvertedObject = convertStringToDate(JSON.parse(JSON.stringify(requestFormData)), "date");

  const parsedRequestForm = requestFormZodSchema.safeParse(requestFormData);

  if (!parsedRequestForm.success) {
    const errorDesc = parsedRequestForm["error"].issues
      .map(issue => `'${issue.path.join(".")}': ${issue.message}`)
      .join("\n");

    log.trace(`manageRequestForm : ${JSON.stringify(requestFormData, null, 2)}`);

    throw new ConversionError("invalid_conversion_action", {
      errorDescription: `Error in conversion from rest post to Request Form model : ${errorDesc}`,
    });
  }

  const data = parsedRequestForm.data;
  const schemaName = "PDA1RequestFormSchema";

  const requestFormSchema = await getSchema(schemaName);
  const validatorResult = await validatePayload(
    data as RequestFormType,
    requestFormSchema,
    schemaName
  );

  if (!validatorResult.valid) {
    throw new Error(JSON.stringify(validatorResult.errors));
  }

  const storeResult = await storeRequestForm(data as RequestFormType, validatorResult.valid);

  log.trace("manageRequestForm : finished");

  return storeResult;
}

export function convertObjectFromBracketNotation(object: object): object {
  const newObject: object = {};

  for (const [key, value] of Object.entries(object)) {
    const [objectName, propertyName] = key.split("[");

    if (!newObject[objectName]) {
      newObject[objectName] = {};
    }

    newObject[objectName][propertyName.split("]")[0]] = value;
  }

  return newObject;
}

export function convertObjectFieldsStringTo(
  type: Date | Record<string, any>,
  object: object
): Record<string, any> {
  const newObject: object = {};

  for (const [key, value] of Object.entries(object)) {
    console.log("key", key);
    if (value instanceof Object) {
      for (const [key, values] of Object.entries(value)) {
        console.log(key + " : " + values);
      }
    }

    const [objectName, propertyName] = key.split("[");

    if (!newObject[objectName]) {
      newObject[objectName] = {};
    }

    newObject[objectName][propertyName.split("]")[0]] = value;
  }

  return newObject;
}

export function convertStringToDate(object: object, key: string): object {
  for (const [k, v] of Object.entries(object)) {
    if (k.toLowerCase().includes(key)) {
      object[k] = new Date(v);
    }

    if (v instanceof Object) {
      convertStringToDate(v, key);
    }
  }

  return object;
}
