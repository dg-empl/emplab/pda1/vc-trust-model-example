import * as mongoose from "mongoose";
import { RequestFormStatusEnum } from "@esspass-web-wallet/domain-lib";

const pda1Schema = new mongoose.Schema({
  pda1Data: { type: String, default: {} },
  requestFormId: { type: String, default: "" },
  vcPda1Id: { type: String, default: "" },
  dateOfRegistration: Date,
  validationResult: { type: String, enum: ["VALID", "INVALID"] },
  pda1Status: {
    type: String,
    enum: RequestFormStatusEnum,
    default: "Issued",
  },
  emailedTo: { type: String, default: "" },
});

export const pda1SchemaDbModel = mongoose.model("pda1document", pda1Schema);

const vcPda1Schema = new mongoose.Schema({
  vcPda1Data: { type: String, default: {} },
  requestFormId: { type: String, default: "" },
  pda1Id: { type: String, default: "" },
  dateOfRegistration: Date,
  vcPda1Status: {
    type: String,
    enum: ["Active", "Suspended", "Revoked", "Expired"],
    default: "Active",
  },
});

export const vcPda1SchemaDbModel = mongoose.model("VCPDA1Document", vcPda1Schema);
