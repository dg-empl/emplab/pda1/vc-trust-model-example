import * as mongoose from "mongoose";
import { RequestFormStatusEnum } from "@esspass-web-wallet/domain-lib";

const requestFormSchema = new mongoose.Schema({
  pda1Id: String,
  requestorEmail: String,
  requestFormData: String,
  dateOfRegistration: Date,
  requestorLastName: String,
  requestorFirstName: String,
  validationResult: { type: String, enum: ["VALID", "INVALID"] },
  requestFormStatus: { type: String, enum: RequestFormStatusEnum },
  previousFormStatus: { type: String, enum: RequestFormStatusEnum },
});

export const requestFormSchemaModel = mongoose.model("pda1requestform", requestFormSchema);
