import { requestFormZodSchema } from "./request-form-zod-schema";
import { ConversionError } from "@esspass-web-wallet/backend-utils-lib";

describe("RequestFormZodSchema", () => {
  const testData = {
    part1: {
      personalIdentificationNumber: "1234567890",
      surname: "Doe",
      forenames: "John",
      surnameAtBirth: "Doe",
      dateOfBirth: "2000-01-01",
      nationality: "Belgian",
      placeOfBirth: "Brussels",
      gender: "01",
      email: "test@test.com",
      address: {
        street: "Rue de la Loi",
        number: "16",
        town: "Brussels",
        postCode: "1000",
        countryCode: "BE",
      },
    },
    part2: {
      memberState: "Belgium",
      startingDate: "2021-01-01",
      endingDate: "2022-01-01",
    },
    part3: {
      postedEmployedPerson: "Yes",
      employedInTwoOrMoreMemberStates: "No",
      postedSelfEmployedPerson: "No",
      selfEmployedInTwoOrMoreMemberStates: "No",
      civilServant: "No",
      contractStaff: "No",
      mariners: "No",
      employedAndSelfEmployed: "No",
      civilServantAndEmployedOrSelfEmployed: "No",
      flightCrewMember: "No",
      exception: "No",
      exceptionDescription: "no exception",
      employedOrSelfEmployedInState21: "No",
    },
    part4: {
      employee: "No",
      selfEmployedActivity: "Yes",
      employedSelfEmployedActivityCode: "1234",
      nameOrBusinessName: "Doe",
      registeredAddress: {
        street: "Rue de la Loi",
        number: "16",
        town: "Brussels",
        postCode: "1000",
        countryCode: "BE",
      },
    },
    part5: {
      pursuedActivityName: "Software development",
      pursuedBusinessName: "Business activity",
      addressInHostMemberState: {
        street: "Rue de la Loi",
        number: "16",
        town: "Brussels",
        postCode: "1000",
        countryCode: "BE",
      },
      noFixedAddress: "No",
    },
    part6: {
      name: "Doe",
      street: "Rue de la Loi",
      town: "Brussels",
      postCode: "1000",
      countryCode: "BE",
      institutionId: "1234",
      officeFaxNumber: "123456789",
      officePhoneNumber: "123456789",
      email: "223@test.com",
      date: "2021-01-01",
      signature: "signature",
    },
    part7: {
      dataProcessingConsent: "yes",
      declarationOfHonor: "yes",
    },
  };

  it("should create an instance of requestFormScheme", () => {
    //given when
    const result = requestFormZodSchema.safeParse(testData);

    if (!result.success) {
      const errorDesc = result["error"].issues
        .map(issue => `'${issue.path.join(".")}': ${issue.message}`)
        .join("\n");

      throw new ConversionError("invalid_conversion_action", {
        errorDescription: `Error in conversion from Request Form to Pda1Type model : ${errorDesc}`,
      });
    }

    //then
    expect(result.success).toBeTruthy();
  });

  it("should create an error when parsing requestFormScheme with wrong countryCode", () => {
    //given
    const data = {
      ...testData,
      part1: {
        ...testData.part1,
        address: {
          ...testData.part1.address,
          countryCode: "BE1",
        },
      },
    };

    // when
    const result = requestFormZodSchema.safeParse(data);

    //then
    expect(result.success).toBeFalsy();
    const errorDesc = result["error"].issues
      .map(issue => `'${issue.path.join(".")}': ${issue.message}`)
      .join("\n");

    expect(errorDesc).toBe("'part1.address.countryCode': Invalid country code");
  });
});
