import { union, z } from "zod";

export const addressZodSchema = z.object({
  street: z.string(),
  number: z.string(),
  town: z.string(),
  postCode: z.string(),
  countryCode: z
    .string()
    .refine(value => countryCodes.includes(value), { message: "Invalid country code" }),
});

export const requestFormZodSchema = z.object({
  part1: z.object({
    personalIdentificationNumber: z.string(),
    surname: z.string(),
    forenames: z.string(),
    surnameAtBirth: z.string(),
    gender: z.string().refine(value => gender.includes(value), { message: "Invalid gender code" }),
    dateOfBirth: z.string(),
    nationality: z.string(),
    placeOfBirth: z.string(),
    address: addressZodSchema,
    email: z.string().email(),
  }),
  part2: z.object({
    memberState: z.string(),
    startingDate: z.string(),
    endingDate: z.string(),
  }),
  part3: z.object({
    postedEmployedPerson: z.string(),
    employedInTwoOrMoreMemberStates: z.string(),
    postedSelfEmployedPerson: z.string(),
    selfEmployedInTwoOrMoreMemberStates: z.string(),
    civilServant: z.string(),
    contractStaff: z.string(),
    mariners: z.string(),
    employedAndSelfEmployed: z.string(),
    civilServantAndEmployedOrSelfEmployed: z.string(),
    flightCrewMember: z.string(),
    exception: z.string(),
    exceptionDescription: z.string(),
    employedOrSelfEmployedInState21: z.string(),
  }),
  part4: z.object({
    employee: z.string(),
    selfEmployedActivity: z.string(),
    employedSelfEmployedActivityCode: z.string(),
    nameOrBusinessName: z.string(),
    registeredAddress: addressZodSchema,
  }),
  part5: z.object({
    pursuedActivityName: z.string(),
    pursuedBusinessName: z.string(),
    addressInHostMemberState: addressZodSchema,
    noFixedAddress: z.string(),
  }),
  part6: z.object({
    name: z.string(),
    street: z.string(),
    town: z.string(),
    postCode: z.string(),
    countryCode: z
      .string()
      .refine(value => countryCodes.includes(value), { message: "Invalid country code" }),
    institutionId: z.string(),
    officeFaxNumber: z.string(),
    officePhoneNumber: z.string(),
    email: z.string().email(),
    date: z.string(),
    signature: z.string(),
  }),
  part7: z.object({ dataProcessingConsent: z.string(), declarationOfHonor: z.string() }),
});

export type RequestFormType = z.infer<typeof requestFormZodSchema>;

const countryCodes = [
  "AT",
  "BE",
  "BG",
  "HR",
  "CY",
  "CZ",
  "DK",
  "EE",
  "FI",
  "FR",
  "DE",
  "EL",
  "HU",
  "IS",
  "IE",
  "IT",
  "LV",
  "LI",
  "LT",
  "LU",
  "MT",
  "NL",
  "NO",
  "PL",
  "PT",
  "RO",
  "SK",
  "SI",
  "ES",
  "SE",
  "CH",
  "UK",
];

export const gender = ["01", "02", "98", "99"];
