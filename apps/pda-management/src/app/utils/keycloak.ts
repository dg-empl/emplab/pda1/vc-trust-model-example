import * as core from "express-serve-static-core";
import Keycloak from "keycloak-connect";
import session from "express-session";

import "dotenv/config";

const secret = "C0SbeTswpqkTA5n2!m4^7@QYqt2ntbIsd";

const config = {
  realm: process.env["IDP_REALM"] || "realm",
  "auth-server-url": process.env["IDP"] + "/" || "auth-server-url",
  "ssl-required": "external" || "ssl-required",
  resource: process.env["CLIENT_ID"] || "resource",
  credentials: {
    secret: "TO ADD",
  },
  "confidential-port": 0,
};

/*------------------ Keycloak ------------------------------------------------*/
export function keycloakSetup(app: core.Express): Keycloak.Keycloak {
  const memoryStore = new session.MemoryStore();

  app.use(
    session({
      secret: secret,
      resave: false,
      saveUninitialized: true,
      store: memoryStore,
    })
  );

  return new Keycloak({ store: memoryStore }, config);
}
/*----------------------------------------------------------------------------*/
