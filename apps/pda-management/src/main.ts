import { Tracer, Logger, logger } from "@esspass-web-wallet/logging-lib";
const applicationName = "pda-management";

/**
 * OpenTelemetry
 */
const tracer = new Tracer(applicationName);
import { api } from "@opentelemetry/sdk-node";
/**
 * OpenTelemetry
 */

import express from "express";
import "express-async-errors";
import "dotenv/config";
import cors from "cors";
import * as bodyParser from "body-parser";
import { Database, errorHandler } from "@esspass-web-wallet/backend-utils-lib";
import { NotFoundError } from "@esspass-web-wallet/domain-lib";
import { keycloakSetup } from "./app/utils/keycloak";

const log: Logger = logger(applicationName);

const app = express();
app.set("trust proxy", true);

app.use(cors());
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);

app.use(bodyParser.json());

//keycloak
const keycloak = keycloakSetup(app);
app.use(keycloak.middleware());

/**
 * end-points
 */
import {
  tokenAdapter,
  queryPda1Adapter,
  postRequestFormAdapter,
  deleteRequestFormAdapter,
  updateRequestFormAdapter,
  createQrCodeAdapter,
  jwksAdapter,
  credentialAdapter,
  queryRequestFormAdapter,
} from "./app/adapters";
import { revokePda1Adapter } from "./app/adapters/rest/pda/revoke-pda1-adapter";
import { sendQrCodeByEmail } from "./app/adapters/rest/v3/v3-qr-code-generator.adapter";
import { IdpTokenService } from "@esspass-web-wallet/security-lib";
import { queryRequestFormFilterAdapter } from "./app/adapters/rest/request-form/query-request-form-filter-adapter";

function setupRoutes() {
  /**
   * version - version deployed
   */
  app.get("/version", (req, res) => {
    return res.send({
      version: process.env.VERSION || "development",
      revision: process.env.GIT_REVISION || "1",
      branch: process.env.GIT_BRANCH || "master",
    });
  });

  /**
   * post / get request form
   */
  app.get("/request-form", keycloak.protect(), queryRequestFormFilterAdapter);
  app.get("/request-form/:id", keycloak.protect(), queryPda1Adapter);
  app.get("/request-form/pda1/:id", keycloak.protect(), queryRequestFormAdapter);
  app.post("/request-form", keycloak.protect(), postRequestFormAdapter);
  app.put("/request-form/:id", keycloak.protect(), updateRequestFormAdapter);
  app.delete("/request-form/:id", keycloak.protect(), deleteRequestFormAdapter);

  /**
   * post / get pda1
   */
  app.get("/pda1/:id", keycloak.protect(), queryPda1Adapter);
  /**
   * v3 - get pda1 to be signed by 3rd party functionality
   */
  app.get("/pda1/request-form/:id", keycloak.protect(), queryPda1Adapter);
  app.put("/pda1/revoke/:id", keycloak.protect(), revokePda1Adapter);

  /**
   * iii v2 / Revocation - flow
   */
  app.get("/v2/generate-qr-code/:requestFormId/email/:email", createQrCodeAdapter);
  app.post("/v2/token", tokenAdapter);
  app.post("/v2/credential", credentialAdapter);

  /**
   * iii v3 / 3rd party providers - flow
   */
  app.post("/v3/send-emails", keycloak.protect(), sendQrCodeByEmail);

  /**
   * jwks - public key
   */
  app.get("/jwks", jwksAdapter);

  /**
   * error handler
   */
  app.all("*", (req, res) => {
    throw new NotFoundError("Not found route");
  });
  app.use(errorHandler(log));
}

function startServer() {
  log.info("App version : " + process.env.VERSION || "development");
  log.info("Environment : " + process.env.ENVIRONMENT || "development");

  IdpTokenService.getInstance(log);

  /**
   * connect to database
   */
  Database.getInstance(
    process.env.DB_HOST,
    process.env.DB_USERNAME,
    process.env.DB_PASSWORD,
    process.env.DB_PORT,
    "esspass-nssi-be",
    log
  );

  const port = 3100;
  const server = app.listen(port, () => log.info(`Listening at port: ${port}`));
  server.on("error", log.error);
}

setupRoutes();
startServer();
