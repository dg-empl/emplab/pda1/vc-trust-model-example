/* eslint-disable */
export default {
  displayName: "versions",
  preset: "../../jest.preset.js",
  globals: {
    "ts-jest": {
      tsconfig: "<rootDir>/tsconfig.spec.json",
    },
  },
  testMatch: ["**/+(*.)+(spec|test).+(ts|js)?(x)", "!**/+(*.)+(integration).+(spec|test).+(ts|js)?(x)"],
  testEnvironment: "node",
  transform: {
    "^.+\\.[tj]s$": "ts-jest",
  },
  moduleFileExtensions: ["ts", "js", "html"],
  coverageDirectory: "../../coverage/apps/versions",
};
