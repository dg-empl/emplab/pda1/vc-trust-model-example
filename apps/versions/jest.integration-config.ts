/* eslint-disable */
export default {
  displayName: "versions",
  preset: "../../jest.preset.js",
  globals: {
    globals: {
      'process.env.version': '#{args:version}',
    },
    "ts-jest": {
      "extends": "./tsconfig.json",
      "compilerOptions": {
        "outDir": "../../dist/out-tsc",
        "module": "commonjs",
        "types": ["jest", "node"]
      },
      "include": [
        "jest.config.ts",
        "src/**/*.int.spec.ts",
        "src/**/*.d.ts"
      ]
    },
  },
  testEnvironment: "node",
  transform: {
    "^.+\\.[tj]s$": "ts-jest",
  },
  moduleFileExtensions: ["ts", "js", "html"]
};
