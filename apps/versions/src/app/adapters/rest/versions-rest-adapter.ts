import fetch from "node-fetch";
import { logger } from "@esspass-web-wallet/logging-lib";
const log = logger("versions");

const versionByUrl = async url =>
  await fetch(`${url}/version`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  });

const version = async url => {
  let response;
  const log = logger("versions");

  try {
    log.info(`url: ${url}`);
    response = await (await versionByUrl(url)).json();
  } catch (e) {
    log.error(e);
    response = { error: e };
  }

  return response;
};

export const versionsRestAdapter = async (req, res) => {
  return res.send({
    "auth-server": await version(
      "http://" +
        `${process.env.AUTH_SERVER_HOST}${process.env.ENV}:${
          process.env[`${process.env.AUTH_SERVER_PORT}`]
        }`
    ),
    "backend-web-wallet": await version(
      "http://" +
        `${process.env.BACKEND_HOST}${process.env.ENV}:${
          process.env[`${process.env.BACKEND_PORT}`]
        }`
    ),
    "pda-management": await version(
      "http://" +
        `${process.env.PDA_MANAGEMENT_HOST}${process.env.ENV}:${
          process.env[`${process.env.PDA_MANAGEMENT_PORT}`]
        }`
    ),
    "issuer-backoffice": await version(
      "http://" +
        `${process.env.ISSUER_BACKOFFICE_HOST}${process.env.ENV}:${
          process.env[`${process.env.ISSUER_BACKOFFICE_PORT}`]
        }`
    ),
    "issuer-revocation": await version(
      "http://" +
        `${process.env.ISSUER_REVOCATION_HOST}${process.env.ENV}:${
          process.env[`${process.env.ISSUER_REVOCATION_PORT}`]
        }`
    ),
    // "issuer-frontend": await version(
    //   "http://" +
    //     `${process.env.ISSUER_FE_HOST}${process.env.ENV}:${
    //       process.env[`${process.env.ISSUER_FE_PORT}`]
    //     }`
    // ),
    "issuer-wallet": await version(
      "http://" +
        `${process.env.ISSUER_WALLET_HOST}${process.env.ENV}:${
          process.env[`${process.env.ISSUER_WEB_WALLET_PORT}`]
        }`
    ),
    versions: {
      version: process.env.VERSION || "development",
      revision: process.env.GIT_REVISION || "1",
      branch: process.env.GIT_BRANCH || "master",
    },
  });
};
