import { GenericContainer, StartedTestContainer, Wait } from "testcontainers";

import fetch from "node-fetch";

export const checkContainerIsHealthy = async (container: StartedTestContainer, port = 8080): Promise<void> => {
  const url = `http://${container.getHost()}:${container.getMappedPort(port)}`;
  const response = await fetch(`${url}`);
  expect(response.status).toBe(404);
};

export const getMappedPort = async (container: StartedTestContainer, port: number): Promise<string> => {
  return `${container.getMappedPort(port)}`;
};

describe("versions uservice integration test", () => {
  jest.setTimeout(10_000);

  let container: StartedTestContainer;
  let mappedPort: string;
  const startingPort = 3500;

  beforeAll(async () => {
    container = await new GenericContainer(`esspass/esspass-versions:${process.env.version}`)
      .withExposedPorts(startingPort).withEnvironment({ PORT: startingPort.toString() })
      .withWaitStrategy(Wait.forLogMessage(`Listening at port: ${startingPort}`))
      .start();

    await checkContainerIsHealthy(container, startingPort);
    mappedPort = await getMappedPort(container, startingPort);
  });

  afterAll(async () => {
    await container.stop();
  });

  it("returns 200 OK on /versions", async () => {
    //given & when
    const response = await fetch(`http://127.0.0.1:${mappedPort}/versions`);

    //then
    expect(response.status).toEqual(200);
    expect(await response.json()).toEqual(
      {
        "backend-web-wallet": {
          "error": {
            "code": "ERR_INVALID_URL",
            "input": "http://undefinedundefined:undefined/version",
            "level": "error"
          }
        },
        "issuer-backend": {
          "error": {
            "code": "ERR_INVALID_URL",
            "input": "http://undefinedundefined:undefined/version",
            "level": "error"
          }
        },
        "issuer-revocation": {
          "error": {
            "input": "http://undefinedundefined:undefined/version",
            "code": "ERR_INVALID_URL",
            "level": "error"
          }
        },
        "issuer-frontend": {
          "error": {
            "code": "ERR_INVALID_URL",
            "input": "http://undefinedundefined:undefined/version",
            "level": "error"
          }
        },
        "issuer-wallet": {
          "error": {
            "code": "ERR_INVALID_URL",
            "input": "http://undefinedundefined:undefined/version",
            "level": "error"
          }
        },
        "versions": { "version": "development" }
      }
    );
  });
});