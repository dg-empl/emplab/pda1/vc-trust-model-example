import { Tracer, logger } from "@esspass-web-wallet/logging-lib";
const applicationName = "versions";

/**
 * OpenTelemetry
 */
const tracer = new Tracer(applicationName);
import { api } from "@opentelemetry/sdk-node";
/**
 * OpenTelemetry
 */

import * as express from "express";
import * as cors from "cors";
import * as bodyParser from "body-parser";

import * as dotenv from "dotenv";

const log = logger(applicationName);

dotenv.config();

log.infoObj("App version : ", process.env.VERSION || "development");

//versions-uservice port
const PORT = 3500;

const app = express();
/* needed for TSL termination */
app.set("trust proxy", true);

app.use(cors());

app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);

app.use(bodyParser.json());

import { versionsRestAdapter } from "./app/adapters/rest/versions-rest-adapter";

/**
 * end-points
 */

function setupRoutes() {
  /**
   * version - version deployed
   */
  app.get("/versions", versionsRestAdapter);
}

function startServer() {
  log.info("Application : " + applicationName);
  log.info("App version : " + process.env.VERSION || "development");
  log.info("Environment : " + process.env.ENVIRONMENT || "development");

  const server = app.listen(PORT, () => log.info(`Versions: listening at port: ${PORT}`));
  server.on("error", log.error);
}

setupRoutes();
startServer();
