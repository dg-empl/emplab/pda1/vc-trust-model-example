# async-comm-lib

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test async-comm-lib` to execute the unit tests via [Jest](https://jestjs.io).
