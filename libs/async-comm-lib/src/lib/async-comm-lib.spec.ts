import { asyncCommLib } from "./async-comm-lib";

describe("asyncCommLib", () => {
  it("should work", () => {
    expect(asyncCommLib()).toEqual("async-comm-lib");
  });
});
