/**
 * @jest-environment node
 */
import * as path from "path";
import * as fs from "fs";

import PizZip from "pizzip";
import Docxtemplater from "docxtemplater";

describe("doc-generator", () => {
  it("should properly generate docx from template", () => {
    //Get the path to the template pda1 docx file
    const docxTemplatePath = path.resolve(__dirname, "../assets/pdaTemplate/pda1PlaceHolder.docx");

    //Load the template pda1 docx file as a binary
    const content = fs.readFileSync(docxTemplatePath);
    expect(content).toBeDefined();

    const zip = new PizZip(content);
    const doc = new Docxtemplater(zip, {
      paragraphLoop: true,
      linebreaks: true,
    });

    expect(doc).toBeDefined();
  });
});
