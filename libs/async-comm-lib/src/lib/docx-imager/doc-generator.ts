import path from "path";
import fs from "fs";
import { DocxImager } from "./docx-imager";
import PizZip from "pizzip";
import Docxtemplater from "docxtemplater";
import { logger } from "@esspass-web-wallet/logging-lib";
import { NotFoundError } from "@esspass-web-wallet/domain-lib";

const log = logger("pda-management");

export async function generateDocx(j, paperQRCodePath: string, pda1Id: string, version = "v2") {
  log.trace("generateDocx:started");

  const stampPath = path.resolve(__dirname, "./assets/images/stampAccepted.svg");
  const docxTemplatePath = path.resolve(__dirname, "./assets/pdaTemplate/pda1PlaceHolder.docx");
  const docxPath = path.resolve(__dirname, `./generated/${version}/${pda1Id}/pda1_${pda1Id}.docx`);

  //Load the template pda1 docx file as a binary
  const content = fs.readFileSync(docxTemplatePath);
  const zip = new PizZip(content);

  const doc = new Docxtemplater(zip);

  //set the templateVariables, mapping between pda1Data and the docx data
  doc.setData({
    sex:
      j.section1.sex === "01"
        ? "☒ Male  ☐ Female  ☐ Other"
        : j.session1.sex === "02"
        ? "☐ Male  ☒ Female  ☐ Other"
        : "☐ Male  ☐ Female  ☒ Other",
    f11: j.section1.personalIdentificationNumber || "",
    f12: j.section1.surname || "",
    f13: j.section1.forenames || "",
    f14: j.section1.surnameAtBirth || "",
    f15: j.section1.dateBirth || "",
    f16: j.section1.nationalities ? j.section1.nationalities[0] || "" : "",
    f17: j.section1.placeBirth ? j.section1.placeBirth.town || "" : "",
    f181: j.section1.stateOfResidenceAddress
      ? j.section1.stateOfResidenceAddress.streetNo || ""
      : "",
    f182: j.section1.stateOfResidenceAddress ? j.section1.stateOfResidenceAddress.town || "" : "",
    f183: j.section1.stateOfResidenceAddress
      ? j.section1.stateOfResidenceAddress.postCode || ""
      : "",
    f184: j.section1.stateOfResidenceAddress
      ? j.section1.stateOfResidenceAddress.countryCode || ""
      : "",
    f191: j.section1.stateOfStayAddress ? j.section1.stateOfStayAddress.streetNo || "" : "",
    f192: j.section1.stateOfStayAddress ? j.section1.stateOfStayAddress.town || "" : "",
    f193: j.section1.stateOfStayAddress ? j.section1.stateOfStayAddress.postCode || "" : "",
    f194: j.section1.stateOfStayAddress ? j.section1.stateOfStayAddress.countryCode || "" : "",
    f21: j.section2.memberStateWhichLegislationApplies || "",
    f22: j.section2.startingDate || "",
    f23: j.section2.endingDate || "",
    f24: j.section2.certificateForDurationActivity ? "☒" : "☐",
    f25: j.section2.determinationProvisional ? "☒" : "☐",
    f26: j.section2.transitionRulesApplyAsEC8832004 ? "☒" : "☐",
    f31: j.section3.postedEmployedPerson ? "☒" : "☐",
    f32: j.section3.employedTwoOrMoreStates ? "☒" : "☐",
    f33: j.section3.postedSelfEmployedPerson ? "☒" : "☐",
    f34: j.section3.selfEmployedTwoOrMoreStates ? "☒" : "☐",
    f35: j.section3.civilServant ? "☒" : "☐",
    f36: j.section3.contractStaff ? "☒" : "☐",
    f37: j.section3.mariner ? "☒" : "☐",
    f38: j.section3.employedAndSelfEmployed ? "☒" : "☐",
    f39: j.section3.civilAndEmployedSelfEmployed ? "☒" : "☐",
    f310: j.section3.flightCrewMember ? "☒" : "☐",
    f311: j.section3.exception ? "☒" : "☐",
    f311d: j.section3.exceptionDescription || "",
    f312: j.section3.workingInStateUnder21 ? "☒" : "☐",
    f411: j.section4.employee ? "☒" : "☐",
    f412: j.section4.selfEmployedActivity ? "☒" : "☐",
    f42: j.section4.employerSelfEmployedActivityCodes
      ? j.section4.employerSelfEmployedActivityCodes[0] || ""
      : "",
    f43: j.section4.nameBusinessName || "",
    f441: j.section4.registeredAddress ? j.section4.registeredAddress.streetNo || "" : "",
    f442: j.section4.registeredAddress ? j.section4.registeredAddress.countryCode || "" : "",
    f443: j.section4.registeredAddress ? j.section4.registeredAddress.town || "" : "",
    f444: j.section4.registeredAddress ? j.section4.registeredAddress.postCode || "" : "",
    f51: j.section5.workPlaceNames ? j.section5.workPlaceNames[0].companyNameVesselName : "",
    f52: j.section5.workPlaceAddresses
      ? `${j.section5.workPlaceAddresses[0].address.streetNo}  -  ${j.section5.workPlaceAddresses[0].address.postCode}  ${j.section5.workPlaceAddresses[0].address.town}  (${j.section5.workPlaceAddresses[0].address.countryCode})`
      : "",
    f53: j.section5.noFixedAddress ? "☒" : "☐",
    f61: j.section6.name || "",
    f62: j.section6.address ? j.section6.address.streetNo || "" : "",
    f63: j.section6.address ? j.section6.address.town || "" : "",
    f64: j.section6.address ? j.section6.address.postCode || "" : "",
    f65: j.section6.address ? j.section6.address.countryCode || "" : "",
    f66: j.section6.institutionID || "",
    f67: j.section6.officeFaxNo || "",
    f68: j.section6.officePhoneNo || "",
    f69: j.section6.email || "",
    f610: j.section6.date || "",
    f611: j.section6.signature || "",
    Stamp: "{{Stamp stamp png 300 100}}",
    QRCode: "{{QRCode qrcode png 300 300}}",
  });

  try {
    // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
    doc.render();
  } catch (error) {
    throw new Error("generateDocx: render" + error);
  }

  const buf = doc.getZip().generate({ type: "nodebuffer" });

  const docxHandler = new DocxImager(log);
  await docxHandler.load(buf); // works with file or buffer, here we use the buffer produced by the previous step

  try {
    await docxHandler.insertImage({
      stamp: { path: stampPath, owType: "png", owWidth: 200, owHeight: 100 },
      qrcode: {
        path: paperQRCodePath,
        owType: "png",
        owWidth: 200,
        owHeight: 200,
      },
    });
  } catch (e) {
    throw new NotFoundError("generateDocx: insertImage : " + e);
  }

  await docxHandler.save(docxPath);

  log.trace("generateDocx:finished");

  return docxPath;
}
