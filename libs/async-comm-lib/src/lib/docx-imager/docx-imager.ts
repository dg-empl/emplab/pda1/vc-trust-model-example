import { Logger } from "@esspass-web-wallet/logging-lib";
import * as fs from "fs";
import JSZip from "jszip";
import * as https from "https";
import path from "path";
import { Builder, parseString } from "xml2js";

const IMAGE_URI = "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image";

const IMAGE_RETRIEVAL_TYPE = {
  URL: "url",
  LOCAL: "local",
};

const IMGPATH = "img_path";
const EXT = "ext";
const HEIGHT = "height";
const WIDTH = "width";
const NAME = "name";
const BUFFER = "buffer";
const MEDIA_PATH = "path";
const REL_ID = "rel_id";
const START_INDEX = "start_index";
const END_INDEX = "end_index";

// USAGE:

// INSERT LOCAL PICTURE
// await docxImager.load("./templates/office_365.docx");
// await docxImager.insertImage({"img1" : "QR-Code.png"});
// await docxImager.save("./instances/office_365__LOCAL.docx");

// INSERT PICTURE FROM URL
// await docxImager.load("./templates/office_old.docx");
// await docxImager.insertImage({"img1" : "https://pngimg.com/uploads/qr_code/qr_code_PNG6.png"});
// await docxImager.save("./instances/office_old__URL.docx");

// REPLACE BY LOCAL PICTURE
// await docxImager.load("./templates/image_to_replace.docx");
// await docxImager.replaceWithLocalImage('./QR-Code2.png','1','png');
// await docxImager.save("./instances/image_to_replace__LOCAL.docx");

// INSERT LOCAL AND URL PICTURES
// await docxImager.load("./templates/Two_images.docx");
// await docxImager.insertImage({"img1" : "QR-Code.png", "img2" : "https://pngimg.com/uploads/qr_code/qr_code_PNG6.png"});
// await docxImager.save("./instances/Two_images__LOCAL_URL.docx");

export class DocxImager {
  private zip: any;
  private docxName: string;
  private readonly log: Logger;
  /**
   * Represents a DocxImager instance
   * @constructor
   */
  constructor(private logger: Logger) {
    this.zip = null;
    this.docxName = null;
    this.log = logger;
  }

  /**
   * Load the DocxImager instance with the docx.
   * @param {String} path_or_buffer full path of the template docx, or the buffer
   * @returns {Promise}
   */
  async load(path_or_buffer: string | Buffer) {
    this.docxName = path_or_buffer as string;
    return this.__loadDocx(path_or_buffer as Buffer).catch(e => {
      this.log.exception("docxImager:load():An exception occurred: [" + e + "].");
    });
  }

  async __loadDocx(path_or_buffer: Buffer) {
    const zip = new JSZip();
    let buffer = path_or_buffer;
    if (!Buffer.isBuffer(path_or_buffer)) {
      buffer = fs.readFileSync(path_or_buffer);
    }
    this.zip = await zip.loadAsync(buffer);
  }

  /**
   * Replaces the template image with the image obtained from the web url
   * @param {String} image_uri web uri of the image
   * @param {String} image_id id of the image in the docx
   * @param {String} type type of the template image
   * @returns {Promise}
   */
  replaceWithImageURL(image_uri, image_id, type) {
    this.__validateDocx();
    const req = https.request(image_uri, res => {
      const buffer = [];
      res.on("data", d => {
        buffer.push(d);
      });
      res.on("end", () => {
        fs.writeFileSync("t1." + type, Buffer.concat(buffer));
        this.__replaceImage(Buffer.concat(buffer), image_id, type);
      });
    });

    req.on("error", e => {
      console.error(e);
    });
    req.end();
  }

  /**
   * Replaces the template image with the image obtained from the local path
   * @param {String} image_path full path of the image in the local system
   * @param {String} image_id id of the image in the docx
   * @param {String} type type of the template image
   * @returns {Promise}
   */
  replaceWithLocalImage(image_path, image_id, type) {
    this.__validateDocx();
    const image_buffer = fs.readFileSync(image_path);
    this.__replaceImage(image_buffer, image_id, type);
  }

  async __replaceImage(buffer, image_id, type) {
    return new Promise((res, rej) => {
      try {
        const path = "word/media/image" + image_id + "." + type;
        this.zip.file(path, buffer);
        res(true);
      } catch (e) {
        rej();
      }
    });
  }

  async insertImage(context) {
    // get the list of all variables
    const variables = await this.__getVariableNames();

    for (const k of Object.keys(context)) {
      if (!Object.keys(variables).includes(k)) {
        return {
          result: "KO",
          errorDescription: `Picture '${k}' not referenced in '${this.docxName}'!`,
        };
      }
    }

    for (const k of Object.keys(context)) {
      if (typeof context[k] === "string") context[k] = { path: context[k] };
    }

    // download/retrieve images
    const buffers = await this.__getImages(variables, context);

    const final_context = {};
    for (const var_name in context) {
      // coming from placeholder on docx
      const ext = path.extname(context[var_name].path).slice(1);
      const width = variables[var_name][WIDTH];
      const height = variables[var_name][HEIGHT];

      // overwriting from context
      const owExt = context[var_name].owExt;
      const owWidth = context[var_name].owWidth;
      const owHeight = context[var_name].owHeight;

      final_context[var_name] = {};
      final_context[var_name][IMGPATH] = context[var_name].path;
      final_context[var_name][NAME] = variables[var_name][NAME]; //img1
      final_context[var_name][EXT] = owExt ? owExt : ext ? ext : "svg";
      final_context[var_name][
        MEDIA_PATH
      ] = `media/${final_context[var_name][NAME]}.${final_context[var_name][EXT]}`;
      final_context[var_name][WIDTH] = owWidth ? owWidth : width ? width : 300;
      final_context[var_name][HEIGHT] = owHeight ? owHeight : height ? height : 300;
      final_context[var_name][START_INDEX] = variables[var_name][START_INDEX];
      final_context[var_name][END_INDEX] = variables[var_name][END_INDEX];
      final_context[var_name][BUFFER] = buffers[var_name];
      final_context[var_name][REL_ID] = "XXX"; // to be deduced from relationships
    }

    // insert entry in /word/_rels/document.xml.rels
    const relationships = await this._addRelationship(final_context);

    for (const var_name in context) {
      final_context[var_name][REL_ID] = relationships[var_name][REL_ID];
    }

    // insert entry in [Content-Type].xml
    await this._addContentType(final_context);

    // write image in media folder in word/
    await this._addImage(final_context);

    // insert in document.xml after calculating EMU.
    await this._addInDocumentXML(final_context);

    return { result: "OK" };
  }

  /**
   * Saves the transformed docx.
   * @param {String} op_file_name Output file name with full path.
   * @returns {Promise}
   */
  async save(op_file_name = "./instances/") {
    let outName = op_file_name;
    if (op_file_name.endsWith("/")) {
      outName = `${op_file_name}${path.basename(
        this.docxName,
        path.extname(this.docxName)
      )}.instance${path.extname(this.docxName)}`;
    }

    return new Promise((res, rej) => {
      this.zip
        .generateNodeStream({ streamFiles: true })
        .pipe(fs.createWriteStream(outName))
        .on("finish", () => res({ result: "OK", fileName: outName }));
    });
  }

  // when a change is made in the docx file, Word adds tags (like <w:r>...</w:r>) that alter the expected searched string
  // whose format must remain : {{insert_image img1 png 300 300}}. So first we have to clean it up, that may have become :
  // {{insert_image img</w:t></w:r><w:r w:rsidR="006E34F9"><w:t>2</w:t></w:r><w:r w:rsidRPr="008F4FDE"><w:t xml:space="preserve"> png 300 </w:t></w:r><w:r w:rsidR="006E34F9"><w:t>3</w:t></w:r><w:r w:rsidRPr="008F4FDE"><w:t>00}}
  static __cleanXML(str) {
    const regex = /<[^>]*>/g;
    return str.replace(regex, "");
  }

  async __getVariableNames() {
    return new Promise(async (res, rej) => {
      try {
        let content = await this.zip.file("word/document.xml").async("nodebuffer");
        content = content.toString();
        const variables = {};

        const regex = new RegExp(/{{(.*?)}}/g);
        let matches;
        let c = 0;
        while ((matches = regex.exec(content))) {
          c++;
          if (matches.length) {
            const xml = DocxImager.__cleanXML(matches[1]);
            const splits = xml.split(" ");
            const node = {};
            node[NAME] = splits[1];
            //node[EXT] = splits[2]? splits[2] : "svg"; // can be overwritten by context later
            splits[2] = "";
            node[WIDTH] = splits[3] ? splits[3] : 200; // can be overwritten by context later
            node[HEIGHT] = splits[4] ? splits[4] : 200; // can be overwritten by context later
            node[START_INDEX] = matches.index;
            node[END_INDEX] = regex.lastIndex - 1;
            variables[splits[1]] = node;
          }
        }
        if (c > 0) res(variables);
        else rej(new Error("Invalid Docx: no section {{...}} found!"));
      } catch (e) {
        this.log.exception("docxImager:__getVariableNames : " + e);
        rej(e);
      }
    });
  }

  async __getImages(variables, context) {
    // context (from the parameters of the call):   { img1: 'QR-Code.png' }
    // variables (from the docx): { img1: { type: 'png', width: '300', height: '500}}' } }

    return new Promise(async (res, rej) => {
      try {
        const buffers = {};
        for (const variable_name in context) {
          if (variables.hasOwnProperty(variable_name)) {
            const path = context[variable_name].path;

            const isValidHttpUrl = str => {
              let url;

              try {
                url = new URL(str);
              } catch (_) {
                return false;
              }

              return url.protocol === "http:" || url.protocol === "https:";
            };

            let img_retrieval_type = IMAGE_RETRIEVAL_TYPE.LOCAL;
            if (isValidHttpUrl(path)) img_retrieval_type = IMAGE_RETRIEVAL_TYPE.URL;

            buffers[variable_name] = await this.__getImageBuffer(path, img_retrieval_type);
          }
        }
        res(buffers);
      } catch (e) {
        this.log.exception("docxImager:__getImages():An exception occurred: [" + e + "].");
        rej(e);
      }
    });
  }

  async __getImageBuffer(path, retrieval_type) {
    if (retrieval_type === IMAGE_RETRIEVAL_TYPE.LOCAL) {
      // image is local
      const buffer = fs.readFileSync(path);
      return buffer;
    } else if (retrieval_type === IMAGE_RETRIEVAL_TYPE.URL) {
      // image is URL

      return new Promise((res, rej) => {
        try {
          const req = https.request(path, result => {
            const buffer = [];
            result.on("data", d => buffer.push(d));
            result.on("end", () => res(Buffer.concat(buffer)));
          });
          req.on("error", e => {
            throw e;
          });
          req.end();
        } catch (e) {
          this.log.exception("docxImager:__getImageBuffer():An exception occurred: [" + e + "].");
          rej(e);
        }
      });
    }
    return new Promise((res, rej) => rej("Wrong IMAGE_RETRIEVAL_TYPE"));
  }

  async _addContentType(final_context) {
    return new Promise(async (res, rej) => {
      try {
        const content = await this.zip.file("[Content_Types].xml").async("nodebuffer");
        const contentStr = content.toString();
        let new_str = "";
        for (const var_name in final_context) {
          if (final_context.hasOwnProperty(var_name)) {
            new_str +=
              '<Override PartName="/word/' +
              final_context[var_name][MEDIA_PATH] +
              '" ContentType="' +
              final_context[var_name][EXT] +
              '"/>';
          }
        }
        const c =
          contentStr.replace("</Types>", "") +
          new_str +
          (contentStr.slice(contentStr.length - "</Types>".length) === "</Types>"
            ? "</Types>"
            : "");
        this.zip.file("[Content_Types].xml", c);
        res(true);
        //}
      } catch (e) {
        this.log.exception("docxImager:_addContentType():An exception occurred: [" + e + "].");
        rej(e);
      }
    });
  }

  async _addImage(final_context) {
    return new Promise(async (res, rej) => {
      try {
        for (const var_name in final_context) {
          if (final_context.hasOwnProperty(var_name)) {
            this.zip.file(
              "word/" + final_context[var_name][MEDIA_PATH],
              final_context[var_name][BUFFER]
            );
          }
        }
        res(true);
      } catch (e) {
        this.log.exception("docxImager:_addImage():An exception occurred: [" + e + "].");
        rej(e);
      }
    });
  }

  async _addRelationship(final_context) {
    return new Promise(async (res, rej) => {
      try {
        const content = await this.zip.file("word/_rels/document.xml.rels").async("nodebuffer");
        parseString(content.toString(), (err, relation) => {
          if (err) {
            this.log.exception(
              "docxImager:_addRelationship():An exception occurred (parseString): [" + err + "]."
            );
            rej(err);
          }
          let cnt = relation.Relationships.Relationship.length;
          const relationships = {};
          for (const var_name in final_context) {
            if (final_context.hasOwnProperty(var_name)) {
              //let o = final_context[var_name];
              const rel_id = "rId" + ++cnt;
              relationships[var_name] = {};
              relationships[var_name][REL_ID] = rel_id;
              relation.Relationships.Relationship.push({
                $: {
                  Id: rel_id,
                  Type: IMAGE_URI,
                  Target: final_context[var_name][MEDIA_PATH],
                },
              });
            }
          }
          const builder = new Builder();
          const modifiedXML = builder.buildObject(relation);
          this.zip.file("word/_rels/document.xml.rels", modifiedXML);
          res(relationships);
        });
      } catch (e) {
        this.log.exception("docxImager:_addRelationship():An exception occurred: [" + e + "].");
        rej(e);
      }
    });
  }

  async _addInDocumentXML(final_context) {
    const sorted = [];
    for (const k in final_context) sorted.push(final_context[k]);
    sorted.sort((a, b) => a[START_INDEX] - b[START_INDEX]); // sorted in place

    return new Promise(async (res, rej) => {
      try {
        let contentStr = (await this.zip.file("word/document.xml").async("nodebuffer")).toString();
        let accu = 0;

        for (const obj of sorted) {
          const new_xml = "</w:t></w:r>" + DocxImager.__getImgXMLElement(obj) + "<w:r><w:t>";
          contentStr =
            contentStr.slice(0, obj[START_INDEX] + accu) +
            new_xml +
            contentStr.slice(obj[END_INDEX] + 1 + accu);
          accu += new_xml.length - (obj[END_INDEX] - obj[START_INDEX] + 1);
        }
        this.zip.file("word/document.xml", contentStr);
        res(true);
      } catch (e) {
        this.log.exception("docxImager:_addInDocumentXML(): " + e);
        rej(e);
      }
    });
  }

  static __getImgXMLElement(obj) {
    // http://polymathprogrammer.com/2009/10/22/english-metric-units-and-open-xml/
    // https://startbigthinksmall.wordpress.com/2010/01/04/points-inches-and-emus-measuring-units-in-office-open-xml/
    // http://officeopenxml.com/anatomyofOOXML.php
    // http://officeopenxml.com/drwPic.php

    const height = obj[HEIGHT]; // px
    const width = obj[WIDTH]; // px
    const rId = obj[REL_ID];
    const img = `${obj[NAME]}.${obj[EXT]}`;

    // width and height calculated assuming resolution as 96 dpi
    // 1 inch = 914400 EMU ; suppose we work with 96dpi screen (capable of displaying 96 pixels per inch),
    // then an image with width of 300 pixels correspond to 300px / 96px/inch * 914499 EMU/inch = 300px * 9525 EMU/px = 2857500 EMU
    const EMU_height = 9525 * height; // EMU
    const EMU_width = 9525 * width; // EMU

    return (
      "<w:r>" +
      "<w:rPr>" +
      "<w:noProof/>" +
      "</w:rPr>" +
      `<w:drawing>
                    <wp:inline distT="0" distB="0" distL="0" distR="0">
                        <wp:extent cx="${EMU_width}" cy="${EMU_height}"/>
                        <wp:effectExtent l="0" t="0" r="0" b="0"/>
                        <wp:docPr id="99${rId.slice(
                          3
                        )}" name="${img}" title="${img}" descr="${img}"/>
                        <wp:cNvGraphicFramePr>
                            <a:graphicFrameLocks xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" noChangeAspect="true"/>
                        </wp:cNvGraphicFramePr>
                        <a:graphic xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main">
                            <a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/picture">
                                <pic:pic xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture">
                                    <pic:nvPicPr>
                                        <pic:cNvPr id="11${rId.slice(
                                          3
                                        )}" name="${img}" title="${img}" descr="${img}"/>
                                        <pic:cNvPicPr>
                                            <a:picLocks noChangeAspect="true" noResize="true" />
                                        </pic:cNvPicPr>
                                        <pic:nvPr/>
                                    </pic:nvPicPr>
                                    <pic:blipFill>
                                        <a:blip r:embed="${rId}" cstate="hqprint"/>
                                        <a:srcRect/>
                                        <a:stretch>
                                            <a:fillRect/>
                                        </a:stretch>
                                    </pic:blipFill>
                                    <pic:spPr bwMode="auto">
                                        <a:xfrm>
                                            <a:off x="0" y="0"/>
                                            <a:ext cx="${EMU_width}" cy="${EMU_height}"/>
                                        </a:xfrm>
                                        <a:prstGeom prst="rect">
                                            <a:avLst/>
                                        </a:prstGeom>
                                        <a:noFill/>
                                        <a:ln>
                                            <a:noFill/>
                                        </a:ln>
                                    </pic:spPr>
                                </pic:pic>
                            </a:graphicData>
                        </a:graphic>
                    </wp:inline>
                </w:drawing>` +
      "</w:r>"
    );
  }

  __validateDocx() {
    if (!this.zip) {
      throw new Error("Invalid docx path or format. Please load docx.");
    }
  }
}
