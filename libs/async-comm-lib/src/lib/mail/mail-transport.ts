import * as fs from "fs";
import * as path from "path";
import * as nodemailer from "nodemailer";
import { logger } from "@esspass-web-wallet/logging-lib";

const log = logger("pda-management");

const transporter = nodemailer.createTransport({
  host: process.env.MAIL_HOST,
  port: parseInt(<string>process.env.MAIL_PORT, 10),
  secure: process.env.MAIL_SECURE === "true",
  auth: {
    user: process.env.MAIL_USER,
    pass: process.env.MAIL_PASSWORD,
  },
});

export async function sendEmailPDA1(
  senderEmail: string,
  receiverName: string,
  receiverEmail: string,
  attachmentDocxPath: string,
  attachmentQRCodePath: string,
  regDate: string
) {
  try {
    log.trace("sendEmailPDA1:content:started.");

    const emailTemplatePath = path.resolve(__dirname, "./assets/mailTemplate/PDA1.html");

    const mailTemplate = fs.readFileSync(emailTemplatePath, "utf8");

    const regex = /\[\[SENDEREMAIL\]\]/g;

    const messageBody = mailTemplate
      .replace("[[NAME]]", receiverName)
      .replace("[[DD/MM/YYYY]]", regDate)
      .replace(regex, senderEmail);

    const result = await transporter.sendMail({
      from: senderEmail,
      to: receiverEmail,
      subject: "A PDA1 form has been issued for you",
      html: messageBody,
      attachments: [
        {
          filename: "PDA1.docx",
          path: attachmentDocxPath,
        },
        {
          filename: "QRCode.png",
          path: attachmentQRCodePath,
        },
      ],
    });

    log.trace("sendEmailPDA1:content:" + JSON.stringify(result));
    log.trace("sendEmailPDA1:content:finished");
    return result;
  } catch (ex) {
    throw new Error("sendEmailPDA1" + ex);
  }
}

export async function sendEmailPin(
  senderEmail: string,
  receiverName: string,
  receiverEmail: string,
  pin: string,
  regDate: string
) {
  try {
    log.trace("sendEmailPin:pin:started.");

    const emailTemplatePath = path.resolve(__dirname, "./assets/mailTemplate/Pin.html");

    const mailTemplate = fs.readFileSync(emailTemplatePath, "utf8");

    const regex = /\[\[SENDEREMAIL\]\]/g;

    const messageBody = mailTemplate
      .replace("[[NAME]]", receiverName)
      .replace("[[DD/MM/YYYY]]", regDate)
      .replace("[[PIN]]", pin)
      .replace(regex, senderEmail);

    const result = await transporter.sendMail({
      from: senderEmail,
      to: receiverEmail,
      subject: "A PIN has been issued for you",
      html: messageBody,
    });

    log.trace("sendEmailPDA1:pin: " + JSON.stringify(result));
    log.trace("sendEmailPDA1:pin:finished");
    return result;
  } catch (ex) {
    throw new Error("sendEmailPin" + ex);
  }
}

export async function sendEmailGetSignedVC(
  senderEmail: string,
  receiverName: string,
  receiverEmail: string,
  attachmentPath: string,
  regDate: string
) {
  try {
    log.trace("sendEmailGetSignedVC:Started.");

    const emailTemplatePath = path.resolve(__dirname, "../mailTemplate/getSignedVC.html");

    const mailTemplate = fs.readFileSync(emailTemplatePath, "utf8");

    const regex = /\[\[SENDEREMAIL\]\]/g;

    const messageBody = mailTemplate
      .replace("[[NAME]]", receiverName)
      .replace("[[DD/MM/YYYY]]", regDate)
      .replace(regex, senderEmail);

    const result = await transporter.sendMail({
      from: senderEmail,
      to: receiverEmail,
      subject: "Your Wallet PDA1",
      html: messageBody,
      attachments: [
        {
          filename: "QRCode.png",
          path: attachmentPath,
        },
      ],
    });

    log.trace("sendEmailGetSignedVC " + result);
    log.trace("toolBoxPDA1:sendEmailGetSignedVC:Done.");
    return result;
  } catch (ex) {
    new Error("sendEmailGetSignedVC : " + ex);
  }
}
