import { QRCodeToFileOptions, toFile } from "qrcode";
import { Pda1Type } from "@esspass-web-wallet/domain-lib";
import { existsSync, mkdirSync } from "fs";
import * as path from "path";
import { PreAuthCodeGrants } from "@esspass-web-wallet/crypto-lib";

import { logger } from "@esspass-web-wallet/logging-lib";

const log = logger("pda-management");

export type QrCodeOptions = {
  qrCodePath: string;
  encodedTxt: string;
  outputType: string;
};

/**
 * Common function to generate QR code
 * params
 *
 * @param options : QrCodeOptions
 * @param color - color of QR code
 **/
export async function generateQrCodeToFile(options: QrCodeOptions, color = "#000"): Promise<void> {
  await toFile(options.qrCodePath, options.encodedTxt, {
    type: options.outputType,
    width: 360,
    color: { dark: color },
  } as QRCodeToFileOptions);
}

/**
 * Generate a QR code for a paper PDA1
 * @param pdaDocument
 * @param requestorEmail
 * @param pda1Id
 * @param colorHex
 */
export async function generatePaperQRCode(
  pdaDocument: Pda1Type,
  requestorEmail: string,
  pda1Id: string,
  colorHex = "#000", //black
  version = "v2"
) {
  try {
    log.trace("generatePaperQRCode:started");

    const directoryPath = path.resolve(__dirname, `./generated/${version}/${pda1Id}`);

    if (!existsSync(directoryPath)) {
      log.trace("generatePaperQRCode:directory does not exist, creating it.");
      mkdirSync(directoryPath, { recursive: true });
    }

    const encodedTxt = `${pda1Id}|${pdaDocument.section6.institutionID}|${pdaDocument.section1.surname}|${pdaDocument.section1.forenames}|${pdaDocument.section1.personalIdentificationNumber}|${pdaDocument.section1.sex}|${requestorEmail}|${pdaDocument.section1.nationalities}|${pdaDocument.section2.memberStateWhichLegislationApplies}|${pdaDocument.section2.startingDate}|${pdaDocument.section2.endingDate}`;

    const outputType = "png";

    const qrCodePath = path.resolve(
      __dirname,
      `./generated/${version}/${pda1Id}/qrcode_paper_${pda1Id}.png`
    );
    await generateQrCodeToFile({ qrCodePath, encodedTxt, outputType }, colorHex);

    log.trace("generatePaperQRCode:finished");

    return qrCodePath;
  } catch (ex) {
    throw new Error("generatePaperQRCode : " + ex);
  }
}

export async function generateQrCode(
  preAuthCode: PreAuthCodeGrants,
  pda1Id: string,
  color = "#00F" // blue
): Promise<string> {
  try {
    log.trace("generateQrCode:started.");

    const directoryPath = path.resolve(__dirname, `./generated/v2/${pda1Id}`);

    if (!existsSync(directoryPath)) {
      mkdirSync(directoryPath, { recursive: true });
    }

    const encodedTxt = JSON.stringify({ ...preAuthCode });

    const outputType = "png";
    log.trace("qr-code content:" + encodedTxt);

    const qrCodePath = path.resolve(__dirname, `./generated/v2/${pda1Id}/qrcode_iii_${pda1Id}.png`);
    await generateQrCodeToFile({ qrCodePath, encodedTxt, outputType }, color);

    log.trace("generateQrCode:finished");

    return qrCodePath;
  } catch (ex) {
    throw new Error("generateQrCode : " + ex);
  }
}
