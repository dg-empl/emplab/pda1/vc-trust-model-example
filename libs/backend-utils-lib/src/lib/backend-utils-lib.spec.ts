import { backendUtilsLib } from "./backend-utils-lib";

describe("backendUtilsLib", () => {
  it("should work", () => {
    expect(backendUtilsLib()).toEqual("backend-utils-lib");
  });
});
