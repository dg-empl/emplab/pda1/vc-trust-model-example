import mongoose from "mongoose";
import { Logger } from "@esspass-web-wallet/logging-lib";

export function buildDbUrl(dbHost, user, password, port, databaseName: string) {
  return `mongodb://${user}:${password}@${dbHost}:${port}/${databaseName}?authSource=admin`;
}

mongoose.set("strictQuery", true);

import * as dotenv from "dotenv";

dotenv.config();

export class Database {
  private static _database: Database;

  private static dbHost: string;
  private static dbUsername: string;
  private static dbPassword: string;
  private static dbPort: string;
  private static dbName: string;

  private static log: Logger;

  private constructor() {
    Database.log.info("process.env.DB_HOST : " + process.env["DB_HOST"]);
    Database.log.info("process.env.DB_PORT : " + process.env["DB_PORT"]);

    mongoose
      .connect(
        buildDbUrl(
          Database.dbHost,
          Database.dbUsername,
          Database.dbPassword,
          Database.dbPort,
          Database.dbName
        )
      )
      .then(() => Database.log.info("Connected with database : " + Database.dbName))
      .catch(e => Database.log.info("Not connected with database " + e));
  }

  static getInstance(
    dbHost: string,
    dbUsername: string,
    dbPassword: string,
    dbPort: string,
    dbName: string,
    log: Logger
  ) {
    Database.dbHost = dbHost;
    Database.dbUsername = dbUsername;
    Database.dbPassword = dbPassword;
    Database.dbPort = dbPort;
    Database.dbName = dbName;

    Database.log = log;

    if (this._database) {
      return this._database;
    }
    this._database = new Database();
    return this._database;
  }
}
