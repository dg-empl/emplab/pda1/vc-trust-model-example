import { OAuth2Error, OAuth2ErrorOptions } from "./OAuth2Error";

/**
 * Credential Error
 * @see https://openid.net/specs/openid-4-verifiable-credential-issuance-1_0-11.html#section-7.3.1
 */
export type ConversionErrorCode =
  /**
   * Conversion cannot be made
   */
  "invalid_conversion_action";

export class ConversionError extends OAuth2Error<ConversionErrorCode> {
  constructor(errorCode: ConversionErrorCode, options?: OAuth2ErrorOptions) {
    super(errorCode, options);
    this.name = "ConversionErrorCode";
  }
}

export default ConversionError;
