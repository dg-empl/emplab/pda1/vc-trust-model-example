import { isErrorType } from "./error-handler";

describe("error-handler", () => {
  it("should work", () => {
    //given
    const error = {
      status: 400,
      message: '{"errorCode":418,"errorDescription":"No vc found !"}',
    };

    //when
    const res = isErrorType(error);

    //then
    expect(res).toBeTruthy();

    const statusCode = JSON.parse(error.message).errorCode;
    const errorMessage = JSON.parse(error.message).errorDescription;
    console.log(statusCode, errorMessage);
  });
});
