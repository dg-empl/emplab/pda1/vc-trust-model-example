import { Request, Response, NextFunction } from "express";
import { Logger } from "@esspass-web-wallet/logging-lib";
import { ErrorType, CustomError } from "@esspass-web-wallet/domain-lib";
import OAuth2Error from "./OAuth2Error";
import ConversionError from "./ConversionError";

interface ErrorResponse {
  message: string;
  status: number;
}

export function isErrorType(error: unknown): error is ErrorType {
  return typeof error === "object" && error !== null && "message" in error;
}
export const errorHandler = (log: Logger) => {
  return (error: Error, req: Request, res: Response, next: NextFunction) => {
    let statusCode: number;
    let errorMessage: string;

    log.error(error["errorDescription"] ?? error.message ?? error);

    if (error instanceof CustomError) {
      return res.status(error.statusCode).send({ errors: error.serializeErrors() });
    }

    if (isErrorType(error)) {
      if (error.message["errorCode"]) {
        statusCode = JSON.parse(error.message)["errorCode"] ?? 418;
        errorMessage = JSON.parse(error.message);
      } else if (error instanceof OAuth2Error) {
        statusCode = 400;
        errorMessage = error["errorDescription"];
      } else if (error instanceof ConversionError) {
        statusCode = 418;
        errorMessage = error["errorDescription"];
      } else {
        statusCode = 418;
        return res.status(Number(statusCode)).json({
          errors: [
            {
              message: error.message,
            },
          ],
        });
      }

      return res.status(Number(statusCode)).json(errorMessage);
    } else {
      statusCode = 500;
      errorMessage = "Internal Server Error";
    }
    const response: ErrorResponse = {
      status: statusCode,
      message: errorMessage,
    };

    log.debug("" + response.status + "" + response.message);

    return res.status(response.status).json(response);
  };
};
