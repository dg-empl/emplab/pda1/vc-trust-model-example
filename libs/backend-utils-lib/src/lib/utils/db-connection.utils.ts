import { MongoMemoryServer } from "mongodb-memory-server";
import { MongoClient } from "mongodb";
import mongoose from "mongoose";
import { Logger } from "@esspass-web-wallet/logging-lib";
import { buildDbUrl } from "../db/open-db-connection";

export const createMongoMemoryServer = () => {
  return MongoMemoryServer.create({
    instance: {
      ip: process.env.DB_HOST,
      port: +process.env.DB_PORT,
    },
    auth: {
      enable: true,
      customRootName: process.env.DB_USERNAME,
      customRootPwd: process.env.DB_PASSWORD,
    },
  });
};

export const memoryServerConnect = async (mongoServer: MongoMemoryServer): Promise<MongoClient> => {
  const uri = mongoServer.getUri();
  return await MongoClient.connect(uri, {
    auth: { username: process.env.DB_USERNAME, password: process.env.DB_PASSWORD },
  });
};

export const closeDatabase = async (mongoServer: MongoMemoryServer) => {
  // await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
  await mongoServer.stop();
};

// export const clearDatabase = async (mongoServer: MongoMemoryServer) => {
//   const collections = mongoose.connection.collections;
//   for (const key in collections) {
//     const collection = collections[key];
//     await collection.deleteMany({});
//   }
// };

export const connect = (databaseName: string, log: Logger) => {
  if (!databaseName) {
    throw new Error("Database name is not defined");
  }
  log.info("process.env.DB_HOST : " + process.env.DB_HOST);
  log.info("process.env.DB_PORT : " + process.env.DB_PORT);
  log.info("process.env.DB_USERNAME : " + process.env.DB_USERNAME);
  log.info("process.env.DB_PASSWORD : " + process.env.DB_PASSWORD);

  const connection = buildDbUrl(
    process.env.DB_HOST,
    process.env.DB_USERNAME,
    process.env.DB_PASSWORD,
    process.env.DB_PORT,
    databaseName
  );

  mongoose.connect(connection).then(
    () => log.info("Mongoose opened connection"),
    err => {
      log.error("Mongoose error opening connection " + err);
    }
  );
};

export async function disconnect(log: Logger) {
  log.info("Mongoose disconnecting");
  await mongoose.disconnect();
}
