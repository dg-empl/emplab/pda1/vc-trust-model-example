import { MongoClient, ObjectId } from "mongodb";

export const createTestData = async (
  uri: string,
  dbName: string,
  testData: Record<string, any>
) => {
  let client: MongoClient;
  try {
    client = await MongoClient.connect(uri, {
      auth: { username: process.env.DB_USERNAME, password: process.env.DB_PASSWORD },
    });
    const db = client.db(dbName);

    const col = db.collection(testData.collection);
    const document = { ...testData.data, _id: new ObjectId(testData.id) };
    const result = await col.insertOne(document);
    const cursor = await col.find(testData.check).toArray();

    if (!result || !cursor || cursor.length === 0 || cursor.length > 1) {
      throw new Error("Test data not inserted");
    }
  } catch (e) {
    console.error(e);
    throw new Error(e);
  } finally {
    await client.close();
  }
};

export const purgeTestData = async (uri: string, dbName: string, testData: Record<string, any>) => {
  // Connect to the container using the MongoDB driver
  let client: MongoClient;
  try {
    client = await MongoClient.connect(uri, {
      auth: { username: process.env.DB_USERNAME, password: process.env.DB_PASSWORD },
    });

    const db = client.db(dbName);
    const col = db.collection(testData.collection);
    const result = await col.drop();

    if (!result) {
      throw new Error("Collection not dropped");
    }
  } catch (e) {
    console.error(e);
    throw new Error(e);
  } finally {
    await client.close();
  }
};

export const deleteTestData = async (
  uri: string,
  dbName: string,
  testData: Record<string, any>
) => {
  // Connect to the container using the MongoDB driver
  let client: MongoClient;
  try {
    client = await MongoClient.connect(uri, {
      auth: { username: process.env.DB_USERNAME, password: process.env.DB_PASSWORD },
    });

    const db = client.db(dbName);
    const col = db.collection(testData.collection);
    const result = await col.deleteOne(testData.filter);

    if (!result) {
      throw new Error("Test data not deleted");
    }
  } catch (e) {
    console.error(e);
    throw new Error(e);
  } finally {
    await client.close();
  }
};
