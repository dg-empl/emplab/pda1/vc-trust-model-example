import { NgModule } from "@angular/core";
import { CommonModule, NgOptimizedImage } from "@angular/common";
import { LoginComponent } from "./components/login/login.component";
import { SpinnerComponent } from "./components/spinner/spinner.component";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { SpinnerTableComponent } from "./components/spinner/ep-spinner-table.component";
import { FooterComponent } from "./components/footer/footer.component";
import { LoginButtonComponent } from "./components/login-button/login-button.component";
import { ErrorHandlingModule } from "./error-handling";

@NgModule({
  declarations: [
    LoginComponent,
    SpinnerComponent,
    SpinnerTableComponent,
    FooterComponent,
    LoginButtonComponent,
  ],
  imports: [CommonModule, NgOptimizedImage, MatProgressSpinnerModule, ErrorHandlingModule],
  exports: [LoginComponent, SpinnerComponent, FooterComponent, LoginButtonComponent],
})
export class CommonWebLibModule {}
