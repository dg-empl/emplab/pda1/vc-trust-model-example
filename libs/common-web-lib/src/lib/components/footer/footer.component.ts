import { Component, Input } from "@angular/core";
import * as process from "process";

@Component({
  selector: "issuer-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.scss"],
})
export class FooterComponent {
  @Input({ required: true }) officeName: string;
  version = process.env["APP_VERSION"] || "development";
}
