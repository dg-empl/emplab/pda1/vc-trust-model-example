import { Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
  selector: "issuer-login-button",
  templateUrl: "./login-button.component.html",
  styleUrls: ["./login-button.component.scss"],
})
export class LoginButtonComponent {
  @Input({ required: true }) isLoggedIn: boolean;
  @Input({ required: true }) loggedLabel: string;
  @Output() emitter = new EventEmitter<{ action: string }>();

  action(name: string) {
    this.emitter.emit({ action: name });
  }
}
