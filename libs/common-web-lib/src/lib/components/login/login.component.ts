import { Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
  selector: "issuer-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent {
  @Input({ required: true }) isLoggedIn: boolean;
  @Output() loginEvent: EventEmitter<boolean> = new EventEmitter<boolean>(false);

  logout() {
    this.loginEvent.emit(false);
  }

  login() {
    this.loginEvent.emit(true);
  }
}
