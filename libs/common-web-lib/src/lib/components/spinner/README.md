## How to use spinner

This is the basic structure expected to use a spinner on a table.

Example:

```html
<div class="mat-elevation-z8 position-relative">
  <table class="w-100" mat-table [dataSource]="dataSource" matSort>
    (...)
  </table>
  <digit-epso-spinner-table
    [isEmpty]="!hasData()"
    *ngIf="(dataSource.loading$ | async)"></digit-epso-spinner-table>
  ( ... no results component ... ) ( ... paginator component ... )
</div>
```

The important part is the "position-relative" class. The spinner is an absolute component and needs a relative parent. This way the spinner and backsdrop fills the whole table component.
