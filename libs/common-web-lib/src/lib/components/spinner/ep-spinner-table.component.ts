import { Component, Input } from "@angular/core";

@Component({
  selector: "issuer-spinner-table",
  styleUrls: ["./spinner-table.component.scss"],
  templateUrl: "./spinner-table.component.html",
})
export class SpinnerTableComponent {
  @Input() isEmpty? = true;
}
