import { Component, Input } from "@angular/core";
import { NumberInput } from "@angular/cdk/coercion";

@Component({
  selector: "issuer-spinner",
  styleUrls: ["./spinner.component.scss"],
  templateUrl: "./spinner.component.html",
})
export class SpinnerComponent {
  @Input() htmlLabelExpression: string;
  @Input() diameter?: NumberInput;
  @Input() alertTypeClass?: string;
  @Input() isInline? = false;
}
