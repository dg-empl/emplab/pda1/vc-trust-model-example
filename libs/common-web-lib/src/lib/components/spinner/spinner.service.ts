import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable({ providedIn: "root" })
export class SpinnerService {
  private data = new BehaviorSubject<boolean>(false);
  data$ = this.data.asObservable();

  load() {
    this.data.next(true);
  }

  stop() {
    this.data.next(false);
  }
}
