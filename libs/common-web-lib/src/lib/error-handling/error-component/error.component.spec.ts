import { Location } from "@angular/common";
import { SpyLocation } from "@angular/common/testing";
import { Provider } from "@angular/core";
import { waitForAsync, ComponentFixture, TestBed } from "@angular/core/testing";
import { By } from "@angular/platform-browser";
import { ActivatedRoute } from "@angular/router";
import { RouterTestingModule } from "@angular/router/testing";

import { ErrorComponent } from "./error.component";

describe("ErrorComponent", () => {
  let component: ErrorComponent;
  let fixture: ComponentFixture<ErrorComponent>;

  const test_title1 = "Test title 1";
  const test_title2 = "Test title 2";
  const test_message1 = "Test message 1";
  const test_message2 = "Test message 2";

  function createComponent(providers: Provider[] = []) {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [ErrorComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: { data: {} },
          },
        },
        ...providers,
      ],
    });

    fixture = TestBed.createComponent(ErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }

  function getDOMElement(selector: string) {
    return fixture.debugElement.query(By.css(selector));
  }

  describe("Default values", () => {
    beforeEach(waitForAsync(() => {
      createComponent([
        {
          provide: Location,
          useClass: SpyLocation,
        },
      ]);
    }));

    it("should create", () => {
      expect(component).toBeTruthy();
    });

    it("should populate template with default values", () => {
      // given
      const title = getDOMElement(".error-title").nativeElement.textContent;
      const message = getDOMElement(".error-messages strong").nativeElement.textContent;
      const support = getDOMElement(".error-support");

      // then
      expect(title).toBe("Oops...");
      expect(message).toBe("Something went wrong");
      expect(support).toBeFalsy();
    });

    it("should navigate back on button click", () => {
      // given
      const location: Location = TestBed.inject(Location);
      jest.spyOn(location, "back");
      const btn = fixture.debugElement.query(By.css("button"));

      // when
      btn.triggerEventHandler("click", null);

      // then
      expect(location.back).toHaveBeenCalled();
    });
  });

  describe("ActivatedRoute", () => {
    beforeEach(waitForAsync(() => {
      createComponent([
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              data: {
                title: test_title1,
                message: test_message1,
                helpText: true,
              },
            },
          },
        },
      ]);
    }));

    it("should create", () => {
      expect(component).toBeTruthy();
    });

    it("should populate template with values from activated route", () => {
      // given
      const title = getDOMElement(".error-title").nativeElement.textContent;
      const message = getDOMElement(".error-messages strong").nativeElement.textContent;
      const support = getDOMElement(".error-support");

      // when
      fixture.detectChanges();

      // then
      expect(title).toBe(test_title1);
      expect(message).toBe(test_message1);
      expect(support).toBeTruthy();
    });
  });

  describe("History", () => {
    beforeEach(waitForAsync(() => {
      window.history.pushState(
        {
          customError: {
            title: test_title2,
            message: test_message2,
            helpText: true,
          },
        },
        "",
        ""
      );
      createComponent();
    }));

    it("should create", () => {
      expect(component).toBeTruthy();
    });

    it("should populate template with values from history", () => {
      // given
      const title = getDOMElement(".error-title").nativeElement.textContent;
      const message = getDOMElement(".error-messages strong").nativeElement.textContent;
      const support = getDOMElement(".error-support");

      // when
      fixture.detectChanges();

      // then
      expect(title).toBe(test_title2);
      expect(message).toBe(test_message2);
      expect(support).toBeTruthy();
    });
  });
});
