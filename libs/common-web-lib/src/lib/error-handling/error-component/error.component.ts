import { Location } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { ErrorData } from "../model/error-data";

@Component({
  templateUrl: "./error.component.html",
  styleUrls: ["./error.component.scss"],
})
export class ErrorComponent implements OnInit {
  errorDetails: ErrorData | null = null;

  constructor(private activatedRoute: ActivatedRoute, private location: Location) {}

  ngOnInit() {
    this.getError();
  }

  goBack() {
    this.location.back();
  }

  private getError() {
    const activatedRoute = <ErrorData>this.activatedRoute.snapshot.data;
    const errorParam = window.history.state?.customError;
    let errorReceived = null;

    if (activatedRoute && Object.keys(activatedRoute).length > 0) {
      errorReceived = activatedRoute;
    } else if (errorParam) {
      errorReceived = errorParam;
    }

    this.errorDetails = new ErrorData(errorReceived);
  }
}
