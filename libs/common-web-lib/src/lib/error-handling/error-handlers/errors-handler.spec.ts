import { TestBed, inject } from "@angular/core/testing";
import { RouterTestingModule } from "@angular/router/testing";

import { ErrorService } from "../error-service/error.service";
import { ErrorServiceMock } from "../error-service/error.service.mock";
import { ErrorsHandler } from "./errors-handler";
import { HttpErrorResponse, HttpHeaders } from "@angular/common/http";

describe("ErrorsHandler", () => {
  let errorServiceServerSpy: jest.SpyInstance;
  let errorServiceClientSpy: jest.SpyInstance;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        ErrorsHandler,
        {
          provide: ErrorService,
          useClass: ErrorServiceMock,
        },
      ],
    });
  });

  beforeEach(inject([ErrorService], (service: ErrorService) => {
    const errorServiceServerSpy = jest.spyOn(service, "handleServerError");
    const errorServiceClientSpy = jest.spyOn(service, "handleClientError");
    jest.spyOn(console, "error");
  }));

  it("should raise properly formatted error when error is instance of HttpErrorResponse", inject(
    [ErrorsHandler, ErrorService],
    (handler: ErrorsHandler, service: ErrorService) => {
      //given
      const error: HttpErrorResponse = new HttpErrorResponse({
        error: {
          headers: new HttpHeaders("Content-Type: application/json;charset=UTF-8"),
          status: 500,
          statusText: "Server unavailable",
        },
        headers: new HttpHeaders("Content-Type: application/json;charset=UTF-8"),
        status: 500,
        statusText: "Server unavailable",
      });

      //when
      handler.handleError(error);

      expect(true).toBeTruthy();

      // expect(errorServiceServerSpy).not.toHaveBeenCalled();
      // expect(errorServiceClientSpy).not.toHaveBeenCalled();
    }
  ));

  // it("should raise properly formatted error when error is 404 not found", inject(
  //   [ErrorsHandler, ErrorService],
  //   (handler: ErrorsHandler, service: ErrorService ) => {
  //     //given
  //     const error: HttpErrorResponse = new HttpErrorResponse({
  //       error: {
  //         headers: new HttpHeaders("Content-Type: application/json;charset=UTF-8"),
  //         status: 404,
  //         statusText: "Resource not found",
  //       },
  //       headers: new HttpHeaders("Content-Type: application/json;charset=UTF-8"),
  //       status: 404,
  //       statusText: "Resource not found",
  //     });
  //
  //     //when
  //     handler.handleError(error);
  //
  //     //then
  //     expect(errorServiceServerSpy).toHaveBeenCalledWith(error);
  //     expect(errorServiceClientSpy).not.toHaveBeenCalled();
  //   }
  // ));

  // it("should raise properly formatted error when error is other type", inject(
  //   [ErrorsHandler],
  //   (handler: ErrorsHandler) => {
  //     const error =
  //       '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Draft//EN">\n' +
  //       "<HTML>\n" +
  //       "<HEAD>\n" +
  //       "<TITLE>Error 500--Internal Server Error</TITLE>\n" +
  //       "</HEAD>\n" +
  //       '<BODY bgcolor="white">\n' +
  //       "<FONT FACE=Helvetica><BR CLEAR=all>\n" +
  //       "<TABLE border=0 cellspacing=5><TR><TD><BR CLEAR=all>\n" +
  //       '<FONT FACE="Helvetica" COLOR="black" SIZE="3"><H2>Error 500--Internal Server Error</H2>\n' +
  //       "</FONT></TD></TR>\n" +
  //       "</TABLE>\n" +
  //       "<TABLE border=0 width=100% cellpadding=10><TR><TD VALIGN=top WIDTH=100% BGCOLOR=white>" +
  //       '<FONT FACE="Courier New"><pre>org.springframework.orm.jpa.JpaSystemException: ' +
  //       "Exception [EclipseLink-4002]\n" +
  //       "Internal Exception: java.sql.SQLRecoverableException: No more data to read from socket\n" +
  //       "Error Code: 17410\n" +
  //       "\t... 117 more\n" +
  //       "</pre></FONT></TD></TR>\n" +
  //       "</TABLE>\n" +
  //       "\n" +
  //       "</BODY>\n" +
  //       "</HTML>";
  //
  //     //when
  //     handler.handleError(error);
  //
  //     //then
  //     expect(errorServiceServerSpy).not.toHaveBeenCalledWith();
  //     expect(errorServiceClientSpy).toHaveBeenCalledWith(error);
  //   }
  // ));
});
