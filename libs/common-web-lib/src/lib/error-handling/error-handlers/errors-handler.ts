import { HttpErrorResponse } from "@angular/common/http";
import { ErrorHandler, inject, Injectable, Injector } from "@angular/core";

import { ErrorService } from "../error-service/error.service";

@Injectable({ providedIn: "root" })
export class ErrorsHandler implements ErrorHandler {
  private errorService = inject(ErrorService);

  handleError(err: any) {
    const error = err && err.rejection ? err.rejection : err;
    // const errorService = this.injector.get(ErrorService);

    if (error instanceof HttpErrorResponse) {
      // Server error
      this.errorService.handleServerError(error);
    } else if (err) {
      // Client Error
      this.errorService.handleClientError(error);
    }
    // Always log errors
    console.error(error);
  }
}
