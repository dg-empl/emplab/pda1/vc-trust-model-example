import { waitForAsync, TestBed } from "@angular/core/testing";

import { ErrorHandlingModule } from "./error-handling.module";

describe("ErrorsHandlingLibModule", () => {
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [ErrorHandlingModule],
    });
  }));

  it("should create", () => {
    expect(ErrorHandlingModule).toBeDefined();
  });
});
