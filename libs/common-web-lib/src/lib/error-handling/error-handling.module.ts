import { CommonModule } from "@angular/common";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { ErrorHandler, NgModule } from "@angular/core";
import { MatButtonModule } from "@angular/material/button";
import { MatDialogModule } from "@angular/material/dialog";
import { RouterModule } from "@angular/router";

import { ErrorComponent } from "./error-component/error.component";
import { ErrorsHandler } from "./error-handlers/errors-handler";
import { ErrorModalComponent } from "./error-modal-component/error-modal.component";
import { ErrorRoutingModule } from "./error-routing/error-routing.module";
import { ErrorService } from "./error-service/error.service";
import { HttpRequestInterceptor } from "./http-request-interceptor/http-request.interceptor";

@NgModule({
  imports: [CommonModule, RouterModule, ErrorRoutingModule, MatButtonModule, MatDialogModule],
  declarations: [ErrorComponent, ErrorModalComponent],
  exports: [ErrorModalComponent],
  providers: [
    ErrorService,
    { provide: ErrorHandler, useClass: ErrorsHandler },
    { provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor, multi: true },
  ],
})
export class ErrorHandlingModule {}
