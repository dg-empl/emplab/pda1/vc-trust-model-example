import { waitForAsync, ComponentFixture, TestBed } from "@angular/core/testing";
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from "@angular/material/dialog";
import { RouterTestingModule } from "@angular/router/testing";

import { ErrorModalComponent } from "./error-modal.component";
import { By } from "@angular/platform-browser";

describe("ErrorModalComponent", () => {
  let component: ErrorModalComponent;
  let fixture: ComponentFixture<ErrorModalComponent>;

  const test_title = "Test title";
  const test_message = "Test message";

  function getDOMElement(selector: string) {
    return fixture.debugElement.query(By.css(selector));
  }

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, MatDialogModule],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {
            close: () => {
              return "close";
            },
          },
        },
        { provide: MAT_DIALOG_DATA, useValue: { title: test_title, message: test_message } },
      ],
      declarations: [ErrorModalComponent],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should populate template", () => {
    // given
    const title = getDOMElement(".error-title span").nativeElement.textContent;
    const message = getDOMElement(".error-messages strong").nativeElement.textContent;

    // then
    expect(title).toBe(test_title);
    expect(message).toBe(test_message);
  });

  it("should dismiss dialog on button click", () => {
    // given
    const dialog: MatDialogRef<ErrorModalComponent> = TestBed.inject(MatDialogRef);
    jest.spyOn(dialog, "close");
    const btn = fixture.debugElement.query(By.css("button"));

    // when
    btn.triggerEventHandler("click", null);

    // then
    expect(dialog.close).toHaveBeenCalled();
  });
});
