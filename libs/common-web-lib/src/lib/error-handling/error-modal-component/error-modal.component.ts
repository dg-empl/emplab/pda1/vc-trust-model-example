import { Component, Inject, OnInit } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";

import { ErrorData } from "../model/error-data";

@Component({
  templateUrl: "./error-modal.component.html",
  styleUrls: ["./error-modal.component.scss"],
})
export class ErrorModalComponent {
  error: ErrorData;

  constructor(
    private dialogRef: MatDialogRef<ErrorModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ErrorData
  ) {}

  close() {
    this.dialogRef.close();
  }

  refresh() {
    window.location.reload();
  }
}
