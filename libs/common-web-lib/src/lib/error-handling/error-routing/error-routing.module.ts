import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { ErrorComponent } from "../error-component/error.component";
import { ErrorData } from "../model/error-data";

const routes: Routes = [
  { path: "error", component: ErrorComponent },
  {
    path: "**",
    component: ErrorComponent,
    data: <ErrorData>{ title: "Oops...", message: "Page not found.", helpText: true },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ErrorRoutingModule {}
