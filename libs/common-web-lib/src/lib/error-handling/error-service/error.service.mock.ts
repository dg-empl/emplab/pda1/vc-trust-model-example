import { HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { EMPTY } from "rxjs";

@Injectable({ providedIn: "root" })
export class ErrorServiceMock {
  handleServerError(errorResponse: HttpErrorResponse): void {
    console.log("ErrorServiceMock.handleServerError");
  }

  handleClientError(error: Error): void {
    console.log("ErrorServiceMock.handleClientError");
  }

  handleResolverError(error: any) {
    console.log("ErrorServiceMock.handleResolverError");
  }

  handleResolverInvalidParam(errorMessage: string) {
    return EMPTY;
  }
}

export const httpErrorResponseMock = (status: number, body?: any) => {
  let statusText = "";

  switch (status) {
    case 401:
      statusText = "Unauthorized";
      break;
    case 403:
      statusText = "Forbidden";
      break;
    case 404:
      statusText = "Not Found";
      break;
    case 418:
      statusText = "Application status change not allowed.";
      break;
    case 500:
      statusText = "Internal server error";
      break;
    case 503:
      statusText = "Service unavailable";
      break;
  }

  return new HttpErrorResponse({
    error: body || `ERROR_${status}`,
    status: status,
    statusText: statusText,
  });
};
