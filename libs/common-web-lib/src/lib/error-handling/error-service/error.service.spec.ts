import { inject, TestBed } from "@angular/core/testing";
import { MatDialogModule, MatDialog } from "@angular/material/dialog";
import {
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  Router,
  RouterEvent,
} from "@angular/router";
import { RouterTestingModule } from "@angular/router/testing";
import { ReplaySubject, of } from "rxjs";

import { ErrorModalComponent } from "../error-modal-component/error-modal.component";
import { ErrorData } from "../model/error-data";
import { ErrorService } from "./error.service";
import { httpErrorResponseMock } from "./error.service.mock";

const eventSubject = new ReplaySubject<RouterEvent>(1);

const routerMock = {
  navigate: jest.fn(function () {
    return {
      then: (callback: (args: any) => void) => {
        return callback({ foo: "bar" });
      },
    };
  }),
  events: eventSubject.asObservable(),
  url: "test/url",
};

const httpErrorErrorData: ErrorData = {
  message: "Resource is not available",
  title: "<strong>Sorry,</strong> we had a problem",
  code: 404,
  refresh: false,
  helpText: false,
};

export class MatDialogMock {
  // When the component calls this.dialog.open(...) we'll return an object
  // with an afterClosed method that allows to subscribe to the dialog result observable.
  open() {
    return {
      componentInstance: {},
      afterClosed: () => of({ action: true }),
    };
  }
}

const errorRoute = ["/error"];
const rootRoute = ["/"];

const clearNavigateMock = (routerM: any) => {
  routerM.navigate.mockClear();
};

describe("ErrorService", () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, MatDialogModule],
      providers: [
        ErrorService,
        { provide: MatDialog, useClass: MatDialogMock },
        {
          provide: Router,
          useValue: routerMock,
        },
      ],
    });
  });

  it("should be created", inject([ErrorService], (service: ErrorService) => {
    expect(service).toBeTruthy();
  }));

  it("should navigate to error page on NavigationStart event", inject(
    [ErrorService],
    (service: ErrorService) => {
      // given
      const router: Router = TestBed.inject(Router);
      // when
      eventSubject.next(new NavigationStart(1, "test-route"));
      const error = new Error("TEST_ERROR");
      service.handleClientError(error);

      // then
      expect(router.navigate).toHaveBeenCalledTimes(1);
      expect(router.navigate).toHaveBeenCalledWith(errorRoute, {
        state: {
          customError: {
            message: "TEST_ERROR",
            title: "Something went wrong",
          },
        },
      });

      clearNavigateMock(routerMock);
    }
  ));

  // describe("handleServerError:", () => {
  //   it("should show dialog on NavigationEnd event", inject(
  //     [ErrorService],
  //     (service: ErrorService) => {
  //       // given
  //       const methodSpy = jest.spyOn(service["dialog"], "open").mockReturnThis();
  //
  //       // when
  //       eventSubject.next(new NavigationStart(1, "test-route"));
  //       eventSubject.next(new NavigationEnd(1, "test-route", "redirectUrl"));
  //       service.handleServerError(httpErrorResponseMock(404));
  //
  //       expect(methodSpy).toHaveBeenCalledTimes(1);
  //     }
  //   ));
  //
  //   it("should show dialog on NavigationCancel event", inject(
  //     [ErrorService],
  //     (service: ErrorService) => {
  //       // given
  //       const methodSpy = jest.spyOn(service["dialog"], "open").mockReturnThis();
  //
  //       // when
  //       eventSubject.next(new NavigationStart(1, "test-route"));
  //       eventSubject.next(new NavigationCancel(1, "test-route", "redirectUrl"));
  //       service.handleServerError(httpErrorResponseMock(404));
  //
  //       expect(methodSpy).toHaveBeenCalledTimes(1);
  //       expect(methodSpy).toHaveBeenCalledWith(ErrorModalComponent, { data: httpErrorErrorData });
  //     }
  //   ));
  //
  //   it("should navigate to error page on NavigationStart event", inject(
  //     [ErrorService],
  //     (service: ErrorService) => {
  //       // given
  //       const router: Router = TestBed.inject(Router);
  //       // when
  //       eventSubject.next(new NavigationStart(1, "test-route"));
  //       service.handleServerError(httpErrorResponseMock(404));
  //
  //       // then
  //       expect(router.navigate).toHaveBeenCalledTimes(1);
  //       expect(router.navigate).toHaveBeenCalledWith(errorRoute, {
  //         state: { customError: httpErrorErrorData },
  //       });
  //
  //       clearNavigateMock(routerMock);
  //     }
  //   ));
  //
  //   it("should navigate to error page on NavigationError event", inject(
  //     [ErrorService],
  //     (service: ErrorService) => {
  //       // given
  //       const router: Router = TestBed.inject(Router);
  //
  //       // when
  //       eventSubject.next(new NavigationStart(1, "test-route"));
  //       eventSubject.next(new NavigationError(1, "test-route", "redirectUrl"));
  //       service.handleServerError(httpErrorResponseMock(404));
  //
  //       // then
  //       expect(router.navigate).toHaveBeenCalledTimes(1);
  //       expect(router.navigate).toHaveBeenCalledWith(errorRoute, {
  //         state: { customError: httpErrorErrorData },
  //       });
  //
  //       clearNavigateMock(routerMock);
  //     }
  //   ));
  //
  //   it("should have custom message for 403 error", inject(
  //     [ErrorService],
  //     (service: ErrorService) => {
  //       // given
  //       eventSubject.next(new NavigationCancel(1, "test-route", "redirectUrl"));
  //       const methodSpy = jest.spyOn(service["dialog"], "open").mockReturnThis();
  //       // when
  //       service.handleServerError(httpErrorResponseMock(403));
  //
  //       // then
  //       expect(methodSpy).toHaveBeenCalledWith(ErrorModalComponent, {
  //         data: {
  //           message: "You do not have access to this page or resource.",
  //           title: "Access Forbidden",
  //           code: 403,
  //           refresh: false,
  //           helpText: false,
  //         },
  //       });
  //     }
  //   ));
  //
  //   it("should have custom message for 500 error", inject(
  //     [ErrorService],
  //     (service: ErrorService) => {
  //       // given
  //       eventSubject.next(new NavigationCancel(1, "test-route", "redirectUrl"));
  //       const methodSpy = jest.spyOn(service["dialog"], "open").mockReturnThis();
  //       // when
  //       service.handleServerError(httpErrorResponseMock(500));
  //
  //       // then
  //       expect(methodSpy).toHaveBeenCalledWith(ErrorModalComponent, {
  //         data: {
  //           message:
  //             "There was a problem contacting the server. <br> If the problem persists, please contact support.",
  //           title: "<strong>Sorry,</strong> we had a problem",
  //           code: 500,
  //           refresh: false,
  //           helpText: true,
  //         },
  //       });
  //     }
  //   ));
  //
  //   it("should have custom message for 503 error", inject(
  //     [ErrorService],
  //     (service: ErrorService) => {
  //       // given
  //       eventSubject.next(new NavigationCancel(1, "test-route", "redirectUrl"));
  //       const methodSpy = jest.spyOn(service["dialog"], "open").mockReturnThis();
  //       // when
  //       service.handleServerError(httpErrorResponseMock(503));
  //
  //       // then
  //       expect(methodSpy).toHaveBeenCalledWith(ErrorModalComponent, {
  //         data: {
  //           message:
  //             "There was a problem contacting the server. <br> If the problem persists, please contact support.",
  //           title: "Service unavailable",
  //           code: 503,
  //           refresh: false,
  //           helpText: false,
  //         },
  //       });
  //     }
  //   ));
  //
  //   it("should have custom message when server not responding", inject(
  //     [ErrorService],
  //     (service: ErrorService) => {
  //       // given
  //       eventSubject.next(new NavigationCancel(1, "test-route", "redirectUrl"));
  //       const methodSpy = jest.spyOn(service["dialog"], "open").mockReturnThis();
  //       // when
  //       service.handleServerError(httpErrorResponseMock(0));
  //
  //       // then
  //       expect(methodSpy).toHaveBeenCalledWith(ErrorModalComponent, {
  //         data: {
  //           message: "Server is not responding. <br> Try again later and reload the page.",
  //           title: "<strong>Sorry,</strong> we had a problem",
  //           code: 0,
  //           refresh: true,
  //           helpText: false,
  //         },
  //       });
  //     }
  //   ));
  //
  //   it("should have custom message for not handled http error", inject(
  //     [ErrorService],
  //     (service: ErrorService) => {
  //       // given
  //       eventSubject.next(new NavigationCancel(1, "test-route", "redirectUrl"));
  //       const methodSpy = jest.spyOn(service["dialog"], "open").mockReturnThis();
  //       // when
  //       service.handleServerError(httpErrorResponseMock(300));
  //
  //       // then
  //       expect(methodSpy).toHaveBeenCalledWith(ErrorModalComponent, {
  //         data: {
  //           message: "Http failure response for (unknown url): 300 ",
  //           title: "<strong>Sorry,</strong> we had a problem",
  //           code: 300,
  //           refresh: false,
  //           helpText: true,
  //         },
  //       });
  //     }
  //   ));
  //
  //   it("should have message body returned from API", inject(
  //     [ErrorService],
  //     (service: ErrorService) => {
  //       // given
  //       eventSubject.next(new NavigationCancel(1, "test-route", "redirectUrl"));
  //       const methodSpy = jest.spyOn(service["dialog"], "open").mockReturnThis();
  //       // when
  //       service.handleServerError(
  //         httpErrorResponseMock(404, { errorCode: 222, errorMessage: "Test not found" })
  //       );
  //
  //       // then
  //       expect(methodSpy).toHaveBeenCalledWith(ErrorModalComponent, {
  //         data: {
  //           message: "Test not found",
  //           title: "<strong>Sorry,</strong> we had a problem",
  //           code: 222,
  //           refresh: false,
  //           helpText: false,
  //         },
  //       });
  //     }
  //   ));
  // });

  // describe("handleClientError:", () => {
  //   it("should open dialog on error", inject([ErrorService], (service: ErrorService) => {
  //     // given
  //     const methodSpy = jest.spyOn(service["dialog"], "open").mockReturnThis();
  //     // when
  //     const error = new Error("TEST_ERROR");
  //
  //     service.handleClientError(error);
  //
  //     // then
  //     expect(methodSpy).toHaveBeenCalledWith(ErrorModalComponent, {
  //       data: {
  //         message: "TEST_ERROR",
  //         title: "Something went wrong",
  //       },
  //     });
  //   }));
  // });

  describe("handleResolverError:", () => {
    it("should throw error when HttpErrorResponse", inject(
      [ErrorService, Router],
      (service: ErrorService, router: Router) => {
        expect(function () {
          service.handleResolverError(httpErrorResponseMock(404));
        }).toThrowError(httpErrorResponseMock(404));
        expect(router.navigate).not.toHaveBeenCalled();
      }
    ));

    it("should navigate root and throw error when Error", inject(
      [ErrorService, Router],
      (service: ErrorService, router: Router) => {
        // given
        const error = new Error("TEST_ERROR");

        // then
        expect(function () {
          service.handleResolverError(error);
        }).toThrowError("TEST_ERROR");
        expect(router.navigate).toHaveBeenCalledTimes(1);
        expect(router.navigate).toHaveBeenCalledWith(rootRoute);

        clearNavigateMock(routerMock);
      }
    ));
  });

  // it("should display dialog once", inject([ErrorService], (service: ErrorService) => {
  //   // given
  //   eventSubject.next(new NavigationCancel(1, "test-route", "redirectUrl"));
  //   const methodSpy = jest.spyOn(service["dialog"], "open");
  //
  //   // when
  //   service.handleServerError(
  //     httpErrorResponseMock(404, { errorCode: 222, errorMessage: "Test not found" })
  //   );
  //   service.handleServerError(
  //     httpErrorResponseMock(404, { errorCode: 222, errorMessage: "Test not found" })
  //   );
  //
  //   expect(service["dialogRef"]).toBeDefined();
  //   // then
  //   expect(methodSpy).toHaveBeenCalledWith(ErrorModalComponent, {
  //     data: {
  //       message: "Test not found",
  //       title: "<strong>Sorry,</strong> we had a problem",
  //       code: 222,
  //       refresh: false,
  //       helpText: false,
  //     },
  //   });
  //   expect(methodSpy).toHaveBeenCalledTimes(1);
  // }));
});
