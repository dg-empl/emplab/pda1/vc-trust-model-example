import { HttpErrorResponse } from "@angular/common/http";
import { Injectable, NgZone } from "@angular/core";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";
import {
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationExtras,
  NavigationStart,
  Router,
} from "@angular/router";
import HTTP_STATUS_CODES from "http-status-enum";
import { EMPTY } from "rxjs";

import { ErrorModalComponent } from "../error-modal-component/error-modal.component";
import { ErrorData } from "../model/error-data";

@Injectable({ providedIn: "root" })
export class ErrorService {
  private isLoadingRoute = false;
  private dialogRef: MatDialogRef<ErrorModalComponent> | undefined;

  constructor(private router: Router, private zone: NgZone, private dialog: MatDialog) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart || event instanceof NavigationError) {
        this.isLoadingRoute = true;
      } else if (event instanceof NavigationEnd || event instanceof NavigationCancel) {
        this.isLoadingRoute = false;
      }
    });
  }

  handleServerError(errorResponse: HttpErrorResponse) {
    const formattedResponse = this.formatHttpError(errorResponse);

    if (this.isLoadingRoute) {
      const navigationExtras: NavigationExtras = {
        state: { customError: formattedResponse },
      };

      this.zone.run(() => {
        this.router.navigate(["/error"], navigationExtras);
      });
    } else {
      this.raiseDialogError(formattedResponse);
    }
  }

  handleClientError(error: Error) {
    const message = this.getClientErrorMessage(error);

    const formattedResponse: ErrorData = {
      message: message,
      title: "Something went wrong",
    };

    if (this.isLoadingRoute) {
      const navigationExtras: NavigationExtras = {
        state: { customError: formattedResponse },
      };

      this.zone.run(() => {
        this.router.navigate(["/error"], navigationExtras);
      });
    } else {
      this.raiseDialogError(formattedResponse);
    }
  }

  handleResolverInvalidParam(errorMessage: string) {
    const formattedResponse: ErrorData = {
      message: errorMessage,
      title: "Something went wrong",
    };

    const navigationExtras: NavigationExtras = {
      state: { customError: formattedResponse },
    };

    this.router.navigate(["/error"], navigationExtras);

    return EMPTY;
  }

  handleResolverError(error: any) {
    if (!(error instanceof HttpErrorResponse)) {
      this.router.navigate(["/"]).then(after => {
        throw error;
      });
      return EMPTY;
    }

    throw error;
  }

  // const stack = error instanceof HttpErrorResponse ? null : StackTraceParser.parse(error);
  private formatHttpError(errorResponse: HttpErrorResponse) {
    const error = errorResponse.error;
    const { status, message, statusText } = errorResponse;

    const formattedError = new ErrorData({
      title: "<strong>Sorry,</strong> we had a problem",
      message: message || this.getClientErrorMessage(errorResponse),
    });

    switch (status) {
      case 0:
        formattedError.message =
          "Server is not responding. <br> Try again later and reload the page.";
        formattedError.refresh = true;
        break;
      case HTTP_STATUS_CODES.FORBIDDEN:
        formattedError.title = "Access Forbidden";
        formattedError.message = "You do not have access to this page or resource.";
        break;
      case HTTP_STATUS_CODES.NOT_FOUND:
        formattedError.message = "Resource is not available";
        break;
      case HTTP_STATUS_CODES.SERVICE_UNAVAILABLE:
        formattedError.title = statusText;
        formattedError.message = message;
        break;
      default:
        formattedError.helpText = true;
        break;
    }

    // All 5XX errors
    if (status >= 500) {
      formattedError.message =
        "There was a problem contacting the server. <br> If the problem persists, please contact support.";
    }

    formattedError.code = error?.errorCode || status;
    formattedError.message = error?.errorMessage || formattedError.message;

    return formattedError;
  }

  private raiseDialogError(error: ErrorData) {
    this.zone.run(() => {
      if (this.dialogRef) {
        this.dialogRef.componentInstance.data = error;
        return;
      }

      this.dialogRef = this.dialog.open(ErrorModalComponent, {
        data: error,
      });
      this.dialogRef.afterClosed().subscribe(result => this.unsetDialogRef());
    });
  }

  private getClientErrorMessage(error: Error): string {
    return error.message ? error.message : error.toString();
  }

  private unsetDialogRef() {
    this.dialogRef = undefined;
  }
}
