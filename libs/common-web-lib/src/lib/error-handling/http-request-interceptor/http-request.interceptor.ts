import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { retry } from "rxjs/operators";

@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.method === "POST" || req.method === "PUT") {
      req = this.setContentType(req);
      return next.handle(req);
    }

    return next.handle(req).pipe(retry(1));
  }

  private setContentType(req: HttpRequest<any>): HttpRequest<any> {
    if (!req.headers.has("content-type")) {
      return req.clone({ setHeaders: { "Content-Type": "application/json; charset=utf-8" } });
    }
    return req;
  }
}
