export class ErrorData {
  message: string;
  title: string;
  code?: number;
  helpText?: boolean;
  refresh?: boolean;

  constructor(error: any = null) {
    this.message = error?.message || "Something went wrong";
    this.title = error?.title || "Oops...";
    this.code = error?.code || null;
    this.helpText = error?.helpText || false;
    this.refresh = error?.refresh || false;
  }
}
