import { Injectable, signal } from "@angular/core";
import { FakeData } from "../schemas/fake-data.schema";
import { PersonalDetailsType } from "../schemas/personal-details.schema";

@Injectable({
  providedIn: "root",
})
export class DataPasserService {
  private data = signal<FakeData>({} as FakeData);

  set dataToPass(data: FakeData) {
    this.data.set(data);
  }

  get dataToPass(): FakeData {
    return this.data();
  }
}
