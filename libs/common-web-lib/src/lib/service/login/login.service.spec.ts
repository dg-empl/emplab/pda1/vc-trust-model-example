import { LoginService } from "./login.service";
import { TestBed } from "@angular/core/testing";
import { KeycloakService } from "keycloak-angular";
import { APP_CONFIG } from "@esspass-web-wallet/security-web-lib";
import { Router } from "@angular/router";

describe("LoginService", () => {
  it("should be authenticated when KeyCloak is authenticated", async () => {
    //given when
    TestBed.configureTestingModule({
      providers: [
        LoginService,
        {
          provide: KeycloakService,
          useValue: {
            isLoggedIn: () => true,
          },
        },
        {
          provide: APP_CONFIG,
          useValue: {},
        },
      ],
    });

    const loginService = TestBed.inject(LoginService);

    //then
    expect(loginService).toBeTruthy();
    expect(await loginService.isAuthenticated()).toBeTruthy();
  });

  it("should not be authenticated when KeyCloak is not authenticated", async () => {
    //given when
    TestBed.configureTestingModule({
      providers: [
        LoginService,
        {
          provide: KeycloakService,
          useValue: {
            isLoggedIn: jest.fn().mockReturnValue(false),
          },
        },
        {
          provide: APP_CONFIG,
          useValue: {},
        },
        {
          provide: Router,
          useValue: jest.fn().mockReturnValue({ navigate: jest.fn() }),
        },
      ],
    });

    const loginService = TestBed.inject(LoginService);
    const router = TestBed.inject(Router);

    //then
    expect(loginService).toBeTruthy();
    expect(await loginService.isAuthenticated()).toBeFalsy();
  });

  it("should route to redirectUri if login is called", async () => {
    //given when
    TestBed.configureTestingModule({
      providers: [
        LoginService,
        {
          provide: KeycloakService,
          useValue: {
            isLoggedIn: jest.fn().mockReturnValue(true),
            logout: jest.fn(),
            login: jest.fn(),
          },
        },
        {
          provide: APP_CONFIG,
          useValue: {},
        },
      ],
    });

    const loginService = TestBed.inject(LoginService);
    const keycloakService = TestBed.inject(KeycloakService);

    //when
    await loginService.login();

    //then
    expect(keycloakService.login).toHaveBeenCalledWith({ redirectUri: "http://localhost" });
    expect(await loginService.isAuthenticated()).toBeTruthy();
  });

  it("should route to /home if logout is called", async () => {
    //given
    TestBed.configureTestingModule({
      providers: [
        LoginService,
        {
          provide: KeycloakService,
          useValue: {
            isLoggedIn: jest.fn().mockReturnValue(true),
            logout: jest.fn(),
            login: jest.fn(),
          },
        },
        {
          provide: APP_CONFIG,
          useValue: {},
        },
        {
          provide: Router,
          useValue: {
            navigate: jest.fn(),
          },
        },
      ],
    });

    const loginService = TestBed.inject(LoginService);
    const router = TestBed.inject(Router);

    //when
    await loginService.logout();

    //then
    expect(await loginService.isAuthenticated()).toBeTruthy();
    expect(router.navigate).toHaveBeenCalledWith(["/home"]);
  });
});
