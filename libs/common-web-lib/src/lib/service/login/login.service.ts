import { inject, Injectable, signal } from "@angular/core";
import { KeycloakService } from "keycloak-angular";
import { Router } from "@angular/router";
import { APP_CONFIG } from "@esspass-web-wallet/security-web-lib";

@Injectable({ providedIn: "root" })
export class LoginService {
  private _isAuthenticated = signal(false);

  private keycloak = inject(KeycloakService);
  private config = inject(APP_CONFIG);
  private router = inject(Router);

  async isAuthenticated() {
    this._isAuthenticated.set(await this.keycloak.isLoggedIn());
    return this._isAuthenticated();
  }

  async login() {
    await this.keycloak.login({
      redirectUri: window.location.origin,
    });
    this._isAuthenticated.set(true);
  }

  async logout() {
    try {
      await this.keycloak.logout(this.config.redirectUrl);
      await this.router.navigate(["/home"]);
      this._isAuthenticated.set(false);
    } catch (e) {
      console.error(e);
    }
  }
}
