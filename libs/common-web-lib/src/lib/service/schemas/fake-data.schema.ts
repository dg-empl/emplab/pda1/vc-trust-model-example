import { z } from "zod";

const fakeDataSchema = z.object({
  country: z.string(),
  city: z.string(),
  streetName: z.string(),
  postcode: z.string(),
  countryCode: z.string(),
  emailDomain: z.string(),
  company: z.string(),
  companyActivity: z.string(),
  companyRole: z.string(),
  streetNameCompany: z.string(),
  cityCompany: z.string(),
  postcodeCompany: z.string(),
  nssi: z.string(),
  streetNameNssi: z.string(),
  cityNssi: z.string(),
  postcodeNssi: z.string(),
});

export const dataSchema = z.record(fakeDataSchema);
export type FakeDataByCountry = z.infer<typeof fakeDataSchema>;
export type FakeData = z.infer<typeof dataSchema>;

const addressSchema = z.object({
  streetName: z.string(),
  postCode: z.string(),
  countryCode: z.string(),
  city: z.string(),
});

export type AddressType = z.infer<typeof addressSchema>;
