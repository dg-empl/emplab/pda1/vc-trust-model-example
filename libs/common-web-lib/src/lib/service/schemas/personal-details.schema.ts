import { z } from "zod";

export const personalDetailSchema = z.object({
  section1: z.object({
    personalIdentificationNumber: z.string(),
    sex: z.string(),
    surname: z.string(),
    forenames: z.string(),
    dateBirth: z.string(),
    surnameAtBirth: z.string(),
    countryOfBirth: z.string(),
    placeOfBirth: z.object({
      town: z.string(),
      region: z.string(),
      countryCode: z.string(),
    }),
    nationalities: z.array(z.string()),
    stateOfResidenceAddress: z.object({
      streetName: z.string(),
      postCode: z.string(),
      countryCode: z.string(),
      city: z.string(),
    }),
    stateOfStayAddress: z.object({
      streetName: z.string(),
      postCode: z.string(),
      countryCode: z.string(),
      city: z.string(),
    }),
  }),
});

export type PersonalDetailsType = z.infer<typeof personalDetailSchema>;
