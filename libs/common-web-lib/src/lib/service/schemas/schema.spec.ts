import { z } from "zod";
import { dataSchema } from "./fake-data.schema";

describe("Schemas", () => {
  it("should create an instance of DataType validated by dataSchema", () => {
    type DataType = z.infer<typeof dataSchema>;

    const obj: DataType = {
      AD1: {
        country: "UK",
        city: "London",
        streetName: "Welwyn Garden City",
        postcode: "AL7 1TW",
        countryCode: "44",
        emailDomain: "CO.UK",
        company: "Tesco",
        companyActivity: "Retail",
        companyRole: "Senior Sales Executive",
        streetNameCompany: "Welwyn Garden City",
        cityCompany: "Hertfordshire",
        postcodeCompany: "AL7 1TW",
        nssi: "Department of Social Security",
        streetNameNssi: "Welwyn Garden City",
        cityNssi: "Hertfordshire",
        postcodeNssi: "AL7 1TW",
      },
      AD2: {
        country: "UK",
        city: "London",
        streetName: "Welwyn Garden City",
        postcode: "AL7 1TW",
        countryCode: "44",
        emailDomain: "CO.UK",
        company: "Tesco",
        companyActivity: "Retail",
        companyRole: "Senior Sales Executive",
        streetNameCompany: "Welwyn Garden City",
        cityCompany: "Hertfordshire",
        postcodeCompany: "AL7 1TW",
        nssi: "Department of Social Security",
        streetNameNssi: "Welwyn Garden City",
        cityNssi: "Hertfordshire",
        postcodeNssi: "AL7 1TW",
      },
    };

    expect(dataSchema.parse(obj)).toBeTruthy();
  });
});
