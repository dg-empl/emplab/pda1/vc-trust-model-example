import { ElementRef, Injectable, signal } from "@angular/core";

export type ScrollElementId =
  | ""
  | "#top1"
  | "#top2"
  | "#top3"
  | "#top4"
  | "#top5"
  | "#requestPda1Form"
  | "#memberState"
  | "#statusConfirmation"
  | "#detailsEmployer"
  | "#activityPursued"
  | "#institutionCompleting";

@Injectable({
  providedIn: "root",
})
export class ScrollerService {
  scroll = signal<ScrollElementId>("");
  private scrollMap = new Map<ScrollElementId, ElementRef>();

  clearAllSelected() {
    this.scrollMap.forEach((_, key) => {
      const elementRef = this.scrollMap.get(key);
      if (elementRef) {
        const classList = elementRef.nativeElement.classList as DOMTokenList;
        if (classList.contains("selected")) {
          classList.remove("selected");
        }
      }
    });
  }

  addElement(elementId: ScrollElementId, element: ElementRef) {
    this.scrollMap.set(elementId, element);
  }

  scrollToElement(id: string) {
    if (!id) {
      return;
    }
    const selector = document.querySelector(id);
    if (selector !== null) {
      selector.scrollIntoView({ behavior: "smooth" });
    }
  }

  addCssSelectedClassToElement(element: ScrollElementId) {
    const elementRef = this.scrollMap.get(element);
    if (elementRef) {
      const classList = elementRef.nativeElement.classList as DOMTokenList;
      if (!classList.contains("selected")) {
        classList.add("selected");
      }
    }
  }
}
