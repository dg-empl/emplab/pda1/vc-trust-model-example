import { Injectable, signal } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class SubmitterService {
  private data = new BehaviorSubject<{ submit: boolean }>({ submit: false });
  private valid = signal<{ valid: boolean }>({ valid: false });
  private dataSignal = signal<{ submit: boolean }>({ submit: false });

  constructor() {
    // Subscribe to data and update dataSignal reactively
    this.data.subscribe((newData) => {
      this.dataSignal.set(newData);
    });
  }

  submit() {
    this.data.next({ submit: true });
  }

  clearSubmit() {
    this.data.next({ submit: false });
  }

  getSubmit() {
    return this.data;
  }

  get dataSignalValue() {
    return this.dataSignal().submit;
  }

  set validData(valid: boolean) {
    this.valid.set({ valid });
  }

  get validData() {
    return this.valid().valid;
  }
}