export function mapError() {
  return (err: {
    name: string;
    message: string | undefined;
    error: { errors: { message: string | undefined }[] };
  }) => {
    const errs = err.error.errors;
    if (err.name === "HttpErrorResponse") {
      throw new Error(errs ? errs[0].message : err.message);
    } else {
      throw new Error(errs[0].message);
    }
  };
}
