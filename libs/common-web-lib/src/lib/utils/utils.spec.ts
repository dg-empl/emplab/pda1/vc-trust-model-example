import { generateDate } from "./utils";

describe("Utils", () => {
  it("should create an instance", () => {
    //given when
    const birthDate = generateDate();

    //then
    expect(birthDate).toBeTruthy();
  });
});
