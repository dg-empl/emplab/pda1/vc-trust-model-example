import { random } from "lodash";

export function generateDate() {
  const date = new Date();
  let month, day;

  const year = date.getFullYear() - random(18, 60);
  month = random(1, 12);
  month >= 10 ? (month = `${month}`) : (month = `0${month}`);
  day = random(1, 28);
  day >= 10 ? (day = `${day}`) : (day = `0${day}`);

  return `${year}-${month}-${day}`;
}

export function generateStartDate() {
  const date = new Date();
  let month, day;

  const year = date.getFullYear() + random(1, 2);
  month = random(1, 12);
  month >= 10 ? (month = `${month}`) : (month = `0${month}`);
  day = random(1, 28);
  day >= 10 ? (day = `${day}`) : (day = `0${day}`);

  return `${year}-${month}-${day}`;
}

export function generateEndDate() {
  const startDate = generateStartDate();
  const year = +startDate.substring(0, 4) + random(1, 2);
  const month = startDate.substring(5, 7);
  const day = startDate.substring(8, 10);

  return `${year}-${month}-${day}`;
}
