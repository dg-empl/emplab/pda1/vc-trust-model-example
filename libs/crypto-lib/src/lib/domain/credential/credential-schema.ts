import * as mongoose from "mongoose";

const verifiableCredential = new mongoose.Schema({
  id: String,
  did: String,
  vc: String,
});

export const verifiableCredentialModel = mongoose.model(
  "verifiableCredential",
  verifiableCredential
);

export type VerifiableCredentialDbModel = typeof verifiableCredentialModel;
