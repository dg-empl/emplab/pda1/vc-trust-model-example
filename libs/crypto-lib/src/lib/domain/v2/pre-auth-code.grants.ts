import { z } from "zod";

export const preAuthCodeGrantsSchema = z.object({
  "urn:ietf:params:oauth:grant-type:pre-authorized_code": z.object({
    "pre-authorized_code": z.string(),
    user_pin_required: z.boolean(),
  }),
});

export type PreAuthCodeGrants = z.infer<typeof preAuthCodeGrantsSchema>;

export const authorizationDetailSchema = z.object({
  // type: z.literal("openid_credential"),
  // format: z.literal("jwt_vc"),
  // locations: z.array(z.string()),
  types: z.array(z.string()),
});

export const preAuthCodeGrantsPayloadSchema = z.object({
  authorization_details: z.array(authorizationDetailSchema),
  iat: z.number(),
  exp: z.number(),
  iss: z.string().url(),
  aud: z.string().email(),
  sub: z.string(),
});

export type PreAuthCodeGrantsPayload = z.infer<typeof preAuthCodeGrantsPayloadSchema>;
