import { z } from "zod";

export const tokenRequestPayloadSchema = z.object({
  iss: z.string(),
  aud: z.string().url(),
  nonce: z.string(),
  pdaId: z.string(),
  iat: z.number(),
});

export type TokenRequestPayload = z.infer<typeof tokenRequestPayloadSchema>;

export const tokenRequestSchema = z.object({
  format: z.string(),
  types: z.array(z.string()),
  proof: z.object({ proof_type: z.string(), jwt: tokenRequestPayloadSchema }),
});

export type TokenRequest = z.infer<typeof tokenRequestSchema>;
