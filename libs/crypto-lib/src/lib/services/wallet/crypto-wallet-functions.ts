import * as ethers from "ethers";
import { EbsiWallet } from "@cef-ebsi/wallet-lib";
import { calculateJwkThumbprint, exportJWK, generateKeyPair } from "jose";
import { base64url } from "multiformats/bases/base64";
import { logger } from "@esspass-web-wallet/logging-lib";

const applicationName = "esspass-web-wallet";
const log = logger("crypto-lib");

function generateDidDocument(nssiWallet: any) {
  try {
    const context = [
      "https://www.w3.org/ns/did/v1",
      "https://w3id.org/security/suites/jws-2020/v1",
    ];

    const didDocument = {
      "@context": context,
      id: nssiWallet.did,
      verificationMethod: [] as any[],
      authentication: [] as string[],
      assertionMethod: [] as string[],
    };

    const ebsiWallet = JSON.parse(nssiWallet.ebsiWallet);

    didDocument.verificationMethod.push({
      id: ebsiWallet.keys.ES256.id,
      type: "JsonWebKey2020", // all type are the same string so made it easy for now.
      controller: nssiWallet.did,
      publicKeyJwk: ebsiWallet.keys.ES256.publicKeyJwk,
    });

    didDocument.authentication.push(ebsiWallet.keys.ES256.id);
    didDocument.assertionMethod.push(ebsiWallet.keys.ES256.id);
    return didDocument;
  } catch (ex) {
    log.exception(
      applicationName +
        ":walletCryptoFunctions:generateDidDocument:An Exception occurred [" +
        ex +
        "]."
    );
  }
  return null;
}

function newKeyId() {
  const ids = ["keys-1", "keys-2", "keys-3", "keys-4", "keys-5"];
  /* check isf key is already used => doesn't work in client.js*/
  return ids[0];
}

function getPublicKeyES256KJwk(jwk: any) {
  const { d, ...publicJwk } = jwk;
  return publicJwk;
}

function getES256KJWK(did: any, privateKeyJwk: any) {
  type T = { [keys: string | number]: string };
  const keyStruct: T | any = {};

  try {
    keyStruct.id = did + "#" + newKeyId();
    keyStruct.privateKeyJwk = privateKeyJwk;
    keyStruct.publicKeyJwk = getPublicKeyES256KJwk(privateKeyJwk);
    keyStruct.privateKeyEncryptionJwk = privateKeyJwk;
    keyStruct.publicKeyEncryptionJwk = keyStruct.publicKeyJwk;
  } catch (ex) {
    log.exception(
      applicationName + ":walletCryptoFunctions:getES256KJWK:An Exception occurred [" + ex + "]."
    );
  }

  return keyStruct;
}

function getPrivateKeyJwk(privateKeyHex: any) {
  try {
    const publicKeyJWK = new EbsiWallet(privateKeyHex).getPublicKey({
      format: "jwk",
    });
    const d = Buffer.from(removePrefix0x(privateKeyHex), "hex")
      .toString("base64")
      .replace(/\+/g, "-")
      .replace(/\//g, "_")
      .replace(/=/g, "");
    return { publicKeyJWK, d };
  } catch (ex) {
    log.exception(
      applicationName +
        ":walletCryptoFunctions:getPrivateKeyJwk:An Exception occurred [" +
        ex +
        "]."
    );
  }
  throw new Error("walletCryptoFunctions:getPrivateKeyJwk:An Exception occurred");
}

async function createES256KJWK(did: any, privateKey: any) {
  try {
    const ES256K: any = {};

    log.trace(applicationName + ":walletCryptoFunctions:createES256KJWK:Started ");

    const privateKeyJwk = getPrivateKeyJwk(privateKey);
    ES256K.ES256K = getES256KJWK(did, privateKeyJwk);

    log.trace(applicationName + ":walletCryptoFunctions:createES256KJWK:Done ");
    return ES256K;
  } catch (ex) {
    log.exception(
      applicationName + ":walletCryptoFunctions:createES256KJWK:An Exception occurred [" + ex + "]."
    );
  }
  throw new Error("walletCryptoFunctions:createES256KJWK:An Exception occurred");
}

async function init() {
  try {
    log.trace(applicationName + ":walletCryptoFunctions:init:Started ");
    log.trace(applicationName + ":walletCryptoFunctions:init:Done ");
  } catch (ex) {
    log.exception(
      applicationName + ":walletCryptoFunctions:init:An Exception occurred [" + ex + "]."
    );
  }
}

function removePrefix0x(key: any) {
  try {
    log.trace(applicationName + ":walletCryptoFunctions:removePrefix0x:Started ");

    const output = key.startsWith("0x") ? key.slice(2) : key;

    log.debug(applicationName + ":walletCryptoFunctions:removePrefix0x:output:[" + output + "] ");
    log.trace(applicationName + ":walletCryptoFunctions:removePrefix0x:Done ");
    return output;
  } catch (ex) {
    log.exception(
      applicationName + ":walletCryptoFunctions:removePrefix0x:An Exception occurred [" + ex + "]."
    );
  }
}

function fromHexString(hexString: any) {
  try {
    log.trace(applicationName + ":walletCryptoFunctions:fromHexString:Started ");

    const strippedhexString = removePrefix0x(hexString);
    const output = Buffer.from(strippedhexString, "hex");

    log.debug(applicationName + ":walletCryptoFunctions:fromHexString:output:[" + output + "] ");
    log.trace(applicationName + ":walletCryptoFunctions:fromHexString:Done ");
    return output;
  } catch (ex) {
    log.exception(
      applicationName + ":walletCryptoFunctions:fromHexString:An Exception occurred [" + ex + "]."
    );
  }
  throw new Error("walletCryptoFunctions:fromHexString:An Exception occurred");
}

/**
 *  Produce NPDID v2
 */
async function createRandomNPDID() {
  try {
    const ALG = "ES256K";
    await log.trace(applicationName + ":walletCryptoFunctions:createRandomNPDID:Started ");

    const response = {} as any;
    const { publicKey } = await generateKeyPair(ALG);
    const jwk = await exportJWK(publicKey);
    const thumbprint = await calculateJwkThumbprint(jwk, "sha256");

    log.traceObj("thumbprint=>", thumbprint);
    const subjectIdentifier = base64url.baseDecode(thumbprint);
    log.traceObj("subjectIdentifier=>", subjectIdentifier);
    response.did = EbsiWallet.createDid("NATURAL_PERSON", subjectIdentifier);
    log.traceObj("response.did=>", response.did);
    response.id = `${response.did}#${thumbprint}`;
    await log.trace(applicationName + ":walletCryptoFunctions:createRandomNPDID:Done ");
    log.traceObj("response=>", response);
    return response;
  } catch (ex) {
    await log.exception(
      applicationName +
        ":walletCryptoFunctions:createRandomNPDID:An Exception Occurred [" +
        ex +
        "]."
    );
  }

  await log.error(
    applicationName + ":walletCryptoFunctions:createRandomNPDID:An Exception Occurred"
  );

  throw new Error("walletCryptoFunctions:createRandomNPDID:An Exception occurred");
}

async function createRandomWallet() {
  try {
    return ethers.Wallet.createRandom();
  } catch (ex) {
    log.exception(
      applicationName +
        ":walletCryptoFunctions:createRandomWallet: An Exception Occurred [" +
        ex +
        "]."
    );
  }

  throw new Error("':walletCryptoFunctions:createRandomWallet: An Exception Occurred");
}

export {
  init,
  createRandomWallet,
  createES256KJWK,
  generateDidDocument,
  createRandomNPDID,
  getPrivateKeyJwk,
};
