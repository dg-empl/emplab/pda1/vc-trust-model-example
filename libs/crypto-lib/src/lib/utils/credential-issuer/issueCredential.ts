import { EbsiVerifiableAttestation } from "@cef-ebsi/verifiable-credential";
import { randomBytes, randomUUID } from "node:crypto";
import type { CredentialRequest } from "./validators";
import { Pda1Type } from "@esspass-web-wallet/domain-lib";

export async function issueCredential(
  issuerDid: string,
  issuerAccreditationUrl: string,
  authorisationCredentialSchema: string,
  additionalVcPayload: Partial<EbsiVerifiableAttestation>,
  credentialRequest: CredentialRequest,
  pdaPayload: Pda1Type,
  // TODO fix #73 4 - proof from the 3rd party wallet containing wallet holder's DID jwt / body / iss
  proofIssuerDid: string
): Promise<{
  reservedAttributeId: string;
  vcJwt: string;
}> {
  /**
   * Reserved Attribute ID
   *
   * When the Verifiable Credential is intended to be in the Trusted
   * Issuers Registry the issuer mock makes a preregistration (aka invitation)
   * defining the metadata, and later on the subject registers the
   * content of the credential (aka acceptance). The "reserved attribute
   * ID" acts an identifier to be able to update or consult this credential
   * in the Trusted Issuers Registry.
   */
  let reservedAttributeId = "";
  if (
    credentialRequest.types.some(t =>
      [
        "VerifiableAuthorisationForTrustChain",
        "VerifiableAccreditationToAccredit",
        "VerifiableAccreditationToAttest",
      ].includes(t)
    )
  ) {
    reservedAttributeId = `0x${randomBytes(32).toString("hex")}`;
  }

  const issuedAt = new Date();
  const issuanceDate = `${issuedAt.toISOString().slice(0, -5)}Z`;
  const expiresAt = new Date(
    issuedAt.getTime() + 1000 * 60 * 60 * 24 // 24 hours
  );
  const expirationDate = `${expiresAt.toISOString().slice(0, -5)}Z`;

  const vcPayload: EbsiVerifiableAttestation = {
    "@context": ["https://www.w3.org/2018/credentials/v1"],
    id: `vc:ebsi:conformance#${randomUUID()}`,
    type: credentialRequest.types,
    issuer: issuerDid,
    issuanceDate,
    issued: issuanceDate,
    validFrom: issuanceDate,
    expirationDate,
    credentialSubject: {
      id: proofIssuerDid,
      // id: accessTokenPayload.sub,
      ...(issuerAccreditationUrl &&
        ["VerifiableAccreditationToAttest", "VerifiableAccreditationToAccredit"].some(type =>
          credentialRequest.types.includes(type as any)
        ) && {
          accreditedFor: [
            {
              schemaId: authorisationCredentialSchema,
              types: [
                "VerifiableCredential",
                "VerifiableAttestation",
                "CTRevocable", // Is allowed to issue CTRevocable VCs
              ],
              limitJurisdiction: "https://publications.europa.eu/resource/authority/atu/FIN",
            },
          ],
        }),
      ...(reservedAttributeId && { reservedAttributeId }),
      ...(credentialRequest.types.includes("VerifiablePortableDocumentA1") && {
        ...pdaPayload,
        id: proofIssuerDid,
      }),
    },
    credentialSchema: {
      id: authorisationCredentialSchema,
      type: "FullJsonSchemaValidator2021",
    },
    ...(issuerAccreditationUrl && {
      termsOfUse: {
        id: issuerAccreditationUrl,
        type: "IssuanceCertificate",
      },
    }),
    ...additionalVcPayload,
  };

  const vcJwt = JSON.stringify(vcPayload);
  return { vcJwt, reservedAttributeId };
}

export default issueCredential;
