import mongoose from "mongoose";

const Schema = mongoose.Schema;

const pda1Schema = new Schema({
  pda1Data: String,
  requestFormId: { type: String, default: "" },
  vcPda1Id: { type: String, default: "" },
  dateOfRegistration: Number,
  validationResult: String,
  pda1Status: {
    type: String,
    enum: ["Issued", "Emailed", "Revoked"],
    default: "Issued",
  },
  emailedTo: { type: String, default: "" },
});

export const pda1documents = mongoose.model("pda1documents", pda1Schema);
