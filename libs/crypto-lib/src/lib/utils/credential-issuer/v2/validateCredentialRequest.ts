import { CredentialError } from "../errors";
import {
  TokenRequestPayload,
  tokenRequestPayloadSchema,
} from "../../../domain/v2/token-request.schema";
import { decodeJwt } from "jose";

export async function validateCredentialRequest(
  proofToken: string,
  nonceValue: string
): Promise<void> {
  if (!proofToken) {
    throw new CredentialError("invalid_token", {
      errorDescription: "Token Request Jwt is missing",
    });
  }

  const rawProofToken = decodeJwt(proofToken);

  // Validate Token Request header
  const parseTokenRequestObject = tokenRequestPayloadSchema.safeParse(rawProofToken);
  if (!parseTokenRequestObject.success) {
    const errorDesc = parseTokenRequestObject["error"].issues
      .map((issue: { path: any[]; message: any }) => `'${issue.path.join(".")}': ${issue.message}`)
      .join("\n");
    throw new CredentialError("invalid_token", {
      errorDescription: `Invalid Token Request object: ${errorDesc}`,
    });
  }

  const tokenRequestPayload: TokenRequestPayload = tokenRequestPayloadSchema.parse(rawProofToken);

  if (tokenRequestPayload["nonce"] !== nonceValue) {
    throw new CredentialError("invalid_token", {
      errorDescription: "Invalid Token Request payload. Nonce mismatch.",
    });
  }
}
