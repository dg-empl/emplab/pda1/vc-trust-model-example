import { decodeJwt, decodeProtectedHeader, importJWK, JWTPayload, jwtVerify } from "jose";

import { CredentialError } from "../errors";
import {
  PreAuthCodeGrants,
  preAuthCodeGrantsPayloadSchema,
  preAuthCodeGrantsSchema,
} from "../../../domain/v2/pre-auth-code.grants";
import { JWKWithKid } from "../../getKeyPair";

export async function validatePreAuthCode(
  preAuthJwt: PreAuthCodeGrants,
  issuerUrl: string,
  authPublicKeyJwk: JWKWithKid,
  userPin = ""
): Promise<void> {
  if (!preAuthJwt) {
    throw new CredentialError("invalid_token", {
      errorDescription: "PreAuthJwt header is missing",
    });
  }

  if (!preAuthJwt["urn:ietf:params:oauth:grant-type:pre-authorized_code"]) {
    throw new CredentialError("invalid_token", {
      errorDescription: "PreAuthJwt header must contain a pre-authorized_code element",
    });
  }

  // Validate PreAuthCode Token JWT header
  const parsedPreAuthCodeObject = preAuthCodeGrantsSchema.safeParse(preAuthJwt);
  if (!parsedPreAuthCodeObject.success) {
    const errorDesc = parsedPreAuthCodeObject["error"].issues
      .map((issue: { path: any[]; message: any }) => `'${issue.path.join(".")}': ${issue.message}`)
      .join("\n");
    throw new CredentialError("invalid_token", {
      errorDescription: `Invalid PreAuthCode Token header: ${errorDesc}`,
    });
  }

  const preAuthorizedCodeObject =
    preAuthJwt["urn:ietf:params:oauth:grant-type:pre-authorized_code"];

  // Try to decode PreAuthCode Token header
  let rawPreAuthCodeHeaderParams;
  try {
    rawPreAuthCodeHeaderParams = decodeProtectedHeader(
      preAuthorizedCodeObject["pre-authorized_code"]
    );
  } catch (e) {
    console.log(e);
    throw new CredentialError("invalid_token", {
      errorDescription: "Invalid PreAuthCode Token header. Parsing failed.",
    });
  }

  // Try to decode PreAuthCode Token payload
  let rawPreAuthCodeTokenPayload: JWTPayload;
  try {
    rawPreAuthCodeTokenPayload = decodeJwt(preAuthorizedCodeObject["pre-authorized_code"]);
  } catch (e) {
    throw new CredentialError("invalid_token", {
      errorDescription: "Invalid Pre Auth Code grants payload. Parsing failed.",
    });
  }

  // Validate PreAuthCode Token payload
  const parsedPreAuthCodeTokenPayload = preAuthCodeGrantsPayloadSchema.safeParse(
    rawPreAuthCodeTokenPayload
  );

  if (!parsedPreAuthCodeTokenPayload.success) {
    const errorDesc = parsedPreAuthCodeTokenPayload["error"].issues
      .map((issue: { path: any[]; message: any }) => `'${issue.path.join(".")}': ${issue.message}`)
      .join("\n");

    throw new CredentialError("invalid_token", {
      errorDescription: `Invalid PreAuthCode Token payload: ${errorDesc}`,
    });
  }

  // Validate that Issuer URI is included in the access token "aud"
  const preAuthTokenPayload = parsedPreAuthCodeTokenPayload.data;
  if (!preAuthTokenPayload.iss.includes(issuerUrl)) {
    throw new CredentialError("invalid_token", {
      errorDescription: `Invalid PreAuthCode Token payload: the iss should be from ${issuerUrl}`,
    });
  }

  // Make sure the Access Token is valid
  const now = Math.floor(Date.now() / 1000);
  if (preAuthTokenPayload.exp < now) {
    throw new CredentialError("invalid_token", {
      errorDescription: "The PreAuthCode token is expired",
    });
  }

  if (preAuthTokenPayload.iat > now) {
    throw new CredentialError("invalid_token", {
      errorDescription: "The PreAuthCode token is not yet valid",
    });
  }

  // Verify pre auth gen token kid + signature
  // Note: ideally, the Issuer Mock should dynamically retrieve the public key of the Auth Mock.
  // However, since they're both on the same server, we can import the public key directly.
  if (rawPreAuthCodeHeaderParams.kid !== authPublicKeyJwk.kid) {
    throw new CredentialError("invalid_token", {
      errorDescription: "Invalid PreAuthCode Token header: kid doesn't match public key",
    });
  }

  const authPublicKey = await importJWK(authPublicKeyJwk);
  try {
    await jwtVerify(
      preAuthJwt["urn:ietf:params:oauth:grant-type:pre-authorized_code"]["pre-authorized_code"],
      authPublicKey
    );
  } catch (e) {
    console.log(e);
    throw new CredentialError("invalid_token", {
      errorDescription: `Invalid PreAuthCode Token: ${
        e instanceof Error ? e.message : "invalid signature"
      }`,
    });
  }
}
