import { decodeJwt, decodeProtectedHeader, importJWK, jwtVerify } from "jose";

import { CredentialError } from "../errors";
import {
  PreAuthCodeGrantsPayload,
  preAuthCodeGrantsPayloadSchema,
} from "../../../domain/v2/pre-auth-code.grants";
import { JWKWithKid } from "../../getKeyPair";
import getUserPin from "../../getUserPin";

export async function validateTokenRequest(
  preAuthorizedCode: string,
  authPublicKeyJwk: JWKWithKid,
  userPin = ""
): Promise<void> {
  if (!preAuthorizedCode) {
    throw new CredentialError("invalid_token", {
      errorDescription: "PreAuthCodeGrant Jwt is missing",
    });
  }

  if (!userPin) {
    throw new CredentialError("invalid_token", {
      errorDescription: "UserPin is missing",
    });
  }

  const preAuthTokenPayload: PreAuthCodeGrantsPayload = preAuthCodeGrantsPayloadSchema.parse(
    decodeJwt(preAuthorizedCode as string)
  );

  // Try to decode PreAuthCode Token header
  let rawPreAuthCodeHeaderParams;
  try {
    rawPreAuthCodeHeaderParams = decodeProtectedHeader(preAuthorizedCode as string);
  } catch (e) {
    console.log(e);
    throw new CredentialError("invalid_token", {
      errorDescription: "Invalid PreAuthCode Token header. Parsing failed.",
    });
  }

  // Make sure the Access Token is valid
  const now = Math.floor(Date.now() / 1000);
  if (preAuthTokenPayload.exp < now) {
    throw new CredentialError("invalid_token", {
      errorDescription: "The PreAuthCode token is expired",
    });
  }

  if (preAuthTokenPayload.iat > now) {
    throw new CredentialError("invalid_token", {
      errorDescription: "The PreAuthCode token is not yet valid",
    });
  }

  // Verify pre auth gen token kid + signature
  // Note: ideally, the Issuer Mock should dynamically retrieve the public key of the Auth Mock.
  // However, since they're both on the same server, we can import the public key directly.
  if (rawPreAuthCodeHeaderParams.kid !== authPublicKeyJwk.kid) {
    throw new CredentialError("invalid_token", {
      errorDescription: "Invalid PreAuthCode Token header: kid doesn't match public key",
    });
  }

  const authPublicKey = await importJWK(authPublicKeyJwk);
  try {
    await jwtVerify(preAuthorizedCode, authPublicKey);
  } catch (e) {
    console.log(e);
    throw new CredentialError("invalid_token", {
      errorDescription: `Invalid PreAuthCode Token: ${
        e instanceof Error ? e.message : "invalid signature"
      }`,
    });
  }

  const pin = String(getUserPin(preAuthorizedCode)) === String(userPin);
  if (!pin) {
    throw new CredentialError("invalid_token", {
      errorDescription: `Invalid PreAuthCode Token: ${userPin} is invalid`,
    });
  }
}
