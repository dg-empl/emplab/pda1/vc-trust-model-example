import { z } from "zod";
import { authorizationDetailsSchema } from "../../../validators";

/**
 * Access Token Payload, as issued by Auth Mock.
 */
export const accessTokenPayloadSchema = z.object({
  iss: z.string().url(),
  aud: z.union([z.array(z.string().url()), z.string().url()]),
  sub: z.string(),
  iat: z.number(),
  exp: z.number(),
  nonce: z.string(),
  claims: z.object({
    authorization_details: authorizationDetailsSchema,
    c_nonce: z.string(),
    c_nonce_expires_in: z.number(),
    client_id: z.string().url(),
  }),
});

export type AccessTokenPayload = z.infer<typeof accessTokenPayloadSchema>;

/**
 * Access Token Header
 */
export const accessTokenHeaderSchema = z.object({
  typ: z.literal("JWT"),
  alg: z.literal("ES256"),
  kid: z.string(),
});

export type AccessTokenHeader = z.infer<typeof accessTokenHeaderSchema>;
