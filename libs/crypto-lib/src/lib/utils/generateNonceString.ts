import { randomBytes } from "crypto";

/**
 * Generates a random nonce string of the given length.
 * @param length
 */
export function generateNonceString(length = 16): string {
  const nonceBytes = randomBytes(length);
  return nonceBytes.toString("hex");
}
