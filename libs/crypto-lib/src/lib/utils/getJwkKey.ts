import axios from "axios";
import { calculateJwkThumbprint, importJWK, JWK } from "jose";
import { JWKWithKid } from "./getKeyPair";
import { NotFoundError } from "@esspass-web-wallet/domain-lib";

export async function getJwkKey(jwksEndpoint: any, kid: any = "") {
  let jwksResponse, key;

  try {
    jwksResponse = await axios.get(jwksEndpoint);
  } catch (e) {
    throw new NotFoundError(`Failed to get JWKS from ${jwksEndpoint}`);
  }

  const jwks = jwksResponse && jwksResponse.data;

  if (!kid) {
    key = jwks.keys[0];
  } else {
    key = jwks.keys.find((key: { kid: any }) => key.kid === kid);
  }

  const jwkWithKid: JWKWithKid = {
    ...key,
    kid: await calculateJwkThumbprint(key as JWK),
  };

  return await importJWK(jwkWithKid);
}
