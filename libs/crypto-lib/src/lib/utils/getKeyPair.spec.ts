/**
 * @jest-environment node
 */
import { getKeyPair } from "./getKeyPair";
import { stringToHex } from "./string-hex/string-hex";

describe("getKeyPair", () => {
  it("should generate jwk key pair ", async () => {
    const privateKeyString = "%%#ASdwyy43\\sds*798$";
    const privateKeyHex = stringToHex(privateKeyString);

    const keyPair = await getKeyPair(privateKeyHex);

    expect(keyPair.publicKeyJwk).toMatchSnapshot();
    expect(keyPair.privateKeyJwk).toMatchSnapshot();
  });
});
