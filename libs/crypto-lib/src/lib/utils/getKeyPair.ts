import { ec as EC } from "elliptic";
import { base64url, calculateJwkThumbprint } from "jose";
import type { JWK } from "jose";

export type JWKWithKid = JWK & { kid: string };

export type KeyPair = {
  privateKeyJwk: JWKWithKid;
  publicKeyJwk: JWKWithKid;
};

export const issuerKeyPair: Record<"ES256" | "ES256K", KeyPair | undefined> = {
  ES256: undefined,
  ES256K: undefined,
};

export async function getKeyPair(privateKey: string | JWK, alg = "ES256"): Promise<KeyPair> {
  if (!privateKey) {
    throw new Error("You must provide a non-empty hexadecimal private key");
  }

  let jwk: JWK;
  let d: string;
  if (typeof privateKey === "string") {
    let ec: EC;
    if (alg === "ES256") {
      ec = new EC("p256");
    } else if (alg === "ES256K") {
      ec = new EC("secp256k1");
    } else {
      throw new Error(`alg ${alg} not supported`);
    }

    // Get key pair from hex private key
    const keyPair = ec.keyFromPrivate(privateKey, "hex");

    // Validate key pair
    const validation = keyPair.validate();
    if (!validation.result) {
      throw new Error(validation.reason);
    }

    // Format as JWK
    const pubPoint = keyPair.getPublic();
    jwk = {
      kty: "EC",
      crv: alg === "ES256" ? "P-256" : "secp256k1",
      alg,
      x: base64url.encode(pubPoint.getX().toBuffer("be", 32)),
      y: base64url.encode(pubPoint.getY().toBuffer("be", 32)),
    };

    d = base64url.encode(Buffer.from(privateKey, "hex"));
  } else {
    const { d: privateExponent, ...pubKeyJwk } = privateKey;
    d = privateExponent as string;
    jwk = pubKeyJwk;
  }

  const thumbprint = await calculateJwkThumbprint(jwk);

  const publicKeyJwk = {
    ...jwk,
    kid: thumbprint,
  };

  const privateKeyJwk = {
    ...publicKeyJwk,
    d,
  };

  return {
    publicKeyJwk,
    privateKeyJwk,
  };
}
