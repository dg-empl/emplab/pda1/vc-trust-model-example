import { describe, expect, it } from "@jest/globals";
import { getUserPin } from "./getUserPin";

describe("getUserPin", () => {
  it("should return the expected PIN for a given DID", () => {
    const did =
      "did:key:z2dmzD81cgPx8Vki7JbuuMmFYrWPgYoytykUZ3eyqht1j9KbsDbVZXdb3jzCagESyY4EE2x7Yjx3gNwctoEuRCKKDrdNP3HPFtG8RTvBiYStT5ghBHhHizH2Dy6xQtW3Pd2SecizL9b2jzDCMr7Ka5cRAWZFwvqwAtwTT7xet769y9ERh6";
    expect(getUserPin(did)).toBe("5910");
  });
});
