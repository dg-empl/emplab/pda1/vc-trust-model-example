import { createHash } from "node:crypto";

/**
 * Computes the user PIN based on the DID:
 * - Hashes the DID with SHA256
 * - Takes the last 4 bytes
 * - Maps each byte to a digit between 0 and 9
 * - Concatenates the digits
 *
 * @param data - The user generated nonce string
 * @returns The user PIN
 */
export function getUserPin(data: string) {
  return createHash("sha256")
    .update(data)
    .digest()
    .slice(-4)
    .map(byte => byte % 10)
    .reduce((acc, digit) => `${acc}${digit}`, "");
}

export default getUserPin;
