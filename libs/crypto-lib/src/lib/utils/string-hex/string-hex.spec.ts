/**
 * @jest-environment node
 */
import { hexToBytes, hexToString, stringToHex } from "./string-hex";

describe("StringHex", () => {
  it("should convert to 32chars hex", async () => {
    //given
    const original32CharsString = 'Th!Is IsA0$%(*@ str (") ESSPASA%';

    if (original32CharsString.length !== 32) throw new Error("Invalid string length");

    //when
    const hex32chars = stringToHex(original32CharsString);

    //then
    expect(hex32chars).toEqual("546821497320497341302425282a402073747220282229204553535041534125");
  });

  it("should convert from 32chars to string", async () => {
    //given
    const originalHex32String = "546821497320497341302425282a402073747220282229204553535041534125";

    //when
    const chars = hexToString(originalHex32String);

    //then
    expect(chars).toEqual('Th!Is IsA0$%(*@ str (") ESSPASA%');
  });
});
