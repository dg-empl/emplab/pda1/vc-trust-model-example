import * as u8a from "uint8arrays";

/**
 * Convert a string to a hex string.
 * @param input
 * @returns {string} The hex string.
 */
export function stringToHex(input: string): string {
  // Create a buffer from the string.
  const buffer = Buffer.from(input);

  // Convert the buffer to a hex string.
  let hexString = buffer.toString("hex");

  // Add leading zeros to the hex string until it is 32 characters long.
  while (hexString.length < 32) {
    hexString = "0" + hexString;
  }

  // Return the hex string.
  return hexString;
}

/**
 * Convert a hex string to a string.
 * @param hexString
 * @returns {string} The string.
 */
export function hexToString(hexString: string): string {
  // Create a buffer from the hex string.
  const buffer = Buffer.from(hexString, "hex");
  // Return the string.
  return buffer.toString();
}

export function hexToBytes(s: string): Uint8Array {
  const input = s.startsWith("0x") ? s.substring(2) : s;
  return u8a.fromString(input.toLowerCase(), "base16");
}
