/* eslint-disable */
export default {
  displayName: "domain-lib",
  preset: "../../jest.preset.js",
  testEnvironment: "node",
  setupFiles: [
    // example
    // "<rootDir>/src/app/application/pda1/.env.js",
  ],
  transform: {
    "^.+\\.[tj]s$": ["ts-jest", { tsconfig: "<rootDir>/tsconfig.spec.json" }],
  },
  moduleFileExtensions: ["ts", "js", "html"],
  coverageDirectory: "../../coverage/libs/domain-lib",
};
