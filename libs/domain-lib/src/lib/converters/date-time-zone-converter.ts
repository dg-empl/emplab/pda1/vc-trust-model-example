import { JsonConverter, JsonCustomConvert } from 'json2typescript';
import moment from 'moment';

@JsonConverter
export class DateTimeZoneConverter implements JsonCustomConvert<Date | null> {
    serialize(date: Date): string {
        return moment.parseZone(date).toJSON();
    }

    deserialize(date: any): Date | null {
        return date ? new Date(date) : null;
    }
}
