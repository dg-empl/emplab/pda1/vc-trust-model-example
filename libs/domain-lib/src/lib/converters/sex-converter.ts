import { JsonConverter, JsonCustomConvert } from "json2typescript";

@JsonConverter
export class SexConverter implements JsonCustomConvert<string> {
  serialize(sexString: string): string {
    switch (sexString) {
      case "01":
        return "Male";
      case "02":
        return "Female";
      case "03":
        return "Other";
    }
    return "";
  }

  deserialize(sexString: string): string  {
    switch (sexString) {
      case "Male":
        return "01";
      case "Female":
        return "02";
      case "Other":
        return "03";
    }
    return "01";
  }
}
