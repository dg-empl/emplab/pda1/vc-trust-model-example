import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject("Did")
export class Did {
  @JsonProperty("did", String)
  did = ''
}
