import { CustomError } from "./custom-error";

export class DbConnectionError extends CustomError {
  readonly statusCode: number = 500;
  reason = 'Error connecting to database';

  constructor() {
    super('Error connecting to database');

    Object.setPrototypeOf(this, DbConnectionError.prototype);
  }

  serializeErrors(): { message: string; field?: string }[] {
    return [{message: this.reason}];
  }

}