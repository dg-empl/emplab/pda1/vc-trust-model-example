export type ErrorType = {
  errorCode: number;
  errorDescription: string;
}