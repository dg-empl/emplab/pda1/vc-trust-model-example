import { CustomError } from "./custom-error";

export class NotFoundError extends CustomError {
  readonly statusCode = 404;
  private readonly reason;

  constructor(message: string) {
    super(message);
    this.reason = message;
    Object.setPrototypeOf(this, NotFoundError.prototype);
  }

  serializeErrors() {
    return [{message: this.reason}];
  }
}