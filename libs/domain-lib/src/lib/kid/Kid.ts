import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject("Kid")
export class Kid {
  @JsonProperty("kid", String)
  kid = ''
}
