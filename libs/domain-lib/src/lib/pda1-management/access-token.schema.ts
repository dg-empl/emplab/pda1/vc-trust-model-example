import { z } from "zod";

// Example
// {
//   access_token: jwt,
//     token_type: "bearer",
//   expires_in: 86_400,
//   c_nonce: nonce,
//   c_nonce_expires_in: 86_400,
// };

export const accessTokenSchema = z.object({
  access_token: z.string(),
  token_type: z.string().includes("bearer"),
  expires_in: z.number(),
  c_nonce: z.string(),
  c_nonce_expires_in: z.number(),
});

export type AccessTokenType = z.infer<typeof accessTokenSchema>;
