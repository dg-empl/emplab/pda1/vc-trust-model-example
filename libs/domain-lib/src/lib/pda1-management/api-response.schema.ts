import { z } from "zod";
import { RequestFormStatusEnum } from "./request-form-action-status.types";
import { zodEnumFromObjKeys } from "./utils";

export const RequestFormsDb = z.object({
  index: z.number(),
  id: z.string(),
  requestFormData: z.string(),
  requestorEmail: z.string(),
  requestorFirstName: z.string(),
  requestorLastName: z.string(),
  dateOfRegistration: z.date(),
  validationResult: z.string(),
  requestFormStatus: zodEnumFromObjKeys(RequestFormStatusEnum),
});

export type RequestFormsDbType = z.infer<typeof RequestFormsDb>;

export const ApiResponseSchema = z.object({
  items: z.array(RequestFormsDb),
  size: z.number(),
});

export type ApiResponseType = z.infer<typeof ApiResponseSchema>;
