import {
  RequestFormActionEnum,
  requestFormActionEnumSchema,
  isInEnum,
} from "./request-form-action-status.types";

describe("Pda1ManagementInterfaces", () => {
  it("should work", () => {
    expect(requestFormActionEnumSchema.Accept.description).toEqual(
      requestFormActionEnumSchema["Accept"].description
    );
  });

  it("should validate that proper request action has been passed", () => {
    expect(isInEnum("Accept", RequestFormActionEnum)).toEqual(true);
    expect(isInEnum("Accepts", RequestFormActionEnum)).toEqual(false);
  });
});
