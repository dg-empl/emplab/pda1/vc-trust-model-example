import { z } from "zod";
import { pda1Schema } from "./pda1.interfaces";
import { zodEnumFromObjKeys } from "./utils";
import { RequestFormStatusEnum } from "./request-form-action-status.types";

export const Pda1Db = z.object({
  _id: z.string(),
  requestFormId: z.string(),
  vcPda1Id: z.string(),
  emailedTo: z.string(),
  pda1Data: pda1Schema,
  validationResult: z.string(),
  dateOfRegistration: z.date(),
  pda1Status: zodEnumFromObjKeys(RequestFormStatusEnum),
});

export type Pda1DbType = z.infer<typeof Pda1Db>;
