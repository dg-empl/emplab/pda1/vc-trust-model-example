// export enum Pda1StatusEnum {
//   Issued = "Issued",
//   Deleted = "Deleted",
//   Revoked = "Revoked",
//   Downloaded = "Downloaded",
//   // Rejected = "Rejected",
// }

import { z } from "zod";

export const pda1Schema = z.object({
  id: z.string(),
  section1: z.object({
    personalIdentificationNumber: z.string(),
    sex: z.string(),
    surname: z.string(),
    forenames: z.string(),
    dateBirth: z.string(),
    surnameAtBirth: z.string(),
    placeBirth: z.object({
      town: z.string(),
      region: z.string(),
      countryCode: z.string(),
    }),
    nationalities: z.array(z.string()),
    stateOfResidenceAddress: z.object({
      streetNo: z.string(),
      postCode: z.string(),
      town: z.string(),
      countryCode: z.string(),
    }),
    stateOfStayAddress: z.object({
      streetNo: z.string(),
      postCode: z.string(),
      town: z.string(),
      countryCode: z.string(),
    }),
  }),
  section2: z.object({
    memberStateWhichLegislationApplies: z.string(),
    startingDate: z.string(),
    endingDate: z.string(),
    certificateForDurationActivity: z.union([z.string(), z.boolean()]),
    determinationProvisional: z.boolean(),
    transitionRulesApplyAsEC8832004: z.boolean(),
  }),
  section3: z.object({
    postedEmployedPerson: z.union([z.string(), z.boolean()]),
    employedTwoOrMoreStates: z.union([z.string(), z.boolean()]),
    postedSelfEmployedPerson: z.union([z.string(), z.boolean()]),
    selfEmployedTwoOrMoreStates: z.union([z.string(), z.boolean()]),
    civilServant: z.union([z.string(), z.boolean()]),
    contractStaff: z.union([z.string(), z.boolean()]),
    mariner: z.union([z.string(), z.boolean()]),
    employedAndSelfEmployed: z.union([z.string(), z.boolean()]),
    civilAndEmployedSelfEmployed: z.union([z.string(), z.boolean()]),
    flightCrewMember: z.union([z.string(), z.boolean()]),
    exception: z.union([z.string(), z.boolean()]),
    exceptionDescription: z.string(),
    workingInStateUnder21: z.union([z.string(), z.boolean()]),
  }),
  section4: z.object({
    employee: z.union([z.string(), z.boolean()]),
    selfEmployedActivity: z.union([z.string(), z.boolean()]),
    employerSelfEmployedActivityCodes: z.array(z.string()),
    nameBusinessName: z.string(),
    registeredAddress: z.object({
      streetNo: z.string(),
      postCode: z.string(),
      town: z.string(),
      countryCode: z.string(),
    }),
  }),
  section5: z.object({
    workPlaceNames: z.optional(z.array(z.object({ companyNameVesselName: z.string() }))),
    workPlaceAddresses: z.optional(
      z.array(
        z.object({
          address: z.object({
            streetNo: z.string(),
            town: z.string(),
            postCode: z.string(),
            countryCode: z.string(),
          }),
        })
      )
    ),
    noFixedAddress: z.union([z.string(), z.boolean()]),
  }),
  section6: z.object({
    name: z.string(),
    address: z.object({
      streetNo: z.string(),
      postCode: z.string(),
      town: z.string(),
      countryCode: z.string(),
    }),
    institutionID: z.string(),
    officePhoneNo: z.string(),
    officeFaxNo: z.string(),
    email: z.string().email(),
    date: z.string(),
    signature: z.string(),
  }),
});

export type Pda1Type = z.infer<typeof pda1Schema>;
