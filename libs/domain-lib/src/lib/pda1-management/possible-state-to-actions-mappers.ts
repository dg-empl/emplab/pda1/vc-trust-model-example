import { RequestFormActionEnum, RequestFormStatusEnum, requestFormStatusEnumAdapter } from "./";

export const possibleStateToActionsMapper = (
  requestStatus: RequestFormStatusEnum
): RequestFormActionEnum[] => {
  let possibleActions: RequestFormActionEnum[] = [];
  switch (requestFormStatusEnumAdapter[requestStatus]) {
    case RequestFormActionEnum.Accept:
      possibleActions = [
        RequestFormActionEnum.Delete,
        RequestFormActionEnum.Issue,
        RequestFormActionEnum.Reject,
      ];
      return possibleActions;
    case RequestFormActionEnum.Issue:
      possibleActions = [
        RequestFormActionEnum.Present,
        RequestFormActionEnum.Delete,
        RequestFormActionEnum.RevokeFunctionality,
        RequestFormActionEnum["3RDParty"],
      ];
      return possibleActions;
    case RequestFormActionEnum.RevokeFunctionality:
      possibleActions = [RequestFormActionEnum.Revoke, RequestFormActionEnum.Delete];
      return possibleActions;
    case RequestFormActionEnum.Delete:
      return possibleActions;
    case RequestFormActionEnum.Revoke:
      possibleActions = [RequestFormActionEnum.Delete];
      return possibleActions;
    case RequestFormActionEnum.Reject:
      possibleActions = [RequestFormActionEnum.Delete];
      return possibleActions;
    case RequestFormActionEnum["3RDParty"]:
      return possibleActions;
    default:
      return possibleActions;
  }
};
