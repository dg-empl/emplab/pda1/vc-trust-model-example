export enum RequestFormStatusEnum {
  Accepted = "Accepted",
  Issued = "Issued",
  Pending = "Pending",
  Deleted = "Deleted",
  Revoked = "Revoked",
  Downloaded = "Downloaded",
  "3RDPartyDownloaded" = "3RDPartyDownloaded",
  Rejected = "Rejected",
  Presented = "Presented",
}

export enum RequestFormActionEnum {
  Accept = "Accept",
  Issue = "Issue",
  Delete = "Delete",
  RevokeFunctionality = "RevokeFunctionality",
  Revoke = "Revoke",
  "3RDParty" = "3RDParty",
  Reject = "Reject",
  Present = "Present",
}

export const requestFormActionEnumSchema: Record<
  keyof typeof RequestFormActionEnum,
  { value: string; description: string }
> = {
  Accept: { value: "Accept", description: "Accept request form ?" },
  Issue: { value: "Issue PDA1 document", description: "Issue request form ? " },
  RevokeFunctionality: {
    value: "Generate verifiable credential with revocation functionality",
    description: "Generate QR code and send it through the email ?",
  },
  Revoke: { value: "Revocation", description: "Revoke pda1 application " },
  Delete: { value: "Hide", description: "Hide request form ?" },
  "3RDParty": {
    value: "3rd party wallet providers",
    description: "Generate QR code and security pin code ?",
  },
  Reject: { value: "Reject", description: "Reject request form ?" },
  Present: { value: "Present", description: "Present request form ?" },
};

export const validateRequestFormAction = (arg: string): boolean => {
  return isInEnum(arg, RequestFormActionEnum);
};

export const isInEnum = <T extends object>(arg: string, enumType: T): boolean => {
  const enumValues = Object.keys(enumType);
  return enumValues.some(value => value === arg);
};

export type RequestFormStatusEnumTypePartial = Partial<
  Record<keyof typeof RequestFormActionEnum, keyof typeof RequestFormStatusEnum>
>;

export const requestFormActionEnumAdapter: RequestFormStatusEnumTypePartial = {
  Accept: "Accepted",
  Issue: "Issued",
  RevokeFunctionality: "Downloaded",
  "3RDParty": "3RDPartyDownloaded",
  Revoke: "Revoked",
  Delete: "Deleted",
  Reject: "Rejected",
  Present: "Presented",
};

export const requestFormStatusEnumAdapter: Record<
  keyof typeof RequestFormStatusEnum,
  keyof typeof RequestFormActionEnum
> = {
  Accepted: "Accept",
  Issued: "Issue",
  Downloaded: "RevokeFunctionality",
  "3RDPartyDownloaded": "3RDParty",
  Revoked: "Revoke",
  Deleted: "Delete",
  Rejected: "Reject",
  Pending: "Accept",
  Presented: "Present",
};
