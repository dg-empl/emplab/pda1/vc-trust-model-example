import { z } from "zod";
import { credentialStatusSchema } from "../verifiable-credential-payload";

export const pda1DtoSchema = z.object({
  id: z.string(),
  date: z.string(),
  endingDate: z.string(),
  time: z.string(),
  status: z.string(),
  memberStateWhichLegislationApplies: z.string(),
  govInstitutionName: z.string(),
  credentialStatus: credentialStatusSchema,
});

export type Pda1Dto = z.infer<typeof pda1DtoSchema>;
