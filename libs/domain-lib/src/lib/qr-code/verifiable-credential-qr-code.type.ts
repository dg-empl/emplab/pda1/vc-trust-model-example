import { z } from "zod";

export const verifiableCredentialQrCodeSchema = z.object({
  "urn:ietf:params:oauth:grant-type:pre-authorized_code": z.object({
    "pre-authorized_code": z.string(),
    user_pin_required: z.boolean(),
  }),
});

export type VerifiableCredentialQrCode = z.infer<typeof verifiableCredentialQrCodeSchema>;
