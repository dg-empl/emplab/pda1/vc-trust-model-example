import { schemaPresentationDefinition } from "./verifibale-presentation-qr-code.type";

describe("VerifiablePresentationQRCodeType", () => {
  const response = {
    id: "verification_vp_request",
    input_descriptors: [
      {
        id: "verification_vp",
        name: "Verification VP",
        purpose: "Only accept a VP containing a Verification VA",
        constraints: {
          fields: [
            {
              path: ["$.vc.credentialSchema"],
              filter: {
                allOf: [
                  {
                    type: "array",
                    contains: {
                      type: "object",
                      properties: {
                        id: {
                          type: "string",
                          pattern:
                            "https://api-pilot.ebsi.eu/trusted-schemas-registry/v2/schemas/0xeb6d8131264327f3cbc5ddba9c69cb9afd34732b3b787e4b3e3507a25d3079e9",
                        },
                      },
                      required: ["id"],
                    },
                  },
                ],
              },
            },
          ],
        },
      },
    ],
    format: {
      jwt_vp: {
        alg: ["ES256"],
      },
    },
  };

  it("should create an instance", () => {
    const parse = schemaPresentationDefinition.safeParse(response);

    expect(parse.success).toBeTruthy();

    // expect(new VerifiablePresentationQRCodeType()).toBeTruthy();
  });
});
