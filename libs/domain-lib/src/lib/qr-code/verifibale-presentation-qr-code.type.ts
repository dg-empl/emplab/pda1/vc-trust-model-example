class VpToken {
  private readonly _presentation_definition: PresentationDefinition;
  private readonly _presentation_definition_uri: string;

  constructor(
    presentation_definition: PresentationDefinition,
    presentation_definition_uri: string
  ) {
    this._presentation_definition = presentation_definition;
    this._presentation_definition_uri = presentation_definition_uri;
  }

  get presentation_definition_uri(): string {
    return this._presentation_definition_uri;
  }

  get presentation_definition(): PresentationDefinition {
    return this._presentation_definition;
  }
}

class Claims {
  constructor(id_token: string, vp_token: VpToken) {
    this._id_token = id_token;
    this._vp_token = vp_token;
  }

  private _id_token: string;

  get id_token(): string {
    return this._id_token;
  }

  set id_token(value: string) {
    this._id_token = value;
  }

  private _vp_token: VpToken;

  get vp_token(): VpToken {
    return this._vp_token;
  }

  set vp_token(value: VpToken) {
    this._vp_token = value;
  }
}

export class VerifiablePresentationQrCode {
  constructor(
    clientId: string,
    redirectUri: string,
    claims: Claims,
    nonce: string,
    verification: string
  ) {
    this._clientId = clientId;
    this._redirectUri = redirectUri;
    this._claims = claims;
    this._nonce = nonce;
    this._verification = verification;
  }

  private _clientId: string;

  get clientId(): string {
    return this._clientId;
  }

  set clientId(value: string) {
    this._clientId = value;
  }

  private _redirectUri: string;

  get redirectUri(): string {
    return this._redirectUri;
  }

  set redirectUri(value: string) {
    this._redirectUri = value;
  }

  private _claims: Claims;

  get claims(): Claims {
    return this._claims;
  }

  set claims(value: Claims) {
    this._claims = value;
  }

  private _nonce: string;

  get nonce(): string {
    return this._nonce;
  }

  set nonce(value: string) {
    this._nonce = value;
  }

  private _verification: string;

  get verification(): string {
    return this._verification;
  }

  set verification(value: string) {
    this._verification = value;
  }

  getPresentationDefinitionUri(): string {
    return this.claims.vp_token.presentation_definition_uri;
  }
}

import { z } from "zod";

export const schemaPresentationDefinition = z.object({
  id: z.string(),
  input_descriptors: z.array(
    z.object({
      id: z.string(),
      name: z.string(),
      purpose: z.string(),
      constraints: z.object({
        fields: z.array(
          z.object({
            path: z.array(z.string()),
            filter: z.object({
              allOf: z.array(
                z.object({
                  type: z.string(),
                  contains: z.object({
                    type: z.string(),
                    properties: z.object({
                      id: z.object({ type: z.string(), pattern: z.string() }),
                    }),
                    required: z.array(z.string()),
                  }),
                })
              ),
            }),
          })
        ),
      }),
    })
  ),
  format: z.object({ jwt_vp: z.object({ alg: z.array(z.string()) }) }),
});

export type PresentationDefinition = z.infer<typeof schemaPresentationDefinition>;

export const extractCredentialId = (presentationDefinition: PresentationDefinition): string => {
  return presentationDefinition.input_descriptors[0].constraints.fields[0].filter.allOf[0].contains
    .properties.id.pattern;
};
