export type SortDirection = "asc" | "desc" | "";

export interface PageSort {
  pageIndex: number;
  pageSize: number;
  sort: {
    active: string;
    direction: SortDirection;
  };
}
