import { JsonConvert, ValueCheckingMode } from "json2typescript";

export declare interface Type<T> {
  new (...args: any[]): T;
}

const toTypeScriptObjectOfType = <T>(source: any, type: new () => T): T => {
  const jsonConvert = new JsonConvert();
  jsonConvert.valueCheckingMode = ValueCheckingMode.ALLOW_NULL;

  let destination: T;
  try {
    // @ts-expect-error: Unreachable code error
    destination = <T>jsonConvert.deserialize(source, type);
  } catch (e) {
    throw <Error>e;
  }
  return destination;
};

const toJsonFromTypeScriptObject = (source: any): string => {
  const jsonConvert = new JsonConvert();
  let destination: string;
  try {
    destination = jsonConvert.serialize(source);
  } catch (e) {
    throw <Error>e;
  }
  return JSON.stringify(destination);
};

const fromJSON = <T>(source: string, type: Type<T>): T => {
  const jsonObj = JSON.parse(source);
  return toTypeScriptObjectOfType(jsonObj, type);
};

const fromObjArrayToType = <T>(source: any[], type: Type<T>): T[] => {
  const jsonConvert = new JsonConvert();
  jsonConvert.valueCheckingMode = ValueCheckingMode.ALLOW_NULL;
  jsonConvert.ignorePrimitiveChecks = false;

  return source.map(item => {
    let destination: T;
    try {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      destination = <T>jsonConvert.deserialize(item, type);
    } catch (e) {
      throw <Error>e;
    }
    return destination;
  });
};

export { fromJSON, toTypeScriptObjectOfType, fromObjArrayToType, toJsonFromTypeScriptObject };
