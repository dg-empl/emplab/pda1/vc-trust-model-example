import { z } from "zod";

export const statusCredentialListSchema = z.object({
  "@context": z.array(z.string()),
  id: z.string(),
  type: z.array(z.string()),
  issuer: z.string(),
  issuanceDate: z.any(),
  validFrom: z.any(),
  expirationDate: z.any(),
  issued: z.any(),
  credentialSubject: z.object({
    id: z.string(),
    type: z.string(),
    statusPurpose: z.string(),
    encodedList: z.string(),
  }),
  credentialSchema: z.object({ id: z.string(), type: z.string() }),
});

export type StatusCredentialListType = z.infer<typeof statusCredentialListSchema>;
