// {
//   "access_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6Ikp..sHQ",
//   "token_type": "bearer",
//   "expires_in": 86400,
//   "c_nonce": "tZignsnFbp",
//   "c_nonce_expires_in": 86400
// }

import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject("TokenResponse")
export class TokenResponse {
  @JsonProperty("access_token", String)
  access_token: any = undefined;
  @JsonProperty("token_type", String)
  token_type: any = undefined;
  @JsonProperty("expires_in", Number)
  expires_in: any = undefined;
  @JsonProperty("c_nonce", String)
  c_nonce: any = undefined;
  @JsonProperty("c_nonce_expires_in", Number)
  c_nonce_expires_in: any = undefined;
}
