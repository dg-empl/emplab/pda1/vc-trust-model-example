
export enum TypeEnum {
  VerifiableCredential = "VerifiableCredential",
  VerifiableAttestation = "VerifiableAttestation"
}
