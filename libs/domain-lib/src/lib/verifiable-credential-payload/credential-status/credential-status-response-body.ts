import { credentialStatusSchema } from "./credential-status";
import { revocationStatusSchema } from "./revocation-status";
import { z } from "zod";

export const credentialStatusResponseBodySchema = z.object({
  credentialStatusObject: credentialStatusSchema,
  revocationStatus: revocationStatusSchema,
});

export type CredentialStatusResponseBody = z.infer<typeof credentialStatusResponseBodySchema>;
