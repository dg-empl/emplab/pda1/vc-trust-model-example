import { credentialStatusResponseBodySchema } from "./credential-status-response-body";
import { z } from "zod";

export const credentialStatusResponseSchema = z.object({
  returnCode: z.number(),
  resultClass: z.number(),
  returnMsg: z.string(),
  body: z.array(credentialStatusResponseBodySchema),
});

export type CredentialStatusResponse = z.infer<typeof credentialStatusResponseSchema>;
