// credentialStatus: {
//   id: "did:ebsi:zsqBYQuKunNdr3XfGfPUvU7",
//     type: "StatusList2021Entry",
//     statusPurpose: "revocation",
//     statusListIndex: "96",
//     statusListCredential:
//   "https://api-pilot.ebsi.eu/trusted-issuers-registry/v4/issuers/did:ebsi:zsqBYQuKunNdr3XfGfPUvU7/proxies/0xe34e137dbefa842d71c0c840ddb4900af3611e297d8403e80f99a13e187e8d63/credentials/status/1#list",
// },

import { z } from "zod";

export const credentialStatusSchema = z.object({
  id: z.string(),
  type: z.string(),
  statusPurpose: z.string(),
  statusListIndex: z.string(),
  statusListCredential: z.string(),
});

export type CredentialStatus = z.infer<typeof credentialStatusSchema>;
