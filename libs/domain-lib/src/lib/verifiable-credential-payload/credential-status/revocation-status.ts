import { z } from "zod";

export const revocationStatusSchema = z.object({
  returnCode: z.number(),
  resultClass: z.number(),
  returnMsg: z.string(),
  body: z.string(),
});

export type RevocationStatus = z.infer<typeof revocationStatusSchema>;
