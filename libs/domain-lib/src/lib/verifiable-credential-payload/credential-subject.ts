import { JsonObject, JsonProperty } from "json2typescript";
import { Section1 } from "./sections/Section1";
import { Section2 } from "./sections/Section2";
import { Section3 } from "./sections/Section3";
import { Section4 } from "./sections/Section4";
import { Section5 } from "./sections/Section5";
import { Section6 } from "./sections/Section6";

@JsonObject("CredentialSubject")
export class CredentialSubject {
  @JsonProperty("id", String, true)
  id = undefined;
  @JsonProperty("section1", Section1, true)
  section1 = undefined;
  @JsonProperty("section2", Section2, true)
  section2 = new Section2();
  @JsonProperty("section3", Section3, true)
  section3 = undefined;
  @JsonProperty("section4", Section4, true)
  section4 = undefined;
  @JsonProperty("section5", Section5, true)
  section5 = undefined;
  @JsonProperty("section6", Section6, true)
  section6 = undefined;
  @JsonProperty("status", String, true)
  status = undefined;
  @JsonProperty("time", String, true)
  time = "";
  @JsonProperty("govInstitutionName", String, true)
  govInstitutionName = "";
  @JsonProperty("pdaId", String, true)
  pdaId = "";
}
