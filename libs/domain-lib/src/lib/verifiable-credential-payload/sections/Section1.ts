import { JsonObject, JsonProperty } from 'json2typescript';
import { SexConverter } from "../../converters";
import { PlaceBirth } from './place-birth/PlaceBirth';
import { StateOfResidenceAddress } from './state-of-residence-address/StateOfResidenceAddress';

@JsonObject('Section1')
export class Section1 {
  @JsonProperty('personalIdentificationNumber', String, true)
  personalIdentificationNumber = "";
  @JsonProperty('sex', SexConverter , true)
  sex = "01";
  @JsonProperty('surname', String , true)
  surname = "";
  @JsonProperty('forenames', String , true)
  forenames = "";
  @JsonProperty('dateBirth', String , true)
  dateBirth = "";
  @JsonProperty('surnameAtBirth', String , true)
  surnameAtBirth = "";
  @JsonProperty('placeBirth', PlaceBirth , true)
  placeBirth = null;
  @JsonProperty('nationalities', [String] , true)
  nationalities = [];
  @JsonProperty('stateOfResidenceAddress', StateOfResidenceAddress , true)
  stateOfResidenceAddress = null;
  @JsonProperty('stateOfStayAddress', StateOfResidenceAddress , true)
  stateOfStayAddress = null;
}
