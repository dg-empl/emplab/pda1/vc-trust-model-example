import { JsonObject, JsonProperty } from 'json2typescript';

@JsonObject('Section2')
export class Section2 {
  @JsonProperty('memberStateWhichLegislationApplies', String, true)
  memberStateWhichLegislationApplies = '';
  @JsonProperty('startingDate', String , true)
  startingDate = "";
  @JsonProperty('endingDate', String , true)
  endingDate = "";
  @JsonProperty('certificateForDurationActivity', Boolean , true)
  certificateForDurationActivity = false;
  @JsonProperty('determinationProvisional', Boolean , true)
  determinationProvisional = false;
  @JsonProperty('transitionRulesApplyAsEC8832004', Boolean , true)
  transitionRulesApplyAsEC8832004 = false;
}
