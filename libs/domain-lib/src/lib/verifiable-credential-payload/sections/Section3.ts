import { JsonObject, JsonProperty } from 'json2typescript';

@JsonObject('Section3')
export class Section3 {
  @JsonProperty('postedEmployedPerson', Boolean, true)
  postedEmployedPerson = false;
  @JsonProperty('employedTwoOrMoreStates', Boolean , true)
  employedTwoOrMoreStates = false;
  @JsonProperty('postedSelfEmployedPerson', Boolean , true)
  postedSelfEmployedPerson = false;
  @JsonProperty('selfEmployedTwoOrMoreStates', Boolean , true)
  selfEmployedTwoOrMoreStates = false;
  @JsonProperty('civilServant', Boolean , true)
  civilServant = false;
  @JsonProperty('contractStaff', Boolean , true)
  contractStaff = false;
  @JsonProperty('mariner', Boolean , true)
  mariner = false;
  @JsonProperty('employedAndSelfEmployed', Boolean , true)
  employedAndSelfEmployed = false;
  @JsonProperty('civilAndEmployedSelfEmployed', Boolean , true)
  civilAndEmployedSelfEmployed = false;
  @JsonProperty('flightCrewMember', Boolean , true)
  flightCrewMember = false;
  @JsonProperty('exception', Boolean , true)
  exception = false;
  @JsonProperty('exceptionDescription', String , true)
  exceptionDescription = "";
  @JsonProperty('workingInStateUnder21', Boolean , true)
  workingInStateUnder21 = false;
}
