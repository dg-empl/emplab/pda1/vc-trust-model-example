import { JsonObject, JsonProperty } from 'json2typescript';
import { RegisteredAddress } from "./registered-address/RegisteredAddress";

@JsonObject('Section4')
export class Section4 {
  //  "section4": {
  //    "employee": false,
  //    "selfEmployedActivity": true,
  //    "employerSelfEmployedActivityCodes": "BE1234231",
  //    "nameBusinessName": "1",
  //    "registeredAddress": {
  //    }
  //  },
  @JsonProperty('employee', Boolean, true)
  employee = false;
  @JsonProperty('selfEmployedActivity', Boolean , true)
  selfEmployedActivity = false;
  @JsonProperty('employerSelfEmployedActivityCodes', [String] , true)
  employerSelfEmployedActivityCodes = ''
  @JsonProperty('nameBusinessName', String , true)
  nameBusinessName = '';
  @JsonProperty('registeredAddress', RegisteredAddress , true)
  registeredAddress = null;
}
