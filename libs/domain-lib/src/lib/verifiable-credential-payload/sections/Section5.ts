import { JsonObject, JsonProperty } from 'json2typescript';
import { RegisteredAddress } from "./registered-address/RegisteredAddress";

@JsonObject('WorkPlaceName')
export class WorkPlaceName {

  @JsonProperty('companyNameVesselName', String, true)
  companyNameVesselName = "";
}

@JsonObject('WorkPlaceAddresses')
export class WorkPlaceAddresses {

  @JsonProperty('address', RegisteredAddress, true)
  address = undefined;
}

@JsonObject('Section5')
export class Section5 {
  @JsonProperty('noFixedAddress', Boolean, true)
  noFixedAddress = false;
  @JsonProperty('workPlaceAddresses', [WorkPlaceAddresses], true)
  workPlaceAddresses = undefined;

  @JsonProperty('workPlaceNames', [WorkPlaceName], true)
  workPlaceNames = undefined;
}
