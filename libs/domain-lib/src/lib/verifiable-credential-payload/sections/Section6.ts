import { JsonObject, JsonProperty } from 'json2typescript';
import { RegisteredAddress } from "./registered-address/RegisteredAddress";

@JsonObject('Section6')
export class Section6 {
  //    "name": "National Institute for the Social Security of the Self-employed (NISSE)",
  //    "address": {
  //      "streetNo": "Quai de Willebroeck 35",
  //      "postCode": "1000",
  //      "town": "Bruxelles",
  //      "countryCode": "BE"
  //    },
  //    "institutionID": "NSSIE/INASTI/RSVZ",
  //    "officeFaxNo": "",
  //    "officePhoneNo": "0800 12 018",
  //    "email": "info@rsvz-inasti.fgov.be",
  //    "date": "2022-10-28",
  //    "signature": "Official signature"
  //  },
  @JsonProperty('name', String, true)
  name = '';
  @JsonProperty('address', RegisteredAddress , true)
  address = undefined;
  @JsonProperty('institutionID', String , true)
  institutionID = '';
  @JsonProperty('officeFaxNo', String , true)
  officeFaxNo = '';
  @JsonProperty('officePhoneNo', String , true)
  officePhoneNo = '';
  @JsonProperty('email', String , true)
  email = '';
  @JsonProperty('date', String , true)
  date = '';
  @JsonProperty('signature', String , true)
  signature = '';
}
