import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject('PlaceBirth')
export class PlaceBirth {
  @JsonProperty('region', String , true)
  region = "";
  @JsonProperty('town', String , true)
  town = "";
  @JsonProperty('countryCode', String , true)
  countryCode = "";
}
