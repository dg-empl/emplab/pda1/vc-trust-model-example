import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject('RegisteredAddress')
export class RegisteredAddress {

  //      "streetNo": "1, 1 1",
  //      "postCode": "1",
  //      "town": "1",
  //      "countryCode": "DE"

  @JsonProperty('streetNo', String, true)
  streetNo = "";
  @JsonProperty('postCode', String , true)
  postCode = "";
  @JsonProperty('town', String , true)
  town = '';
  @JsonProperty('countryCode', String , true)
  countryCode = '';
}
