import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject('StateOfResidenceAddress')
export class StateOfResidenceAddress {

  @JsonProperty('streetNo', String , true)
  streetNo = "";
  @JsonProperty('postCode', String , true)
  postCode = "";
  @JsonProperty('town', String , true)
  town = "";
  @JsonProperty('countryCode', String , true)
  countryCode = "";

}
