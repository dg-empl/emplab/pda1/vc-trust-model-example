import { VerifiableCredential } from "./verifiable-credential";

export type VerifiableCredentialStatusExtended = VerifiableCredential & {
  status: string;
  time: string;
  govInstitutionName: string;
};
