import { verifiableCredentialSchema } from "./verifiable-credential";

describe("VerifiableCredential", () => {
  const json = {
    "@context": ["https://www.w3.org/2018/credentials/v1"],
    id: "did:ebsi:zezz8fVpDYK3FNitovUL8izwrRqo5z8wmPMvQUAgJJH2K",
    type: ["VerifiableCredential", "VerifiableAttestation"],
    issuer: "did:ebsi:zhkNKqkC86BZuzrdGEU9acH",
    issuanceDate: "2023-10-23T23:11:53Z",
    validFrom: "2023-10-23T23:11:53Z",
    issued: "2023-10-23T23:11:53Z",
    credentialStatus: {
      id: "did:ebsi:zsqBYQuKunNdr3XfGfPUvU7",
      type: "StatusList2021Entry",
      statusPurpose: "revocation",
      statusListIndex: "96",
      statusListCredential:
        "https://api-pilot.ebsi.eu/trusted-issuers-registry/v4/issuers/did:ebsi:zsqBYQuKunNdr3XfGfPUvU7/proxies/0xe34e137dbefa842d71c0c840ddb4900af3611e297d8403e80f99a13e187e8d63/credentials/status/1#list",
    },
    credentialSubject: {
      id: "did:ebsi:zezz8fVpDYK3FNitovUL8izwrRqo5z8wmPMvQUAgJJH2K",
      section1: {
        personalIdentificationNumber: "AT4235049",
        sex: "01",
        surname: "a",
        forenames: "a",
        dateBirth: "1998-09-25",
        surnameAtBirth: "null",
        placeBirth: { region: "Vienna", countryCode: "AT", town: "Vienna" },
        nationalities: ["AT"],
        stateOfResidenceAddress: {
          streetNo: "Trabrennstraße 6-8",
          postCode: "1020",
          town: "Vienna",
          countryCode: "AT",
        },
        stateOfStayAddress: {
          streetNo: "Trabrennstraße 6-8",
          postCode: "1020",
          town: "Vienna",
          countryCode: "AT",
        },
      },
      section2: {
        memberStateWhichLegislationApplies: "EL",
        startingDate: "2025-01-07",
        endingDate: "2025-01-24",
        certificateForDurationActivity: true,
        determinationProvisional: true,
        transitionRulesApplyAsEC8832004: true,
      },
      section3: {
        postedEmployedPerson: "yes",
        employedTwoOrMoreStates: "yes",
        postedSelfEmployedPerson: "yes",
        selfEmployedTwoOrMoreStates: "yes",
        civilServant: "yes",
        contractStaff: "yes",
        mariner: "yes",
        employedAndSelfEmployed: "yes",
        civilAndEmployedSelfEmployed: "yes",
        flightCrewMember: "yes",
        exception: "yes",
        exceptionDescription: "no exceptions",
        workingInStateUnder21: "yes",
      },
      section4: {
        employee: true,
        selfEmployedActivity: true,
        employerSelfEmployedActivityCodes: [""],
        nameBusinessName: "13609158",
        registeredAddress: {
          streetNo: "Trabrennstraße 6-8, 213 A",
          postCode: "1020",
          town: "Vienne",
          countryCode: "EL",
        },
      },
      section5: {
        workPlaceNames: [
          {
            companyNameVesselName: "test",
          },
        ],
        workPlaceAddresses: [
          {
            address: {
              streetNo: `Downing Street 10, 2WE London`,
              town: "London",
              postCode: "A1200",
              countryCode: "GB",
            },
          },
        ],
        noFixedAddress: false,
      },
      section6: {
        name: "Office National de Sécurité Sociale / Rijksdienst voor Sociale Zekerheid",
        address: {
          streetNo: "Main Street 1",
          postCode: "1000",
          town: "Brussels",
          countryCode: "BE",
        },
        institutionID: "NSSI-BE-01",
        officePhoneNo: "0800 12345",
        officeFaxNo: "0800 98765",
        email: "esspass.noreply@gmail.com",
        date: "2023-10-23",
        signature: "Official signature",
      },
    },
    credentialSchema: {
      id: "ebsiHostUrl/trusted-schemas-registry/v2/schemas/0xe5e53f7bcebe95fc9576e27298de64bfacf592dd9be673eb6b8edbc4795ac96a",
      type: "FullJsonSchemaValidator2021",
    },
    expirationDate: "2027-01-01T00:00:00Z",
  };

  const ebsiJson = {
    "@context": ["https://www.w3.org/2018/credentials/v1"],
    id: "vc:ebsi:conformance#c6ac9182-9ce9-403c-b0de-646531b28679",
    type: ["VerifiableCredential", "VerifiableAttestation", "VerifiablePortableDocumentA1"],
    issuer: "did:ebsi:zsqBYQuKunNdr3XfGfPUvU7",
    issuanceDate: "2023-12-15T17:23:45Z",
    issued: "2023-12-15T17:23:45Z",
    validFrom: "2023-12-15T17:23:45Z",
    expirationDate: "2023-12-16T17:23:45Z",
    credentialSubject: {
      id: "did:key:z2dmzD81cgPx8Vki7JbuuMmFYrWPgYoytykUZ3eyqht1j9KbqD9Bqnr71DyshGohwUWPEeibWcynakeXmUqJAgDTEe6xzw17VwGUSPpAMHWxivLskpPgNFjjChQFQQ9DsVwDHJSLg3Srk6mZq28VYiMCLXRyv1fyPvefTMYZY957kj7DuN",
      section1: {
        personalIdentificationNumber: "BG25323748",
        sex: "01",
        surname: "a",
        forenames: "a",
        dateBirth: "1971-11-16",
        surnameAtBirth: "null",
        placeBirth: { town: "Sofia", region: "Sofia", countryCode: "BG" },
        nationalities: ["BG"],
        stateOfResidenceAddress: {
          streetNo: "Gorubliane",
          postCode: "1138",
          town: "Sofia",
          countryCode: "BG",
        },
        stateOfStayAddress: {
          streetNo: "Gorubliane",
          postCode: "1138",
          town: "Sofia",
          countryCode: "BG",
        },
      },
      section2: {
        memberStateWhichLegislationApplies: "CZ",
        startingDate: "2024-08-19",
        endingDate: "2026-07-09",
        certificateForDurationActivity: true,
        determinationProvisional: true,
        transitionRulesApplyAsEC8832004: true,
      },
      section3: {
        postedEmployedPerson: "yes",
        employedTwoOrMoreStates: "no",
        postedSelfEmployedPerson: "yes",
        selfEmployedTwoOrMoreStates: "no",
        civilServant: "yes",
        contractStaff: "yes",
        mariner: "yes",
        employedAndSelfEmployed: "yes",
        civilAndEmployedSelfEmployed: "yes",
        flightCrewMember: "yes",
        exception: "yes",
        exceptionDescription: "no exceptions",
        workingInStateUnder21: "yes",
      },
      section4: {
        employee: true,
        selfEmployedActivity: true,
        employerSelfEmployedActivityCodes: [""],
        nameBusinessName: "32597689",
        registeredAddress: {
          streetNo: "Trabrennstraße 6-8, 213 A",
          postCode: "1020",
          town: "Vienne",
          countryCode: "CZ",
        },
      },
      section5: {
        workPlaceNames: [
          {
            companyNameVesselName: "test",
          },
        ],
        workPlaceAddresses: [
          {
            address: {
              streetNo: `Downing Street 10, 2WE London`,
              town: "London",
              postCode: "B1200",
              countryCode: "GB",
            },
          },
        ],
        noFixedAddress: false,
      },
      section6: {
        name: "Office National de Sécurité Sociale / Rijksdienst voor Sociale Zekerheid",
        address: {
          streetNo: "Main Street 1",
          postCode: "1000",
          town: "Brussels",
          countryCode: "BE",
        },
        institutionID: "NSSI-BE-01",
        officePhoneNo: "0800 12345",
        officeFaxNo: "0800 98765",
        email: "esspass.noreply@gmail.com",
        date: "2023-10-31",
        signature: "Official signature",
      },
    },
    credentialSchema: {
      id: "https://api-pilot.ebsi.eu/trusted-schemas-registry/v2/schemas/z5qB8tydkn3Xk3VXb15SJ9dAWW6wky1YEoVdGzudWzhcW",
      type: "FullJsonSchemaValidator2021",
    },
  };

  it("should create an instance of verifiableCredentialSchema", () => {
    const instance = verifiableCredentialSchema.parse(json);
    expect(instance).toBeTruthy();
  });

  it("should create an instance of verifiableCredentialSchema from ebsi json ", () => {
    const instance = verifiableCredentialSchema.parse(ebsiJson);
    expect(instance).toBeTruthy();
  });
});
