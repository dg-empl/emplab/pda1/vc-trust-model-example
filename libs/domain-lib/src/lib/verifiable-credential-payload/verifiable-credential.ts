import { z } from "zod";
import { pda1Schema } from "../pda1-management";

export const verifiableCredentialStatusSchema = z.object({
  id: z.string(),
  type: z.string(),
  statusPurpose: z.string(),
  statusListIndex: z.string(),
  statusListCredential: z.string(),
});

export type VerifiableCredentialStatus = z.infer<typeof verifiableCredentialStatusSchema>;

export type VerifiableCredentialStatusPdaId = VerifiableCredentialStatus & { pdaId: string };

export const verifiableCredentialSchema = z.object({
  "@context": z.array(z.string()),
  id: z.string(),
  type: z.array(z.string()),
  issuanceDate: z.string(),
  validFrom: z.string(),
  issued: z.string(),
  credentialStatus: z.optional(verifiableCredentialStatusSchema),
  credentialSubject: pda1Schema,
  credentialSchema: z.object({ id: z.string(), type: z.string() }),
  issuer: z.string(),
  expirationDate: z.string(),
});

export type VerifiableCredential = z.infer<typeof verifiableCredentialSchema>;
