// {
//   "result": true,
//   "validations": {
//     "vpFormat": {
//       "status": true
//     },
//     "presentation": {
//       "status": true
//     },
//     "share-credential": {
//       "status": true
//     }
//   }
// }
import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject("Status")
export class Status {
  @JsonProperty("status", Boolean, true)
  "status" = undefined;
  @JsonProperty("error", String, true)
  "error" = undefined;
  @JsonProperty("details", String, true)
  "details" = undefined;
}

@JsonObject("Validations")
export class Validations {
  @JsonProperty("vpFormat", Status, true)
  "vpFormat" = undefined;

  @JsonProperty("presentation", Status, true)
  "presentation" = undefined;

  @JsonProperty("credential", Status, true)
  "credential" = undefined;
}

@JsonObject("VerifiablePresentationDtoResponse")
export class VerifiablePresentationDtoResponse {
  @JsonProperty("result", Boolean, true)
  "result" = undefined;

  @JsonProperty("validations", Validations)
  "validations" = undefined;
}
