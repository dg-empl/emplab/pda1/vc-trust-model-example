import { z } from "zod";

export const WalletDb = z.object({
  _id: z.string(),
  creationDate: z.string(),
  did: z.string(),
  wallet: z.string(),
  email: z.string().email(),
});

export type WalletDbType = z.infer<typeof WalletDb>;
