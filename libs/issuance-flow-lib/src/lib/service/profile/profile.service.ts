import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { KeycloakProfile } from "keycloak-js";

@Injectable({
  providedIn: "root",
})
export class ProfileService {
  userProfile: BehaviorSubject<KeycloakProfile> = new BehaviorSubject<KeycloakProfile>({});
  userProfile$ = this.userProfile.asObservable();

  public getUserProfile(): KeycloakProfile {
    return this.userProfile.value;
  }

}
