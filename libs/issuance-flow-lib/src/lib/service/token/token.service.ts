import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import {
  AccessTokenType,
  Did,
  VerifiableCredential,
  VerifiablePresentationQrCode,
} from "@esspass-web-wallet/domain-lib";

@Injectable({
  providedIn: "root",
})
export class TokenService {
  authCode: BehaviorSubject<string> = new BehaviorSubject<string>("");
  authCode$ = this.authCode.asObservable();

  token: BehaviorSubject<AccessTokenType> = new BehaviorSubject<AccessTokenType>({} as any);
  token$ = this.token.asObservable();

  credentialResponse: BehaviorSubject<VerifiableCredential> =
    new BehaviorSubject<VerifiableCredential>({} as any);
  credentialResponse$ = this.credentialResponse.asObservable();

  did: BehaviorSubject<Did> = new BehaviorSubject<Did>({} as any);
  did$ = this.did.asObservable();

  vpRequestCommand: BehaviorSubject<VerifiablePresentationQrCode> =
    new BehaviorSubject<VerifiablePresentationQrCode>({} as any);
  vpRequestCommand$ = this.vpRequestCommand.asObservable();
}
