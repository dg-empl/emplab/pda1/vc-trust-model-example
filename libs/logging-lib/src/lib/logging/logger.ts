import * as winston from "winston";
import { api } from "@opentelemetry/sdk-node";

const LOG_TRACE_LEVEL = process.env["LOG_LEVEL"] || "trace";
const DATE_FORMAT_OPTIONS = {};

const dateFormat = (): string => new Date(Date.now()).toLocaleString("de-DE", DATE_FORMAT_OPTIONS);

const loggingLevels = {
  levels: {
    exception: 0,
    error: 1,
    warn: 2,
    info: 3,
    http: 4,
    debug: 5,
    trace: 6,
  },
};

const constructFileName = (applicationName: string) => {
  const temp = dateFormat().split(",")[0].split("/");
  return `${applicationName}-${temp[0]}`;
};

export class Logger {
  private readonly logger;
  private readonly fileName;
  private readonly log_data: null;

  constructor(private applicationName?: string) {
    this.log_data = null;
    this.fileName = this.applicationName ? constructFileName(this.applicationName) : null;

    this.logger = winston.createLogger({
      levels: loggingLevels.levels,
      level: LOG_TRACE_LEVEL,
      transports: [
        new winston.transports.Console(),
        new winston.transports.File({
          filename: `./logs/${this.fileName}.log`,
        }),
      ],
      format: winston.format.printf(info => {
        const currentSpan = api.trace.getSpan(api.context.active());
        const LogString = `${info.level.toUpperCase()}`;
        const paddedLogString = LogString.padStart(9);
        let message = `${dateFormat()} | ${paddedLogString} | ${info.message}`;

        // add telemetry data to log
        const traceId = currentSpan?.spanContext().traceId ?? null;
        message = traceId ? message.concat(`| trace_id=${traceId}`) : message;
        const spanId = currentSpan?.spanContext().spanId ?? null;
        message = spanId ? message.concat(`| span_id=${spanId}`) : message;

        message = info["obj"] ? message + `data:${JSON.stringify(info["obj"])} | ` : message;
        message = this.log_data
          ? message + `log_data:${JSON.stringify(this.log_data)} | `
          : message;
        return message;
      }),
    });
  }

  info(message: string) {
    if (typeof message !== "undefined") {
      this.logger.log("info", message);
    } else console.log("ERROR :! message passed!!!");
  }

  infoObj(message: string, obj: string) {
    if (typeof message !== "undefined") {
      if (typeof obj !== "undefined") {
        this.logger.log("info", JSON.stringify(obj));
      } else this.logger.log("info", message);
    } else console.log("ERROR :! message passed!!!");
  }

  trace(message: string) {
    this.logger.log("trace", `${message} `);
  }

  traceObj(message: string, obj: string | object) {
    if (typeof obj !== "undefined") {
      this.logger.log("trace", JSON.stringify(obj, null, 2));
    } else {
      this.logger.log("trace", message);
    }
  }

  debug(message: string) {
    this.logger.log("debug", message);
  }

  debugJSON(message: string, obj: string | object) {
    if (typeof obj !== "undefined") {
      this.logger.log("debug", JSON.stringify(obj, null, 2) + " ");
    } else {
      this.logger.log("debug", message);
    }
  }

  error(message: string | object | unknown) {
    this.logger.error("error : " + message);
  }

  errorObj(message: string, obj: string) {
    this.logger.log("error", message, {
      obj,
    });
  }

  exception(message: string) {
    this.logger.log("exception", message);
  }
}

export const logger = (application: string) => new Logger(application);
