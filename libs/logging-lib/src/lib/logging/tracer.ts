import { OTLPTraceExporter } from "@opentelemetry/exporter-trace-otlp-http";
import { NodeSDK } from "@opentelemetry/sdk-node";
import {
  BatchSpanProcessor,
  NodeTracerProvider,
  SimpleSpanProcessor,
} from "@opentelemetry/sdk-trace-node";
import { Resource } from "@opentelemetry/resources";
import { SemanticResourceAttributes } from "@opentelemetry/semantic-conventions";
import { WinstonInstrumentation } from "@opentelemetry/instrumentation-winston";
import { ExpressInstrumentation } from "@opentelemetry/instrumentation-express";
import { HttpInstrumentation } from "@opentelemetry/instrumentation-http";
import { InMemorySpanExporter } from "@opentelemetry/sdk-trace-base";

// Constants
const TRACER_INITIALIZED = "The tracer has been initialized : ";
const TRACER_FAILED_INIT = "Failed to initialize the tracer : ";
const TEMP_URL = "http://tempo.monitoring.svc.cluster.local:4318/v1/traces";

export class Tracer {
  private readonly applicationName: string;
  private readonly provider: NodeTracerProvider;
  private readonly exporter: OTLPTraceExporter;

  public constructor(applicationName: string, exporterUrl: string = TEMP_URL) {
    this.applicationName = applicationName || "undefined";
    this.exporter = new OTLPTraceExporter({ url: exporterUrl });
    this.provider = new NodeTracerProvider({
      resource: new Resource({
        [SemanticResourceAttributes.SERVICE_NAME]: this.applicationName,
      }),
    });

    this.provider.addSpanProcessor(new SimpleSpanProcessor(new InMemorySpanExporter()));
    this.provider.addSpanProcessor(new BatchSpanProcessor(this.exporter));
    this.provider.register();

    this.setupNodeSDK();
  }

  private setupNodeSDK() {
    try {
      new NodeSDK({
        traceExporter: this.exporter,
        instrumentations: [
          new ExpressInstrumentation(),
          new HttpInstrumentation(),
          new WinstonInstrumentation({
            enabled: true,
            logHook: (_span, record) => {
              record["resource.service.name"] = this.applicationName;
            },
          }),
        ],
      });
      console.info(TRACER_INITIALIZED + this.applicationName);
    } catch (e) {
      console.error(TRACER_FAILED_INIT + e);
    }
  }

  public getTracer() {
    return this.provider.getTracer(<string>this.applicationName);
  }
}
