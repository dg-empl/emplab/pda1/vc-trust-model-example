export default {
  displayName: "security-lib",
  preset: "../../jest.preset.js",
  testEnvironment: "node",
  setupFiles: ["<rootDir>/src/lib/idp-token/.env.js"],
  transform: {
    "^.+\\.[tj]s$": ["ts-jest", { tsconfig: "<rootDir>/tsconfig.spec.json" }],
  },
  moduleFileExtensions: ["ts", "js", "html"],
  coverageDirectory: "../../coverage/libs/security-lib",
};
