/**
 * @jest-environment node
 */
import { IdpTokenService } from "./idp-token.service";
import { logger } from "@esspass-web-wallet/logging-lib";
import { NotFoundError } from "@esspass-web-wallet/domain-lib";

const log = logger("idp-token-service");

describe("IdpTokenService", () => {
  it("should create an instance", () => {
    const service = IdpTokenService.getInstance(log);

    expect(service).toBeTruthy();
  });

  it("get bearer token", async () => {
    const service = IdpTokenService.getInstance(log);

    try {
      await service.bearerToken();
    } catch (error) {
      expect(error).toEqual(
        new NotFoundError(
          "IdpTokenService: getRefreshedAccessToken error : ERR_JWT_INVALID: JWTs must use Compact JWS serialization, JWT must be a string"
        )
      );
    }
  });
});
