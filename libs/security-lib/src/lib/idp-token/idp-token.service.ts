import fetch from "node-fetch";
import axios, { AxiosResponse } from "axios";
import { decodeJwt, JWTPayload } from "jose";
import { Logger } from "@esspass-web-wallet/logging-lib";

import { NotFoundError } from "@esspass-web-wallet/domain-lib";

type IdpConfig = {
  idp: string;
  realm: string;
  client_id: string;
  client_secret: string;
  username: string;
  password: string;
};

const idpConfig: IdpConfig = {
  idp: process.env["IDP"] ?? "",
  realm: process.env["IDP_REALM"] ?? "",
  client_id: process.env["CLIENT_ID"] ?? "",
  client_secret: process.env["CLIENT_SECRET"] ?? "",
  username: process.env["IDP_USERNAME"] ?? "",
  password: process.env["IDP_PASSWORD"] ?? "",
};

const DID_API_PATH = "/issuer-did";

export class IdpTokenService {
  private _bearerToken: string;
  private issuerDID = "";
  private static idpToken: IdpTokenService;
  private static log: Logger;

  private constructor(log?: Logger, callback?: () => boolean) {
    IdpTokenService.log = log ?? new Logger();

    this.initService(callback).then(
      () => {
        IdpTokenService.log.info("IdpTokenService: idpToken done");
      },
      err => {
        IdpTokenService.log.error(err);
      }
    );
  }

  get issuerDid() {
    return this.issuerDID;
  }

  async bearerToken(): Promise<string> {
    await this.getRefreshedAccessToken();
    return this._bearerToken;
  }

  public static getInstance(log: Logger, callback?: () => boolean): IdpTokenService {
    if (!this.idpToken) {
      this.idpToken = new IdpTokenService(log, callback);
      IdpTokenService.log.debug("Creating new instance of IdpTokenService");
    }
    return this.idpToken;
  }

  //check if bearerToken has not expired and if it has then get a new one and update the bearerToken
  public async getRefreshedAccessToken() {
    let decodedJwt: JWTPayload;
    try {
      decodedJwt = decodeJwt(this._bearerToken);
    } catch (e: any) {
      const error = `${e.code}: ${e.message}`;
      throw new NotFoundError("IdpTokenService: getRefreshedAccessToken error : " + error);
    }

    const currentTime = Date.now() / 1000; // convert to seconds
    if (decodedJwt.exp && decodedJwt.exp < currentTime) {
      IdpTokenService.log.debug("IdpTokenService: refreshing access token");
      const getIDPTokenResponse = await this.getIDPToken(idpConfig);
      const parsedGetIDPToken = JSON.parse(getIDPTokenResponse["response"]);
      this._bearerToken = parsedGetIDPToken.access_token;
    }

    return this._bearerToken;
  }

  private async getIssuerDid(bearerToken: string): Promise<string> {
    let response: AxiosResponse;
    const hostname = `${process.env["ISSUER_WALLET_URL"]}` + DID_API_PATH;

    IdpTokenService.log.debug("IdpTokenService: getIssuerDid hostname: " + hostname);

    try {
      response = await axios({
        method: "GET",
        url: hostname,
        data: JSON.stringify({ scope: "ebsi users onboarding" }),
        headers: {
          accept: "application/json",
          "Content-Type": "application/json",
          authorization: "Bearer " + bearerToken,
        },
      });
    } catch (e: any) {
      IdpTokenService.log.error(
        "IdpTokenService: getIssuerDid error: " + JSON.stringify(e.response.data)
      );
      throw new NotFoundError("IdpTokenService: issuer-wallet connection error :" + e.message);
    }

    return await response.data.did;
  }

  private async getIDPToken(config: IdpConfig): Promise<Record<string, any>> {
    const hostname = config.idp + `/realms/${config.realm}/protocol/openid-connect/token`;
    const body = new URLSearchParams({
      scope: "openid",
      grant_type: "password",
      ...config,
    });

    const result: { response: string | null } = { response: null };
    const requestObj = {
      method: "POST",
      body: body,
      headers: {
        accept: "application/x-www-form-urlencoded",
        "Content-Type": "application/x-www-form-urlencoded",
      },
    };
    const response = await fetch(hostname, requestObj);
    const data = await response.json();

    if (data && data["error"]) {
      throw new NotFoundError("IdpTokenService: getIDPToken error :" + JSON.stringify(data));
    }

    result.response = JSON.stringify(data);
    return result;
  }

  private async initService(callback?: () => boolean) {
    IdpTokenService.log.info("IdpTokenService: initService");
    const getIDPTokenResponse = await this.getIDPToken(idpConfig);
    const parsedGetIDPToken = JSON.parse(getIDPTokenResponse["response"]);
    this._bearerToken = parsedGetIDPToken.access_token;

    /**
     * if callback passed -> no need to get issuerDID
     * only bearerToken is needed
     */
    if (callback && callback()) return;

    /**
     * get issuerDID as well
     */
    this.issuerDID = await this.getIssuerDid(this._bearerToken);
    IdpTokenService.log.info("IdpTokenService: issuerDID: " + this.issuerDID);
  }
}
