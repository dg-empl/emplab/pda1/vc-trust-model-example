import { InjectionToken } from "@angular/core";

import { AppConfigInterface } from "./app-config.interface";

export const APP_DI_CONFIG: AppConfigInterface = {
  serverUrl: process.env["API_URL"],
  redirectUrl: process.env["REDIRECT_URL"],
  version: process.env["APP_VERSION"] || "development",
};

export const APP_CONFIG = new InjectionToken<AppConfigInterface>("app.config");
