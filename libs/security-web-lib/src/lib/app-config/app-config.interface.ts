export interface AppConfigInterface {
  serverUrl?: string;
  redirectUrl?: string;
  version: string;
}
