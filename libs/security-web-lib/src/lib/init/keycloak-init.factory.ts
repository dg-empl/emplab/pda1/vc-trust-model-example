import { KeycloakService } from "keycloak-angular";

export function initializeKeycloak(keycloak: KeycloakService) {
  return () =>
    keycloak.init({
      config: {
        url: process.env["KEYCLOAK_URL"],
        realm: process.env["KEYCLOAK_REALM"] || "",
        clientId: process.env["KEYCLOAK_CLIENT_ID"] || "",
      },
      initOptions: {
        useNonce: true,
        onLoad: "check-sso",
        silentCheckSsoRedirectUri: window.location.origin + "/assets/silent-check-sso.html",
      },
    });
}
