export abstract class AbstractEndPoint {
  abstract serverUrl(): string;
}
