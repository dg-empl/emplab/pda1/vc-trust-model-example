import { HttpParams } from "@angular/common/http";

import { HttpUrlComponentCodec } from "./http-param-codec";

describe("HttpUrlEncodingCodec", () => {
  let http: HttpParams;
  beforeEach(() => {
    http = new HttpParams({ encoder: new HttpUrlComponentCodec() });
  });

  it("should encode + characters correctly", () => {
    const body = http.append("param", "1+2");

    expect(body.toString()).toBe("param=1%2B2");
  });

  it("should encode = characters correctly", () => {
    const body = http.append("a=b", "c=d");
    expect(body.toString()).toBe("a%3Db=c%3Dd");
  });

  it("should encode : characters correctly", () => {
    const body = http.append("param", "1:2");
    expect(body.toString()).toBe("param=1%3A2");
  });

  it("should encode space characters correctly", () => {
    const body = http.append("param", "1 2");
    expect(body.toString()).toBe("param=1+2");
  });

  it("should encode an email characters correctly", () => {
    const body = http.append("param", "email+1@email.com");
    expect(body.toString()).toBe("param=email%2B1%40email.com");
  });
});
