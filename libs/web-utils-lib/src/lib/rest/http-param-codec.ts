/**
 * Quick fix of the HttpParam encoding.
 * This fix can be removed and adapt with update to Angular starting version 10 (to be confirmed).
 *
 * Sort of official statement of one of the angular team
 * https://github.com/angular/angular/issues/11058#issuecomment-636114557
 *
 * Source of the fix
 * https://github.com/alxhub/angular/commit/fa5a37ecd8c3aeaa1269746818e8f6b9fbf063e5
 */

export interface HttpParameterCodec {
  encodeKey(key: string): string;

  encodeValue(value: string): string;

  decodeKey(key: string): string;

  decodeValue(value: string): string;
}

export class HttpUrlComponentCodec implements HttpParameterCodec {
  encodeKey(key: string): string {
    return encodeURIComponentWrapped(key);
  }

  encodeValue(value: string): string {
    return encodeURIComponentWrapped(value);
  }

  decodeKey(key: string): string {
    return decodeURIComponent(key);
  }

  decodeValue(value: string): string {
    return decodeURIComponent(value);
  }
}

/**
 * Encodes using `encodeURIComponent`, but replaces '%20' which encodes a space character with
 * '+'.
 */
function encodeURIComponentWrapped(value: string): string {
  return encodeURIComponent(value).replace(/%20/g, "+");
}
