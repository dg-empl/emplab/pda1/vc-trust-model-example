/**
 * @jest-environment jsdom
 */
import { TestBed, getTestBed } from "@angular/core/testing";

import { HttpWrapperService } from "./http-wrapper.service";
import { HttpTestingController, HttpClientTestingModule } from "@angular/common/http/testing";
import { AbstractEndPoint } from "./abstract-end-point.service";
import { UnitTestEndPoint } from "./unit-test-end-point.service";

describe("HttpWrapperService", () => {
  let injector: TestBed;
  let service: HttpWrapperService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        HttpWrapperService,
        {
          provide: AbstractEndPoint,
          useClass: UnitTestEndPoint,
        },
      ],
    });

    injector = getTestBed();
    service = injector.inject(HttpWrapperService);
    httpMock = injector.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it("should create()", () => {
    //given
    const value = { key: "value" };
    //when
    service.create(value, "rest/create").subscribe();
    //then
    const req = httpMock.expectOne("rest/create");
    expect(req.request.method).toBe("POST");
    expect(req.request.body).toEqual(JSON.stringify(value));
  });

  it("should createWithRequestBody()", () => {
    //given
    const value = { key: "value" };
    //when
    service.createWithRequestBody("rest/create", value).subscribe();
    //then
    const req = httpMock.expectOne("rest/create");
    expect(req.request.method).toBe("POST");
    expect(req.request.body).toEqual(value);
  });

  it("should delete()", () => {
    //given
    const value = 1;
    //when
    service.delete(`rest/delete/${value}`).subscribe();
    //then
    const req = httpMock.expectOne(`rest/delete/${value}`);
    expect(req.request.method).toBe("DELETE");
  });

  it("should deleteRaw()", () => {
    //given
    //when
    service.deleteRaw("rest/delete").subscribe();
    //then
    const req = httpMock.expectOne("rest/delete");
    expect(req.request.method).toBe("DELETE");
  });

  // it('should deleteResource()', () => {
  //     //given
  //     //when
  //     service.deleteResource('rest/delete').subscribe();
  //     //then
  //     const req = httpMock.expectOne('rest/delete');
  //     expect(req.request.method).toBe('DELETE');
  // });

  it("should find()", () => {
    //given
    const value = 1;
    //when
    service.find(value, "rest/find", Number).subscribe();
    //then
    const req = httpMock.expectOne("rest/find/1");
    expect(req.request.method).toBe("GET");
  });

  it("should findAll()", () => {
    //given
    //when
    service.findAll("rest/findAll", Boolean).subscribe();
    //then
    const req = httpMock.expectOne("rest/findAll");
    expect(req.request.method).toBe("GET");
  });

  it("should findArray()", () => {
    //given
    //when
    service.findArray("rest/findArray", Boolean).subscribe();
    //then
    const req = httpMock.expectOne("rest/findArray");
    expect(req.request.method).toBe("GET");
  });

  it("should findArrayFromEndPoint()", () => {
    //given
    //when
    service.findArrayFromEndPoint("rest/findArrayFromEndPoint", Boolean).subscribe();
    //then
    const req = httpMock.expectOne("rest/findArrayFromEndPoint");
    expect(req.request.method).toBe("GET");
  });

  it("should findArrayWithParams()", () => {
    //given
    const params = { key: "value" };
    //when
    service.findArrayWithParams(params, "rest/findArrayWithParams", Boolean).subscribe();
    //then
    const req = httpMock.expectOne("rest/findArrayWithParams?key=value");
    expect(req.request.method).toBe("GET");
  });

  it("should findFromEndPoint()", () => {
    //given
    //when
    service.findFromEndPoint("rest/findFromEndPoint", Boolean).subscribe();
    //then
    const req = httpMock.expectOne("rest/findFromEndPoint");
    expect(req.request.method).toBe("GET");
  });

  it("should findFromPostMethod()", () => {
    //given
    const body = { key: "value" };
    //when
    service.findFromPostMethod(body, "rest/findFromPostMethod", Boolean).subscribe();
    //then
    const req = httpMock.expectOne("rest/findFromPostMethod");
    expect(req.request.method).toBe("POST");
    expect(req.request.body).toBe(body);
  });

  it("should findRawFromPostMethod()", () => {
    //given
    const body = { key: "value" };
    //when
    service.findRawFromPostMethod("rest/findRawFromPostMethod", body).subscribe();
    //then
    const req = httpMock.expectOne("rest/findRawFromPostMethod");
    expect(req.request.method).toBe("POST");
    expect(req.request.body).toBe(body);
  });

  it("should findObjectWithParams()", () => {
    //given
    const params = { key: "value" };
    //when
    service.findObjectWithParams(params, "rest/findObjectWithParams/suffix").subscribe();
    //then
    const req = httpMock.expectOne("rest/findObjectWithParams/suffix?key=value");
    expect(req.request.method).toBe("GET");
  });

  it("should findObjectWithParamsRaw()", () => {
    //given
    const params = { key: "value" };
    //when
    service
      .findObjectWithParamsRaw("/suffix", "rest/findObjectWithParamsRaw", params, Boolean)
      .subscribe();
    //then
    const req = httpMock.expectOne("rest/findObjectWithParamsRaw/suffix?key=value");
    expect(req.request.method).toBe("GET");
  });

  it("should findRaw()", () => {
    //given
    //when
    service.findRaw("rest/findRaw").subscribe();
    //then
    const req = httpMock.expectOne("rest/findRaw");
    expect(req.request.method).toBe("GET");
  });

  it("should findRawWithParams()", () => {
    //given
    const params = { key: "value" };
    //when
    service.findRawWithParams("rest/findRawWithParams", params).subscribe();
    //then
    const req = httpMock.expectOne("rest/findRawWithParams?key=value");
    expect(req.request.method).toBe("GET");
  });

  // it('should getList()', () => {
  //     //given
  //     //when
  //     service.getList('rest/getList').subscribe();
  //     //then
  //     const req = httpMock.expectOne('rest/getList');
  //     expect(req.request.method).toBe('GET');
  // });

  // it('should getListPaginated()', () => {
  //     //given
  //     const params = { key: 'value' };
  //     //when
  //     service.getListPaginated('rest/getListPaginated', params).subscribe();
  //     //then
  //     const req = httpMock.expectOne('rest/getListPaginated?key=value');
  //     expect(req.request.method).toBe('GET');
  // });

  it("should getWithParams()", () => {
    //given
    const params = { key: "value" };
    //when
    service.getWithParams("rest/getWithParams", params).subscribe();
    //then
    const req = httpMock.expectOne("rest/getWithParams");
    expect(req.request.method).toBe("GET");
  });

  // it('should postResource()', () => {
  //     //given
  //     //when
  //     service.postResource('rest/postResource', Boolean).subscribe();
  //     //then
  //     const req = httpMock.expectOne('rest/postResource');
  //     expect(req.request.method).toBe('POST');
  // });

  it("should update()", () => {
    //given
    const value = 1;
    const obj = { key: "value" };
    //when
    service.update(value, "rest/update", obj).subscribe();
    //then
    const req = httpMock.expectOne(`rest/update/${value}`);
    expect(req.request.method).toBe("PUT");
    expect(req.request.body).toBe(JSON.stringify(obj));
  });

  it("should update() with no items", () => {
    //given
    const value = 1;
    //when
    service.update(value, "rest/update").subscribe();
    //then
    const req = httpMock.expectOne("rest/update");
    expect(req.request.method).toBe("POST");
  });

  it("should updateByPost()", () => {
    //given
    //when
    service.updateByPost("rest/updateByPost").subscribe();
    //then
    const req = httpMock.expectOne("rest/updateByPost");
    expect(req.request.method).toBe("POST");
  });

  it("should updateByPostWithText()", () => {
    //given
    const text = "value";
    //when
    service.updateByPostWithText("rest/updateByPostWithText", text).subscribe();
    //then
    const req = httpMock.expectOne("rest/updateByPostWithText");
    expect(req.request.method).toBe("POST");
    expect(req.request.body).toBe(text);
  });

  it("should updateWithBody()", () => {
    //given
    const obj = { key: "value" };
    //when
    service.updateWithBody("rest/updateWithBody", obj).subscribe();
    //then
    const req = httpMock.expectOne("rest/updateWithBody");
    expect(req.request.method).toBe("PUT");
    expect(req.request.body).toBe(JSON.stringify(obj));
  });
});
