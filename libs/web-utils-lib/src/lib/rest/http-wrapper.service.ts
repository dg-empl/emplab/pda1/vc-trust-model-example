import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable, Type } from "@angular/core";
import { Observable, tap } from "rxjs";
import { map } from "rxjs/operators";

import { HttpUrlComponentCodec } from "./http-param-codec";
import {
  fromObjArrayToType,
  RequestFormActionEnum,
  toTypeScriptObjectOfType,
} from "@esspass-web-wallet/domain-lib";

@Injectable({ providedIn: "root" })
export class HttpWrapperService {
  private headers = new HttpHeaders();

  constructor(private httpClient: HttpClient) {
    this.headers.append("Content-Type", "application/json; charset=utf-8");
  }

  public create<T>(item: any, endpoint: string): Observable<any> {
    return this.httpClient.post<T>(endpoint, JSON.stringify(item)).pipe(map(data => data));
  }

  public createWithParams<T>(endpoint: string, body: any, options: any): Observable<any> {
    return this.httpClient.post<T>(endpoint, body, options).pipe(
      map(data => data),
      error => {
        return error;
      }
    );
  }

  public createWithRequestBody<T>(
    endpoint: string,
    requestBody: any,
    options?: any
  ): Observable<any> {
    return this.httpClient.post<T>(endpoint, requestBody, options);
  }

  public delete(endpoint: string): Observable<any> {
    return this.httpClient.delete(endpoint);
  }

  public deleteRaw(endpoint: string) {
    return this.httpClient.delete(endpoint);
  }

  public find<T>(id: number | string, endpoint: string, type: Type<T>): Observable<T> {
    return this.httpClient
      .get(endpoint + "/" + id)
      .pipe(map((data: any) => toTypeScriptObjectOfType(data, type)));
  }

  public findAll<T>(endpoint: string, type: Type<T>): Observable<T> {
    return this.httpClient.get(endpoint).pipe(map(data => toTypeScriptObjectOfType(data, type)));
  }

  /**
   * Find array of type T on backend
   * @param endpoint
   * @param type
   */
  public findArray<T>(endpoint: string, type: Type<T>): Observable<T[]> {
    return this.httpClient
      .get(endpoint)
      .pipe(map((data: any) => fromObjArrayToType(data, type) as T[]));
  }

  /**
   * Find array of type T on backend
   * @param endpoint
   * @param type
   */
  public findArrayNoGeneric(endpoint: string): Observable<any[]> {
    return this.httpClient.get(endpoint).pipe(map((data: any) => data));
  }

  // /**
  //  * Find array of type T on backend
  //  * @param endpoint
  //  * @param schema
  //  */
  // public findArray<T extends z.ZodTypeAny>(endpoint: string, schema: T): Observable<T[]> {
  //   return this.httpClient.get(endpoint).pipe(
  //     map((data: any) => this.parse(schema, data)));
  // }
  //
  // /**
  //  * Find array of type T on backend
  //  * @param schema
  //  * @param data
  //  */
  // public parse = <T extends z.ZodTypeAny>(schema: T, data: unknown): z.infer<T> => {
  //   try {
  //     return schema.parse(data);
  //   } catch (err) {
  //     // handle error
  //     throw new Error();
  //   }
  // };

  public findArrayFromEndPoint<T>(endpoint: string, type: Type<T>): Observable<T[]> {
    return this.httpClient
      .get(endpoint)
      .pipe(map((data: any) => fromObjArrayToType(data, type) as T[]));
  }

  public findArrayWithParams<T>(params: any, endpoint: string, type: Type<T>): Observable<T[]> {
    return this.httpClient
      .get(endpoint, {
        params: this.toHttpParams(params),
      })
      .pipe(map((data: any) => fromObjArrayToType(data, type) as T[]));
  }

  public findFromEndPoint<T>(endpoint: string, type: Type<T>): Observable<T> {
    return this.httpClient
      .get(endpoint)
      .pipe(map((data: any) => toTypeScriptObjectOfType(data, type) as T));
  }

  public findFromPostMethod<T>(item: object, endpoint: string, type: Type<T>): Observable<T> {
    return this.httpClient
      .post(endpoint, item)
      .pipe(map((data: any) => toTypeScriptObjectOfType(data, type) as T));
  }

  public findArrayFromPostMethod<T>(
    item: object,
    endpoint: string,
    type: Type<T>
  ): Observable<T[]> {
    return this.httpClient
      .post(endpoint, item)
      .pipe(map((data: any) => fromObjArrayToType(data, type) as T[]));
  }

  public findRawFromPostMethod<T>(endpoint: string, body: object | null, params?: object): any {
    const prams = this.toHttpParams(params);
    return this.httpClient.post(endpoint, body, prams);
  }

  public findRawFromPostMethd(endpoint: string, body: object | null): any {
    const prams = this.toHttpParams(body);
    return this.httpClient.post(endpoint, body, prams);
  }

  public findRawFromGetMethod(endpoint: string): any {
    return this.httpClient.get(endpoint);
  }

  /**
   *
   * Find object of type @type by GET method with parameters
   *
   * @param endpoint
   * @param params
   * @param type
   */
  public findWithParams<T>(endpoint: string, params: any, type: Type<T>): Observable<T> {
    return this.httpClient
      .get(endpoint, {
        params: this.toHttpParams(params),
      })
      .pipe(map((data: any) => toTypeScriptObjectOfType(data, type) as T));
  }

  public findObjectWithParams(params: any, endpoint: string): Observable<any> {
    return this.httpClient.get(endpoint, {
      params: this.toHttpParams(params),
    });
  }

  public findObjectWithParamsRaw<T>(
    suffix: string,
    endpoint: string,
    params: any,
    type: Type<T>
  ): Observable<T> {
    return this.httpClient.get<T>(endpoint + suffix, {
      params: this.toHttpParams(params),
    });
  }

  public findRaw(endpoint: string): Observable<any> {
    return this.httpClient.get(endpoint);
  }

  public send(endpoint: string): Observable<any> {
    return this.httpClient.get(endpoint);
  }

  public sendWithTextResponse(endpoint: string): Observable<any> {
    return this.httpClient.get(endpoint, { responseType: "text" });
  }

  public findRawWithParams<T>(endpoint: string, params: any): Observable<T> {
    return this.httpClient.get<T>(endpoint, {
      params: this.toHttpParams(params),
    });
  }

  public getWithParams(endpoint: string, options: any): Observable<any> {
    options.params = this.toHttpParams(options.params);

    return this.httpClient.get(endpoint, options);
  }

  public update(id: number, endpoint: string, item?: any): Observable<any> {
    if (!item) {
      return this.httpClient.post(endpoint, {} as Record<string, never>);
    }
    return this.httpClient
      .put(endpoint + "/" + id, JSON.stringify(item), {
        headers: this.headers,
      })
      .pipe(map(data => data));
  }

  public updateByPost(endpoint: string): Observable<any> {
    return this.httpClient.post(endpoint, {}).pipe(map(data => data));
  }

  public updateByPostWithText(endpoint: string, item: string): Observable<any> {
    return this.httpClient.post(endpoint, item);
  }

  public updateWithBody(endpoint: string, item: any): Observable<any> {
    return this.httpClient.put(endpoint, JSON.stringify(item), {
      headers: this.headers,
    });
  }

  public updateByPut(
    endpoint: string,
    item: Record<"action", keyof typeof RequestFormActionEnum>
  ): Observable<any> {
    return this.httpClient.put(endpoint, item, {
      headers: this.headers,
    });
  }

  private toHttpParams(params: any) {
    if (params) {
      return Object.getOwnPropertyNames(params).reduce(
        (p, key) => p.set(key, params[key]),
        new HttpParams({ encoder: new HttpUrlComponentCodec() })
      );
    }

    return params;
  }
}
