import { AbstractEndPoint } from "./abstract-end-point.service";

export class UnitTestEndPoint extends AbstractEndPoint {
  public serverUrl(): string {
    return "";
  }
}
