#------------------------------------------------------------------------------------------------------------
AWS_ACCOUNT=960693787136
AWS_REGION=eu-west-1

ECR="${AWS_ACCOUNT}".dkr.ecr."${AWS_REGION}".amazonaws.com/esspass #ECR address for account "${AWS_ACCOUNT}"

DEV_CLUSTER=arn:aws:eks:"${AWS_REGION}":"${AWS_ACCOUNT}":cluster/dev-esspass-k8s
TEST_CLUSTER=arn:aws:eks:"${AWS_REGION}":"${AWS_ACCOUNT}":cluster/test-esspass-k8s
DEMO_CLUSTER=arn:aws:eks:"${AWS_REGION}":"${AWS_ACCOUNT}":cluster/demo-esspass-k8s

CLUSTER_DEV_NAMESPACE=dev-esspass
CLUSTER_TEST_NAMESPACE=test-esspass
CLUSTER_DEMO_NAMESPACE=demo-esspass
#-----------------------------------------------------------------------------------------------------------

#set credentials for ecr repository
function loginToEcr() {
  aws ecr get-login-password --region "${AWS_REGION}" | docker login --username AWS --password-stdin "${ECR}" || exit
}

# Select dev-cluster
function selectDevCluster() {
  kubectl config use-context "${DEV_CLUSTER}"
  kubectl cluster-info

  # shellcheck disable=SC2181
  if [ $? -ne 0 ]; then
    echo "Error: The command failed with error code $?"
    exit 101
  else
    echo "The command was successful"
  fi

  kubectl config set-context --current --namespace="${CLUSTER_DEV_NAMESPACE}"
}

# Select test-cluster
function selectTestCluster() {
  kubectl config use-context "${TEST_CLUSTER}"
  kubectl cluster-info

  # shellcheck disable=SC2181
  if [ $? -ne 0 ]; then
    echo "Error: The command failed with error code $?"
    exit 101
  else
    echo "The command was successful"
  fi

  kubectl config set-context --current --namespace="${CLUSTER_TEST_NAMESPACE}"
}

# Select demo-cluster
function selectDemoCluster() {
  kubectl config use-context "${DEMO_CLUSTER}"
  kubectl cluster-info

  # shellcheck disable=SC2181
  if [ $? -ne 0 ]; then
    echo "Error: The command failed with error code $?"
    exit 101
  else
    echo "The command was successful"
  fi

  kubectl config set-context --current --namespace="${CLUSTER_DEMO_NAMESPACE}"
}

#Common deployment functionalities
function common() {
  # shellcheck disable=SC2153
  REPOSITORY="${ECR}":esspass-"${APPLICATION_NAME}"-"${VERSION}"
  docker tag esspass/esspass-"${APPLICATION_NAME}":"${VERSION}" "${REPOSITORY}" || exit
  docker push "${REPOSITORY}" || exit

  cd "${ROOT_DIR}"/tools/k8s-scripts/"${ENVIRONMENT}"/helm-charts || exit
}

#Helm upgrade
function upgrade() {
  REPOSITORY="${ECR}":esspass-"${APPLICATION_NAME}"-"${VERSION}"
  helm upgrade --install  "${APPLICATION_NAME}" ./"${APPLICATION_NAME}" --set image.repository="${REPOSITORY}" --set deployment.version="${VERSION}" --version="${VERSION}" || exit
}

function deployment() {
  common
  upgrade
}

function push() {
  common
}

function waitForPod() {
  local cluster="$1"
  local application="$2"
  local namespace="$3"
  local port="$4"
  local idpNamespace="$5"

  echo "Waiting for pod to be ready..."
  sleep 45

  local clusterArn=arn:aws:eks:"${AWS_REGION}":"${AWS_ACCOUNT}":cluster/"${cluster}"
  kubectl config use-context "${clusterArn}"

  local previous_state="false"

  until false; do
    current_state=$(kubectl get pod -n "${namespace}" -l app="${application}" -o jsonpath="{.items[*].status.containerStatuses[*].started}")

    case "$current_state" in
    "$previous_state")
      echo "No deployment detected."
      ;;
    *)
      echo "New deployment detected"
      getPodIp "$namespace" "$application"
      updateApisix "$ip" "$port" "$idpNamespace"
      return 0
      ;;
    esac

    previous_state="$current_state"
    sleep 1
  done
}

function getPodIp() {
  local namespace="$1"
  local application="$2"

  # shellcheck disable=SC1083
  ip=$(kubectl get pod -n "${namespace}" -l app="${application}" -o jsonpath=\"{.items[*].status.podIP}\")

  # shellcheck disable=SC2181
  if [ $? -ne 0 ]; then
    echo "Error: The command failed with error code $?"
    exit 101
  else
    echo "The command was successful"
  fi
}

function checkImageExists() {
  local application="$1"
  local version="$2"
  local imageExists

  # Check if image exists in ECR
  aws ecr describe-images \
          --region "${AWS_REGION}" \
          --repository-name "esspass" \
          --image-ids imageTag='esspass-'"${application}"'-'"${version}"'' \
          > /dev/null 2>&1

  imageExists=$?

  # Check the exit status of the previous command
  if [[ "$imageExists" -ne 0 ]]; then
      return 0
  elif [[ "$imageExists" -eq 0 ]]; then
      return 1
  fi

}

function updateApisix() {
  local ip="$1"
  local port="$2"
  local idpNamespace="$3"
  echo "IP: $ip"

  curl 'https://api.pda1.'"${idpNamespace}"'.esspass-poc.eu/apisix/admin/upstreams/2' -H "X-API-KEY: ${X_API_KEY}" -X PUT -d '
        {
          "nodes": [
            {
              "host": '"${ip}"',
              "port":'"${port}"',
              "weight": 100
            }
          ],
           "type": "roundrobin",
           "scheme": "http",
           "name": "backend-web-wallet-upstream"
          }'
}
