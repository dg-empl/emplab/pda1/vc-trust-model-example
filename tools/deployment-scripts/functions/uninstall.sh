# shellcheck disable=SC2120
function uninstall() {
  HELM_INSTALL=$(helm list | grep "backend-web-wallet" | awk "{print $3}")
  if [ -z "$HELM_INSTALL" -o "$HELM_INSTALL" == " " ]; then
    echo "---- No helm installation for backend-web-wallet ----"
  else
    helm uninstall backend-web-wallet
  fi
}