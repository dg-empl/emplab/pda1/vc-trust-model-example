kubens esspass

kubectl apply -f ./mongo/mongo-secret.yaml
kubectl apply -f ./mongo/mongo-config-map.yaml
kubectl apply -f ./mongo/mongo-db.yaml
kubectl apply -f ./mongo/mongo-express.yaml
kubectl apply -f ./issuer/issuer-be.yaml
kubectl apply -f ./issuer/issuer-fe.yaml

# FOR LOCAL MINIKUBE support
#minikube service mongo-express-service -n esspass
#minikube service issuer-be-service -n esspass
#minikube service issuer-fe-service -n esspass
