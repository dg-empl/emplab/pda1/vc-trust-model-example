import * as fs from "fs-extra";
import { createObjectCsvWriter as createCsvWriter } from "csv-writer";

interface DataItem {
  // Define the structure of your data items here
  [key: string]: any;
}

async function convertJsonToCsv(jsonPath: string, csvPath: string) {
  // Read JSON data
  const jsonData: DataItem[] = await fs.readJson(jsonPath);

  // Check if JSON data is not empty and is an array
  if (!Array.isArray(jsonData) || jsonData.length === 0) {
    console.error("JSON data is empty or not an array.");
    return;
  }

  const header = Object.keys(jsonData[0]).map(key => ({ id: key, title: key }));

  const csvWriter = createCsvWriter({
    path: csvPath,
    header,
  });

  // Write CSV data
  csvWriter.writeRecords(jsonData).then(() => console.log("CSV file was written successfully."));
}

const jsonPath = process.argv[2];
const csvPath = process.argv[3];

convertJsonToCsv(jsonPath, csvPath).catch(err => console.error(err));
